<?php

	abstract class Action_CLI_Console_CreateQueryModel extends Action {

		/* ----------------------------------------------------------------- */

		abstract protected function getData();

		/* ----------------------------------------------------------------- */

		public function execute(Application $application) {
			$data = $this->getData();

			$this->buildValueClass();
			foreach ( array_keys($data) as $name ) {
				$this->buildValueBaseClass($name, $data);
			}
			foreach ( array_keys($data) as $name ) {
				$this->buildValueSubClass($name, $data);
			}

			$this->buildQueryClass();
			foreach ( array_keys($data) as $name ) {
				$this->buildQueryBaseClass($name, $data);
			}
			foreach ( array_keys($data) as $name ) {
				$this->buildQuerySubClass($name, $data);
			}

			foreach ( array_keys($data) as $name ) {
				$this->buildUnitTest($name, $data);
			}
			$this->buildUnitTestSuite($data);
		}

		/* ----------------------------------------------------------------- */

		protected function camel($string) {
			return trim(preg_replace('/_(.)/ie', "strtoupper('\\1')", '_' . $string), '_');
		}

		/* ------------------------------------------------------------------ */

		protected function safe($name) {
			$ret = preg_replace('/[^a-z0-9]/i', '_', $name);
			if ( preg_match('/^[0-9]/', $name) ) {
				$ret = '_' . $ret;
			}
			while ( strpos($ret, '__') ) {
				$ret = str_replace('__', '_', $ret);
			}
			return trim($ret, '_');
		}

		/* ----------------------------------------------------------------- */

		protected function buildValueClass() {
			$file = ROOT_DIR . "/modules/query/value.class.php";

			if ( false === mkdir_recursive(dirname($file)) ) {
				printf("Failed: Could not create dir - %s\n", dirname($file));
				return false;
			}
			if ( false === is_writable(dirname($file)) ) {
				printf("Failed: Could not write to dir - %s\n", dirname($file));
				return false;
			}
			if ( true == is_file($file) && false === is_writable($file) ) {
				printf("Failed: Could not write to file - %s\n", $file);
				return false;
			}

			$ret = '';
			$ret .= sprintf("<?php\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	/**\n");
			$ret .= sprintf("	* Value\n");
			$ret .= sprintf("	*\n");
			$ret .= sprintf("	* This file is auto generated and should not be modified.\n");
			$ret .= sprintf("	*\n");
			$ret .= sprintf("	* This class creates the abstract base for all Value objects to inherit\n");
			$ret .= sprintf("	* from.\n");
			$ret .= sprintf("	*\n");
			$ret .= sprintf("	* @since %s\n", date('Y-m-d H:i:s'));
			$ret .= sprintf("	* @abstract\n");
			$ret .= sprintf("	*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	abstract class Value {\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		protected \$_modified = null;\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* save\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* This abstract function sets the pattern for all Value objects to save\n");
			$ret .= sprintf("		* their contents back to the selected database.\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* i.e.\n");
			$ret .= sprintf("		* \$value->save(\$db);\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @param DB \$db\n");
			$ret .= sprintf("		* @return integer\n");
			$ret .= sprintf("		* @see Query\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		abstract public function save(DB \$db);\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* delete\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* This abstract function sets the pattern for all Value objects to deletes\n");
			$ret .= sprintf("		* their contents from the selected database.\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* i.e.\n");
			$ret .= sprintf("		* \$value->delete(\$db);\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @param DB \$db\n");
			$ret .= sprintf("		* @return integer\n");
			$ret .= sprintf("		* @see Query\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		abstract public function delete(DB \$db);\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("?>");

			file_put_contents($file, $ret);

			printf("Created - Value\n");
		}

		/* ----------------------------------------------------------------- */

		protected function buildQueryClass() {
			$file = ROOT_DIR . "/modules/query/query.class.php";

			if ( false === mkdir_recursive(dirname($file)) ) {
				printf("Failed: Could not create dir - %s\n", dirname($file));
				return false;
			}
			if ( false === is_writable(dirname($file)) ) {
				printf("Failed: Could not write to dir - %s\n", dirname($file));
				return false;
			}
			if ( true == is_file($file) && false === is_writable($file) ) {
				printf("Failed: Could not write to file - %s\n", $file);
				return false;
			}

			$ret = '';
			$ret .= sprintf("<?php\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	/**\n");
			$ret .= sprintf("	* Query\n");
			$ret .= sprintf("	*\n");
			$ret .= sprintf("	* This file is auto generated and should not be modified.\n");
			$ret .= sprintf("	*\n");
			$ret .= sprintf("	* This class creates the abstract base for all Query objects to inherit\n");
			$ret .= sprintf("	* from. The class provides functionality for database queries to be built\n");
			$ret .= sprintf("	* and executed in a friendly manner.\n");
			$ret .= sprintf("	*\n");
			$ret .= sprintf("	* i.e.\n");
			$ret .= sprintf("	* \$user = Query_User::create(\$db)->filterByUsername('joebloggs')->findOne();\n");
			$ret .= sprintf("	*\n");
			$ret .= sprintf("	* @since %s\n", date('Y-m-d H:i:s'));
			$ret .= sprintf("	* @abstract\n");
			$ret .= sprintf("	*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	abstract class Query {\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		const FILTER_IS_NULL = 0x01;\n");
			$ret .= sprintf("		const FILTER_NOT_IS_NULL = 0x02;\n");
			$ret .= sprintf("		const FILTER_EQUALS = 0x03;\n");
			$ret .= sprintf("		const FILTER_NOT_EQUALS = 0x04;\n");
			$ret .= sprintf("		const FILTER_LESS_THAN = 0x05;\n");
			$ret .= sprintf("		const FILTER_GREATER_THAN = 0x06;\n");
			$ret .= sprintf("		const FILTER_LESS_EQUALS = 0x07;\n");
			$ret .= sprintf("		const FILTER_GREATER_EQUALS = 0x08;\n");
			$ret .= sprintf("		const FILTER_IN = 0x09;\n");
			$ret .= sprintf("		const FILTER_NOT_IN = 0x0A;\n");
			$ret .= sprintf("		const FILTER_CONTAINS = 0x0B;\n");
			$ret .= sprintf("		const FILTER_STARTS_WITH = 0x0C;\n");
			$ret .= sprintf("		const FILTER_ENDS_WITH = 0x0D;\n");
			$ret .= sprintf("		const FILTER_BETWEEN = 0x0E;\n");
			$ret .= sprintf("		const FILTER_NOT_BETWEEN = 0x0F;\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		const DIRECTION_ASC = 0x01;\n");
			$ret .= sprintf("		const DIRECTION_DESC = 0x02;\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		protected static \$table = null;\n");
			$ret .= sprintf("		protected static \$fields = null;\n");
			$ret .= sprintf("		protected static \$value = null;\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		protected \$db = null;\n");
			$ret .= sprintf("		protected \$filters = array();\n");
			$ret .= sprintf("		protected \$groups = array();\n");
			$ret .= sprintf("		protected \$orders = array();\n");
			$ret .= sprintf("		protected \$limit = null;\n");
			$ret .= sprintf("		protected \$offset = null;\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* create\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* This static function assists in creating Query objects and allows\n");
			$ret .= sprintf("		* additional filtering etc to be chained into one line.\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* i.e.\n");
			$ret .= sprintf("		* \$result = Query_User::create(\$db)->limit(10)->findAll();\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @static\n");
			$ret .= sprintf("		* @param DB \$db\n");
			$ret .= sprintf("		* @return Query\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		abstract public static function create(DB \$db = null);\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* __construct\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* This function is the constructor for creating new Query objects.\n");
			$ret .= sprintf("		* However, you should ideally use the static create() function as this\n");
			$ret .= sprintf("		* provides a slightly cleaner query syntax.\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* i.e.\n");
			$ret .= sprintf("		* \$query = new Query_User(\$db);\n");
			$ret .= sprintf("		* \$result = \$query->limit(10)->findAll();\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @param DB \$db\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		public function __construct(DB \$db = null) {\n");
			$ret .= sprintf("			if ( \$db ) {\n");
			$ret .= sprintf("				\$this->db = \$db;\n");
			$ret .= sprintf("			}\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* setDB\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* This function allows you to easily set and change the database on\n");
			$ret .= sprintf("		* which the query will be executed. This allows for scenarios where\n");
			$ret .= sprintf("		* queries need to be run on the master or slave databases.\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* i.e.\n");
			$ret .= sprintf("		* \$query = new Query_User();\n");
			$ret .= sprintf("		* \$slave = \$query->setDB(\$slaveDB)->limit(10)->findAll();\n");
			$ret .= sprintf("		* \$master = \$query->setDB(\$masterDB)->limit(10)->findAll();\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @param DB \$db\n");
			$ret .= sprintf("		* @return Query\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		public function setDB(DB \$db) {\n");
			$ret .= sprintf("			\$this->db = \$db;\n");
			$ret .= sprintf("			return \$this;\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* getDB\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* This function returns the current database assigned to this Query\n");
			$ret .= sprintf("		* object.\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* i.e.\n");
			$ret .= sprintf("		* \$db = \$query->getDB();\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @return DB\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		public function getDB() {\n");
			$ret .= sprintf("			return \$this->db;\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* filterBy\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* This protected function provides the base functionality for concrete\n");
			$ret .= sprintf("		* Query objects to filter results by the specified field, value and\n");
			$ret .= sprintf("		* criteria.\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* i.e.\n");
			$ret .= sprintf("		* \$query->filterBy('username', 'joebloggs', Query::FILTER_EQUALS);\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access protected\n");
			$ret .= sprintf("		* @param string \$field\n");
			$ret .= sprintf("		* @param mixed \$value\n");
			$ret .= sprintf("		* @param integer \$type\n");
			$ret .= sprintf("		* @return Query\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		protected function filterBy(\$field, \$value, \$type = null) {\n");
			$ret .= sprintf("			if ( \$type == null ) {\n");
			$ret .= sprintf("				if ( is_array(\$value) ) {\n");
			$ret .= sprintf("					\$type = self::FILTER_IN;\n");
			$ret .= sprintf("				}\n");
			$ret .= sprintf("				else {\n");
			$ret .= sprintf("					\$type = self::FILTER_EQUALS;\n");
			$ret .= sprintf("				}\n");
			$ret .= sprintf("			}\n");
			$ret .= sprintf("			\$this->filters[] = array(\$field, \$value, \$type);\n");
			$ret .= sprintf("			return \$this;\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* groupBy\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* This protected function provides the base functionality for concrete\n");
			$ret .= sprintf("		* Query objects to group results by the specified field and criteria.\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* i.e.\n");
			$ret .= sprintf("		* \$query->groupBy('username', Query::DIRECTION_ASC);\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access protected\n");
			$ret .= sprintf("		* @param mixed \$value\n");
			$ret .= sprintf("		* @param integer \$type\n");
			$ret .= sprintf("		* @return Query\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		protected function groupBy(\$field, \$type = null) {\n");
			$ret .= sprintf("			if ( \$type == null ) {\n");
			$ret .= sprintf("				\$type = self::DIRECTION_ASC;\n");
			$ret .= sprintf("			}\n");
			$ret .= sprintf("			\$this->groups[] = array(\$field, \$type);\n");
			$ret .= sprintf("			return \$this;\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* orderBy\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* This protected function provides the base functionality for concrete\n");
			$ret .= sprintf("		* Query objects to order results by the specified field and criteria.\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* i.e.\n");
			$ret .= sprintf("		* \$query->orderBy('username', Query::DIRECTION_ASC);\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access protected\n");
			$ret .= sprintf("		* @param mixed \$value\n");
			$ret .= sprintf("		* @param integer \$type\n");
			$ret .= sprintf("		* @return Query\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		protected function orderBy(\$field, \$type = null) {\n");
			$ret .= sprintf("			if ( \$type == null ) {\n");
			$ret .= sprintf("				\$type = self::DIRECTION_ASC;\n");
			$ret .= sprintf("			}\n");
			$ret .= sprintf("			\$this->orders[] = array(\$field, \$type);\n");
			$ret .= sprintf("			return \$this;\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* limit\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* This functions provides functionality for limiting the number of\n");
			$ret .= sprintf("		* records returned by the Query. This is commonly used in conjunction\n");
			$ret .= sprintf("		* with offset().\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* i.e.\n");
			$ret .= sprintf("		* \$query->limit(10);\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @param integer \$limit\n");
			$ret .= sprintf("		* @return Query\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		public function limit(\$limit) {\n");
			$ret .= sprintf("			\$this->limit = \$limit === null ? null : intval(\$limit);\n");
			$ret .= sprintf("			return \$this;\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* offset\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* This functions provides functionality for setting the offset of\n");
			$ret .= sprintf("		* records returned by the Query. This is commonly used in conjunction\n");
			$ret .= sprintf("		* with limit().\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* i.e.\n");
			$ret .= sprintf("		* \$query->offset(10);\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @param integer \$offset\n");
			$ret .= sprintf("		* @return Query\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		public function offset(\$offset) {\n");
			$ret .= sprintf("			\$this->offset = \$offset === null ? null : \$offset;\n");
			$ret .= sprintf("			return \$this;\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* findOne\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* This function executes the Query object and returns the first\n");
			$ret .= sprintf("		* matching result.\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* i.e.\n");
			$ret .= sprintf("		* \$user = Query_User::create(\$db)->filterByUsername('joebloggs')->findOne();\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @return Value\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		public function findOne() {\n");
			$ret .= sprintf("			if ( \$this->limit === null && \$this->offset === null ) {\n");
			$ret .= sprintf("				\$this->limit(1);\n");
			$ret .= sprintf("			}\n");
			$ret .= sprintf("			if ( \$ret = \$this->findAll() ) {\n");
			$ret .= sprintf("				return \$ret[0];\n");
			$ret .= sprintf("			}\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* findAll\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* This function executes the Query object and returns an array of the\n");
			$ret .= sprintf("		* matching results.\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* i.e.\n");
			$ret .= sprintf("		* \$users = Query_User::create(\$db)->findAll();\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @return Value[]\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		public function findAll() {\n");
			$ret .= sprintf("			\$ret = array();\n");
			$ret .= sprintf("			\$params = array();\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			if ( \$this->db && static::\$table && static::\$fields && static::\$value ) {\n");
			$ret .= sprintf("				\$fields = array();\n");
			$ret .= sprintf("				foreach ( static::\$fields as \$field ) {\n");
			$ret .= sprintf("					if ( \$field[0] != \$field[1] ) {\n");
			$ret .= sprintf("						\$fields[] = sprintf(\"`%%s` AS `%%s`\", \$field[0], \$field[1]);\n");
			$ret .= sprintf("					}\n");
			$ret .= sprintf("					else {\n");
			$ret .= sprintf("						\$fields[] = sprintf(\"`%%s`\", \$field[0]);\n");
			$ret .= sprintf("					}\n");
			$ret .= sprintf("				}\n");
			$ret .= sprintf("				\$sql[] = sprintf(\"SELECT\\n\\t%%s\\nFROM\\n\\t`%%s`\\n\", implode(', ', \$fields), static::\$table);\n");
			$ret .= sprintf("				if ( \$this->filters ) {\n");
			$ret .= sprintf("					\$sql[] = sprintf(\"WHERE\\n\");\n");
			$ret .= sprintf("					foreach ( \$this->filters as \$filter ) {\n");
			$ret .= sprintf("						if ( \$filter[2] == self::FILTER_IS_NULL ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` IS NULL\\n\");\n");
			$ret .= sprintf("							\$params[] = \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_NOT_IS_NULL ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` NOT IS NULL\\n\");\n");
			$ret .= sprintf("							\$params[] = \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_NOT_EQUALS ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` != ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_LESS_THAN ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` < ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_GREATER_THAN ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` > ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_LESS_EQUALS ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` <= ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_GREATER_EQUALS ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` >= ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_IN ) {\n");
			$ret .= sprintf("							if ( is_array(\$filter[1]) ) {\n");
			$ret .= sprintf("								\$temp = array();\n");
			$ret .= sprintf("								foreach ( \$filter[1] as \$item ) {\n");
			$ret .= sprintf("									\$temp[] = '?';\n");
			$ret .= sprintf("									\$params[] = \$item;\n");
			$ret .= sprintf("								}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("								\$sql[] = sprintf(\"\\t`%%s` IN (%%s)\\n\", \$filter[0], implode(', ', \$temp));\n");
			$ret .= sprintf("							}\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_NOT_IN ) {\n");
			$ret .= sprintf("							if ( is_array(\$filter[1]) ) {\n");
			$ret .= sprintf("								\$temp = array();\n");
			$ret .= sprintf("								foreach ( \$filter[1] as \$item ) {\n");
			$ret .= sprintf("									\$temp[] = '?';\n");
			$ret .= sprintf("									\$params[] = \$item;\n");
			$ret .= sprintf("								}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("								\$sql[] = sprintf(\"\\t`%%s` NOT IN (%%s)\\n\", \$filter[0], implode(', ', \$temp));\n");
			$ret .= sprintf("							}\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_CONTAINS ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` LIKE ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = '%%' . \$filter[1] . '%%';\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_STARTS_WITH ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` LIKE ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = '%%' . \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_ENDS_WITH ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` LIKE ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = \$filter[1] . '%%';\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_BETWEEN ) {\n");
			$ret .= sprintf("							if ( is_array(\$filter[1]) && count(\$filter[1]) > 1 ) {\n");
			$ret .= sprintf("								\$sql[] = sprintf(\"\\t`%%s` BETWEEN ? AND ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("								\$params[] = \$filter[1][0];\n");
			$ret .= sprintf("								\$params[] = \$filter[1][1];\n");
			$ret .= sprintf("							}\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_NOT_BETWEEN ) {\n");
			$ret .= sprintf("							if ( is_array(\$filter[1]) && count(\$filter[1]) > 1 ) {\n");
			$ret .= sprintf("								\$sql[] = sprintf(\"\\t`%%s` NOT BETWEEN ? AND ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("								\$params[] = \$filter[1][0];\n");
			$ret .= sprintf("								\$params[] = \$filter[1][1];\n");
			$ret .= sprintf("							}\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						else {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` = ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("					}\n");
			$ret .= sprintf("				}\n");
			$ret .= sprintf("				if ( \$this->orders ) {\n");
			$ret .= sprintf("					\$sql[] = sprintf(\"ORDER BY\\n\");\n");
			$ret .= sprintf("					foreach ( \$this->orders as \$temp ) {\n");
			$ret .= sprintf("						\$sql[] = sprintf(\"\\t`%%s` %%s\\n\", \$temp[0], \$temp[1] == self::DIRECTION_ASC ? 'ASC' : 'DESC');\n");
			$ret .= sprintf("					}\n");
			$ret .= sprintf("				}\n");
			$ret .= sprintf("				if ( \$this->groups ) {\n");
			$ret .= sprintf("					\$sql[] = sprintf(\"GROUP BY\\n\");\n");
			$ret .= sprintf("					foreach ( \$this->groups as \$temp ) {\n");
			$ret .= sprintf("						\$sql[] = sprintf(\"\\t`%%s` %%s\\n\", \$temp[0], \$temp[1] == self::DIRECTION_ASC ? 'ASC' : 'DESC');\n");
			$ret .= sprintf("					}\n");
			$ret .= sprintf("				}\n");
			$ret .= sprintf("				if ( \$this->limit ) {\n");
			$ret .= sprintf("					if ( \$this->offset ) {\n");
			$ret .= sprintf("						\$sql[] = sprintf(\"LIMIT %%s, %%s\\n\", \$this->limit, \$this->offset);\n");
			$ret .= sprintf("					}\n");
			$ret .= sprintf("					else {\n");
			$ret .= sprintf("						\$sql[] = sprintf(\"LIMIT %%s\\n\", \$this->limit);\n");
			$ret .= sprintf("					}\n");
			$ret .= sprintf("				}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("				if ( empty(\$ret) ) {\n");
			$ret .= sprintf("					\$result = \$this->db->query(implode(\$sql), \$params);\n");
			$ret .= sprintf("					while ( \$row = \$result->fetch(static::\$value) ) {\n");
			$ret .= sprintf("						\$ret[] = \$row;\n");
			$ret .= sprintf("					}\n");
			$ret .= sprintf("				}\n");
			$ret .= sprintf("			}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			return \$ret;\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* delete\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* This function executes the Query object and deletes the matching\n");
			$ret .= sprintf("		* results.\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* i.e.\n");
			$ret .= sprintf("		* Query_User::create(\$db)->filterByUsername('joebloggs')->delete();\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @return integer\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		public function delete() {\n");
			$ret .= sprintf("			\$params = array();\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			if ( \$this->db && static::\$table ) {\n");
			$ret .= sprintf("				\$sql[] = sprintf(\"DELETE FROM\\n\\t`%%s`\\n\", static::\$table);\n");
			$ret .= sprintf("				if ( \$this->filters ) {\n");
			$ret .= sprintf("					\$sql[] = sprintf(\"WHERE\\n\");\n");
			$ret .= sprintf("					foreach ( \$this->filters as \$filter ) {\n");
			$ret .= sprintf("						if ( \$filter[2] == self::FILTER_IS_NULL ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` IS NULL\\n\");\n");
			$ret .= sprintf("							\$params[] = \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_NOT_IS_NULL ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` NOT IS NULL\\n\");\n");
			$ret .= sprintf("							\$params[] = \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_NOT_EQUALS ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` != ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_LESS_THAN ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` < ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_GREATER_THAN ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` > ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_LESS_EQUALS ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` <= ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_GREATER_EQUALS ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` >= ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_IN ) {\n");
			$ret .= sprintf("							if ( is_array(\$filter[1]) ) {\n");
			$ret .= sprintf("								\$temp = array();\n");
			$ret .= sprintf("								foreach ( \$filter[1] as \$item ) {\n");
			$ret .= sprintf("									\$temp[] = '?';\n");
			$ret .= sprintf("									\$params[] = \$item;\n");
			$ret .= sprintf("								}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("								\$sql[] = sprintf(\"\\t`%%s` IN (%%s)\\n\", \$filter[0], implode(', ', \$temp));\n");
			$ret .= sprintf("							}\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_NOT_IN ) {\n");
			$ret .= sprintf("							if ( is_array(\$filter[1]) ) {\n");
			$ret .= sprintf("								\$temp = array();\n");
			$ret .= sprintf("								foreach ( \$filter[1] as \$item ) {\n");
			$ret .= sprintf("									\$temp[] = '?';\n");
			$ret .= sprintf("									\$params[] = \$item;\n");
			$ret .= sprintf("								}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("								\$sql[] = sprintf(\"\\t`%%s` NOT IN (%%s)\\n\", \$filter[0], implode(', ', \$temp));\n");
			$ret .= sprintf("							}\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_CONTAINS ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` LIKE ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = '%%' . \$filter[1] . '%%';\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_STARTS_WITH ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` LIKE ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = '%%' . \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_ENDS_WITH ) {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` LIKE ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = \$filter[1] . '%%';\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_BETWEEN ) {\n");
			$ret .= sprintf("							if ( is_array(\$filter[1]) && count(\$filter[1]) > 1 ) {\n");
			$ret .= sprintf("								\$sql[] = sprintf(\"\\t`%%s` BETWEEN ? AND ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("								\$params[] = \$filter[1][0];\n");
			$ret .= sprintf("								\$params[] = \$filter[1][1];\n");
			$ret .= sprintf("							}\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						elseif ( \$filter[2] == self::FILTER_NOT_BETWEEN ) {\n");
			$ret .= sprintf("							if ( is_array(\$filter[1]) && count(\$filter[1]) > 1 ) {\n");
			$ret .= sprintf("								\$sql[] = sprintf(\"\\t`%%s` NOT BETWEEN ? AND ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("								\$params[] = \$filter[1][0];\n");
			$ret .= sprintf("								\$params[] = \$filter[1][1];\n");
			$ret .= sprintf("							}\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("						else {\n");
			$ret .= sprintf("							\$sql[] = sprintf(\"\\t`%%s` = ?\\n\", \$filter[0]);\n");
			$ret .= sprintf("							\$params[] = \$filter[1];\n");
			$ret .= sprintf("						}\n");
			$ret .= sprintf("					}\n");
			$ret .= sprintf("				}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("				\$this->db->query(implode(\$sql), \$params);\n");
			$ret .= sprintf("				return \$this->db->getAffectedRows();\n");
			$ret .= sprintf("			}\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("?>");

			file_put_contents($file, $ret);

			printf("Created - Query\n");
		}

		/* ----------------------------------------------------------------- */

		protected function buildValueBaseClass($name, $data) {
			$file = ROOT_DIR . "/modules/query/value/base/" . str_replace('_', '', strtolower($name)) . ".class.php";

			if ( false === mkdir_recursive(dirname($file)) ) {
				printf("Failed: Could not create dir - %s\n", dirname($file));
				return false;
			}
			if ( false === is_writable(dirname($file)) ) {
				printf("Failed: Could not write to dir - %s\n", dirname($file));
				return false;
			}
			if ( true == is_file($file) && false === is_writable($file) ) {
				printf("Failed: Could not write to file - %s\n", $file);
				return false;
			}

			$ret = '';
			$ret .= sprintf("<?php\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	/**\n");
			$ret .= sprintf("	* Value_Base_%s\n", $this->camel($name));
			$ret .= sprintf("	*\n");
			$ret .= sprintf("	* This file is auto generated and should not be modified.\n");
			$ret .= sprintf("	*\n");
			$note = sprintf("This abstract class creates the base functionality for Value_%s objects. This class should not be modified directly. If you wish to override or add functionality then you should edit the Value_%s class.", $this->camel($name), $this->camel($name));
			foreach ( explode("\n", wordwrap($note, 74, "\n")) as $wrap ) {
				$ret .= sprintf("	* %s\n", $wrap);
			}
			if ( $data[$name]['comment'] ) {
				$ret .= sprintf("	*\n");
				foreach ( explode("\n", wordwrap($data[$name]['comment'], 74, "\n")) as $wrap ) {
					$ret .= sprintf("	* %s\n", $wrap);
				}
			}
			$ret .= sprintf("	*\n");
			$ret .= sprintf("	* @since %s\n", date('Y-m-d H:i:s'));
			$ret .= sprintf("	* @see Value\n");
			$ret .= sprintf("	*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	abstract class Value_Base_%s extends Value {\n", $this->camel($name));
			foreach ( $data[$name]['fields'] as $item ) {
				$ret .= sprintf("\n");
				$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		/**\n");
				$ret .= sprintf("		* %s\n", $this->safe($item['field']));
				if ( $item['comment'] ) {
					$ret .= sprintf("		*\n");
					foreach ( explode("\n", wordwrap($item['comment'], 70, "\n")) as $wrap ) {
						$ret .= sprintf("		* %s\n", $wrap);
					}
				}
				$ret .= sprintf("		*\n");
				$ret .= sprintf("		* @access protected\n");
				$ret .= sprintf("		* @var %s%s\n", $item['type'], strtolower($item['null']) == 'yes' ? ' | NULL' : '');
				if ( strtolower($item['type']) == 'string' ) {
					if ( array_key_exists('length', $item) && $item['length'] ) {
						$ret .= sprintf("		* @length %s\n", $item['length']);
					}
				}
				$ret .= sprintf("		*/\n");
				$ret .= sprintf("\n");
				if ( array_key_exists('default', $item) ) {
					if ( $item['default'] === null ) {
						$ret .= sprintf("		protected \$%s = null;\n", $this->safe($item['field']));
					}
					else {
						$ret .= sprintf("		protected \$%s = '%s';\n", $this->safe($item['field']), addslashes($item['default']));
					}
				}
				else {
					$ret .= sprintf("		protected \$%s;\n", $this->safe($item['field']));
				}
			}
			foreach ( $data[$name]['fields'] as $item ) {
				if ( $item['key'] == 'PRI' ) {
					$ret .= sprintf("\n");
					$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		/**\n");
					$ret .= sprintf("		* getPK\n");
					$ret .= sprintf("		*\n");
					$note = sprintf("This function returns the current value of the primary key.");
					foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
						$ret .= sprintf("		* %s\n", $wrap);
					}
					$ret .= sprintf("		*\n");
					$ret .= sprintf("		* @access public\n");
					$ret .= sprintf("		* @return %s%s\n", $item['type'], strtolower($item['null']) == 'yes' ? ' | NULL' : '');
					$ret .= sprintf("		*/\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		public function getPK() {\n");
					$ret .= sprintf("			return \$this->%s;\n", $this->safe($item['field']));
					$ret .= sprintf("		}\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		/**\n");
					$ret .= sprintf("		* setPK\n");
					$ret .= sprintf("		*\n");
					$note = sprintf("This function sets the value of the primary key making sure it is a valid %s", $item['type']);
					if ( strtolower($item['null']) == 'yes' ) {
						$note .= ' or NULL';
					}
					$note .= '.';
					foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
						$ret .= sprintf("		* %s\n", $wrap);
					}
					$ret .= sprintf("		*\n");
					$ret .= sprintf("		* @access public\n");
					$ret .= sprintf("		* @param %s%s \$value\n", $item['type'], strtolower($item['null']) == 'yes' ? ' | NULL' : '');
					$ret .= sprintf("		* @return Value_Base_%s\n", $this->camel($name));
					$ret .= sprintf("		*/\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		public function setPK(\$value) {\n");
					$ret .= sprintf("			if ( \$this->%s != \$value ) {\n", $this->safe($item['field']));
					$ret .= sprintf("				\$this->_modified = true;\n");

					$value = sprintf("\$value");
					if ( strtolower($item['type']) == 'integer' ) {
						$value = sprintf("intval(%s)", $value);
					}
					elseif ( strtolower($item['type']) == 'float' ) {
						$value = sprintf("floatval(%s)", $value);
					}
					if ( strtolower($item['type']) == 'string' ) {
						$value = sprintf("strval(%s)", $value);
						if ( array_key_exists('length', $item) && $item['length'] ) {
							$value = sprintf("substr(%s, 0, %s)", $value, $item['length']);
						}
					}
					if ( strtolower($item['null']) == 'yes' ) {
						$value = sprintf("(\$value === null ? null : %s)", $value);
					}
					$ret .= sprintf("				\$this->%s = %s;\n", $this->safe($item['field']), $value);
					$ret .= sprintf("			}\n");
					$ret .= sprintf("			return \$this;\n");
					$ret .= sprintf("		}\n");
				}
			}
			foreach ( $data[$name]['fields'] as $item ) {
				$ret .= sprintf("\n");
				$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		/**\n");
				$ret .= sprintf("		* get%s\n", $this->camel($item['field']));
				$ret .= sprintf("		*\n");
				$note = sprintf("This function returns the current value of the '%s' member.", $this->safe($item['field']));
				foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
					$ret .= sprintf("		* %s\n", $wrap);
				}
				$ret .= sprintf("		*\n");
				$ret .= sprintf("		* @access public\n");
				$ret .= sprintf("		* @return %s%s\n", $item['type'], strtolower($item['null']) == 'yes' ? ' | NULL' : '');
				$ret .= sprintf("		*/\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		public function get%s() {\n", $this->camel($item['field']));
				$ret .= sprintf("			return \$this->%s;\n", $this->safe($item['field']));
				$ret .= sprintf("		}\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		/**\n");
				$ret .= sprintf("		* set%s\n", $this->camel($item['field']));
				$ret .= sprintf("		*\n");
				$note = sprintf("This function sets the value of the '%s' member making sure it is a valid %s", $this->safe($item['field']), $item['type']);
				if ( strtolower($item['null']) == 'yes' ) {
					$note .= ' or NULL';
				}
				$note .= '.';
				foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
					$ret .= sprintf("		* %s\n", $wrap);
				}
				$ret .= sprintf("		*\n");
				$ret .= sprintf("		* @access public\n");
				$ret .= sprintf("		* @param %s%s \$%s\n", $item['type'], strtolower($item['null']) == 'yes' ? ' | NULL' : '', $this->safe($item['field']));
				$ret .= sprintf("		* @return Value_Base_%s\n", $this->camel($name));
				$ret .= sprintf("		*/\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		public function set%s(\$%s) {\n", $this->camel($item['field']), $this->safe($item['field']));
				$ret .= sprintf("			if ( \$this->%s != \$%s ) {\n", $this->safe($item['field']), $this->safe($item['field']));
				$ret .= sprintf("				\$this->_modified = true;\n");

				$value = sprintf("\$%s", $this->safe($item['field']));
				if ( strtolower($item['type']) == 'integer' ) {
					$value = sprintf("intval(%s)", $value);
				}
				elseif ( strtolower($item['type']) == 'float' ) {
					$value = sprintf("floatval(%s)", $value);
				}
				if ( strtolower($item['type']) == 'string' ) {
					$value = sprintf("strval(%s)", $value);
					if ( array_key_exists('length', $item) && $item['length'] ) {
						$value = sprintf("substr(%s, 0, %s)", $value, $item['length']);
					}
				}
				if ( strtolower($item['null']) == 'yes' ) {
					$value = sprintf("(\$%s === null ? null : %s)", $this->safe($item['field']), $value);
				}
				$ret .= sprintf("				\$this->%s = %s;\n", $this->safe($item['field']), $value);
				$ret .= sprintf("			}\n");
				$ret .= sprintf("			return \$this;\n");
				$ret .= sprintf("		}\n");
			}
			if ( array_key_exists('foreign', $data[$name]) ) {
				foreach ( $data[$name]['foreign'] as $temp ) {
					$foreign = null;
					$fields = array();
					foreach ( $temp as $item ) {
						$foreign = $item['foreign_table'];
						$fields[] = $item['field'];
					}
					$ret .= sprintf("\n");
					$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		/**\n");
					$ret .= sprintf("		* get%sQueryBy%s\n", $this->camel($foreign), $this->camel(implode('_', $fields)));
					$ret .= sprintf("		*\n");
					$note = sprintf("This function returns a Query_%s object which has already been initialised to return the Value_%s object(s) associated with this Value_%s object.", $this->camel($foreign), $this->camel($foreign), $this->camel($name));
					foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
						$ret .= sprintf("		* %s\n", $wrap);
					}
					$ret .= sprintf("		*\n");
					$ret .= sprintf("		* i.e.\n");
					$ret .= sprintf("		* \$result = \$value->get%sQueryBy%s(\$db)->findOne();\n", $this->camel($foreign), $this->camel(implode('_', $fields)));
					$ret .= sprintf("		*\n");
					$ret .= sprintf("		* @access public\n");
					$ret .= sprintf("		* @param DB \$db\n");
					$ret .= sprintf("		* @return Query_%s\n", $this->camel($foreign));
					$ret .= sprintf("		*/\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		public function get%sQueryBy%s(DB \$db) {\n", $this->camel($foreign), $this->camel(implode('_', $fields)));
					$ret .= sprintf("			\$ret = new Query_%s(\$db);\n", $this->camel($foreign));
					foreach ( $temp as $item ) {
						$ret .= sprintf("			\$ret->filterBy%s(\$this->%s, Query::FILTER_EQUALS);\n", $this->camel($item['foreign_field']), $this->safe($item['field']));
					}
					$ret .= sprintf("			return \$ret;\n");
					$ret .= sprintf("		}\n");
				}
			}
			if ( array_key_exists('reference', $data[$name]) ) {
				foreach ( $data[$name]['reference'] as $temp ) {
					$foreign = null;
					$fields = array();
					foreach ( $temp as $item ) {
						$foreign = $item['table'];
						$fields[] = $item['foreign_field'];
					}
					$ret .= sprintf("\n");
					$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		/**\n");
					$ret .= sprintf("		* get%sQueryBy%s\n", $this->camel($foreign), $this->camel(implode('_', $fields)));
					$ret .= sprintf("		*\n");
					$note = sprintf("This function returns a Query_%s object which has already been initialised to return the Value_%s object(s) associated with this Value_%s object.", $this->camel($foreign), $this->camel($foreign), $this->camel($name));
					foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
						$ret .= sprintf("		* %s\n", $wrap);
					}
					$ret .= sprintf("		*\n");
					$ret .= sprintf("		* i.e.\n");
					$ret .= sprintf("		* \$result = \$value->get%sQueryBy%s(\$db)->findOne();\n", $this->camel($foreign), $this->camel(implode('_', $fields)));
					$ret .= sprintf("		*\n");
					$ret .= sprintf("		* @access public\n");
					$ret .= sprintf("		* @param DB \$db\n");
					$ret .= sprintf("		* @return Query_%s\n", $this->camel($foreign));
					$ret .= sprintf("		*/\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		public function get%sQueryBy%s(DB \$db) {\n", $this->camel($foreign), $this->camel(implode('_', $fields)));
					$ret .= sprintf("			\$ret = new Query_%s(\$db);\n", $this->camel($foreign));
					foreach ( $temp as $item ) {
						$ret .= sprintf("			\$ret->filterBy%s(\$this->%s, Query::FILTER_EQUALS);\n", $this->camel($item['field']), $this->safe($item['foreign_field']));
					}
					$ret .= sprintf("			return \$ret;\n");
					$ret .= sprintf("		}\n");
				}
			}
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* save\n");
			$ret .= sprintf("		*\n");
			$note = sprintf("This function saves the contents of the Query_%s object to the specified database.", $this->camel($name));
			foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
				$ret .= sprintf("		* %s\n", $wrap);
			}
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* i.e.\n");
			$ret .= sprintf("		* \$value->save(\$db);\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @param DB \$db\n");
			$ret .= sprintf("		* @return integer\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		public function save(DB \$db ) {\n");
			$primary = array();
			if ( array_key_exists('primary', $data[$name]) ) {
				foreach ( $data[$name]['primary'] as $temp ) {
					foreach ( $temp as $item ) {
						$primary[] = $this->safe($item['field']);
					}
				}
			}
			$ret .= sprintf("			\$ret = false;\n");
			$ret .= sprintf("			if ( \$this->_modified ) {\n");
			$ret .= sprintf("				\$this->_modified = false;\n");
			$ret .= sprintf("				\$data = array();\n");
			foreach ( $data[$name]['fields'] as $item ) {
					$ret .= sprintf("				\$data[] = \$this->%s;\n", $this->safe($item['field']));
			}
			if ( count($primary) ) {
				$ret .= sprintf("				if ( \$this->%s ) {\n", implode(" && \$this->", $primary));
				$fields = array();
				$where = array();
				foreach ( $data[$name]['fields'] as $item ) {
						$fields[] = '`' . $item['field'] . '` = ?';
				}
				foreach ( $data[$name]['primary'] as $temp ) {
					foreach ( $temp as $item ) {
						$where[] = '`' . $item['field'] . '` = ?';
						$ret .= sprintf("					\$data[] = \$this->%s;\n", $this->safe($item['field']));
					}
				}
				$ret .= sprintf("					\$sql = sprintf(\"UPDATE `%s` SET %s WHERE %s\");\n", $name, implode(', ', $fields), implode(' AND ', $where));
				$ret .= sprintf("					\$db->query(\$sql, \$data);\n");
				foreach ( $data[$name]['primary'] as $temp ) {
					foreach ( $temp as $item ) {
						$ret .= sprintf("					\$ret = \$db->getAffectedRows() ? \$this->%s : false;\n", $this->safe($item['field']));
					}
				}
				$ret .= sprintf("				}\n");
				$ret .= sprintf("				else {\n");
				$fields = array();
				foreach ( $data[$name]['fields'] as $item ) {
						$fields[$item['field']] = '?';
				}
				$ret .= sprintf("					\$sql = sprintf(\"INSERT INTO `%s` (`%s`) VALUES (%s)\");\n", $name, implode("`, `", array_keys($fields)), implode(", ", array_values($fields)));
				$ret .= sprintf("					\$db->query(\$sql, \$data);\n");
				$ret .= sprintf("					\$ret = \$db->getInsertID();\n");
				foreach ( $data[$name]['primary'] as $temp ) {
					foreach ( $temp as $item ) {
						$ret .= sprintf("					\$this->%s = \$db->getInsertID();\n", $this->safe($item['field']));
					}
				}
				$ret .= sprintf("				}\n");
			}
			else {
				$fields = array();
				foreach ( $data[$name]['fields'] as $item ) {
						$fields[$item['field']] = '?';
				}
				$ret .= sprintf("				\$sql = sprintf(\"INSERT INTO `%s` (`%s`) VALUES (%s)\");\n", $name, implode("`, `", array_keys($fields)), implode(", ", array_values($fields)));
				$ret .= sprintf("				\$db->query(\$sql, \$data);\n");
				$ret .= sprintf("				\$ret = \$db->getInsertID();\n");
				foreach ( $data[$name]['primary'] as $temp ) {
					foreach ( $temp as $item ) {
						$ret .= sprintf("					\$this->%s = \$db->getInsertID();\n", $this->safe($item['field']));
					}
				}
			}
			$ret .= sprintf("			}\n");
			$ret .= sprintf("			return \$ret;\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* delete\n");
			$ret .= sprintf("		*\n");
			$note = sprintf("This function deletes the contents of the Query_%s object from the specified database.", $this->camel($name));
			foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
				$ret .= sprintf("		* %s\n", $wrap);
			}
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* i.e.\n");
			$ret .= sprintf("		* \$value->delete(\$db);\n");
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @param DB \$db\n");
			$ret .= sprintf("		* @return integer\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		public function delete(DB \$db) {\n");

			$primary = array();
			if ( array_key_exists('primary', $data[$name]) ) {
				foreach ( $data[$name]['primary'] as $temp ) {
					foreach ( $temp as $item ) {
						$primary[] = $this->safe($item['field']);
					}
				}
			}
			$ret .= sprintf("			\$ret = false;\n");
			if ( count($primary) ) {
				$ret .= sprintf("			if ( \$this->%s ) {\n", implode(" && \$this->", $primary));
				$ret .= sprintf("				\$data = array();\n");
				$where = array();
				foreach ( $data[$name]['primary'] as $temp ) {
					foreach ( $temp as $item ) {
						$where[] = '`' . $item['field'] . '` = ?';
						$ret .= sprintf("				\$data[] = \$this->%s;\n", $this->safe($item['field']));
					}
				}
				$ret .= sprintf("				\$sql = sprintf(\"DELETE FROM `%s`  WHERE %s\");\n", $name, implode(' AND ', $where));
				$ret .= sprintf("				\$db->query(\$sql, \$data);\n");
				$ret .= sprintf("				\$ret = \$db->getAffectedRows();\n");
				$ret .= sprintf("			}\n");
			}
			$ret .= sprintf("			return \$ret;\n");


			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("?>");

			file_put_contents($file, $ret);
			printf("Created - Value_Base_%s\n", $this->camel($name));
		}

		/* ----------------------------------------------------------------- */

		protected function buildQueryBaseClass($name, $data) {
			$file = ROOT_DIR . "/modules/query/query/base/" . str_replace('_', '', strtolower($name)) . ".class.php";

			if ( false === mkdir_recursive(dirname($file)) ) {
				printf("Failed: Could not create dir - %s\n", dirname($file));
				return false;
			}
			if ( false === is_writable(dirname($file)) ) {
				printf("Failed: Could not write to dir - %s\n", dirname($file));
				return false;
			}
			if ( true == is_file($file) && false === is_writable($file) ) {
				printf("Failed: Could not write to file - %s\n", $file);
				return false;
			}

			$ret = '';
			$ret .= sprintf("<?php\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	/**\n");
			$ret .= sprintf("	* Query_Base_%s\n", $this->camel($name));
			$ret .= sprintf("	*\n");
			$ret .= sprintf("	* This file is auto generated and should not be modified.\n");
			$ret .= sprintf("	*\n");
			$note = sprintf("This abstract class creates the base functionality for Query_%s objects. This class should not be modified directly. If you wish to override or add functionality then you should edit the Query_%s class.", $this->camel($name), $this->camel($name));
			foreach ( explode("\n", wordwrap($note, 74, "\n")) as $wrap ) {
				$ret .= sprintf("	* %s\n", $wrap);
			}
			$ret .= sprintf("	*\n");
			$ret .= sprintf("	* @since %s\n", date('Y-m-d H:i:s'));
			$ret .= sprintf("	* @see Query\n");
			$ret .= sprintf("	*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	abstract class Query_Base_%s extends Query {\n", $this->camel($name));
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		protected static \$table = '%s';\n", $name);
			$ret .= sprintf("\n");
			$ret .= sprintf("		protected static \$fields = array(\n");
			foreach ( $data[$name]['fields'] as $item ) {
				$ret .= sprintf("			array('%s', '%s'),\n", $item['field'], $this->safe($item['field']));
			}
			$ret .= sprintf("		);\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		protected static \$value = 'Value_%s';\n", $this->camel($name));
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* create\n");
			$ret .= sprintf("		*\n");
			$note = sprintf("This static function creates an instance of Query_%s and easily allows for additional methods to be daisy chained onto it.", $this->camel($name));
			foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
				$ret .= sprintf("		* %s\n", $wrap);
			}
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* i.e.\n");
			$ret .= sprintf("		* \$query = Query_%s::create(\$db);\n", $this->camel($name));;
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @static\n");
			$ret .= sprintf("		* @param DB \$db\n");
			$ret .= sprintf("		* @return Query_%s\n", $this->camel($name));
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		public static function create(DB \$db = null) {\n");
			$ret .= sprintf("			return new Query_%s(\$db);\n", $this->camel($name));
			$ret .= sprintf("		}\n");
			foreach ( $data[$name]['fields'] as $item ) {
				if ( $item['key'] == 'PRI' ) {
					$ret .= sprintf("\n");
					$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		/**\n");
					$ret .= sprintf("		* filterbyPs\n");
					$ret .= sprintf("		*\n");
					$note = sprintf("This function tells the query to filter its results by the primary key field using the specific criteria.");
					foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
						$ret .= sprintf("		* %s\n", $wrap);
					}
					$ret .= sprintf("		*\n");
					$ret .= sprintf("		* i.e.\n");
					$ret .= sprintf("		* \$result = Query_%s::create(\$db)->filterByPK(\$value, Query::Filter_EQUALS)->findOne();\n", $this->camel($name));
					$ret .= sprintf("		*\n");
					$ret .= sprintf("		* @access public\n");
					$ret .= sprintf("		* @param mixed \$value\n");
					$ret .= sprintf("		* @param integer \$type\n");
					$ret .= sprintf("		* @return Query_%s\n", $this->camel($name));
					$ret .= sprintf("		*/\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		public function filterByPK(\$value, \$type = null) {\n");
					$ret .= sprintf("			return \$this->filterBy('%s', \$value, \$type);\n", $this->safe($item['field']));
					$ret .= sprintf("		}\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		/**\n");
					$ret .= sprintf("		* orderByPK\n");
					$ret .= sprintf("		*\n");
					$note = sprintf("This function tells the query to orders its results by the primary key field using the specific criteria.");
					foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
						$ret .= sprintf("		* %s\n", $wrap);
					}
					$ret .= sprintf("		*\n");
					$ret .= sprintf("		* i.e.\n");
					$ret .= sprintf("		* \$results = Query_%s::create(\$db)->orderByPK(Query::DIRECTION_ASC)->findAll();\n", $this->camel($name));
					$ret .= sprintf("		*\n");
					$ret .= sprintf("		* @access public\n");
					$ret .= sprintf("		* @param integer \$type\n");
					$ret .= sprintf("		* @return Query_%s\n", $this->camel($name));
					$ret .= sprintf("		*/\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		public function orderByPK(\$type = null) {\n");
					$ret .= sprintf("			return \$this->orderBy('%s', \$type);\n", $this->safe($item['field']));
					$ret .= sprintf("		}\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		/**\n");
					$ret .= sprintf("		* groupByPK\n");
					$ret .= sprintf("		*\n");
					$note = sprintf("This function tells the query to group its results by the primary key field using the specific criteria.", $item['field']);
					foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
						$ret .= sprintf("		* %s\n", $wrap);
					}
					$ret .= sprintf("		*\n");
					$ret .= sprintf("		* i.e.\n");
					$ret .= sprintf("		* \$results = Query_%s::create(\$db)->groupByPK(Query::DIRECTION_ASC)->findAll();\n", $this->camel($name));
					$ret .= sprintf("		*\n");
					$ret .= sprintf("		* @access public\n");
					$ret .= sprintf("		* @param integer \$type\n");
					$ret .= sprintf("		* @return Query_%s\n", $this->camel($name));
					$ret .= sprintf("		*/\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		public function groupByPK(\$type = null) {\n");
					$ret .= sprintf("			return \$this->groupBy('%s', \$type);\n", $this->safe($item['field']));
					$ret .= sprintf("		}\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		/**\n");
					$ret .= sprintf("		* filterOneByPK\n");
					$ret .= sprintf("		*\n");
					$note = sprintf("This function is a short hand for filtering by the primary key field and then calling the findOne() method.", $item['field']);
					foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
						$ret .= sprintf("		* %s\n", $wrap);
					}
					$ret .= sprintf("		*\n");
					$ret .= sprintf("		* i.e.\n");
					$ret .= sprintf("		* \$result = Query_%s::create(\$db)->findOneByPK(\$value, Query::FILTER_EQUALS);\n", $this->camel($name));
					$ret .= sprintf("		*\n");
					$ret .= sprintf("		* @access public\n");
					$ret .= sprintf("		* @param mixed \$value\n");
					$ret .= sprintf("		* @param integer \$type\n");
					$ret .= sprintf("		* @return Value_%s\n", $this->camel($name));
					$ret .= sprintf("		*/\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		public function findOneByPK(\$value, \$type = null) {\n");
					$ret .= sprintf("			return \$this->filterBy('%s', \$value, \$type)->findOne();\n", $this->safe($item['field']));
					$ret .= sprintf("		}\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		/**\n");
					$ret .= sprintf("		* findAllByPK\n", $this->camel($item['field']));
					$ret .= sprintf("		*\n");
					$note = sprintf("This function is a short hand for filtering by the primary key field and then calling the findAll() method.", $item['field']);
					foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
						$ret .= sprintf("		* %s\n", $wrap);
					}
					$ret .= sprintf("		*\n");
					$ret .= sprintf("		* i.e.\n");
					$ret .= sprintf("		* \$results = Query_%s::create(\$db)->findAllByPK(\$value, Query::FILTER_EQUALS);\n", $this->camel($name));
					$ret .= sprintf("		*\n");
					$ret .= sprintf("		* @access public\n");
					$ret .= sprintf("		* @param mixed \$value\n");
					$ret .= sprintf("		* @param integer \$type\n");
					$ret .= sprintf("		* @return Value_%s[]\n", $this->camel($name));
					$ret .= sprintf("		*/\n");
					$ret .= sprintf("\n");
					$ret .= sprintf("		public function findAllByPK(\$value, \$type = null) {\n");
					$ret .= sprintf("			return \$this->filterBy('%s', \$value, \$type)->findAll();\n", $this->safe($item['field']));
					$ret .= sprintf("		}\n");
				}
			}
			foreach ( $data[$name]['fields'] as $item ) {
				$ret .= sprintf("\n");
				$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		/**\n");
				$ret .= sprintf("		* filterBy%s\n", $this->camel($item['field']));
				$ret .= sprintf("		*\n");
				$note = sprintf("This function tells the query to filter its results by the '%s' field using the specific criteria.", $item['field']);
				foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
					$ret .= sprintf("		* %s\n", $wrap);
				}
				$ret .= sprintf("		*\n");
				$ret .= sprintf("		* i.e.\n");
				$ret .= sprintf("		* \$result = Query_%s::create(\$db)->filterBy%s(\$%s, Query::Filter_EQUALS)->findOne();\n", $this->camel($name), $this->camel($item['field']), $this->safe($item['field']));
				$ret .= sprintf("		*\n");
				$ret .= sprintf("		* @access public\n");
				$ret .= sprintf("		* @param mixed \$%s\n", $this->safe($item['field']));
				$ret .= sprintf("		* @param integer \$type\n");
				$ret .= sprintf("		* @return Query_%s\n", $this->camel($name));
				$ret .= sprintf("		*/\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		public function filterBy%s(\$%s, \$type = null) {\n", $this->camel($item['field']), $this->safe($item['field']));
				$ret .= sprintf("			return \$this->filterBy('%s', \$%s, \$type);\n", $this->safe($item['field']), $this->safe($item['field']));
				$ret .= sprintf("		}\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		/**\n");
				$ret .= sprintf("		* orderBy%s\n", $this->camel($item['field']));
				$ret .= sprintf("		*\n");
				$note = sprintf("This function tells the query to orders its results by the '%s' field using the specific criteria.", $item['field']);
				foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
					$ret .= sprintf("		* %s\n", $wrap);
				}
				$ret .= sprintf("		*\n");
				$ret .= sprintf("		* i.e.\n");
				$ret .= sprintf("		* \$results = Query_%s::create(\$db)->orderBy%s(Query::DIRECTION_ASC)->findAll();\n", $this->camel($name), $this->camel($item['field']));
				$ret .= sprintf("		*\n");
				$ret .= sprintf("		* @access public\n");
				$ret .= sprintf("		* @param integer \$type\n");
				$ret .= sprintf("		* @return Query_%s\n", $this->camel($name));
				$ret .= sprintf("		*/\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		public function orderBy%s(\$type = null) {\n", $this->camel($item['field']));
				$ret .= sprintf("			return \$this->orderBy('%s', \$type);\n", $this->safe($item['field']));
				$ret .= sprintf("		}\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		/**\n");
				$ret .= sprintf("		* groupBy%s\n", $this->camel($item['field']));
				$ret .= sprintf("		*\n");
				$note = sprintf("This function tells the query to group its results by the '%s' field using the specific criteria.", $item['field']);
				foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
					$ret .= sprintf("		* %s\n", $wrap);
				}
				$ret .= sprintf("		*\n");
				$ret .= sprintf("		* i.e.\n");
				$ret .= sprintf("		* \$results = Query_%s::create(\$db)->groupBy%s(Query::DIRECTION_ASC)->findAll();\n", $this->camel($name), $this->camel($item['field']));
				$ret .= sprintf("		*\n");
				$ret .= sprintf("		* @access public\n");
				$ret .= sprintf("		* @param integer \$type\n");
				$ret .= sprintf("		* @return Query_%s\n", $this->camel($name));
				$ret .= sprintf("		*/\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		public function groupBy%s(\$type = null) {\n", $this->camel($item['field']));
				$ret .= sprintf("			return \$this->groupBy('%s', \$type);\n", $this->safe($item['field']));
				$ret .= sprintf("		}\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		/**\n");
				$ret .= sprintf("		* filterOneBy%s\n", $this->camel($item['field']));
				$ret .= sprintf("		*\n");
				$note = sprintf("This function is a short hand for filtering by the '%s' field and then calling the findOne() method.", $item['field']);
				foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
					$ret .= sprintf("		* %s\n", $wrap);
				}
				$ret .= sprintf("		*\n");
				$ret .= sprintf("		* i.e.\n");
				$ret .= sprintf("		* \$result = Query_%s::create(\$db)->findOneBy%s(\$%s, Query::FILTER_EQUALS);\n", $this->camel($name), $this->camel($item['field']), $this->safe($item['field']));
				$ret .= sprintf("		*\n");
				$ret .= sprintf("		* @access public\n");
				$ret .= sprintf("		* @param mixed \$%s\n", $this->safe($item['field']));
				$ret .= sprintf("		* @param integer \$type\n");
				$ret .= sprintf("		* @return Value_%s\n", $this->camel($name));
				$ret .= sprintf("		*/\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		public function findOneBy%s(\$%s, \$type = null) {\n", $this->camel($item['field']), $this->safe($item['field']));
				$ret .= sprintf("			return \$this->filterBy('%s', \$%s, \$type)->findOne();\n", $this->safe($item['field']), $this->safe($item['field']));
				$ret .= sprintf("		}\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		/**\n");
				$ret .= sprintf("		* findAllBy%s\n", $this->camel($item['field']));
				$ret .= sprintf("		*\n");
				$note = sprintf("This function is a short hand for filtering by the '%s' field and then calling the findAll() method.", $item['field']);
				foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
					$ret .= sprintf("		* %s\n", $wrap);
				}
				$ret .= sprintf("		*\n");
				$ret .= sprintf("		* i.e.\n");
				$ret .= sprintf("		* \$results = Query_%s::create(\$db)->findAllBy%s(\$%s, Query::FILTER_EQUALS);\n", $this->camel($name), $this->camel($item['field']), $this->safe($item['field']));
				$ret .= sprintf("		*\n");
				$ret .= sprintf("		* @access public\n");
				$ret .= sprintf("		* @param mixed \$%s\n", $this->safe($item['field']));
				$ret .= sprintf("		* @param integer \$type\n");
				$ret .= sprintf("		* @return Value_%s[]\n", $this->camel($name));
				$ret .= sprintf("		*/\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		public function findAllBy%s(\$%s, \$type = null) {\n", $this->camel($item['field']), $this->safe($item['field']));
				$ret .= sprintf("			return \$this->filterBy('%s', \$%s, \$type)->findAll();\n", $this->safe($item['field']), $this->safe($item['field']));
				$ret .= sprintf("		}\n");
			}
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ----------------------------------------------------------------- */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("?>");

			file_put_contents($file, $ret);
			printf("Created - Query_Base_%s\n", $this->camel($name));
		}

		/* ----------------------------------------------------------------- */

		protected function buildValueSubClass($name, $data) {
			$file = ROOT_DIR . "/modules/query/value/" . str_replace('_', '', strtolower($name)) . ".class.php";

			if ( false === mkdir_recursive(dirname($file)) ) {
				printf("Failed: Could not create dir - %s\n", dirname($file));
				return false;
			}
			if ( false === is_writable(dirname($file)) ) {
				printf("Failed: Could not write to dir - %s\n", dirname($file));
				return false;
			}
			if ( true == is_file($file) && false === is_writable($file) ) {
				printf("Failed: Could not write to file - %s\n", $file);
				return false;
			}

			$ret = '';
			$ret .= sprintf("<?php\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	/**\n");
			$ret .= sprintf("	* Value_%s\n", $this->camel($name));
			$ret .= sprintf("	*\n");
			$ret .= sprintf("	* @since %s\n", date('Y-m-d H:i:s'));
			$ret .= sprintf("	* @see Value, Value_Base_%s\n", $this->camel($name));
			$ret .= sprintf("	*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	class Value_%s extends Value_Base_%s {\n", $this->camel($name), $this->camel($name));
			$ret .= sprintf("\n");
			$ret .= sprintf("	}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("?>");

			if ( is_file($file) == false ) {
				file_put_contents($file, $ret);
				printf("Created - Value_%s\n", $this->camel($name));
			}
		}

		/* ----------------------------------------------------------------- */

		protected function buildQuerySubClass($name, $data) {
			$file = ROOT_DIR . "/modules/query/query/" . str_replace('_', '', strtolower($name)) . ".class.php";

			if ( false === mkdir_recursive(dirname($file)) ) {
				printf("Failed: Could not create dir - %s\n", dirname($file));
				return false;
			}
			if ( false === is_writable(dirname($file)) ) {
				printf("Failed: Could not write to dir - %s\n", dirname($file));
				return false;
			}
			if ( true == is_file($file) && false === is_writable($file) ) {
				printf("Failed: Could not write to file - %s\n", $file);
				return false;
			}

			$ret = '';
			$ret .= sprintf("<?php\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	/**\n");
			$ret .= sprintf("	* Query_%s\n", $this->camel($name));
			$ret .= sprintf("	*\n");
			$ret .= sprintf("	* @since %s\n", date('Y-m-d H:i:s'));
			$ret .= sprintf("	* @see Query, Query_Base_%s\n", $this->camel($name));
			$ret .= sprintf("	*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	class Query_%s extends Query_Base_%s {\n", $this->camel($name), $this->camel($name));
			$ret .= sprintf("\n");
			$ret .= sprintf("	}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("?>");

			if ( is_file($file) == false ) {
				file_put_contents($file, $ret);
				printf("Created - Query_%s\n", $this->camel($name));
			}
		}

		/* ----------------------------------------------------------------- */

		protected function buildUnitTest($name, $data) {
			$file = ROOT_DIR . "/tests/query/" . str_replace('_', '', strtolower($name)) . "Test.php";

			if ( false === mkdir_recursive(dirname($file)) ) {
				printf("Failed: Could not create dir - %s\n", dirname($file));
				return false;
			}
			if ( false === is_writable(dirname($file)) ) {
				printf("Failed: Could not write to dir - %s\n", dirname($file));
				return false;
			}
			if ( true == is_file($file) && false === is_writable($file) ) {
				printf("Failed: Could not write to file - %s\n", $file);
				return false;
			}

			$ret = '';
			$ret .= sprintf("<?php\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	require_once dirname(__FILE__) . '/../../bootstrap.php';\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	/**\n");
			$ret .= sprintf("	* %sTest\n", $this->camel($name));
			$ret .= sprintf("	*\n");
			$note = sprintf("This unit test is designed to test the functionality of the Value_%s and Query_%s objects.", $this->camel($name), $this->camel($name));
			foreach ( explode("\n", wordwrap($note, 73, "\n")) as $wrap ) {
				$ret .= sprintf("	* %s\n", $wrap);
			}
			$ret .= sprintf("	*\n");
			$ret .= sprintf("	* @since %s\n", date('Y-m-d H:i:s'));
			$ret .= sprintf("	* @see Value_%s, Query_%s\n", $this->camel($name), $this->camel($name));
			$ret .= sprintf("	*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	class %sTest extends PHPUnit_Framework_TestCase {\n", $this->camel($name));
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* lipsum\n");
			$ret .= sprintf("		*\n");
			$note = sprintf("This function just provides a neat and clean way to retreieve the lorum ipsum text.");
			foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
				$ret .= sprintf("		* %s\n", $wrap);
			}
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @param integer|null \$length\n");
			$ret .= sprintf("		* @return string\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		protected function lipsum(\$length = null) {\n");
			$ret .= sprintf("			\$data = array_unique(explode(' ', ', , , , . . . . lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum'));\n");
			$ret .= sprintf("			usort(\$data, create_function('\$a, \$b', '{ return rand(-1, 1); }'));\n");
			$ret .= sprintf("			\$ret = str_replace(' ,', '.', str_replace(' .', '.', implode(' ', \$data))) . '.';\n");
			$ret .= sprintf("			return \$length !== null ? substr(\$ret, 0, \$length) : \$ret;\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* setup\n");
			$ret .= sprintf("		*\n");
			$note = sprintf("This function provides the ability to setup and initialise variables before each test.");
			foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
				$ret .= sprintf("		* %s\n", $wrap);
			}
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		public function setup() {\n");
			$ret .= sprintf("			\$config = Registry::getConfig();\n");
			$ret .= sprintf("			\$config->load(ROOT_DIR . '/configs/application.conf');\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* testValue\n");
			$ret .= sprintf("		*\n");
			$note = sprintf("This function tests the creation and type of value class.");
			foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
				$ret .= sprintf("		* %s\n", $wrap);
			}
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @return Value_%s\n", $this->camel($name));
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		public function testValue() {\n");
			$ret .= sprintf("			\$value = new Value_%s();\n", $this->camel($name));
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Test the value is NOT NULL\n");
			$ret .= sprintf("			\$this->assertNotNull(\$value);\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Test the value is an instance of Value_Base_%s\n", $this->camel($name));
			$ret .= sprintf("			\$this->assertInstanceOf('Value_Base_%s', \$value);\n", $this->camel($name));
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Test the value is an instance of Value\n");
			$ret .= sprintf("			\$this->assertInstanceOf('Value', \$value);\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			return \$value;\n");
			$ret .= sprintf("		}\n");
			foreach ( $data[$name]['fields'] as $item ) {
				$ret .= sprintf("\n");
				$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		/**\n");
				$ret .= sprintf("		* testValue%s\n", $this->camel($item['field']));
				$ret .= sprintf("		*\n");
				$note = sprintf("This function tests the type and value range of the '%s' field.", $item['field']);
				foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
					$ret .= sprintf("		* %s\n", $wrap);
				}
				$ret .= sprintf("		*\n");
				$ret .= sprintf("		* @depends testValue\n");
				$ret .= sprintf("		* @access public\n");
				$ret .= sprintf("		* @param Value_%s \$value\n", $this->camel($name));
				$ret .= sprintf("		* @return Value_%s\n", $this->camel($name));
				$ret .= sprintf("		*/\n");
				$ret .= sprintf("\n");
				$ret .= sprintf("		public function testValue%s(Value_%s \$value) {\n", $this->camel($item['field']), $this->camel($name));
				$ret .= sprintf("			// Clone the value object.\n");
				$ret .= sprintf("			\$value = clone \$value;\n");
				$ret .= sprintf("\n");
				if ( strtolower($item['null']) == 'yes' ) {
					$ret .= sprintf("			// Test for NULL allowed\n");
					$ret .= sprintf("			\$this->assertEquals(null, \$value->set%s(null)->get%s());\n", $this->camel($item['field']), $this->camel($item['field']));
					$ret .= sprintf("\n");
				}
				else {
					$ret .= sprintf("			// Test for NULL not allowed\n");
					$ret .= sprintf("			\$this->assertNotEquals(null, \$value->set%s(null)->get%s());\n", $this->camel($item['field']), $this->camel($item['field']));
					$ret .= sprintf("\n");
				}
				if ( $item['type'] == 'integer' ) {
					$ret .= sprintf("			// Test for 0\n");
					$ret .= sprintf("			\$this->assertEquals(0, \$value->set%s(0)->get%s());\n", $this->camel($item['field']), $this->camel($item['field']));
					$ret .= sprintf("\n");
					$rand = rand(0, 32000);
					$ret .= sprintf("			// Test for random integer\n");
					$ret .= sprintf("			\$this->assertEquals(%s, \$value->set%s(%s)->get%s());\n", $rand, $this->camel($item['field']), $rand, $this->camel($item['field']));
					$ret .= sprintf("\n");
					$rand = rand(0, 32000);
					$ret .= sprintf("			// Test for random float\n");
					$ret .= sprintf("			\$this->assertEquals(%s, \$value->set%s(%s)->get%s());\n", intval($rand / 100), $this->camel($item['field']), $rand / 100, $this->camel($item['field']));
					$ret .= sprintf("\n");
					$ret .= sprintf("			// Test for random string\n");
					$ret .= sprintf("			\$this->assertEquals(0, \$value->set%s(\$this->lipsum())->get%s());\n", $this->camel($item['field']), $this->camel($item['field']));
				}
				elseif ( $item['type'] == 'float' ) {
					$ret .= sprintf("			// Test for 0\n");
					$ret .= sprintf("			\$this->assertEquals(0, \$value->set%s(0)->get%s());\n", $this->camel($item['field']), $this->camel($item['field']));
					$ret .= sprintf("\n");
					$rand = rand(0, 32000);
					$ret .= sprintf("			// Test for random integer\n");
					$ret .= sprintf("			\$this->assertEquals(%s, \$value->set%s(%s)->get%s());\n", $rand, $this->camel($item['field']), $rand, $this->camel($item['field']));
					$ret .= sprintf("\n");
					$rand = rand(0, 32000);
					$ret .= sprintf("			// Test for random float\n");
					$ret .= sprintf("			\$this->assertEquals(%s, \$value->set%s(%s)->get%s());\n", $rand / 100, $this->camel($item['field']), $rand / 100, $this->camel($item['field']));
					$ret .= sprintf("\n");
					$ret .= sprintf("			// Test for random string\n");
					$ret .= sprintf("			\$this->assertEquals(0, \$value->set%s(\$this->lipsum())->get%s());\n", $this->camel($item['field']), $this->camel($item['field']));
				}
				elseif ( $item['type'] == 'string' ) {
					$rand = rand(0, 32000);
					$ret .= sprintf("			// Test for random integer\n");
					$ret .= sprintf("			\$this->assertEquals('%s', \$value->set%s(%s)->get%s());\n", $rand, $this->camel($item['field']), $rand, $this->camel($item['field']));
					$ret .= sprintf("\n");
					$rand = rand(0, 32000);
					$ret .= sprintf("			// Test for random float\n");
					$ret .= sprintf("			\$this->assertEquals('%s', \$value->set%s(%s)->get%s());\n", $rand / 100, $this->camel($item['field']), $rand / 100, $this->camel($item['field']));
					$ret .= sprintf("\n");
					if ( array_key_exists('length', $item) && $item['length'] ) {
						$ret .= sprintf("			// Test for random string of %s chars\n", $item['length']);
						$ret .= sprintf("			\$lipsum = \$this->lipsum(%s);\n", $item['length']);
						$ret .= sprintf("			\$this->assertEquals(\$lipsum, \$value->set%s(\$lipsum)->get%s());\n", $this->camel($item['field']), $this->camel($item['field']));
					}
					else {
						$ret .= sprintf("			// Test for random string\n");
						$ret .= sprintf("			\$lipsum = \$this->lipsum();\n");
						$ret .= sprintf("			\$this->assertEquals(\$lipsum, \$value->set%s(\$lipsum)->get%s());\n", $this->camel($item['field']), $this->camel($item['field']));
					}
				}
				$ret .= sprintf("\n");
				$ret .= sprintf("			return \$value;\n");
				$ret .= sprintf("		}\n");
			}
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* testQueryInsert\n");
			$ret .= sprintf("		*\n");
			$note = sprintf("This function tests the query classes ability to insert a new %s.", $name);
			foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
				$ret .= sprintf("		* %s\n", $wrap);
			}
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @depends testValue\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @param Value_%s \$value\n", $this->camel($name));
			$ret .= sprintf("		* @return Value_%s\n", $this->camel($name));
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		public function testQueryInsert(Value_%s \$value) {\n", $this->camel($name));
			$ret .= sprintf("			// Clone the value object.\n");
			$ret .= sprintf("			\$value = clone \$value;\n");
			$ret .= sprintf("\n");
			if ( array_key_exists('foreign', $data[$name]) ) {
				foreach ( $data[$name]['foreign'] as $temp ) {
					foreach ( $temp as $item ) {
						$ret .= sprintf("			// Fetch a %s.\n", $item['foreign_table']);
						$ret .= sprintf("			\$%s = Query_%s::create(Registry::getDB(DB::READ_ONLY))->findOne();\n", $this->safe($item['field']), $this->camel($item['foreign_table']));
						$ret .= sprintf("\n");
						$ret .= sprintf("			// Test the %s is not Null\n", $item['foreign_table']);
						$ret .= sprintf("			\$this->assertNotNull(\$%s);\n", $item['field']);
						$ret .= sprintf("\n");
						$ret .= sprintf("			// Test the %s is an instance of Value_Base_%s\n", $item['foreign_table'], $this->camel($item['foreign_table']));
						$ret .= sprintf("			\$this->assertInstanceOf('Value_Base_%s', \$%s);\n", $this->camel($item['foreign_table']), $item['field']);
						$ret .= sprintf("\n");
						$ret .= sprintf("			// Test the %s is an instance of Value\n", $item['foreign_table']);
						$ret .= sprintf("			\$this->assertInstanceOf('Value', \$%s);\n", $item['field']);
						$ret .= sprintf("\n");
					}
				}
			}
			$ret .= sprintf("			// Initialise the value object.\n");
			$done = array();
			if ( array_key_exists('foreign', $data[$name]) ) {
				foreach ( $data[$name]['foreign'] as $temp ) {
					foreach ( $temp as $item ) {
						$ret .= sprintf("			\$value->set%s(\$%s->get%s());\n", $this->camel($item['field']), $this->safe($item['field']), $this->camel($item['foreign_field'])); 
						$done[] = $item['field'];
					}
				}
			}
			foreach ( $data[$name]['fields'] as $item ) {
				if ( in_array($item['field'], $done) == false && $item['key'] != 'PRI' ) {
					if ( $item['type'] == 'integer' ) {
						$ret .= sprintf("			\$value->set%s(%s);\n", $this->camel($item['field']), rand(0, 999999)); 
					}
					elseif ( $item['type'] == 'float' ) {
						$ret .= sprintf("			\$value->set%s(%s);\n", $this->camel($item['field']), rand(0, 999999) / 100); 
					}
					elseif ( $item['type'] == 'string' ) {
						$ret .= sprintf("			\$value->set%s(\$this->lipsum());\n", $this->camel($item['field']), $this->safe($item['field'])); 
					}
				}
			}
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Save the value object.\n");
			$ret .= sprintf("			\$value->save(Registry::getDB(DB::READ_WRITE));\n");
			$ret .= sprintf("\n");
			foreach ( $data[$name]['fields'] as $item ) {
				if ( $item['key'] == 'PRI' ) {
					$ret .= sprintf("			// Test the primary key the value object.\n");
					$ret .= sprintf("			\$this->assertGreaterThan(0, \$value->get%s());\n", $this->camel($item['field']));
				}
			}
			$ret .= sprintf("\n");
			$ret .= sprintf("			return \$value;\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* testQuerySelect\n");
			$ret .= sprintf("		*\n");
			$note = sprintf("This function tests the query classes ability to select a %s.", $name);
			foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
				$ret .= sprintf("		* %s\n", $wrap);
			}
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @depends testQueryInsert\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @param Value_%s \$value\n", $this->camel($name));
			$ret .= sprintf("		* @return Value_%s\n", $this->camel($name));
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		public function testQuerySelect(Value_%s \$value) {\n", $this->camel($name));
			$ret .= sprintf("			// Clone the value object.\n");
			$ret .= sprintf("			\$value = clone \$value;\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Query the database for the value object.\n");
			$ret .= sprintf("			\$query = new Query_%s(Registry::getDB(DB::READ_ONLY));\n", $this->camel($name));
			foreach ( $data[$name]['fields'] as $item ) {
				if ( $item['key'] == 'PRI' ) {
					$ret .= sprintf("			\$query->filterBy%s(\$value->get%s());\n", $this->camel($item['field']), $this->camel($item['field']));
				}
			}
			$ret .= sprintf("			\$temp = \$query->findOne();\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Test the value is NOT NULL\n");
			$ret .= sprintf("			\$this->assertNotNull(\$temp);\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Test the value is an instance of Value_Base_%s\n", $this->camel($name));
			$ret .= sprintf("			\$this->assertInstanceOf('Value_Base_%s', \$temp);\n", $this->camel($name));
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Test the value is an instance of Value\n");
			$ret .= sprintf("			\$this->assertInstanceOf('Value', \$temp);\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Test the values\n");
			foreach ( $data[$name]['fields'] as $item ) {
				$ret .= sprintf("			\$this->assertEquals(\$value->get%s(), \$temp->get%s());\n", $this->camel($item['field']), $this->camel($item['field']));
			}
			$ret .= sprintf("\n");
			$ret .= sprintf("			return \$value;\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* testQueryUpdate\n");
			$ret .= sprintf("		*\n");
			$note = sprintf("This function tests the query classes ability to update a %s.", $name);
			foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
				$ret .= sprintf("		* %s\n", $wrap);
			}
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @depends testQuerySelect\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @param Value_%s \$value\n", $this->camel($name));
			$ret .= sprintf("		* @return Value_%s\n", $this->camel($name));
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		public function testQueryUpdate(Value_%s \$value) {\n", $this->camel($name));
			$ret .= sprintf("			// Clone the value object.\n");
			$ret .= sprintf("			\$value = clone \$value;\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			// // Update the value object.\n");
			foreach ( $data[$name]['fields'] as $item ) {
				if ( in_array($item['field'], $done) == false && $item['key'] != 'PRI' ) {
					if ( $item['type'] == 'integer' ) {
						$ret .= sprintf("			\$value->set%s(%s);\n", $this->camel($item['field']), rand(0, 999999)); 
					}
					elseif ( $item['type'] == 'float' ) {
						$ret .= sprintf("			\$value->set%s(%s);\n", $this->camel($item['field']), rand(0, 999999) / 100); 
					}
					elseif ( $item['type'] == 'string' ) {
						$ret .= sprintf("			\$value->set%s(\$this->lipsum());\n", $this->camel($item['field']), $this->safe($item['field'])); 
					}
				}
			}
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Save the value object.\n");
			$ret .= sprintf("			\$value->save(Registry::getDB(DB::READ_WRITE));\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Query the database for the value object.\n");
			$ret .= sprintf("			\$query = new Query_%s(Registry::getDB(DB::READ_ONLY));\n", $this->camel($name));
			foreach ( $data[$name]['fields'] as $item ) {
				if ( $item['key'] == 'PRI' ) {
					$ret .= sprintf("			\$query->filterBy%s(\$value->get%s());\n", $this->camel($item['field']), $this->camel($item['field']));
				}
			}
			$ret .= sprintf("			\$temp = \$query->findOne();\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Test the value is NOT NULL\n");
			$ret .= sprintf("			\$this->assertNotNull(\$temp);\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Test the value is an instance of Value_Base_%s\n", $this->camel($name));
			$ret .= sprintf("			\$this->assertInstanceOf('Value_Base_%s', \$temp);\n", $this->camel($name));
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Test the value is an instance of Value\n");
			$ret .= sprintf("			\$this->assertInstanceOf('Value', \$temp);\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Test the values\n");
			foreach ( $data[$name]['fields'] as $item ) {
				if ( in_array($item['field'], $done) == false ) {
					$ret .= sprintf("			\$this->assertEquals(\$value->get%s(), \$temp->get%s());\n", $this->camel($item['field']), $this->camel($item['field']));
				}
			}
			$ret .= sprintf("\n");
			$ret .= sprintf("			return \$value;\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/**\n");
			$ret .= sprintf("		* testQueryDelete\n");
			$ret .= sprintf("		*\n");
			$note = sprintf("This function tests the query classes ability to delete a %s.", $name);
			foreach ( explode("\n", wordwrap($note, 70, "\n")) as $wrap ) {
				$ret .= sprintf("		* %s\n", $wrap);
			}
			$ret .= sprintf("		*\n");
			$ret .= sprintf("		* @depends testQueryUpdate\n");
			$ret .= sprintf("		* @access public\n");
			$ret .= sprintf("		* @param Value_%s \$value\n", $this->camel($name));
			$ret .= sprintf("		* @return Value_%s\n", $this->camel($name));
			$ret .= sprintf("		*/\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		public function testQueryDelete(Value_%s \$value) {\n", $this->camel($name));
			$ret .= sprintf("			// Clone the value object.\n");
			$ret .= sprintf("			\$value = clone \$value;\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Delete the value object from the database.\n");
			$ret .= sprintf("			\$query = new Query_%s(Registry::getDB(DB::READ_WRITE));\n", $this->camel($name));
			foreach ( $data[$name]['fields'] as $item ) {
				if ( $item['key'] == 'PRI' ) {
					$ret .= sprintf("			\$query->filterBy%s(\$value->get%s());\n", $this->camel($item['field']), $this->camel($item['field']));
				}
			}
			$ret .= sprintf("			\$temp = \$query->delete();\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Query the database for the value object.\n");
			$ret .= sprintf("			\$query = new Query_%s(Registry::getDB(DB::READ_ONLY));\n", $this->camel($name));
			foreach ( $data[$name]['fields'] as $item ) {
				if ( $item['key'] == 'PRI' ) {
					$ret .= sprintf("			\$query->filterBy%s(\$value->get%s());\n", $this->camel($item['field']), $this->camel($item['field']));
				}
			}
			$ret .= sprintf("			\$temp = \$query->findOne();\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			// Test the value is NULL\n");
			$ret .= sprintf("			\$this->assertNull(\$temp);\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("			return \$value;\n");
			$ret .= sprintf("		}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("		/* ------------------------------------------------------------------ */\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("	}\n");
			$ret .= sprintf("\n");
			$ret .= sprintf("?>\n");

			if ( is_file($file) == false ) {
				file_put_contents($file, $ret);
				printf("Created - %sTests\n", $this->camel($name));
			}
		}

		/* ----------------------------------------------------------------- */

		public function buildUnitTestSuite($data) {
			$file = ROOT_DIR . "/tests/query.xml";

			if ( false === mkdir_recursive(dirname($file)) ) {
				printf("Failed: Could not create dir - %s\n", dirname($file));
				return false;
			}
			if ( false === is_writable(dirname($file)) ) {
				printf("Failed: Could not write to dir - %s\n", dirname($file));
				return false;
			}
			if ( true == is_file($file) && false === is_writable($file) ) {
				printf("Failed: Could not write to file - %s\n", $file);
				return false;
			}

			$ret = '';
			$ret .= sprintf("<phpunit>\n");
  			$ret .= sprintf("	<testsuites>\n");
    		$ret .= sprintf("		<testsuite name=\"QUERY_VALUE_TEST\">\n");
			foreach ( array_keys($data) as $name ) {
				$ret .= sprintf("			<file>%s</file>\n", ROOT_DIR . "/tests/query/" . str_replace('_', '', strtolower($name)) . "Test.php");
			}
    		$ret .= sprintf("		</testsuite>\n");
  			$ret .= sprintf("	</testsuites>\n");
			$ret .= sprintf("</phpunit>\n");


			if ( is_file($file) == false ) {
				file_put_contents($file, $ret);
				printf("Created - query.xml\n", $this->camel($name));
			}
		}

		/* ----------------------------------------------------------------- */

	}

?>