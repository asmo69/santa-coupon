<?php
	
	class Action_CLI_Console_CreateMysqlQueryModel extends Action_CLI_Console_CreateQueryModel implements Info {
		
		/* ----------------------------------------------------------------- */
		
		const FUNCTION_RANDOM = 'RAND()';
		
		/* ----------------------------------------------------------------- */
		
		public function info() {
			return array(
				'name' => 'create-mysql-query-model',
				'description' => 'Creates a Query database model.'
			);
		}
		
		/* ----------------------------------------------------------------- */
		
		protected function getData() {
			$db = Registry::getDB();
			
			$ret = array();
			$ref = array();
			
			$sql      = sprintf("SELECT DATABASE()");
			$result   = $db->query($sql);
			$database = @array_values($result->fetch());
			
			$sql    = sprintf("SHOW TABLES");
			$result = $db->query($sql);
			while ($table = @array_values($result->fetch())) {
				
				printf("Analysing: %s.%s\n", $database[0], $table[0]);
				
				$ret[$table[0]] = array(
					'name' => $table[0],
					'cache' => 1,
					'comment' => '',
					'engine' => ''
				);
				
				// Get the table engine and comments
				$sql     = sprintf("SELECT ENGINE, TABLE_COMMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = '%s' AND TABLE_NAME = '%s'", $database[0], $table[0]);
				$result2 = $db->query($sql);
				while ($temp = @array_values($result2->fetch())) {
					$ret[$table[0]]['engine'] = empty($temp[0]) ? $temp[1] : $temp[0];
					if (preg_match('/;\s*CACHE\s*=\s*(false|no|off|0)\s*;/i', ';' . $temp[1] . ';', $match)) {
						$ret[$table[0]]['cache'] = 0;
					} elseif (preg_match('/;\s*CACHE\s*=\s*(true|yes|on|1)\s*;/i', ';' . $temp[1] . ';', $match)) {
						$ret[$table[0]]['cache'] = 1;
					}
					if (preg_match('/;\s*COMMENT\s*=\s*([^;]*)\s*;/i', ';' . $temp[1] . ';', $match)) {
						$ret[$table[0]]['comment'] = $match[1];
					}
				}
				
				// Get the fields in the table.
				$sql     = sprintf("DESCRIBE `%s`", $table[0]);
				$result2 = $db->query($sql);
				while ($temp = $result2->fetch()) {
					$field = array();
					foreach ($temp as $name => $value) {
						$field[strtolower($name)] = $value;
					}
					
					$temp = explode(' ', str_replace('  ', ' ', str_replace('(', ' ', str_replace(')', ' ', strtolower($field['type'])))));
					if (preg_match('/^(tinyint|smallint|mediumint|bigint)$/', $temp[0])) {
						$field['type']   = 'integer';
						$field['length'] = count($temp) > 1 ? $temp[1] : null;
					} elseif (preg_match('/^(float|double|decimal)$/', $temp[0])) {
						$field['type']   = 'float';
						$field['length'] = count($temp) > 1 ? $temp[1] : null;
					} elseif (preg_match('/^(char|varchar|tinytext|text|mediumtext|longtext)$/', $temp[0])) {
						$field['type']   = 'string';
						$field['length'] = count($temp) > 1 ? $temp[1] : null;
					} else {
						$field['type']   = $temp[0];
						$field['length'] = count($temp) > 1 ? $temp[1] : null;
					}
					
					$field['comment'] = '';
					
					$sql     = sprintf("SELECT c.COLUMN_COMMENT FROM information_schema.TABLES t JOIN information_schema.COLUMNS c ON ( t.TABLE_SCHEMA = c.TABLE_SCHEMA AND t.TABLE_NAME = c.TABLE_NAME ) WHERE c.TABLE_SCHEMA = '%s' AND c.TABLE_NAME = '%s' AND c.COLUMN_NAME = '%s' AND c.COLUMN_COMMENT != ''", $database[0], $table[0], $field['field']);
					$result3 = $db->query($sql);
					while ($temp = @array_values($result3->fetch())) {
						if (preg_match('/;\s*CACHE\s*=\s*(false|no|off|0)\s*;/i', ';' . $temp[0] . ';', $match)) {
							$field['cache'] = 0;
						} elseif (preg_match('/;\s*CACHE\s*=\s*(true|yes|on|1)\s*;/i', ';' . $temp[0] . ';', $match)) {
							$field['cache'] = 1;
						}
						if (preg_match('/;\s*COMMENT\s*=\s*([^;]*)\s*;/i', ';' . $temp[0] . ';', $match)) {
							$field['comment'] = $match[1];
						}
						if (preg_match('/;\s*ENCODE\s*=\s*([^;]*)\s*;/i', ';' . $temp[0] . ';', $match)) {
							$field['encode'] = $match[1];
						}
					}
					
					$ret[$table[0]]['fields'][$field['field']] = $field;
					
					if (strtolower($ret[$table[0]]['engine']) == 'view') {
						if ($field['field'] == 'id') {
							$temp                                                    = array(
								'key' => 'PRIMARY',
								'type' => 'PRIMARY',
								'order' => 1,
								'field' => $field['field']
							);
							$ret[$table[0]]['keys'][$temp['key']][$temp['field']]    = $temp;
							$ret[$table[0]]['primary'][$temp['key']][$temp['field']] = $temp;
						}
						
						if (preg_match('/(.*)_(id)$/', $field['field'], $match)) {
							$temp                                                    = array(
								'key' => $table[0] . '_' . $field['field'],
								'type' => 'FOREIGN',
								'order' => 1,
								'table' => $table[0],
								'field' => $field['field'],
								'foreign_table' => $match[1],
								'foreign_field' => $match[2]
							);
							$ret[$table[0]]['keys'][$temp['key']][$temp['field']]    = $temp;
							$ret[$table[0]]['foreign'][$temp['key']][$temp['field']] = $temp;
							$ref[]                                                   = $temp;
						}
					}
				}
				
				// Get the keys.
				$sql     = sprintf("SHOW KEYS FROM `%s`", $table[0]);
				$result2 = $db->query($sql);
				while ($temp = @array_values($result2->fetch())) {
					$field                                                  = array(
						'key' => $temp[2],
						'type' => strcasecmp($temp[2], 'primary') == 0 ? 'PRIMARY' : ($temp[1] == 0 ? 'UNIQUE' : 'KEY'),
						'order' => $temp[3],
						'field' => $temp[4]
					);
					$ret[$table[0]]['keys'][$field['key']][$field['field']] = $field;
					if ($field['type'] == 'PRIMARY') {
						$ret[$table[0]]['primary'][$field['key']][$field['field']] = $field;
					}
					if ($field['type'] == 'UNIQUE') {
						$ret[$table[0]]['unique'][$field['key']][$field['field']] = $field;
					}
				}
				
				// Get the foreign keys for InnoDB.
				if (strtolower($ret[$table[0]]['engine']) == 'innodb') {
					$sql     = sprintf("SELECT * FROM information_schema.KEY_COLUMN_USAGE WHERE TABLE_SCHEMA = '%s' AND TABLE_NAME = '%s' AND REFERENCED_TABLE_NAME IS NOT NULL", $database[0], $table[0]);
					$result2 = $db->query($sql);
					while ($temp = @array_values($result2->fetch())) {
						$field                                                     = array(
							'key' => $temp[2],
							'type' => 'FOREIGN',
							'order' => $temp[8],
							'table' => $temp[5],
							'field' => $temp[6],
							'foreign_table' => $temp[10],
							'foreign_field' => $temp[11]
						);
						$ret[$table[0]]['keys'][$field['key']][$field['field']]    = $field;
						$ret[$table[0]]['foreign'][$field['key']][$field['field']] = $field;
						$ref[]                                                     = $field;
					}
				}
				
				// Get the additional foreign keys for both InnoDB & MyISAM.
				$sql     = sprintf("SELECT t.ENGINE, c.TABLE_SCHEMA, c.TABLE_NAME, c.COLUMN_NAME, c.COLUMN_COMMENT FROM information_schema.TABLES t JOIN information_schema.COLUMNS c ON ( t.TABLE_SCHEMA = c.TABLE_SCHEMA AND t.TABLE_NAME = c.TABLE_NAME ) WHERE t.ENGINE IN ('MYISAM', 'INNODB') AND c.TABLE_SCHEMA = '%s' AND c.TABLE_NAME = '%s' AND c.COLUMN_COMMENT != ''", $database[0], $table[0]);
				$result2 = $db->query($sql);
				while ($temp = @array_values($result2->fetch())) {
					if (preg_match('/;\s*FOREIGN_KEY\s*=\s*([^;]*)\s*;/i', ';' . $temp[4] . ';', $match)) {
						$foreign                                                   = explode('.', $match[1]);
						$field                                                     = array(
							'key' => 'fk__' . $table[0] . '__' . $foreign[0] . '__' . $foreign[1],
							'type' => 'FOREIGN',
							'order' => 1,
							'table' => $temp[2],
							'field' => $temp[3],
							'foreign_table' => $foreign[0],
							'foreign_field' => $foreign[1]
						);
						$ret[$table[0]]['keys'][$field['key']][$field['field']]    = $field;
						$ret[$table[0]]['foreign'][$field['key']][$field['field']] = $field;
						$ref[]                                                     = $field;
					}
				}
			}
			
			foreach ($ref as $field) {
				$ret[$field['foreign_table']]['reference'][$field['key']][$field['field']] = $field;
			}
			
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
	}
	
?>