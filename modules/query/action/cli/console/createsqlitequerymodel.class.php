<?php
	
	class Action_CLI_Console_CreateSQLiteQueryModel extends Action_CLI_Console_CreateQueryModel implements Info {
		
		/* ----------------------------------------------------------------- */
		
		const FUNCTION_RANDOM = 'RANDOM()';
		
		/* ----------------------------------------------------------------- */
		
		public function info() {
			return array(
				'name' => 'create-sqlite-query-model',
				'description' => 'Creates a Query database model.'
			);
		}
		
		/* ----------------------------------------------------------------- */
		
		protected function getData() {
			$db = Registry::getDB();
			
			$ret = array();
			$ref = array();
			
			$sql    = sprintf("SELECT name FROM sqlite_master WHERE type = 'table'");
			$result = $db->query($sql);
			while ($table = @array_values($result->fetch())) {
				printf("Analysing: %s\n", $table[0]);
				
				$ret[$table[0]] = array(
					'name' => $table[0],
					'cache' => 1,
					'comment' => '',
					'engine' => 'sqlite'
				);
				
				// Get the fields in the table.
				$sql     = sprintf("PRAGMA table_info(%s)", $table[0]);
				$result2 = $db->query($sql);
				while ($temp = $result2->fetch()) {
					$field = array(
						'field' => $temp['name'],
						'null' => $temp['notnull'] ? 'NO' : 'YES',
						'default' => $temp['dflt_value'],
						'key' => $temp['pk'] ? 'PRI' : ''
					);
					$temp  = explode(' ', str_replace('  ', ' ', str_replace('(', ' ', str_replace(')', ' ', strtolower($temp['type'])))));
					if (preg_match('/^(integer)$/', $temp[0])) {
						$field['type'] = 'integer';
					} elseif (preg_match('/^(real)$/', $temp[0])) {
						$field['type'] = 'float';
					} elseif (preg_match('/^(text)$/', $temp[0])) {
						$field['type'] = 'string';
					} else {
						$field['type'] = $temp[0];
					}
					
					$field['comment'] = '';
					
					$ret[$table[0]]['fields'][$field['field']] = $field;
					
					if ($field['key'] == 'PRI') {
						$key                                                   = array(
							'key' => 'PRIMARY',
							'type' => 'PRIMARY',
							'order' => 1,
							'field' => $field['field']
						);
						$ret[$table[0]]['keys'][$key['key']][$key['field']]    = $key;
						$ret[$table[0]]['primary'][$key['key']][$key['field']] = $key;
					}
				}
				
				// Get the
				$sql     = sprintf("PRAGMA foreign_key_list(%s)", $table[0]);
				$result2 = $db->query($sql);
				while ($temp = @array_values($result2->fetch())) {
					$key                                                   = array(
						'key' => 'fk__' . $table[0] . '__' . $temp[2] . '__' . $temp[4],
						'type' => 'FOREIGN',
						'order' => 1,
						'table' => $table[0],
						'field' => $temp[3],
						'foreign_table' => $temp[2],
						'foreign_field' => $temp[4]
					);
					$ret[$table[0]]['keys'][$key['key']][$key['field']]    = $key;
					$ret[$table[0]]['foreign'][$key['key']][$key['field']] = $key;
					$ref[]                                                 = $key;
				}
				
			}
			
			foreach ($ref as $field) {
				$ret[$field['foreign_table']]['reference'][$field['key']][$field['field']] = $field;
			}
			
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
	}
	
?>