<?php
	
	/**
	 * Value_Status
	 *
	 * @since 2013-10-17 13:39:14
	 * @see Value, Value_Base_Status
	 */
	
	class Value_Status extends Value_Base_Status {
		
		const STATUS_UNCLAIMED = 1;
		const STATUS_WON = 2;
		const STATUS_LOST = 3;
		
	}
	
?>