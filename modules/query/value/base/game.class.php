<?php
	
	/**
	 * Value_Base_Game
	 *
	 * This file is auto generated and should not be modified.
	 *
	 * This abstract class creates the base functionality for Value_Game objects.
	 * This class should not be modified directly. If you wish to override or add
	 * functionality then you should edit the Value_Game class.
	 *
	 * @since 2014-08-19 15:54:26
	 * @see Value
	 */
	
	abstract class Value_Base_Game extends Value {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * id
		 *
		 * @access protected
		 * @var integer | NULL
		 */
		
		protected $id = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * coupon_id
		 *
		 * @access protected
		 * @var integer | NULL
		 */
		
		protected $coupon_id = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * config
		 *
		 * @access protected
		 * @var string | NULL
		 */
		
		protected $config = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getPK
		 *
		 * This function returns the current value of the primary key.
		 *
		 * @access public
		 * @return integer | NULL
		 */
		
		public function getPK() {
			return $this->id;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setPK
		 *
		 * This function sets the value of the primary key making sure it is a
		 * valid integer or NULL.
		 *
		 * @access public
		 * @param integer | NULL $value
		 * @return Value_Base_Game
		 */
		
		public function setPK($value) {
			if ($this->id != $value) {
				$this->_modified = true;
				$this->id        = ($value === null ? null : intval($value));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getId
		 *
		 * This function returns the current value of the 'id' member.
		 *
		 * @access public
		 * @return integer | NULL
		 */
		
		public function getId() {
			return $this->id;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setId
		 *
		 * This function sets the value of the 'id' member making sure it is a
		 * valid integer or NULL.
		 *
		 * @access public
		 * @param integer | NULL $id
		 * @return Value_Base_Game
		 */
		
		public function setId($id) {
			if ($this->id != $id) {
				$this->_modified = true;
				$this->id        = ($id === null ? null : intval($id));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getCouponId
		 *
		 * This function returns the current value of the 'coupon_id' member.
		 *
		 * @access public
		 * @return integer | NULL
		 */
		
		public function getCouponId() {
			return $this->coupon_id;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setCouponId
		 *
		 * This function sets the value of the 'coupon_id' member making sure it
		 * is a valid integer or NULL.
		 *
		 * @access public
		 * @param integer | NULL $coupon_id
		 * @return Value_Base_Game
		 */
		
		public function setCouponId($coupon_id) {
			if ($this->coupon_id != $coupon_id) {
				$this->_modified = true;
				$this->coupon_id = ($coupon_id === null ? null : intval($coupon_id));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getConfig
		 *
		 * This function returns the current value of the 'config' member.
		 *
		 * @access public
		 * @return string | NULL
		 */
		
		public function getConfig() {
			return $this->config;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setConfig
		 *
		 * This function sets the value of the 'config' member making sure it is
		 * a valid string or NULL.
		 *
		 * @access public
		 * @param string | NULL $config
		 * @return Value_Base_Game
		 */
		
		public function setConfig($config) {
			if ($this->config != $config) {
				$this->_modified = true;
				$this->config    = ($config === null ? null : strval($config));
			}
			return $this;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * getCouponQueryByCouponId
		 *
		 * This function returns a Query_Coupon object which has already been
		 * initialised to return the Value_Coupon object(s) associated with this
		 * Value_Game object.
		 *
		 * i.e.
		 * $result = $value->getCouponQueryByCouponId($db)->findOne();
		 *
		 * @access public
		 * @param DB $db
		 * @return Query_Coupon
		 */
		
		public function getCouponQueryByCouponId(DB $db) {
			$ret = new Query_Coupon($db);
			$ret->filterById($this->coupon_id, Query::FILTER_EQUALS);
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * save
		 *
		 * This function saves the contents of the Query_Game object to the
		 * specified database.
		 *
		 * i.e.
		 * $value->save($db);
		 *
		 * @access public
		 * @param DB $db
		 * @return integer
		 */
		
		public function save(DB $db) {
			$ret = false;
			if ($this->_modified) {
				$this->_modified = false;
				$data            = array();
				$data[]          = $this->id;
				$data[]          = $this->coupon_id;
				$data[]          = $this->config;
				if ($this->id) {
					$data[] = $this->id;
					$sql    = sprintf("UPDATE `game` SET `id` = ?, `coupon_id` = ?, `config` = ? WHERE `id` = ?");
					$db->query($sql, $data);
					$ret = $db->getAffectedRows() ? $this->id : false;
				} else {
					$sql = sprintf("INSERT INTO `game` (`id`, `coupon_id`, `config`) VALUES (?, ?, ?)");
					$db->query($sql, $data);
					$ret      = $db->getInsertID();
					$this->id = $db->getInsertID();
				}
			}
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * delete
		 *
		 * This function deletes the contents of the Query_Game object from the
		 * specified database.
		 *
		 * i.e.
		 * $value->delete($db);
		 *
		 * @access public
		 * @param DB $db
		 * @return integer
		 */
		
		public function delete(DB $db) {
			$ret = false;
			if ($this->id) {
				$data   = array();
				$data[] = $this->id;
				$sql    = sprintf("DELETE FROM `game`  WHERE `id` = ?");
				$db->query($sql, $data);
				$ret = $db->getAffectedRows();
			}
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
	}
	
?>