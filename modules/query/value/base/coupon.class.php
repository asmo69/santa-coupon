<?php
	
	/**
	 * Value_Base_Coupon
	 *
	 * This file is auto generated and should not be modified.
	 *
	 * This abstract class creates the base functionality for Value_Coupon
	 * objects. This class should not be modified directly. If you wish to
	 * override or add functionality then you should edit the Value_Coupon class.
	 *
	 * @since 2014-08-19 15:54:26
	 * @see Value
	 */
	
	abstract class Value_Base_Coupon extends Value {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * id
		 *
		 * @access protected
		 * @var integer | NULL
		 */
		
		protected $id = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * code
		 *
		 * @access protected
		 * @var string | NULL
		 */
		
		protected $code = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * prize_id
		 *
		 * @access protected
		 * @var integer | NULL
		 */
		
		protected $prize_id = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * status_id
		 *
		 * @access protected
		 * @var integer | NULL
		 */
		
		protected $status_id = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getPK
		 *
		 * This function returns the current value of the primary key.
		 *
		 * @access public
		 * @return integer | NULL
		 */
		
		public function getPK() {
			return $this->id;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setPK
		 *
		 * This function sets the value of the primary key making sure it is a
		 * valid integer or NULL.
		 *
		 * @access public
		 * @param integer | NULL $value
		 * @return Value_Base_Coupon
		 */
		
		public function setPK($value) {
			if ($this->id != $value) {
				$this->_modified = true;
				$this->id        = ($value === null ? null : intval($value));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getId
		 *
		 * This function returns the current value of the 'id' member.
		 *
		 * @access public
		 * @return integer | NULL
		 */
		
		public function getId() {
			return $this->id;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setId
		 *
		 * This function sets the value of the 'id' member making sure it is a
		 * valid integer or NULL.
		 *
		 * @access public
		 * @param integer | NULL $id
		 * @return Value_Base_Coupon
		 */
		
		public function setId($id) {
			if ($this->id != $id) {
				$this->_modified = true;
				$this->id        = ($id === null ? null : intval($id));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getCode
		 *
		 * This function returns the current value of the 'code' member.
		 *
		 * @access public
		 * @return string | NULL
		 */
		
		public function getCode() {
			return $this->code;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setCode
		 *
		 * This function sets the value of the 'code' member making sure it is a
		 * valid string or NULL.
		 *
		 * @access public
		 * @param string | NULL $code
		 * @return Value_Base_Coupon
		 */
		
		public function setCode($code) {
			if ($this->code != $code) {
				$this->_modified = true;
				$this->code      = ($code === null ? null : strval($code));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getPrizeId
		 *
		 * This function returns the current value of the 'prize_id' member.
		 *
		 * @access public
		 * @return integer | NULL
		 */
		
		public function getPrizeId() {
			return $this->prize_id;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setPrizeId
		 *
		 * This function sets the value of the 'prize_id' member making sure it
		 * is a valid integer or NULL.
		 *
		 * @access public
		 * @param integer | NULL $prize_id
		 * @return Value_Base_Coupon
		 */
		
		public function setPrizeId($prize_id) {
			if ($this->prize_id != $prize_id) {
				$this->_modified = true;
				$this->prize_id  = ($prize_id === null ? null : intval($prize_id));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getStatusId
		 *
		 * This function returns the current value of the 'status_id' member.
		 *
		 * @access public
		 * @return integer | NULL
		 */
		
		public function getStatusId() {
			return $this->status_id;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setStatusId
		 *
		 * This function sets the value of the 'status_id' member making sure it
		 * is a valid integer or NULL.
		 *
		 * @access public
		 * @param integer | NULL $status_id
		 * @return Value_Base_Coupon
		 */
		
		public function setStatusId($status_id) {
			if ($this->status_id != $status_id) {
				$this->_modified = true;
				$this->status_id = ($status_id === null ? null : intval($status_id));
			}
			return $this;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * getStatusQueryByStatusId
		 *
		 * This function returns a Query_Status object which has already been
		 * initialised to return the Value_Status object(s) associated with this
		 * Value_Coupon object.
		 *
		 * i.e.
		 * $result = $value->getStatusQueryByStatusId($db)->findOne();
		 *
		 * @access public
		 * @param DB $db
		 * @return Query_Status
		 */
		
		public function getStatusQueryByStatusId(DB $db) {
			$ret = new Query_Status($db);
			$ret->filterById($this->status_id, Query::FILTER_EQUALS);
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * getPrizeQueryByPrizeId
		 *
		 * This function returns a Query_Prize object which has already been
		 * initialised to return the Value_Prize object(s) associated with this
		 * Value_Coupon object.
		 *
		 * i.e.
		 * $result = $value->getPrizeQueryByPrizeId($db)->findOne();
		 *
		 * @access public
		 * @param DB $db
		 * @return Query_Prize
		 */
		
		public function getPrizeQueryByPrizeId(DB $db) {
			$ret = new Query_Prize($db);
			$ret->filterById($this->prize_id, Query::FILTER_EQUALS);
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * getGameQueryById
		 *
		 * This function returns a Query_Game object which has already been
		 * initialised to return the Value_Game object(s) associated with this
		 * Value_Coupon object.
		 *
		 * i.e.
		 * $result = $value->getGameQueryById($db)->findOne();
		 *
		 * @access public
		 * @param DB $db
		 * @return Query_Game
		 */
		
		public function getGameQueryById(DB $db) {
			$ret = new Query_Game($db);
			$ret->filterByCouponId($this->id, Query::FILTER_EQUALS);
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * getAddressQueryById
		 *
		 * This function returns a Query_Address object which has already been
		 * initialised to return the Value_Address object(s) associated with this
		 * Value_Coupon object.
		 *
		 * i.e.
		 * $result = $value->getAddressQueryById($db)->findOne();
		 *
		 * @access public
		 * @param DB $db
		 * @return Query_Address
		 */
		
		public function getAddressQueryById(DB $db) {
			$ret = new Query_Address($db);
			$ret->filterByCouponId($this->id, Query::FILTER_EQUALS);
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * save
		 *
		 * This function saves the contents of the Query_Coupon object to the
		 * specified database.
		 *
		 * i.e.
		 * $value->save($db);
		 *
		 * @access public
		 * @param DB $db
		 * @return integer
		 */
		
		public function save(DB $db) {
			$ret = false;
			if ($this->_modified) {
				$this->_modified = false;
				$data            = array();
				$data[]          = $this->id;
				$data[]          = $this->code;
				$data[]          = $this->prize_id;
				$data[]          = $this->status_id;
				if ($this->id) {
					$data[] = $this->id;
					$sql    = sprintf("UPDATE `coupon` SET `id` = ?, `code` = ?, `prize_id` = ?, `status_id` = ? WHERE `id` = ?");
					$db->query($sql, $data);
					$ret = $db->getAffectedRows() ? $this->id : false;
				} else {
					$sql = sprintf("INSERT INTO `coupon` (`id`, `code`, `prize_id`, `status_id`) VALUES (?, ?, ?, ?)");
					$db->query($sql, $data);
					$ret      = $db->getInsertID();
					$this->id = $db->getInsertID();
				}
			}
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * delete
		 *
		 * This function deletes the contents of the Query_Coupon object from the
		 * specified database.
		 *
		 * i.e.
		 * $value->delete($db);
		 *
		 * @access public
		 * @param DB $db
		 * @return integer
		 */
		
		public function delete(DB $db) {
			$ret = false;
			if ($this->id) {
				$data   = array();
				$data[] = $this->id;
				$sql    = sprintf("DELETE FROM `coupon`  WHERE `id` = ?");
				$db->query($sql, $data);
				$ret = $db->getAffectedRows();
			}
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
	}
	
?>