<?php
	
	/**
	 * Value_Base_Status
	 *
	 * This file is auto generated and should not be modified.
	 *
	 * This abstract class creates the base functionality for Value_Status
	 * objects. This class should not be modified directly. If you wish to
	 * override or add functionality then you should edit the Value_Status class.
	 *
	 * @since 2014-08-19 15:54:26
	 * @see Value
	 */
	
	abstract class Value_Base_Status extends Value {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * id
		 *
		 * @access protected
		 * @var integer | NULL
		 */
		
		protected $id = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * name
		 *
		 * @access protected
		 * @var string | NULL
		 */
		
		protected $name = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getPK
		 *
		 * This function returns the current value of the primary key.
		 *
		 * @access public
		 * @return integer | NULL
		 */
		
		public function getPK() {
			return $this->id;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setPK
		 *
		 * This function sets the value of the primary key making sure it is a
		 * valid integer or NULL.
		 *
		 * @access public
		 * @param integer | NULL $value
		 * @return Value_Base_Status
		 */
		
		public function setPK($value) {
			if ($this->id != $value) {
				$this->_modified = true;
				$this->id        = ($value === null ? null : intval($value));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getId
		 *
		 * This function returns the current value of the 'id' member.
		 *
		 * @access public
		 * @return integer | NULL
		 */
		
		public function getId() {
			return $this->id;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setId
		 *
		 * This function sets the value of the 'id' member making sure it is a
		 * valid integer or NULL.
		 *
		 * @access public
		 * @param integer | NULL $id
		 * @return Value_Base_Status
		 */
		
		public function setId($id) {
			if ($this->id != $id) {
				$this->_modified = true;
				$this->id        = ($id === null ? null : intval($id));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getName
		 *
		 * This function returns the current value of the 'name' member.
		 *
		 * @access public
		 * @return string | NULL
		 */
		
		public function getName() {
			return $this->name;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setName
		 *
		 * This function sets the value of the 'name' member making sure it is a
		 * valid string or NULL.
		 *
		 * @access public
		 * @param string | NULL $name
		 * @return Value_Base_Status
		 */
		
		public function setName($name) {
			if ($this->name != $name) {
				$this->_modified = true;
				$this->name      = ($name === null ? null : strval($name));
			}
			return $this;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * getCouponQueryById
		 *
		 * This function returns a Query_Coupon object which has already been
		 * initialised to return the Value_Coupon object(s) associated with this
		 * Value_Status object.
		 *
		 * i.e.
		 * $result = $value->getCouponQueryById($db)->findOne();
		 *
		 * @access public
		 * @param DB $db
		 * @return Query_Coupon
		 */
		
		public function getCouponQueryById(DB $db) {
			$ret = new Query_Coupon($db);
			$ret->filterByStatusId($this->id, Query::FILTER_EQUALS);
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * save
		 *
		 * This function saves the contents of the Query_Status object to the
		 * specified database.
		 *
		 * i.e.
		 * $value->save($db);
		 *
		 * @access public
		 * @param DB $db
		 * @return integer
		 */
		
		public function save(DB $db) {
			$ret = false;
			if ($this->_modified) {
				$this->_modified = false;
				$data            = array();
				$data[]          = $this->id;
				$data[]          = $this->name;
				if ($this->id) {
					$data[] = $this->id;
					$sql    = sprintf("UPDATE `status` SET `id` = ?, `name` = ? WHERE `id` = ?");
					$db->query($sql, $data);
					$ret = $db->getAffectedRows() ? $this->id : false;
				} else {
					$sql = sprintf("INSERT INTO `status` (`id`, `name`) VALUES (?, ?)");
					$db->query($sql, $data);
					$ret      = $db->getInsertID();
					$this->id = $db->getInsertID();
				}
			}
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * delete
		 *
		 * This function deletes the contents of the Query_Status object from the
		 * specified database.
		 *
		 * i.e.
		 * $value->delete($db);
		 *
		 * @access public
		 * @param DB $db
		 * @return integer
		 */
		
		public function delete(DB $db) {
			$ret = false;
			if ($this->id) {
				$data   = array();
				$data[] = $this->id;
				$sql    = sprintf("DELETE FROM `status`  WHERE `id` = ?");
				$db->query($sql, $data);
				$ret = $db->getAffectedRows();
			}
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
	}
	
?>