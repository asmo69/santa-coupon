<?php
	
	/**
	 * Value_Base_Address
	 *
	 * This file is auto generated and should not be modified.
	 *
	 * This abstract class creates the base functionality for Value_Address
	 * objects. This class should not be modified directly. If you wish to
	 * override or add functionality then you should edit the Value_Address
	 * class.
	 *
	 * @since 2014-08-19 15:54:26
	 * @see Value
	 */
	
	abstract class Value_Base_Address extends Value {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * id
		 *
		 * @access protected
		 * @var integer | NULL
		 */
		
		protected $id = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * coupon_id
		 *
		 * @access protected
		 * @var integer | NULL
		 */
		
		protected $coupon_id = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * first
		 *
		 * @access protected
		 * @var string | NULL
		 */
		
		protected $first = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * last
		 *
		 * @access protected
		 * @var string | NULL
		 */
		
		protected $last = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * address1
		 *
		 * @access protected
		 * @var string | NULL
		 */
		
		protected $address1 = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * address2
		 *
		 * @access protected
		 * @var string | NULL
		 */
		
		protected $address2 = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * address3
		 *
		 * @access protected
		 * @var string | NULL
		 */
		
		protected $address3 = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * town
		 *
		 * @access protected
		 * @var string | NULL
		 */
		
		protected $town = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * county
		 *
		 * @access protected
		 * @var string | NULL
		 */
		
		protected $county = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * postcode
		 *
		 * @access protected
		 * @var string | NULL
		 */
		
		protected $postcode = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * email
		 *
		 * @access protected
		 * @var string | NULL
		 */
		
		protected $email = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getPK
		 *
		 * This function returns the current value of the primary key.
		 *
		 * @access public
		 * @return integer | NULL
		 */
		
		public function getPK() {
			return $this->id;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setPK
		 *
		 * This function sets the value of the primary key making sure it is a
		 * valid integer or NULL.
		 *
		 * @access public
		 * @param integer | NULL $value
		 * @return Value_Base_Address
		 */
		
		public function setPK($value) {
			if ($this->id != $value) {
				$this->_modified = true;
				$this->id        = ($value === null ? null : intval($value));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getId
		 *
		 * This function returns the current value of the 'id' member.
		 *
		 * @access public
		 * @return integer | NULL
		 */
		
		public function getId() {
			return $this->id;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setId
		 *
		 * This function sets the value of the 'id' member making sure it is a
		 * valid integer or NULL.
		 *
		 * @access public
		 * @param integer | NULL $id
		 * @return Value_Base_Address
		 */
		
		public function setId($id) {
			if ($this->id != $id) {
				$this->_modified = true;
				$this->id        = ($id === null ? null : intval($id));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getCouponId
		 *
		 * This function returns the current value of the 'coupon_id' member.
		 *
		 * @access public
		 * @return integer | NULL
		 */
		
		public function getCouponId() {
			return $this->coupon_id;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setCouponId
		 *
		 * This function sets the value of the 'coupon_id' member making sure it
		 * is a valid integer or NULL.
		 *
		 * @access public
		 * @param integer | NULL $coupon_id
		 * @return Value_Base_Address
		 */
		
		public function setCouponId($coupon_id) {
			if ($this->coupon_id != $coupon_id) {
				$this->_modified = true;
				$this->coupon_id = ($coupon_id === null ? null : intval($coupon_id));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getFirst
		 *
		 * This function returns the current value of the 'first' member.
		 *
		 * @access public
		 * @return string | NULL
		 */
		
		public function getFirst() {
			return $this->first;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setFirst
		 *
		 * This function sets the value of the 'first' member making sure it is a
		 * valid string or NULL.
		 *
		 * @access public
		 * @param string | NULL $first
		 * @return Value_Base_Address
		 */
		
		public function setFirst($first) {
			if ($this->first != $first) {
				$this->_modified = true;
				$this->first     = ($first === null ? null : strval($first));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getLast
		 *
		 * This function returns the current value of the 'last' member.
		 *
		 * @access public
		 * @return string | NULL
		 */
		
		public function getLast() {
			return $this->last;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setLast
		 *
		 * This function sets the value of the 'last' member making sure it is a
		 * valid string or NULL.
		 *
		 * @access public
		 * @param string | NULL $last
		 * @return Value_Base_Address
		 */
		
		public function setLast($last) {
			if ($this->last != $last) {
				$this->_modified = true;
				$this->last      = ($last === null ? null : strval($last));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getAddress1
		 *
		 * This function returns the current value of the 'address1' member.
		 *
		 * @access public
		 * @return string | NULL
		 */
		
		public function getAddress1() {
			return $this->address1;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setAddress1
		 *
		 * This function sets the value of the 'address1' member making sure it
		 * is a valid string or NULL.
		 *
		 * @access public
		 * @param string | NULL $address1
		 * @return Value_Base_Address
		 */
		
		public function setAddress1($address1) {
			if ($this->address1 != $address1) {
				$this->_modified = true;
				$this->address1  = ($address1 === null ? null : strval($address1));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getAddress2
		 *
		 * This function returns the current value of the 'address2' member.
		 *
		 * @access public
		 * @return string | NULL
		 */
		
		public function getAddress2() {
			return $this->address2;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setAddress2
		 *
		 * This function sets the value of the 'address2' member making sure it
		 * is a valid string or NULL.
		 *
		 * @access public
		 * @param string | NULL $address2
		 * @return Value_Base_Address
		 */
		
		public function setAddress2($address2) {
			if ($this->address2 != $address2) {
				$this->_modified = true;
				$this->address2  = ($address2 === null ? null : strval($address2));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getAddress3
		 *
		 * This function returns the current value of the 'address3' member.
		 *
		 * @access public
		 * @return string | NULL
		 */
		
		public function getAddress3() {
			return $this->address3;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setAddress3
		 *
		 * This function sets the value of the 'address3' member making sure it
		 * is a valid string or NULL.
		 *
		 * @access public
		 * @param string | NULL $address3
		 * @return Value_Base_Address
		 */
		
		public function setAddress3($address3) {
			if ($this->address3 != $address3) {
				$this->_modified = true;
				$this->address3  = ($address3 === null ? null : strval($address3));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getTown
		 *
		 * This function returns the current value of the 'town' member.
		 *
		 * @access public
		 * @return string | NULL
		 */
		
		public function getTown() {
			return $this->town;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setTown
		 *
		 * This function sets the value of the 'town' member making sure it is a
		 * valid string or NULL.
		 *
		 * @access public
		 * @param string | NULL $town
		 * @return Value_Base_Address
		 */
		
		public function setTown($town) {
			if ($this->town != $town) {
				$this->_modified = true;
				$this->town      = ($town === null ? null : strval($town));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getCounty
		 *
		 * This function returns the current value of the 'county' member.
		 *
		 * @access public
		 * @return string | NULL
		 */
		
		public function getCounty() {
			return $this->county;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setCounty
		 *
		 * This function sets the value of the 'county' member making sure it is
		 * a valid string or NULL.
		 *
		 * @access public
		 * @param string | NULL $county
		 * @return Value_Base_Address
		 */
		
		public function setCounty($county) {
			if ($this->county != $county) {
				$this->_modified = true;
				$this->county    = ($county === null ? null : strval($county));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getPostcode
		 *
		 * This function returns the current value of the 'postcode' member.
		 *
		 * @access public
		 * @return string | NULL
		 */
		
		public function getPostcode() {
			return $this->postcode;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setPostcode
		 *
		 * This function sets the value of the 'postcode' member making sure it
		 * is a valid string or NULL.
		 *
		 * @access public
		 * @param string | NULL $postcode
		 * @return Value_Base_Address
		 */
		
		public function setPostcode($postcode) {
			if ($this->postcode != $postcode) {
				$this->_modified = true;
				$this->postcode  = ($postcode === null ? null : strval($postcode));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getEmail
		 *
		 * This function returns the current value of the 'email' member.
		 *
		 * @access public
		 * @return string | NULL
		 */
		
		public function getEmail() {
			return $this->email;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setEmail
		 *
		 * This function sets the value of the 'email' member making sure it is a
		 * valid string or NULL.
		 *
		 * @access public
		 * @param string | NULL $email
		 * @return Value_Base_Address
		 */
		
		public function setEmail($email) {
			if ($this->email != $email) {
				$this->_modified = true;
				$this->email     = ($email === null ? null : strval($email));
			}
			return $this;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * getCouponQueryByCouponId
		 *
		 * This function returns a Query_Coupon object which has already been
		 * initialised to return the Value_Coupon object(s) associated with this
		 * Value_Address object.
		 *
		 * i.e.
		 * $result = $value->getCouponQueryByCouponId($db)->findOne();
		 *
		 * @access public
		 * @param DB $db
		 * @return Query_Coupon
		 */
		
		public function getCouponQueryByCouponId(DB $db) {
			$ret = new Query_Coupon($db);
			$ret->filterById($this->coupon_id, Query::FILTER_EQUALS);
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * save
		 *
		 * This function saves the contents of the Query_Address object to the
		 * specified database.
		 *
		 * i.e.
		 * $value->save($db);
		 *
		 * @access public
		 * @param DB $db
		 * @return integer
		 */
		
		public function save(DB $db) {
			$ret = false;
			if ($this->_modified) {
				$this->_modified = false;
				$data            = array();
				$data[]          = $this->id;
				$data[]          = $this->coupon_id;
				$data[]          = $this->first;
				$data[]          = $this->last;
				$data[]          = $this->address1;
				$data[]          = $this->address2;
				$data[]          = $this->address3;
				$data[]          = $this->town;
				$data[]          = $this->county;
				$data[]          = $this->postcode;
				$data[]          = $this->email;
				if ($this->id) {
					$data[] = $this->id;
					$sql    = sprintf("UPDATE `address` SET `id` = ?, `coupon_id` = ?, `first` = ?, `last` = ?, `address1` = ?, `address2` = ?, `address3` = ?, `town` = ?, `county` = ?, `postcode` = ?, `email` = ? WHERE `id` = ?");
					$db->query($sql, $data);
					$ret = $db->getAffectedRows() ? $this->id : false;
				} else {
					$sql = sprintf("INSERT INTO `address` (`id`, `coupon_id`, `first`, `last`, `address1`, `address2`, `address3`, `town`, `county`, `postcode`, `email`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
					$db->query($sql, $data);
					$ret      = $db->getInsertID();
					$this->id = $db->getInsertID();
				}
			}
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * delete
		 *
		 * This function deletes the contents of the Query_Address object from
		 * the specified database.
		 *
		 * i.e.
		 * $value->delete($db);
		 *
		 * @access public
		 * @param DB $db
		 * @return integer
		 */
		
		public function delete(DB $db) {
			$ret = false;
			if ($this->id) {
				$data   = array();
				$data[] = $this->id;
				$sql    = sprintf("DELETE FROM `address`  WHERE `id` = ?");
				$db->query($sql, $data);
				$ret = $db->getAffectedRows();
			}
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
	}
	
?>