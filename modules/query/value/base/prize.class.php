<?php
	
	/**
	 * Value_Base_Prize
	 *
	 * This file is auto generated and should not be modified.
	 *
	 * This abstract class creates the base functionality for Value_Prize
	 * objects. This class should not be modified directly. If you wish to
	 * override or add functionality then you should edit the Value_Prize class.
	 *
	 * @since 2014-08-19 15:54:26
	 * @see Value
	 */
	
	abstract class Value_Base_Prize extends Value {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * id
		 *
		 * @access protected
		 * @var integer | NULL
		 */
		
		protected $id = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * code
		 *
		 * @access protected
		 * @var string | NULL
		 */
		
		protected $code = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * name
		 *
		 * @access protected
		 * @var string | NULL
		 */
		
		protected $name = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * stock
		 *
		 * @access protected
		 * @var integer | NULL
		 */
		
		protected $stock = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getPK
		 *
		 * This function returns the current value of the primary key.
		 *
		 * @access public
		 * @return integer | NULL
		 */
		
		public function getPK() {
			return $this->id;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setPK
		 *
		 * This function sets the value of the primary key making sure it is a
		 * valid integer or NULL.
		 *
		 * @access public
		 * @param integer | NULL $value
		 * @return Value_Base_Prize
		 */
		
		public function setPK($value) {
			if ($this->id != $value) {
				$this->_modified = true;
				$this->id        = ($value === null ? null : intval($value));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getId
		 *
		 * This function returns the current value of the 'id' member.
		 *
		 * @access public
		 * @return integer | NULL
		 */
		
		public function getId() {
			return $this->id;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setId
		 *
		 * This function sets the value of the 'id' member making sure it is a
		 * valid integer or NULL.
		 *
		 * @access public
		 * @param integer | NULL $id
		 * @return Value_Base_Prize
		 */
		
		public function setId($id) {
			if ($this->id != $id) {
				$this->_modified = true;
				$this->id        = ($id === null ? null : intval($id));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getCode
		 *
		 * This function returns the current value of the 'code' member.
		 *
		 * @access public
		 * @return string | NULL
		 */
		
		public function getCode() {
			return $this->code;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setCode
		 *
		 * This function sets the value of the 'code' member making sure it is a
		 * valid string or NULL.
		 *
		 * @access public
		 * @param string | NULL $code
		 * @return Value_Base_Prize
		 */
		
		public function setCode($code) {
			if ($this->code != $code) {
				$this->_modified = true;
				$this->code      = ($code === null ? null : strval($code));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getName
		 *
		 * This function returns the current value of the 'name' member.
		 *
		 * @access public
		 * @return string | NULL
		 */
		
		public function getName() {
			return $this->name;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setName
		 *
		 * This function sets the value of the 'name' member making sure it is a
		 * valid string or NULL.
		 *
		 * @access public
		 * @param string | NULL $name
		 * @return Value_Base_Prize
		 */
		
		public function setName($name) {
			if ($this->name != $name) {
				$this->_modified = true;
				$this->name      = ($name === null ? null : strval($name));
			}
			return $this;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getStock
		 *
		 * This function returns the current value of the 'stock' member.
		 *
		 * @access public
		 * @return integer | NULL
		 */
		
		public function getStock() {
			return $this->stock;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setStock
		 *
		 * This function sets the value of the 'stock' member making sure it is a
		 * valid integer or NULL.
		 *
		 * @access public
		 * @param integer | NULL $stock
		 * @return Value_Base_Prize
		 */
		
		public function setStock($stock) {
			if ($this->stock != $stock) {
				$this->_modified = true;
				$this->stock     = ($stock === null ? null : intval($stock));
			}
			return $this;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * getCouponQueryById
		 *
		 * This function returns a Query_Coupon object which has already been
		 * initialised to return the Value_Coupon object(s) associated with this
		 * Value_Prize object.
		 *
		 * i.e.
		 * $result = $value->getCouponQueryById($db)->findOne();
		 *
		 * @access public
		 * @param DB $db
		 * @return Query_Coupon
		 */
		
		public function getCouponQueryById(DB $db) {
			$ret = new Query_Coupon($db);
			$ret->filterByPrizeId($this->id, Query::FILTER_EQUALS);
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * save
		 *
		 * This function saves the contents of the Query_Prize object to the
		 * specified database.
		 *
		 * i.e.
		 * $value->save($db);
		 *
		 * @access public
		 * @param DB $db
		 * @return integer
		 */
		
		public function save(DB $db) {
			$ret = false;
			if ($this->_modified) {
				$this->_modified = false;
				$data            = array();
				$data[]          = $this->id;
				$data[]          = $this->code;
				$data[]          = $this->name;
				$data[]          = $this->stock;
				if ($this->id) {
					$data[] = $this->id;
					$sql    = sprintf("UPDATE `prize` SET `id` = ?, `code` = ?, `name` = ?, `stock` = ? WHERE `id` = ?");
					$db->query($sql, $data);
					$ret = $db->getAffectedRows() ? $this->id : false;
				} else {
					$sql = sprintf("INSERT INTO `prize` (`id`, `code`, `name`, `stock`) VALUES (?, ?, ?, ?)");
					$db->query($sql, $data);
					$ret      = $db->getInsertID();
					$this->id = $db->getInsertID();
				}
			}
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * delete
		 *
		 * This function deletes the contents of the Query_Prize object from the
		 * specified database.
		 *
		 * i.e.
		 * $value->delete($db);
		 *
		 * @access public
		 * @param DB $db
		 * @return integer
		 */
		
		public function delete(DB $db) {
			$ret = false;
			if ($this->id) {
				$data   = array();
				$data[] = $this->id;
				$sql    = sprintf("DELETE FROM `prize`  WHERE `id` = ?");
				$db->query($sql, $data);
				$ret = $db->getAffectedRows();
			}
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
	}
	
?>