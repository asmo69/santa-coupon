<?php
	
	/**
	 * Value_Game
	 *
	 * @since 2013-10-17 13:39:14
	 * @see Value, Value_Base_Game
	 */
	
	class Value_Game extends Value_Base_Game {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * init
		 *
		 * This function will initialise the config data of the game, creating
		 * and saving a list of game tiles etc.
		 *
		 * @access public
		 * @param DB $db
		 * @param array $tiles
		 * @param integer $size
		 * @param integer $match
		 */
		
		public function init(DB $db, $tiles, $size = 9, $match = 3) {
			$data = json_decode($this->getConfig(), true);
			if (empty($data)) {
				$data = array(
					'tiles' => array(),
					'open' => array(),
					'size' => $size,
					'match' => $match,
					'status' => 'lose',
					'complete' => false
				);
				
				$coupon = Query_Coupon::create($db)->filterById($this->getCouponId())->findOne();
				
				$prize = null;
				if ($coupon->getPrizeId()) {
					$prize = Query_Prize::create($db)->filterById($coupon->getPrizeId())->findOne();
				}
				
				if ($prize) {
					$data['status'] = 'win';
					for ($x = 0; $x < $match; $x++) {
						$data['tiles'][] = 'santa';
					}
				}
				
				$pool = array();
				while (count($pool) < $size) {
					foreach ($tiles as $tile) {
						$pool[] = $tile;
					}
				}
				
				while (count($data['tiles']) < $size) {
					$pos             = rand(0, count($pool) - 1);
					$data['tiles'][] = $pool[$pos];
					unset($pool[$pos]);
					$pool = array_values($pool);
				}
				
				usort($data['tiles'], create_function('$a, $b', 'return rand(-1, 1);'));
				
				$this->setConfig(json_encode($data));
				$this->save($db);
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * open
		 *
		 * This function will open a specific game tile.
		 *
		 * @access public
		 * @param DB $db
		 * @param integer $open
		 */
		
		public function open(DB $db, $open) {
			$data = json_decode($this->getConfig(), true);
			if (strlen($open)) {
				if ($open == 'all') {
					$data['open'] = range(0, $data['size'] - 1);
				} else {
					$data['open'][] = $open;
				}
				$data['open'] = array_unique($data['open']);
				sort($data['open']);
				
				if (count($data['open']) == $data['size']) {
					$data['complete'] = true;
				}
				
				$this->setConfig(json_encode($data));
				$this->save($db);
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>