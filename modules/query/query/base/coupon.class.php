<?php
	
	/**
	 * Query_Base_Coupon
	 *
	 * This file is auto generated and should not be modified.
	 *
	 * This abstract class creates the base functionality for Query_Coupon
	 * objects. This class should not be modified directly. If you wish to
	 * override or add functionality then you should edit the Query_Coupon class.
	 *
	 * @since 2014-08-19 15:54:26
	 * @see Query
	 */
	
	abstract class Query_Base_Coupon extends Query {
		
		/* ----------------------------------------------------------------- */
		
		protected static $table = 'coupon';
		
		protected static $fields = array(array('id', 'id'), array('code', 'code'), array('prize_id', 'prize_id'), array('status_id', 'status_id'));
		
		protected static $value = 'Value_Coupon';
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * create
		 *
		 * This static function creates an instance of Query_Coupon and easily
		 * allows for additional methods to be daisy chained onto it.
		 *
		 * i.e.
		 * $query = Query_Coupon::create($db);
		 *
		 * @access public
		 * @static
		 * @param DB $db
		 * @return Query_Coupon
		 */
		
		public static function create(DB $db = null) {
			return new Query_Coupon($db);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterbyPs
		 *
		 * This function tells the query to filter its results by the primary key
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Coupon::create($db)->filterByPK($value, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $value
		 * @param integer $type
		 * @return Query_Coupon
		 */
		
		public function filterByPK($value, $type = null) {
			return $this->filterBy('id', $value, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByPK
		 *
		 * This function tells the query to orders its results by the primary key
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Coupon::create($db)->orderByPK(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Coupon
		 */
		
		public function orderByPK($type = null) {
			return $this->orderBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByPK
		 *
		 * This function tells the query to group its results by the primary key
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Coupon::create($db)->groupByPK(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Coupon
		 */
		
		public function groupByPK($type = null) {
			return $this->groupBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByPK
		 *
		 * This function is a short hand for filtering by the primary key field
		 * and then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Coupon::create($db)->findOneByPK($value, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $value
		 * @param integer $type
		 * @return Value_Coupon
		 */
		
		public function findOneByPK($value, $type = null) {
			return $this->filterBy('id', $value, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByPK
		 *
		 * This function is a short hand for filtering by the primary key field
		 * and then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Coupon::create($db)->findAllByPK($value, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $value
		 * @param integer $type
		 * @return Value_Coupon[]
		 */
		
		public function findAllByPK($value, $type = null) {
			return $this->filterBy('id', $value, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterById
		 *
		 * This function tells the query to filter its results by the 'id' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Coupon::create($db)->filterById($id, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $id
		 * @param integer $type
		 * @return Query_Coupon
		 */
		
		public function filterById($id, $type = null) {
			return $this->filterBy('id', $id, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderById
		 *
		 * This function tells the query to orders its results by the 'id' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Coupon::create($db)->orderById(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Coupon
		 */
		
		public function orderById($type = null) {
			return $this->orderBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupById
		 *
		 * This function tells the query to group its results by the 'id' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Coupon::create($db)->groupById(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Coupon
		 */
		
		public function groupById($type = null) {
			return $this->groupBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneById
		 *
		 * This function is a short hand for filtering by the 'id' field and then
		 * calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Coupon::create($db)->findOneById($id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $id
		 * @param integer $type
		 * @return Value_Coupon
		 */
		
		public function findOneById($id, $type = null) {
			return $this->filterBy('id', $id, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllById
		 *
		 * This function is a short hand for filtering by the 'id' field and then
		 * calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Coupon::create($db)->findAllById($id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $id
		 * @param integer $type
		 * @return Value_Coupon[]
		 */
		
		public function findAllById($id, $type = null) {
			return $this->filterBy('id', $id, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByCode
		 *
		 * This function tells the query to filter its results by the 'code'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Coupon::create($db)->filterByCode($code, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $code
		 * @param integer $type
		 * @return Query_Coupon
		 */
		
		public function filterByCode($code, $type = null) {
			return $this->filterBy('code', $code, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByCode
		 *
		 * This function tells the query to orders its results by the 'code'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Coupon::create($db)->orderByCode(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Coupon
		 */
		
		public function orderByCode($type = null) {
			return $this->orderBy('code', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByCode
		 *
		 * This function tells the query to group its results by the 'code' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Coupon::create($db)->groupByCode(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Coupon
		 */
		
		public function groupByCode($type = null) {
			return $this->groupBy('code', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByCode
		 *
		 * This function is a short hand for filtering by the 'code' field and
		 * then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Coupon::create($db)->findOneByCode($code, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $code
		 * @param integer $type
		 * @return Value_Coupon
		 */
		
		public function findOneByCode($code, $type = null) {
			return $this->filterBy('code', $code, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByCode
		 *
		 * This function is a short hand for filtering by the 'code' field and
		 * then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Coupon::create($db)->findAllByCode($code, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $code
		 * @param integer $type
		 * @return Value_Coupon[]
		 */
		
		public function findAllByCode($code, $type = null) {
			return $this->filterBy('code', $code, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByPrizeId
		 *
		 * This function tells the query to filter its results by the 'prize_id'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Coupon::create($db)->filterByPrizeId($prize_id, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $prize_id
		 * @param integer $type
		 * @return Query_Coupon
		 */
		
		public function filterByPrizeId($prize_id, $type = null) {
			return $this->filterBy('prize_id', $prize_id, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByPrizeId
		 *
		 * This function tells the query to orders its results by the 'prize_id'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Coupon::create($db)->orderByPrizeId(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Coupon
		 */
		
		public function orderByPrizeId($type = null) {
			return $this->orderBy('prize_id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByPrizeId
		 *
		 * This function tells the query to group its results by the 'prize_id'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Coupon::create($db)->groupByPrizeId(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Coupon
		 */
		
		public function groupByPrizeId($type = null) {
			return $this->groupBy('prize_id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByPrizeId
		 *
		 * This function is a short hand for filtering by the 'prize_id' field
		 * and then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Coupon::create($db)->findOneByPrizeId($prize_id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $prize_id
		 * @param integer $type
		 * @return Value_Coupon
		 */
		
		public function findOneByPrizeId($prize_id, $type = null) {
			return $this->filterBy('prize_id', $prize_id, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByPrizeId
		 *
		 * This function is a short hand for filtering by the 'prize_id' field
		 * and then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Coupon::create($db)->findAllByPrizeId($prize_id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $prize_id
		 * @param integer $type
		 * @return Value_Coupon[]
		 */
		
		public function findAllByPrizeId($prize_id, $type = null) {
			return $this->filterBy('prize_id', $prize_id, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByStatusId
		 *
		 * This function tells the query to filter its results by the 'status_id'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Coupon::create($db)->filterByStatusId($status_id, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $status_id
		 * @param integer $type
		 * @return Query_Coupon
		 */
		
		public function filterByStatusId($status_id, $type = null) {
			return $this->filterBy('status_id', $status_id, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByStatusId
		 *
		 * This function tells the query to orders its results by the 'status_id'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Coupon::create($db)->orderByStatusId(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Coupon
		 */
		
		public function orderByStatusId($type = null) {
			return $this->orderBy('status_id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByStatusId
		 *
		 * This function tells the query to group its results by the 'status_id'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Coupon::create($db)->groupByStatusId(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Coupon
		 */
		
		public function groupByStatusId($type = null) {
			return $this->groupBy('status_id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByStatusId
		 *
		 * This function is a short hand for filtering by the 'status_id' field
		 * and then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Coupon::create($db)->findOneByStatusId($status_id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $status_id
		 * @param integer $type
		 * @return Value_Coupon
		 */
		
		public function findOneByStatusId($status_id, $type = null) {
			return $this->filterBy('status_id', $status_id, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByStatusId
		 *
		 * This function is a short hand for filtering by the 'status_id' field
		 * and then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Coupon::create($db)->findAllByStatusId($status_id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $status_id
		 * @param integer $type
		 * @return Value_Coupon[]
		 */
		
		public function findAllByStatusId($status_id, $type = null) {
			return $this->filterBy('status_id', $status_id, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
	}
	
?>