<?php
	
	/**
	 * Query_Base_Status
	 *
	 * This file is auto generated and should not be modified.
	 *
	 * This abstract class creates the base functionality for Query_Status
	 * objects. This class should not be modified directly. If you wish to
	 * override or add functionality then you should edit the Query_Status class.
	 *
	 * @since 2014-08-19 15:54:26
	 * @see Query
	 */
	
	abstract class Query_Base_Status extends Query {
		
		/* ----------------------------------------------------------------- */
		
		protected static $table = 'status';
		
		protected static $fields = array(array('id', 'id'), array('name', 'name'));
		
		protected static $value = 'Value_Status';
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * create
		 *
		 * This static function creates an instance of Query_Status and easily
		 * allows for additional methods to be daisy chained onto it.
		 *
		 * i.e.
		 * $query = Query_Status::create($db);
		 *
		 * @access public
		 * @static
		 * @param DB $db
		 * @return Query_Status
		 */
		
		public static function create(DB $db = null) {
			return new Query_Status($db);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterbyPs
		 *
		 * This function tells the query to filter its results by the primary key
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Status::create($db)->filterByPK($value, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $value
		 * @param integer $type
		 * @return Query_Status
		 */
		
		public function filterByPK($value, $type = null) {
			return $this->filterBy('id', $value, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByPK
		 *
		 * This function tells the query to orders its results by the primary key
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Status::create($db)->orderByPK(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Status
		 */
		
		public function orderByPK($type = null) {
			return $this->orderBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByPK
		 *
		 * This function tells the query to group its results by the primary key
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Status::create($db)->groupByPK(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Status
		 */
		
		public function groupByPK($type = null) {
			return $this->groupBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByPK
		 *
		 * This function is a short hand for filtering by the primary key field
		 * and then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Status::create($db)->findOneByPK($value, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $value
		 * @param integer $type
		 * @return Value_Status
		 */
		
		public function findOneByPK($value, $type = null) {
			return $this->filterBy('id', $value, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByPK
		 *
		 * This function is a short hand for filtering by the primary key field
		 * and then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Status::create($db)->findAllByPK($value, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $value
		 * @param integer $type
		 * @return Value_Status[]
		 */
		
		public function findAllByPK($value, $type = null) {
			return $this->filterBy('id', $value, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterById
		 *
		 * This function tells the query to filter its results by the 'id' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Status::create($db)->filterById($id, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $id
		 * @param integer $type
		 * @return Query_Status
		 */
		
		public function filterById($id, $type = null) {
			return $this->filterBy('id', $id, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderById
		 *
		 * This function tells the query to orders its results by the 'id' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Status::create($db)->orderById(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Status
		 */
		
		public function orderById($type = null) {
			return $this->orderBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupById
		 *
		 * This function tells the query to group its results by the 'id' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Status::create($db)->groupById(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Status
		 */
		
		public function groupById($type = null) {
			return $this->groupBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneById
		 *
		 * This function is a short hand for filtering by the 'id' field and then
		 * calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Status::create($db)->findOneById($id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $id
		 * @param integer $type
		 * @return Value_Status
		 */
		
		public function findOneById($id, $type = null) {
			return $this->filterBy('id', $id, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllById
		 *
		 * This function is a short hand for filtering by the 'id' field and then
		 * calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Status::create($db)->findAllById($id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $id
		 * @param integer $type
		 * @return Value_Status[]
		 */
		
		public function findAllById($id, $type = null) {
			return $this->filterBy('id', $id, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByName
		 *
		 * This function tells the query to filter its results by the 'name'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Status::create($db)->filterByName($name, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $name
		 * @param integer $type
		 * @return Query_Status
		 */
		
		public function filterByName($name, $type = null) {
			return $this->filterBy('name', $name, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByName
		 *
		 * This function tells the query to orders its results by the 'name'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Status::create($db)->orderByName(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Status
		 */
		
		public function orderByName($type = null) {
			return $this->orderBy('name', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByName
		 *
		 * This function tells the query to group its results by the 'name' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Status::create($db)->groupByName(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Status
		 */
		
		public function groupByName($type = null) {
			return $this->groupBy('name', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByName
		 *
		 * This function is a short hand for filtering by the 'name' field and
		 * then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Status::create($db)->findOneByName($name, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $name
		 * @param integer $type
		 * @return Value_Status
		 */
		
		public function findOneByName($name, $type = null) {
			return $this->filterBy('name', $name, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByName
		 *
		 * This function is a short hand for filtering by the 'name' field and
		 * then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Status::create($db)->findAllByName($name, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $name
		 * @param integer $type
		 * @return Value_Status[]
		 */
		
		public function findAllByName($name, $type = null) {
			return $this->filterBy('name', $name, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
	}
	
?>