<?php
	
	/**
	 * Query_Base_Game
	 *
	 * This file is auto generated and should not be modified.
	 *
	 * This abstract class creates the base functionality for Query_Game objects.
	 * This class should not be modified directly. If you wish to override or add
	 * functionality then you should edit the Query_Game class.
	 *
	 * @since 2014-08-19 15:54:26
	 * @see Query
	 */
	
	abstract class Query_Base_Game extends Query {
		
		/* ----------------------------------------------------------------- */
		
		protected static $table = 'game';
		
		protected static $fields = array(array('id', 'id'), array('coupon_id', 'coupon_id'), array('config', 'config'));
		
		protected static $value = 'Value_Game';
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * create
		 *
		 * This static function creates an instance of Query_Game and easily
		 * allows for additional methods to be daisy chained onto it.
		 *
		 * i.e.
		 * $query = Query_Game::create($db);
		 *
		 * @access public
		 * @static
		 * @param DB $db
		 * @return Query_Game
		 */
		
		public static function create(DB $db = null) {
			return new Query_Game($db);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterbyPs
		 *
		 * This function tells the query to filter its results by the primary key
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Game::create($db)->filterByPK($value, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $value
		 * @param integer $type
		 * @return Query_Game
		 */
		
		public function filterByPK($value, $type = null) {
			return $this->filterBy('id', $value, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByPK
		 *
		 * This function tells the query to orders its results by the primary key
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Game::create($db)->orderByPK(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Game
		 */
		
		public function orderByPK($type = null) {
			return $this->orderBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByPK
		 *
		 * This function tells the query to group its results by the primary key
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Game::create($db)->groupByPK(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Game
		 */
		
		public function groupByPK($type = null) {
			return $this->groupBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByPK
		 *
		 * This function is a short hand for filtering by the primary key field
		 * and then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Game::create($db)->findOneByPK($value, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $value
		 * @param integer $type
		 * @return Value_Game
		 */
		
		public function findOneByPK($value, $type = null) {
			return $this->filterBy('id', $value, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByPK
		 *
		 * This function is a short hand for filtering by the primary key field
		 * and then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Game::create($db)->findAllByPK($value, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $value
		 * @param integer $type
		 * @return Value_Game[]
		 */
		
		public function findAllByPK($value, $type = null) {
			return $this->filterBy('id', $value, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterById
		 *
		 * This function tells the query to filter its results by the 'id' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Game::create($db)->filterById($id, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $id
		 * @param integer $type
		 * @return Query_Game
		 */
		
		public function filterById($id, $type = null) {
			return $this->filterBy('id', $id, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderById
		 *
		 * This function tells the query to orders its results by the 'id' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Game::create($db)->orderById(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Game
		 */
		
		public function orderById($type = null) {
			return $this->orderBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupById
		 *
		 * This function tells the query to group its results by the 'id' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Game::create($db)->groupById(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Game
		 */
		
		public function groupById($type = null) {
			return $this->groupBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneById
		 *
		 * This function is a short hand for filtering by the 'id' field and then
		 * calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Game::create($db)->findOneById($id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $id
		 * @param integer $type
		 * @return Value_Game
		 */
		
		public function findOneById($id, $type = null) {
			return $this->filterBy('id', $id, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllById
		 *
		 * This function is a short hand for filtering by the 'id' field and then
		 * calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Game::create($db)->findAllById($id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $id
		 * @param integer $type
		 * @return Value_Game[]
		 */
		
		public function findAllById($id, $type = null) {
			return $this->filterBy('id', $id, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByCouponId
		 *
		 * This function tells the query to filter its results by the 'coupon_id'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Game::create($db)->filterByCouponId($coupon_id, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $coupon_id
		 * @param integer $type
		 * @return Query_Game
		 */
		
		public function filterByCouponId($coupon_id, $type = null) {
			return $this->filterBy('coupon_id', $coupon_id, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByCouponId
		 *
		 * This function tells the query to orders its results by the 'coupon_id'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Game::create($db)->orderByCouponId(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Game
		 */
		
		public function orderByCouponId($type = null) {
			return $this->orderBy('coupon_id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByCouponId
		 *
		 * This function tells the query to group its results by the 'coupon_id'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Game::create($db)->groupByCouponId(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Game
		 */
		
		public function groupByCouponId($type = null) {
			return $this->groupBy('coupon_id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByCouponId
		 *
		 * This function is a short hand for filtering by the 'coupon_id' field
		 * and then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Game::create($db)->findOneByCouponId($coupon_id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $coupon_id
		 * @param integer $type
		 * @return Value_Game
		 */
		
		public function findOneByCouponId($coupon_id, $type = null) {
			return $this->filterBy('coupon_id', $coupon_id, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByCouponId
		 *
		 * This function is a short hand for filtering by the 'coupon_id' field
		 * and then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Game::create($db)->findAllByCouponId($coupon_id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $coupon_id
		 * @param integer $type
		 * @return Value_Game[]
		 */
		
		public function findAllByCouponId($coupon_id, $type = null) {
			return $this->filterBy('coupon_id', $coupon_id, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByConfig
		 *
		 * This function tells the query to filter its results by the 'config'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Game::create($db)->filterByConfig($config, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $config
		 * @param integer $type
		 * @return Query_Game
		 */
		
		public function filterByConfig($config, $type = null) {
			return $this->filterBy('config', $config, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByConfig
		 *
		 * This function tells the query to orders its results by the 'config'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Game::create($db)->orderByConfig(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Game
		 */
		
		public function orderByConfig($type = null) {
			return $this->orderBy('config', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByConfig
		 *
		 * This function tells the query to group its results by the 'config'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Game::create($db)->groupByConfig(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Game
		 */
		
		public function groupByConfig($type = null) {
			return $this->groupBy('config', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByConfig
		 *
		 * This function is a short hand for filtering by the 'config' field and
		 * then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Game::create($db)->findOneByConfig($config, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $config
		 * @param integer $type
		 * @return Value_Game
		 */
		
		public function findOneByConfig($config, $type = null) {
			return $this->filterBy('config', $config, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByConfig
		 *
		 * This function is a short hand for filtering by the 'config' field and
		 * then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Game::create($db)->findAllByConfig($config, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $config
		 * @param integer $type
		 * @return Value_Game[]
		 */
		
		public function findAllByConfig($config, $type = null) {
			return $this->filterBy('config', $config, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
	}
	
?>