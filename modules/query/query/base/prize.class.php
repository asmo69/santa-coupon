<?php
	
	/**
	 * Query_Base_Prize
	 *
	 * This file is auto generated and should not be modified.
	 *
	 * This abstract class creates the base functionality for Query_Prize
	 * objects. This class should not be modified directly. If you wish to
	 * override or add functionality then you should edit the Query_Prize class.
	 *
	 * @since 2014-08-19 15:54:26
	 * @see Query
	 */
	
	abstract class Query_Base_Prize extends Query {
		
		/* ----------------------------------------------------------------- */
		
		protected static $table = 'prize';
		
		protected static $fields = array(array('id', 'id'), array('code', 'code'), array('name', 'name'), array('stock', 'stock'));
		
		protected static $value = 'Value_Prize';
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * create
		 *
		 * This static function creates an instance of Query_Prize and easily
		 * allows for additional methods to be daisy chained onto it.
		 *
		 * i.e.
		 * $query = Query_Prize::create($db);
		 *
		 * @access public
		 * @static
		 * @param DB $db
		 * @return Query_Prize
		 */
		
		public static function create(DB $db = null) {
			return new Query_Prize($db);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterbyPs
		 *
		 * This function tells the query to filter its results by the primary key
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Prize::create($db)->filterByPK($value, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $value
		 * @param integer $type
		 * @return Query_Prize
		 */
		
		public function filterByPK($value, $type = null) {
			return $this->filterBy('id', $value, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByPK
		 *
		 * This function tells the query to orders its results by the primary key
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Prize::create($db)->orderByPK(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Prize
		 */
		
		public function orderByPK($type = null) {
			return $this->orderBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByPK
		 *
		 * This function tells the query to group its results by the primary key
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Prize::create($db)->groupByPK(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Prize
		 */
		
		public function groupByPK($type = null) {
			return $this->groupBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByPK
		 *
		 * This function is a short hand for filtering by the primary key field
		 * and then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Prize::create($db)->findOneByPK($value, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $value
		 * @param integer $type
		 * @return Value_Prize
		 */
		
		public function findOneByPK($value, $type = null) {
			return $this->filterBy('id', $value, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByPK
		 *
		 * This function is a short hand for filtering by the primary key field
		 * and then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Prize::create($db)->findAllByPK($value, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $value
		 * @param integer $type
		 * @return Value_Prize[]
		 */
		
		public function findAllByPK($value, $type = null) {
			return $this->filterBy('id', $value, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterById
		 *
		 * This function tells the query to filter its results by the 'id' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Prize::create($db)->filterById($id, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $id
		 * @param integer $type
		 * @return Query_Prize
		 */
		
		public function filterById($id, $type = null) {
			return $this->filterBy('id', $id, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderById
		 *
		 * This function tells the query to orders its results by the 'id' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Prize::create($db)->orderById(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Prize
		 */
		
		public function orderById($type = null) {
			return $this->orderBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupById
		 *
		 * This function tells the query to group its results by the 'id' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Prize::create($db)->groupById(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Prize
		 */
		
		public function groupById($type = null) {
			return $this->groupBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneById
		 *
		 * This function is a short hand for filtering by the 'id' field and then
		 * calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Prize::create($db)->findOneById($id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $id
		 * @param integer $type
		 * @return Value_Prize
		 */
		
		public function findOneById($id, $type = null) {
			return $this->filterBy('id', $id, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllById
		 *
		 * This function is a short hand for filtering by the 'id' field and then
		 * calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Prize::create($db)->findAllById($id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $id
		 * @param integer $type
		 * @return Value_Prize[]
		 */
		
		public function findAllById($id, $type = null) {
			return $this->filterBy('id', $id, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByCode
		 *
		 * This function tells the query to filter its results by the 'code'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Prize::create($db)->filterByCode($code, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $code
		 * @param integer $type
		 * @return Query_Prize
		 */
		
		public function filterByCode($code, $type = null) {
			return $this->filterBy('code', $code, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByCode
		 *
		 * This function tells the query to orders its results by the 'code'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Prize::create($db)->orderByCode(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Prize
		 */
		
		public function orderByCode($type = null) {
			return $this->orderBy('code', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByCode
		 *
		 * This function tells the query to group its results by the 'code' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Prize::create($db)->groupByCode(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Prize
		 */
		
		public function groupByCode($type = null) {
			return $this->groupBy('code', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByCode
		 *
		 * This function is a short hand for filtering by the 'code' field and
		 * then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Prize::create($db)->findOneByCode($code, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $code
		 * @param integer $type
		 * @return Value_Prize
		 */
		
		public function findOneByCode($code, $type = null) {
			return $this->filterBy('code', $code, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByCode
		 *
		 * This function is a short hand for filtering by the 'code' field and
		 * then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Prize::create($db)->findAllByCode($code, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $code
		 * @param integer $type
		 * @return Value_Prize[]
		 */
		
		public function findAllByCode($code, $type = null) {
			return $this->filterBy('code', $code, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByName
		 *
		 * This function tells the query to filter its results by the 'name'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Prize::create($db)->filterByName($name, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $name
		 * @param integer $type
		 * @return Query_Prize
		 */
		
		public function filterByName($name, $type = null) {
			return $this->filterBy('name', $name, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByName
		 *
		 * This function tells the query to orders its results by the 'name'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Prize::create($db)->orderByName(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Prize
		 */
		
		public function orderByName($type = null) {
			return $this->orderBy('name', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByName
		 *
		 * This function tells the query to group its results by the 'name' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Prize::create($db)->groupByName(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Prize
		 */
		
		public function groupByName($type = null) {
			return $this->groupBy('name', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByName
		 *
		 * This function is a short hand for filtering by the 'name' field and
		 * then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Prize::create($db)->findOneByName($name, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $name
		 * @param integer $type
		 * @return Value_Prize
		 */
		
		public function findOneByName($name, $type = null) {
			return $this->filterBy('name', $name, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByName
		 *
		 * This function is a short hand for filtering by the 'name' field and
		 * then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Prize::create($db)->findAllByName($name, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $name
		 * @param integer $type
		 * @return Value_Prize[]
		 */
		
		public function findAllByName($name, $type = null) {
			return $this->filterBy('name', $name, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByStock
		 *
		 * This function tells the query to filter its results by the 'stock'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Prize::create($db)->filterByStock($stock, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $stock
		 * @param integer $type
		 * @return Query_Prize
		 */
		
		public function filterByStock($stock, $type = null) {
			return $this->filterBy('stock', $stock, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByStock
		 *
		 * This function tells the query to orders its results by the 'stock'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Prize::create($db)->orderByStock(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Prize
		 */
		
		public function orderByStock($type = null) {
			return $this->orderBy('stock', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByStock
		 *
		 * This function tells the query to group its results by the 'stock'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Prize::create($db)->groupByStock(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Prize
		 */
		
		public function groupByStock($type = null) {
			return $this->groupBy('stock', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByStock
		 *
		 * This function is a short hand for filtering by the 'stock' field and
		 * then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Prize::create($db)->findOneByStock($stock, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $stock
		 * @param integer $type
		 * @return Value_Prize
		 */
		
		public function findOneByStock($stock, $type = null) {
			return $this->filterBy('stock', $stock, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByStock
		 *
		 * This function is a short hand for filtering by the 'stock' field and
		 * then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Prize::create($db)->findAllByStock($stock, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $stock
		 * @param integer $type
		 * @return Value_Prize[]
		 */
		
		public function findAllByStock($stock, $type = null) {
			return $this->filterBy('stock', $stock, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
	}
	
?>