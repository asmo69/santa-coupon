<?php
	
	/**
	 * Query_Base_Address
	 *
	 * This file is auto generated and should not be modified.
	 *
	 * This abstract class creates the base functionality for Query_Address
	 * objects. This class should not be modified directly. If you wish to
	 * override or add functionality then you should edit the Query_Address
	 * class.
	 *
	 * @since 2014-08-19 15:54:26
	 * @see Query
	 */
	
	abstract class Query_Base_Address extends Query {
		
		/* ----------------------------------------------------------------- */
		
		protected static $table = 'address';
		
		protected static $fields = array(array('id', 'id'), array('coupon_id', 'coupon_id'), array('first', 'first'), array('last', 'last'), array('address1', 'address1'), array('address2', 'address2'), array('address3', 'address3'), array('town', 'town'), array('county', 'county'), array('postcode', 'postcode'), array('email', 'email'));
		
		protected static $value = 'Value_Address';
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * create
		 *
		 * This static function creates an instance of Query_Address and easily
		 * allows for additional methods to be daisy chained onto it.
		 *
		 * i.e.
		 * $query = Query_Address::create($db);
		 *
		 * @access public
		 * @static
		 * @param DB $db
		 * @return Query_Address
		 */
		
		public static function create(DB $db = null) {
			return new Query_Address($db);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterbyPs
		 *
		 * This function tells the query to filter its results by the primary key
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->filterByPK($value, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $value
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function filterByPK($value, $type = null) {
			return $this->filterBy('id', $value, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByPK
		 *
		 * This function tells the query to orders its results by the primary key
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->orderByPK(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function orderByPK($type = null) {
			return $this->orderBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByPK
		 *
		 * This function tells the query to group its results by the primary key
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->groupByPK(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function groupByPK($type = null) {
			return $this->groupBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByPK
		 *
		 * This function is a short hand for filtering by the primary key field
		 * and then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->findOneByPK($value, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $value
		 * @param integer $type
		 * @return Value_Address
		 */
		
		public function findOneByPK($value, $type = null) {
			return $this->filterBy('id', $value, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByPK
		 *
		 * This function is a short hand for filtering by the primary key field
		 * and then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->findAllByPK($value, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $value
		 * @param integer $type
		 * @return Value_Address[]
		 */
		
		public function findAllByPK($value, $type = null) {
			return $this->filterBy('id', $value, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterById
		 *
		 * This function tells the query to filter its results by the 'id' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->filterById($id, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $id
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function filterById($id, $type = null) {
			return $this->filterBy('id', $id, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderById
		 *
		 * This function tells the query to orders its results by the 'id' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->orderById(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function orderById($type = null) {
			return $this->orderBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupById
		 *
		 * This function tells the query to group its results by the 'id' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->groupById(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function groupById($type = null) {
			return $this->groupBy('id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneById
		 *
		 * This function is a short hand for filtering by the 'id' field and then
		 * calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->findOneById($id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $id
		 * @param integer $type
		 * @return Value_Address
		 */
		
		public function findOneById($id, $type = null) {
			return $this->filterBy('id', $id, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllById
		 *
		 * This function is a short hand for filtering by the 'id' field and then
		 * calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->findAllById($id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $id
		 * @param integer $type
		 * @return Value_Address[]
		 */
		
		public function findAllById($id, $type = null) {
			return $this->filterBy('id', $id, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByCouponId
		 *
		 * This function tells the query to filter its results by the 'coupon_id'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->filterByCouponId($coupon_id, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $coupon_id
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function filterByCouponId($coupon_id, $type = null) {
			return $this->filterBy('coupon_id', $coupon_id, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByCouponId
		 *
		 * This function tells the query to orders its results by the 'coupon_id'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->orderByCouponId(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function orderByCouponId($type = null) {
			return $this->orderBy('coupon_id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByCouponId
		 *
		 * This function tells the query to group its results by the 'coupon_id'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->groupByCouponId(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function groupByCouponId($type = null) {
			return $this->groupBy('coupon_id', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByCouponId
		 *
		 * This function is a short hand for filtering by the 'coupon_id' field
		 * and then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->findOneByCouponId($coupon_id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $coupon_id
		 * @param integer $type
		 * @return Value_Address
		 */
		
		public function findOneByCouponId($coupon_id, $type = null) {
			return $this->filterBy('coupon_id', $coupon_id, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByCouponId
		 *
		 * This function is a short hand for filtering by the 'coupon_id' field
		 * and then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->findAllByCouponId($coupon_id, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $coupon_id
		 * @param integer $type
		 * @return Value_Address[]
		 */
		
		public function findAllByCouponId($coupon_id, $type = null) {
			return $this->filterBy('coupon_id', $coupon_id, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByFirst
		 *
		 * This function tells the query to filter its results by the 'first'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->filterByFirst($first, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $first
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function filterByFirst($first, $type = null) {
			return $this->filterBy('first', $first, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByFirst
		 *
		 * This function tells the query to orders its results by the 'first'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->orderByFirst(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function orderByFirst($type = null) {
			return $this->orderBy('first', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByFirst
		 *
		 * This function tells the query to group its results by the 'first'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->groupByFirst(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function groupByFirst($type = null) {
			return $this->groupBy('first', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByFirst
		 *
		 * This function is a short hand for filtering by the 'first' field and
		 * then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->findOneByFirst($first, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $first
		 * @param integer $type
		 * @return Value_Address
		 */
		
		public function findOneByFirst($first, $type = null) {
			return $this->filterBy('first', $first, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByFirst
		 *
		 * This function is a short hand for filtering by the 'first' field and
		 * then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->findAllByFirst($first, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $first
		 * @param integer $type
		 * @return Value_Address[]
		 */
		
		public function findAllByFirst($first, $type = null) {
			return $this->filterBy('first', $first, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByLast
		 *
		 * This function tells the query to filter its results by the 'last'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->filterByLast($last, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $last
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function filterByLast($last, $type = null) {
			return $this->filterBy('last', $last, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByLast
		 *
		 * This function tells the query to orders its results by the 'last'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->orderByLast(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function orderByLast($type = null) {
			return $this->orderBy('last', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByLast
		 *
		 * This function tells the query to group its results by the 'last' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->groupByLast(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function groupByLast($type = null) {
			return $this->groupBy('last', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByLast
		 *
		 * This function is a short hand for filtering by the 'last' field and
		 * then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->findOneByLast($last, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $last
		 * @param integer $type
		 * @return Value_Address
		 */
		
		public function findOneByLast($last, $type = null) {
			return $this->filterBy('last', $last, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByLast
		 *
		 * This function is a short hand for filtering by the 'last' field and
		 * then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->findAllByLast($last, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $last
		 * @param integer $type
		 * @return Value_Address[]
		 */
		
		public function findAllByLast($last, $type = null) {
			return $this->filterBy('last', $last, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByAddress1
		 *
		 * This function tells the query to filter its results by the 'address1'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->filterByAddress1($address1, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $address1
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function filterByAddress1($address1, $type = null) {
			return $this->filterBy('address1', $address1, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByAddress1
		 *
		 * This function tells the query to orders its results by the 'address1'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->orderByAddress1(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function orderByAddress1($type = null) {
			return $this->orderBy('address1', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByAddress1
		 *
		 * This function tells the query to group its results by the 'address1'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->groupByAddress1(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function groupByAddress1($type = null) {
			return $this->groupBy('address1', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByAddress1
		 *
		 * This function is a short hand for filtering by the 'address1' field
		 * and then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->findOneByAddress1($address1, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $address1
		 * @param integer $type
		 * @return Value_Address
		 */
		
		public function findOneByAddress1($address1, $type = null) {
			return $this->filterBy('address1', $address1, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByAddress1
		 *
		 * This function is a short hand for filtering by the 'address1' field
		 * and then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->findAllByAddress1($address1, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $address1
		 * @param integer $type
		 * @return Value_Address[]
		 */
		
		public function findAllByAddress1($address1, $type = null) {
			return $this->filterBy('address1', $address1, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByAddress2
		 *
		 * This function tells the query to filter its results by the 'address2'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->filterByAddress2($address2, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $address2
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function filterByAddress2($address2, $type = null) {
			return $this->filterBy('address2', $address2, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByAddress2
		 *
		 * This function tells the query to orders its results by the 'address2'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->orderByAddress2(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function orderByAddress2($type = null) {
			return $this->orderBy('address2', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByAddress2
		 *
		 * This function tells the query to group its results by the 'address2'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->groupByAddress2(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function groupByAddress2($type = null) {
			return $this->groupBy('address2', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByAddress2
		 *
		 * This function is a short hand for filtering by the 'address2' field
		 * and then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->findOneByAddress2($address2, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $address2
		 * @param integer $type
		 * @return Value_Address
		 */
		
		public function findOneByAddress2($address2, $type = null) {
			return $this->filterBy('address2', $address2, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByAddress2
		 *
		 * This function is a short hand for filtering by the 'address2' field
		 * and then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->findAllByAddress2($address2, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $address2
		 * @param integer $type
		 * @return Value_Address[]
		 */
		
		public function findAllByAddress2($address2, $type = null) {
			return $this->filterBy('address2', $address2, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByAddress3
		 *
		 * This function tells the query to filter its results by the 'address3'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->filterByAddress3($address3, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $address3
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function filterByAddress3($address3, $type = null) {
			return $this->filterBy('address3', $address3, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByAddress3
		 *
		 * This function tells the query to orders its results by the 'address3'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->orderByAddress3(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function orderByAddress3($type = null) {
			return $this->orderBy('address3', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByAddress3
		 *
		 * This function tells the query to group its results by the 'address3'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->groupByAddress3(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function groupByAddress3($type = null) {
			return $this->groupBy('address3', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByAddress3
		 *
		 * This function is a short hand for filtering by the 'address3' field
		 * and then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->findOneByAddress3($address3, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $address3
		 * @param integer $type
		 * @return Value_Address
		 */
		
		public function findOneByAddress3($address3, $type = null) {
			return $this->filterBy('address3', $address3, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByAddress3
		 *
		 * This function is a short hand for filtering by the 'address3' field
		 * and then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->findAllByAddress3($address3, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $address3
		 * @param integer $type
		 * @return Value_Address[]
		 */
		
		public function findAllByAddress3($address3, $type = null) {
			return $this->filterBy('address3', $address3, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByTown
		 *
		 * This function tells the query to filter its results by the 'town'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->filterByTown($town, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $town
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function filterByTown($town, $type = null) {
			return $this->filterBy('town', $town, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByTown
		 *
		 * This function tells the query to orders its results by the 'town'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->orderByTown(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function orderByTown($type = null) {
			return $this->orderBy('town', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByTown
		 *
		 * This function tells the query to group its results by the 'town' field
		 * using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->groupByTown(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function groupByTown($type = null) {
			return $this->groupBy('town', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByTown
		 *
		 * This function is a short hand for filtering by the 'town' field and
		 * then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->findOneByTown($town, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $town
		 * @param integer $type
		 * @return Value_Address
		 */
		
		public function findOneByTown($town, $type = null) {
			return $this->filterBy('town', $town, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByTown
		 *
		 * This function is a short hand for filtering by the 'town' field and
		 * then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->findAllByTown($town, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $town
		 * @param integer $type
		 * @return Value_Address[]
		 */
		
		public function findAllByTown($town, $type = null) {
			return $this->filterBy('town', $town, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByCounty
		 *
		 * This function tells the query to filter its results by the 'county'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->filterByCounty($county, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $county
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function filterByCounty($county, $type = null) {
			return $this->filterBy('county', $county, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByCounty
		 *
		 * This function tells the query to orders its results by the 'county'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->orderByCounty(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function orderByCounty($type = null) {
			return $this->orderBy('county', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByCounty
		 *
		 * This function tells the query to group its results by the 'county'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->groupByCounty(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function groupByCounty($type = null) {
			return $this->groupBy('county', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByCounty
		 *
		 * This function is a short hand for filtering by the 'county' field and
		 * then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->findOneByCounty($county, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $county
		 * @param integer $type
		 * @return Value_Address
		 */
		
		public function findOneByCounty($county, $type = null) {
			return $this->filterBy('county', $county, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByCounty
		 *
		 * This function is a short hand for filtering by the 'county' field and
		 * then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->findAllByCounty($county, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $county
		 * @param integer $type
		 * @return Value_Address[]
		 */
		
		public function findAllByCounty($county, $type = null) {
			return $this->filterBy('county', $county, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByPostcode
		 *
		 * This function tells the query to filter its results by the 'postcode'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->filterByPostcode($postcode, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $postcode
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function filterByPostcode($postcode, $type = null) {
			return $this->filterBy('postcode', $postcode, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByPostcode
		 *
		 * This function tells the query to orders its results by the 'postcode'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->orderByPostcode(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function orderByPostcode($type = null) {
			return $this->orderBy('postcode', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByPostcode
		 *
		 * This function tells the query to group its results by the 'postcode'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->groupByPostcode(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function groupByPostcode($type = null) {
			return $this->groupBy('postcode', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByPostcode
		 *
		 * This function is a short hand for filtering by the 'postcode' field
		 * and then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->findOneByPostcode($postcode, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $postcode
		 * @param integer $type
		 * @return Value_Address
		 */
		
		public function findOneByPostcode($postcode, $type = null) {
			return $this->filterBy('postcode', $postcode, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByPostcode
		 *
		 * This function is a short hand for filtering by the 'postcode' field
		 * and then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->findAllByPostcode($postcode, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $postcode
		 * @param integer $type
		 * @return Value_Address[]
		 */
		
		public function findAllByPostcode($postcode, $type = null) {
			return $this->filterBy('postcode', $postcode, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterByEmail
		 *
		 * This function tells the query to filter its results by the 'email'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->filterByEmail($email, Query::Filter_EQUALS)->findOne();
		 *
		 * @access public
		 * @param mixed $email
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function filterByEmail($email, $type = null) {
			return $this->filterBy('email', $email, $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByEmail
		 *
		 * This function tells the query to orders its results by the 'email'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->orderByEmail(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function orderByEmail($type = null) {
			return $this->orderBy('email', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByEmail
		 *
		 * This function tells the query to group its results by the 'email'
		 * field using the specific criteria.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->groupByEmail(Query::DIRECTION_ASC)->findAll();
		 *
		 * @access public
		 * @param integer $type
		 * @return Query_Address
		 */
		
		public function groupByEmail($type = null) {
			return $this->groupBy('email', $type);
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterOneByEmail
		 *
		 * This function is a short hand for filtering by the 'email' field and
		 * then calling the findOne() method.
		 *
		 * i.e.
		 * $result = Query_Address::create($db)->findOneByEmail($email, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $email
		 * @param integer $type
		 * @return Value_Address
		 */
		
		public function findOneByEmail($email, $type = null) {
			return $this->filterBy('email', $email, $type)->findOne();
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAllByEmail
		 *
		 * This function is a short hand for filtering by the 'email' field and
		 * then calling the findAll() method.
		 *
		 * i.e.
		 * $results = Query_Address::create($db)->findAllByEmail($email, Query::FILTER_EQUALS);
		 *
		 * @access public
		 * @param mixed $email
		 * @param integer $type
		 * @return Value_Address[]
		 */
		
		public function findAllByEmail($email, $type = null) {
			return $this->filterBy('email', $email, $type)->findAll();
		}
		
		/* ----------------------------------------------------------------- */
		
	}
	
?>