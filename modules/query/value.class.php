<?php
	
	/**
	 * Value
	 *
	 * This file is auto generated and should not be modified.
	 *
	 * This class creates the abstract base for all Value objects to inherit
	 * from.
	 *
	 * @since 2014-08-19 15:54:26
	 * @abstract
	 */
	
	abstract class Value {
		
		/* ------------------------------------------------------------------ */
		
		protected $_modified = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * save
		 *
		 * This abstract function sets the pattern for all Value objects to save
		 * their contents back to the selected database.
		 *
		 * i.e.
		 * $value->save($db);
		 *
		 * @access public
		 * @param DB $db
		 * @return integer
		 * @see Query
		 */
		
		abstract public function save(DB $db);
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * delete
		 *
		 * This abstract function sets the pattern for all Value objects to deletes
		 * their contents from the selected database.
		 *
		 * i.e.
		 * $value->delete($db);
		 *
		 * @access public
		 * @param DB $db
		 * @return integer
		 * @see Query
		 */
		
		abstract public function delete(DB $db);
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>