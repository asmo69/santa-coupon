<?php
	
	/**
	 * Query
	 *
	 * This file is auto generated and should not be modified.
	 *
	 * This class creates the abstract base for all Query objects to inherit
	 * from. The class provides functionality for database queries to be built
	 * and executed in a friendly manner.
	 *
	 * i.e.
	 * $user = Query_User::create($db)->filterByUsername('joebloggs')->findOne();
	 *
	 * @since 2014-08-19 15:54:26
	 * @abstract
	 */
	
	abstract class Query {
		
		/* ----------------------------------------------------------------- */
		
		const FILTER_IS_NULL = 0x01;
		const FILTER_NOT_IS_NULL = 0x02;
		const FILTER_EQUALS = 0x03;
		const FILTER_NOT_EQUALS = 0x04;
		const FILTER_LESS_THAN = 0x05;
		const FILTER_GREATER_THAN = 0x06;
		const FILTER_LESS_EQUALS = 0x07;
		const FILTER_GREATER_EQUALS = 0x08;
		const FILTER_IN = 0x09;
		const FILTER_NOT_IN = 0x0A;
		const FILTER_CONTAINS = 0x0B;
		const FILTER_STARTS_WITH = 0x0C;
		const FILTER_ENDS_WITH = 0x0D;
		const FILTER_BETWEEN = 0x0E;
		const FILTER_NOT_BETWEEN = 0x0F;
		
		/* ----------------------------------------------------------------- */
		
		const DIRECTION_ASC = 0x01;
		const DIRECTION_DESC = 0x02;
		
		/* ----------------------------------------------------------------- */
		
		protected static $table = null;
		protected static $fields = null;
		protected static $value = null;
		
		/* ----------------------------------------------------------------- */
		
		protected $db = null;
		protected $filters = array();
		protected $groups = array();
		protected $orders = array();
		protected $limit = null;
		protected $offset = null;
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * create
		 *
		 * This static function assists in creating Query objects and allows
		 * additional filtering etc to be chained into one line.
		 *
		 * i.e.
		 * $result = Query_User::create($db)->limit(10)->findAll();
		 *
		 * @access public
		 * @static
		 * @param DB $db
		 * @return Query
		 */
		
		abstract public static function create(DB $db = null);
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * __construct
		 *
		 * This function is the constructor for creating new Query objects.
		 * However, you should ideally use the static create() function as this
		 * provides a slightly cleaner query syntax.
		 *
		 * i.e.
		 * $query = new Query_User($db);
		 * $result = $query->limit(10)->findAll();
		 *
		 * @access public
		 * @param DB $db
		 */
		
		public function __construct(DB $db = null) {
			if ($db) {
				$this->db = $db;
			}
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * setDB
		 *
		 * This function allows you to easily set and change the database on
		 * which the query will be executed. This allows for scenarios where
		 * queries need to be run on the master or slave databases.
		 *
		 * i.e.
		 * $query = new Query_User();
		 * $slave = $query->setDB($slaveDB)->limit(10)->findAll();
		 * $master = $query->setDB($masterDB)->limit(10)->findAll();
		 *
		 * @access public
		 * @param DB $db
		 * @return Query
		 */
		
		public function setDB(DB $db) {
			$this->db = $db;
			return $this;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * getDB
		 *
		 * This function returns the current database assigned to this Query
		 * object.
		 *
		 * i.e.
		 * $db = $query->getDB();
		 *
		 * @access public
		 * @return DB
		 */
		
		public function getDB() {
			return $this->db;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * filterBy
		 *
		 * This protected function provides the base functionality for concrete
		 * Query objects to filter results by the specified field, value and
		 * criteria.
		 *
		 * i.e.
		 * $query->filterBy('username', 'joebloggs', Query::FILTER_EQUALS);
		 *
		 * @access protected
		 * @param string $field
		 * @param mixed $value
		 * @param integer $type
		 * @return Query
		 */
		
		protected function filterBy($field, $value, $type = null) {
			if ($type == null) {
				if (is_array($value)) {
					$type = self::FILTER_IN;
				} else {
					$type = self::FILTER_EQUALS;
				}
			}
			$this->filters[] = array(
				$field,
				$value,
				$type
			);
			return $this;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupBy
		 *
		 * This protected function provides the base functionality for concrete
		 * Query objects to group results by the specified field and criteria.
		 *
		 * i.e.
		 * $query->groupBy('username', Query::DIRECTION_ASC);
		 *
		 * @access protected
		 * @param mixed $value
		 * @param integer $type
		 * @return Query
		 */
		
		protected function groupBy($field, $type = null) {
			if ($type == null) {
				$type = self::DIRECTION_ASC;
			}
			$this->groups[] = array(
				$field,
				$type
			);
			return $this;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * groupByRand
		 *
		 * This function provides the base functionality for concrete
		 * Query objects to group results by a random number.
		 *
		 * i.e.
		 * $query->groupByRand();
		 *
		 * @access public
		 * @return Query
		 */
		
		public function groupByRand() {
			$this->groups[] = array(
				'RANDOM()',
				self::DIRECTION_ASC
			);
			return $this;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderBy
		 *
		 * This protected function provides the base functionality for concrete
		 * Query objects to order results by the specified field and criteria.
		 *
		 * i.e.
		 * $query->orderBy('username', Query::DIRECTION_ASC);
		 *
		 * @access protected
		 * @param mixed $value
		 * @param integer $type
		 * @return Query
		 */
		
		protected function orderBy($field, $type = null) {
			if ($type == null) {
				$type = self::DIRECTION_ASC;
			}
			$this->orders[] = array(
				$field,
				$type
			);
			return $this;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * orderByRand
		 *
		 * This function provides the base functionality for concrete
		 * Query objects to order results by a random number.
		 *
		 * i.e.
		 * $query->orderByRand();
		 *
		 * @access public
		 * @return Query
		 */
		
		public function orderByRand() {
			$this->orders[] = array(
				'RANDOM()',
				self::DIRECTION_ASC
			);
			return $this;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * limit
		 *
		 * This functions provides functionality for limiting the number of
		 * records returned by the Query. This is commonly used in conjunction
		 * with offset().
		 *
		 * i.e.
		 * $query->limit(10);
		 *
		 * @access public
		 * @param integer $limit
		 * @return Query
		 */
		
		public function limit($limit) {
			$this->limit = $limit === null ? null : intval($limit);
			return $this;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * offset
		 *
		 * This functions provides functionality for setting the offset of
		 * records returned by the Query. This is commonly used in conjunction
		 * with limit().
		 *
		 * i.e.
		 * $query->offset(10);
		 *
		 * @access public
		 * @param integer $offset
		 * @return Query
		 */
		
		public function offset($offset) {
			$this->offset = $offset === null ? null : $offset;
			return $this;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findOne
		 *
		 * This function executes the Query object and returns the first
		 * matching result.
		 *
		 * i.e.
		 * $user = Query_User::create($db)->filterByUsername('joebloggs')->findOne();
		 *
		 * @access public
		 * @return Value
		 */
		
		public function findOne() {
			if ($this->limit === null && $this->offset === null) {
				$this->limit(1);
			}
			if ($ret = $this->findAll()) {
				return $ret[0];
			}
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * findAll
		 *
		 * This function executes the Query object and returns an array of the
		 * matching results.
		 *
		 * i.e.
		 * $users = Query_User::create($db)->findAll();
		 *
		 * @access public
		 * @return Value[]
		 */
		
		public function findAll() {
			$ret    = array();
			$params = array();
			
			if ($this->db && static::$table && static::$fields && static::$value) {
				$fields = array();
				foreach (static::$fields as $field) {
					if ($field[0] != $field[1]) {
						$fields[] = sprintf("`%s` AS `%s`", $field[0], $field[1]);
					} else {
						$fields[] = sprintf("`%s`", $field[0]);
					}
				}
				$sql[] = sprintf("SELECT\n\t%s\nFROM\n\t`%s`\n", implode(', ', $fields), static::$table);
				if ($this->filters) {
					$sql[] = sprintf("WHERE\n");
					foreach ($this->filters as $filter) {
						if ($filter[2] == self::FILTER_IS_NULL) {
							$sql[]    = sprintf("\t`%s` IS NULL\n");
							$params[] = $filter[1];
						} elseif ($filter[2] == self::FILTER_NOT_IS_NULL) {
							$sql[]    = sprintf("\t`%s` NOT IS NULL\n");
							$params[] = $filter[1];
						} elseif ($filter[2] == self::FILTER_NOT_EQUALS) {
							$sql[]    = sprintf("\t`%s` != ?\n", $filter[0]);
							$params[] = $filter[1];
						} elseif ($filter[2] == self::FILTER_LESS_THAN) {
							$sql[]    = sprintf("\t`%s` < ?\n", $filter[0]);
							$params[] = $filter[1];
						} elseif ($filter[2] == self::FILTER_GREATER_THAN) {
							$sql[]    = sprintf("\t`%s` > ?\n", $filter[0]);
							$params[] = $filter[1];
						} elseif ($filter[2] == self::FILTER_LESS_EQUALS) {
							$sql[]    = sprintf("\t`%s` <= ?\n", $filter[0]);
							$params[] = $filter[1];
						} elseif ($filter[2] == self::FILTER_GREATER_EQUALS) {
							$sql[]    = sprintf("\t`%s` >= ?\n", $filter[0]);
							$params[] = $filter[1];
						} elseif ($filter[2] == self::FILTER_IN) {
							if (is_array($filter[1])) {
								$temp = array();
								foreach ($filter[1] as $item) {
									$temp[]   = '?';
									$params[] = $item;
								}
								
								$sql[] = sprintf("\t`%s` IN (%s)\n", $filter[0], implode(', ', $temp));
							}
						} elseif ($filter[2] == self::FILTER_NOT_IN) {
							if (is_array($filter[1])) {
								$temp = array();
								foreach ($filter[1] as $item) {
									$temp[]   = '?';
									$params[] = $item;
								}
								
								$sql[] = sprintf("\t`%s` NOT IN (%s)\n", $filter[0], implode(', ', $temp));
							}
						} elseif ($filter[2] == self::FILTER_CONTAINS) {
							$sql[]    = sprintf("\t`%s` LIKE ?\n", $filter[0]);
							$params[] = '%' . $filter[1] . '%';
						} elseif ($filter[2] == self::FILTER_STARTS_WITH) {
							$sql[]    = sprintf("\t`%s` LIKE ?\n", $filter[0]);
							$params[] = '%' . $filter[1];
						} elseif ($filter[2] == self::FILTER_ENDS_WITH) {
							$sql[]    = sprintf("\t`%s` LIKE ?\n", $filter[0]);
							$params[] = $filter[1] . '%';
						} elseif ($filter[2] == self::FILTER_BETWEEN) {
							if (is_array($filter[1]) && count($filter[1]) > 1) {
								$sql[]    = sprintf("\t`%s` BETWEEN ? AND ?\n", $filter[0]);
								$params[] = $filter[1][0];
								$params[] = $filter[1][1];
							}
						} elseif ($filter[2] == self::FILTER_NOT_BETWEEN) {
							if (is_array($filter[1]) && count($filter[1]) > 1) {
								$sql[]    = sprintf("\t`%s` NOT BETWEEN ? AND ?\n", $filter[0]);
								$params[] = $filter[1][0];
								$params[] = $filter[1][1];
							}
						} else {
							$sql[]    = sprintf("\t`%s` = ?\n", $filter[0]);
							$params[] = $filter[1];
						}
					}
				}
				if ($this->orders) {
					$sql[] = sprintf("ORDER BY\n");
					foreach ($this->orders as $temp) {
						if (strpos($temp[0], '(') == true || strpos($temp[0], ')') == true) {
							$sql[] = sprintf("\t%s %s\n", $temp[0], $temp[1] == self::DIRECTION_ASC ? 'ASC' : 'DESC');
						} else {
							$sql[] = sprintf("\t`%s` %s\n", $temp[0], $temp[1] == self::DIRECTION_ASC ? 'ASC' : 'DESC');
						}
					}
				}
				if ($this->groups) {
					$sql[] = sprintf("GROUP BY\n");
					foreach ($this->groups as $temp) {
						if (strpos($temp[0], '(') == true || strpos($temp[0], ')') == true) {
							$sql[] = sprintf("\t%s %s\n", $temp[0], $temp[1] == self::DIRECTION_ASC ? 'ASC' : 'DESC');
						} else {
							$sql[] = sprintf("\t`%s` %s\n", $temp[0], $temp[1] == self::DIRECTION_ASC ? 'ASC' : 'DESC');
						}
					}
				}
				if ($this->limit) {
					if ($this->offset) {
						$sql[] = sprintf("LIMIT %s, %s\n", $this->limit, $this->offset);
					} else {
						$sql[] = sprintf("LIMIT %s\n", $this->limit);
					}
				}
				
				if (empty($ret)) {
					$result = $this->db->query(implode($sql), $params);
					while ($row = $result->fetch(static::$value)) {
						$ret[] = $row;
					}
				}
			}
			
			return $ret;
		}
		
		/* ----------------------------------------------------------------- */
		
		/**
		 * delete
		 *
		 * This function executes the Query object and deletes the matching
		 * results.
		 *
		 * i.e.
		 * Query_User::create($db)->filterByUsername('joebloggs')->delete();
		 *
		 * @access public
		 * @return integer
		 */
		
		public function delete() {
			$params = array();
			
			if ($this->db && static::$table) {
				$sql[] = sprintf("DELETE FROM\n\t`%s`\n", static::$table);
				if ($this->filters) {
					$sql[] = sprintf("WHERE\n");
					foreach ($this->filters as $filter) {
						if ($filter[2] == self::FILTER_IS_NULL) {
							$sql[]    = sprintf("\t`%s` IS NULL\n");
							$params[] = $filter[1];
						} elseif ($filter[2] == self::FILTER_NOT_IS_NULL) {
							$sql[]    = sprintf("\t`%s` NOT IS NULL\n");
							$params[] = $filter[1];
						} elseif ($filter[2] == self::FILTER_NOT_EQUALS) {
							$sql[]    = sprintf("\t`%s` != ?\n", $filter[0]);
							$params[] = $filter[1];
						} elseif ($filter[2] == self::FILTER_LESS_THAN) {
							$sql[]    = sprintf("\t`%s` < ?\n", $filter[0]);
							$params[] = $filter[1];
						} elseif ($filter[2] == self::FILTER_GREATER_THAN) {
							$sql[]    = sprintf("\t`%s` > ?\n", $filter[0]);
							$params[] = $filter[1];
						} elseif ($filter[2] == self::FILTER_LESS_EQUALS) {
							$sql[]    = sprintf("\t`%s` <= ?\n", $filter[0]);
							$params[] = $filter[1];
						} elseif ($filter[2] == self::FILTER_GREATER_EQUALS) {
							$sql[]    = sprintf("\t`%s` >= ?\n", $filter[0]);
							$params[] = $filter[1];
						} elseif ($filter[2] == self::FILTER_IN) {
							if (is_array($filter[1])) {
								$temp = array();
								foreach ($filter[1] as $item) {
									$temp[]   = '?';
									$params[] = $item;
								}
								
								$sql[] = sprintf("\t`%s` IN (%s)\n", $filter[0], implode(', ', $temp));
							}
						} elseif ($filter[2] == self::FILTER_NOT_IN) {
							if (is_array($filter[1])) {
								$temp = array();
								foreach ($filter[1] as $item) {
									$temp[]   = '?';
									$params[] = $item;
								}
								
								$sql[] = sprintf("\t`%s` NOT IN (%s)\n", $filter[0], implode(', ', $temp));
							}
						} elseif ($filter[2] == self::FILTER_CONTAINS) {
							$sql[]    = sprintf("\t`%s` LIKE ?\n", $filter[0]);
							$params[] = '%' . $filter[1] . '%';
						} elseif ($filter[2] == self::FILTER_STARTS_WITH) {
							$sql[]    = sprintf("\t`%s` LIKE ?\n", $filter[0]);
							$params[] = '%' . $filter[1];
						} elseif ($filter[2] == self::FILTER_ENDS_WITH) {
							$sql[]    = sprintf("\t`%s` LIKE ?\n", $filter[0]);
							$params[] = $filter[1] . '%';
						} elseif ($filter[2] == self::FILTER_BETWEEN) {
							if (is_array($filter[1]) && count($filter[1]) > 1) {
								$sql[]    = sprintf("\t`%s` BETWEEN ? AND ?\n", $filter[0]);
								$params[] = $filter[1][0];
								$params[] = $filter[1][1];
							}
						} elseif ($filter[2] == self::FILTER_NOT_BETWEEN) {
							if (is_array($filter[1]) && count($filter[1]) > 1) {
								$sql[]    = sprintf("\t`%s` NOT BETWEEN ? AND ?\n", $filter[0]);
								$params[] = $filter[1][0];
								$params[] = $filter[1][1];
							}
						} else {
							$sql[]    = sprintf("\t`%s` = ?\n", $filter[0]);
							$params[] = $filter[1];
						}
					}
				}
				
				$this->db->query(implode($sql), $params);
				return $this->db->getAffectedRows();
			}
		}
		
		/* ----------------------------------------------------------------- */
		
	}
	
?>