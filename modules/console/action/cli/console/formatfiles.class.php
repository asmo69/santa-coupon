<?php
	
	class Action_CLI_Console_FormatFiles extends Action implements Info {
		
		/* ----------------------------------------------------------------- */
		
		public function info() {
			return array(
				'name' => 'format-files',
				'description' => 'Reformats PHP files.'
			);
		}
		
		/* ----------------------------------------------------------------- */
		
		public function execute(Application $application) {
			$excludes = array();
			
			if (is_dir(ROOT_DIR . '/assets')) {
				$excludes[] = realpath(ROOT_DIR . '/assets') . '/';
			}
			if (is_dir(ROOT_DIR . '/data')) {
				$excludes[] = realpath(ROOT_DIR . '/data') . '/';
			}
			if (is_dir(ROOT_DIR . '/temp')) {
				$excludes[] = realpath(ROOT_DIR . '/temp') . '/';
			}
			if (is_dir(ROOT_DIR . '/templates')) {
				$excludes[] = realpath(ROOT_DIR . '/templates') . '/';
			}
			if (is_dir(ROOT_DIR . '/external')) {
				$excludes[] = realpath(ROOT_DIR . '/external') . '/';
			}
			
			foreach (get_files(ROOT_DIR, '/\.(php|inc)$/i') as $file) {
				$valid = true;
				foreach ($excludes as $exclude) {
					if (substr($file, 0, strlen($exclude)) == $exclude) {
						$valid = false;
					}
				}
				if ($valid) {
					
					$url = 'http://beta.phpformatter.com/Output/';
					
					$post                                       = array();
					$post['spaces_around_map_operator']         = 'on';
					$post['spaces_around_assignment_operators'] = 'on';
					$post['spaces_around_bitwise_operators']    = 'on';
					$post['spaces_around_relational_operators'] = 'on';
					$post['spaces_around_equality_operators']   = 'on';
					$post['spaces_around_logical_operators']    = 'on';
					$post['spaces_around_math_operators']       = 'on';
					$post['space_after_structures']             = 'on';
					$post['align_assignments']                  = 'on';
					$post['indent_case_default']                = 'on';
					$post['indent_number']                      = '1';
					$post['first_indent_number']                = '1';
					$post['indent_char']                        = '\t';
					$post['indent_style']                       = 'K&R';
					$post['code']                               = file_get_contents($file);
					
					$url  = new Net_URL($url);
					$data = $url->fetch($post);
					if ($data->http_code == 200 && $data->response) {
						if ($json = json_decode($data->response, true)) {
							if (array_key_exists('plainoutput', $json) && $content = $json['plainoutput']) {
								if (trim($content)) {
									printf("Formatted: %s\n", $file);
									file_put_contents($file, trim($content));
								}
							}
						}
					}
				}
			}
		}
		
		/* ----------------------------------------------------------------- */
		
	}
	
?>