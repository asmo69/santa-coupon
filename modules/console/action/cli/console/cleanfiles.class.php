<?php
	
	class Action_CLI_Console_CleanFiles extends Action implements Info {
		
		/* ----------------------------------------------------------------- */
		
		public function info() {
			return array(
				'name' => 'clean-files',
				'description' => 'Cleans white spaces from PHP files.'
			);
		}
		
		/* ----------------------------------------------------------------- */
		
		public function execute(Application $application) {
			foreach (get_files(ROOT_DIR, '/\.(php|inc)$/i') as $file) {
				$content = file_get_contents($file);
				$cleaned = $content;
				$cleaned = trim($cleaned);
				$cleaned = str_replace("\r", "\n", str_replace("\r\n", "\n", $cleaned));
				$cleaned = implode("\n", array_map('rtrim', explode("\n", $cleaned)));
				
				if ($cleaned != $content) {
					printf("Cleaned: %s\n", $file);
					file_put_contents($file, $cleaned);
				}
			}
		}
		
		/* ----------------------------------------------------------------- */
		
	}
	
?>