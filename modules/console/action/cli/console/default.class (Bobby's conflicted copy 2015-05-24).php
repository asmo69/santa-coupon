<?php

	class Action_CLI_Console_Default extends Action {

		public function execute(Application $application) {
			// Get the action name.
			$name = get_called_class();

			$info = array();

			foreach (Registry::getAutoLoader()->getModules() as $module) {
				$path = dirname(ROOT_DIR . '/modules/' . strtolower($module) . '/' . strtolower(str_replace('_', '/', $name)));
				if (is_dir($path) && $fd = opendir($path)) {
					while ($file = readdir($fd)) {
						if ($file != '.' && $file != '..' && preg_match('/^(.*)\.class\.php$/', $file)) {
							$classes = get_declared_classes();
							require_once $path . '/' . $file;

							foreach (get_declared_classes() as $name) {
								if (in_array($name, $classes) == false && is_subclass_of($name, 'Action') && is_subclass_of($name, 'Info')) {
									$class              = new $name();
									$ret                = $class->info();
									$info[$ret['name']] = $ret;
								}
							}
						}
					}
					closedir($fd);
				}
			}
			ksort($info);

			$out = '';
			$out .= sprintf("Usage:\n");
			$out .= sprintf("\tphp console.php --action=[ACTION]\n");
			$out .= sprintf("\n");
			$out .= sprintf("Actions:\n");
			foreach ($info as $item) {
				$out .= sprintf("\t%s - %s\n", $item['name'], $item['description']);
			}

			$application->setOutput($out);
		}

	}

?>