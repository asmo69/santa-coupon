<?php

	class Action_CLI_Console_CreatePHPDoc extends Action implements Info {

		/* ----------------------------------------------------------------- */

		public function info() {
			return array(
				'name' => 'create-php-doc',
				'description' => 'Create HTML documentation based on PHP code.'
			);
		}

		/* ----------------------------------------------------------------- */

		public function execute(Application $application) {
			$docs = new DOC_PHP();
			$docs->start(ROOT_DIR);
		}

		/* ----------------------------------------------------------------- */

	}

?>