<?php
	
	/**
	 * Action_Website_Santa_Install
	 *
	 * This action class represents the 'installation action of the website and
	 * is used to create and populate the SQLite database.
	 *
	 * i.e.
	 * index.php?action=win
	 *
	 * @since 2013-10-17
	 * @see Action
	 */
	
	class Action_Website_Santa_Install extends Action {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * This function is automatically called by the Controller class and
		 * represents the main body of the action.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$parser = Registry::getParser();
			
			@unlink(ROOT_DIR . '/data/database.db');
			
			$parser->setVar('status', $this->importStatus($application));
			$parser->setVar('prize', $this->importPrize($application));
			$parser->setVar('coupon', $this->importCoupon($application));
			$parser->setVar('game', $this->importGame($application));
			$parser->setVar('address', $this->importAddress($application));
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * importStatus
		 *
		 * This function creates the 'status' table and imports the contents of
		 * the status.csv file.
		 *
		 * @access protected
		 * @param Application $application
		 * @return integer
		 */
		
		protected function importStatus(Application $application) {
			$db = Registry::getDB();
			
			$sql = sprintf("DROP TABLE IF EXISTS status");
			$db->query($sql);
			
			$sql = sprintf("CREATE TABLE IF NOT EXISTS status (
				id INTEGER PRIMARY KEY,
				name TEXT
			)");
			$db->query($sql);
			
			$sql = sprintf("DELETE FROM status");
			$db->query($sql);
			
			if (is_file(ROOT_DIR . '/data/status.csv')) {
				if ($fd = fopen(ROOT_DIR . '/data/status.csv', 'rb')) {
					$header = null;
					while ($row = fgetcsv($fd)) {
						if ($header === null) {
							$header = $row;
						} else {
							$sql = sprintf("INSERT INTO status (id, name) VALUES(?, ?)");
							$db->query($sql, $row);
						}
					}
					fclose($fd);
				}
			}
			
			$sql  = sprintf("SELECT COUNT(*) AS count FROM status");
			$temp = $db->query($sql)->fetch();
			
			return $temp['count'];
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * importPrize
		 *
		 * This function creates the 'prize' table and imports the contents of
		 * the proze.csv file.
		 *
		 * @access protected
		 * @param Application $application
		 * @return integer
		 */
		
		protected function importPrize(Application $application) {
			$db = Registry::getDB();
			
			$sql = sprintf("DROP TABLE IF EXISTS prize");
			$db->query($sql);
			
			$sql = sprintf("CREATE TABLE IF NOT EXISTS prize (
				id INTEGER PRIMARY KEY,
				code TEXT,
				name TEXT,
				stock INTEGER
			)");
			$db->query($sql);
			
			$sql = sprintf("DELETE FROM prize");
			$db->query($sql);
			
			if (is_file(ROOT_DIR . '/data/prize.csv')) {
				if ($fd = fopen(ROOT_DIR . '/data/prize.csv', 'rb')) {
					$header = null;
					while ($row = fgetcsv($fd)) {
						if ($header === null) {
							$header = $row;
						} else {
							$sql = sprintf("INSERT INTO prize (id, code, name, stock) VALUES(?, ?, ?, ?)");
							$db->query($sql, $row);
						}
					}
					fclose($fd);
				}
			}
			
			$sql  = sprintf("SELECT COUNT(*) AS count FROM prize");
			$temp = $db->query($sql)->fetch();
			
			return $temp['count'];
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * importCoupon
		 *
		 * This function creates the 'coupon' table and imports the contents of
		 * the coupon.csv file.
		 *
		 * @access protected
		 * @param Application $application
		 * @return integer
		 */
		
		protected function importCoupon(Application $application) {
			$db = Registry::getDB();
			
			$sql = sprintf("DROP TABLE IF EXISTS coupon");
			$db->query($sql);
			
			$sql = sprintf("CREATE TABLE IF NOT EXISTS coupon (
				id INTEGER PRIMARY KEY,
				code TEXT,
				prize_id INTEGER,
				status_id INTEGER,
				FOREIGN KEY(prize_id) REFERENCES prize(id),
				FOREIGN KEY(status_id) REFERENCES status(id)
			)");
			$db->query($sql);
			
			$sql = sprintf("DELETE FROM coupon");
			$db->query($sql);
			
			if (is_file(ROOT_DIR . '/data/coupon.csv')) {
				if ($fd = fopen(ROOT_DIR . '/data/coupon.csv', 'rb')) {
					$header = null;
					while ($row = fgetcsv($fd)) {
						if ($header === null) {
							$header = $row;
						} else {
							$row[2] = empty($row[2]) ? NULL : $row[2];
							$row[3] = empty($row[3]) ? NULL : $row[3];
							
							$sql = sprintf("INSERT INTO coupon (id,  code, prize_id, status_id) VALUES(?, ?, ?, ?)");
							$db->query($sql, $row);
						}
					}
					fclose($fd);
				}
			}
			
			$sql  = sprintf("SELECT COUNT(*) AS count FROM coupon");
			$temp = $db->query($sql)->fetch();
			
			return $temp['count'];
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * importGame
		 *
		 * This function creates the 'game' table and imports the contents of
		 * the game.csv file.
		 *
		 * @access protected
		 * @param Application $application
		 * @return integer
		 */
		
		protected function importGame(Application $application) {
			$db = Registry::getDB();
			
			$sql = sprintf("DROP TABLE IF EXISTS game");
			$db->query($sql);
			
			$sql = sprintf("CREATE TABLE IF NOT EXISTS game (
				id INTEGER PRIMARY KEY,
				coupon_id INTEGER,
				config TEXT,
				FOREIGN KEY(coupon_id) REFERENCES coupon(id)
			)");
			$db->query($sql);
			
			$sql = sprintf("DELETE FROM game");
			$db->query($sql);
			
			if (is_file(ROOT_DIR . '/data/game.csv')) {
				if ($fd = fopen(ROOT_DIR . '/data/game.csv', 'rb')) {
					$header = null;
					while ($row = fgetcsv($fd)) {
						if ($header === null) {
							$header = $row;
						} else {
							$row[1] = empty($row[1]) ? NULL : $row[1];
							
							$sql = sprintf("INSERT INTO game (id, coupon_id, config) VALUES(?, ?, ?)");
							$db->query($sql, $row);
						}
					}
					fclose($fd);
				}
			}
			
			$sql  = sprintf("SELECT COUNT(*) AS count FROM game");
			$temp = $db->query($sql)->fetch();
			
			return $temp['count'];
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * importAddress
		 *
		 * This function creates the 'address' table and imports the contents of
		 * the address.csv file.
		 *
		 * @access protected
		 * @param Application $application
		 * @return integer
		 */
		
		protected function importAddress(Application $application) {
			$db = Registry::getDB();
			
			$sql = sprintf("DROP TABLE IF EXISTS address");
			$db->query($sql);
			
			$sql = sprintf("CREATE TABLE IF NOT EXISTS address (
				id INTEGER PRIMARY KEY,
				coupon_id INTEGER,
				first TEXT,
				last TEXT,
				address1 TEXT,
				address2 TEXT,
				address3 TEXT,
				town TEXT,
				county TEXT,
				postcode TEXT,
				email TEXT,
				FOREIGN KEY(coupon_id) REFERENCES coupon(id)
			)");
			$db->query($sql);
			
			$sql = sprintf("DELETE FROM address");
			$db->query($sql);
			
			if (is_file(ROOT_DIR . '/data/address.csv')) {
				if ($fd = fopen(ROOT_DIR . '/data/address.csv', 'rb')) {
					$header = null;
					while ($row = fgetcsv($fd)) {
						if ($header === null) {
							$header = $row;
						} else {
							$row[1] = empty($row[1]) ? NULL : $row[1];
							
							$sql = sprintf("INSERT INTO address (id, coupon_id, first, last, address1, address2, address3, town, county, postcode, email) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
							$db->query($sql, $row);
						}
					}
					fclose($fd);
				}
			}
			
			$sql  = sprintf("SELECT COUNT(*) AS count FROM address");
			$temp = $db->query($sql)->fetch();
			
			return $temp['count'];
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>
