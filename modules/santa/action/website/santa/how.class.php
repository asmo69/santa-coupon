<?php
	
	/**
	 * Action_Website_Santa_How
	 *
	 * This action class represents the 'how to play' action of the website and
	 * is used to display instructions on how the game work.
	 *
	 * i.e.
	 * index.php?action=how
	 *
	 * @since 2013-10-17
	 * @see Action
	 */
	
	class Action_Website_Santa_How extends Action {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * This function is automatically called by the Controller class and
		 * represents the main body of the action.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$db      = Registry::getDB();
			$parser  = Registry::getParser();
			$form    = Registry::getForm();
			$session = Registry::getSession();
			$errors  = array();
			
			// Initialise the objects.
			$coupon  = null;
			$prize   = null;
			$game    = null;
			$address = null;
			
			// Get the coupon from the session.
			$coupon = $session->getVar('coupon');
			
			// If the coupon was NULL then...
			if (empty($coupon)) {
				// Redirect the user to the login page.
				$url = $application->getURL();
				$url->setQuery('action', 'login');
				$application->setRedirect($url);
				return $application->done();
			}
			// Otherwise, if the coupon was NOT NULL...
			else {
				// Get the prize for this coupon (if one exists).
				if ($coupon->getPrizeId()) {
					$prize = Query_Prize::create($db)->filterById($coupon->getPrizeId())->findOne();
				}
				
				// Get the game for this coupon.
				$game = Query_Game::create($db)->filterByCouponId($coupon->getId())->findOne();
				
				// Get the address for this coupon.
				$address = query_Address::create($db)->filterByCouponId($coupon->getId())->findOne();
			}
			
			// Set the variables into the template parser.
			$parser->setVar('errors', $errors);
			$parser->setVar('coupon', $coupon);
			$parser->setVar('game', $game);
			$parser->setVar('prize', $prize);
			$parser->setVar('address', $address);
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>