<?php
	
	/**
	 * Action_Website_Santa_Lose
	 *
	 * This action class represents the 'sorry you lost' action of the website and
	 * is used to display a friendly message to the player informing them that they
	 * have lost and encouraging them to try again with another card.
	 *
	 * i.e.
	 * index.php?action=lose
	 *
	 * @since 2013-10-17
	 * @see Action
	 */
	
	class Action_Website_Santa_Lose extends Action {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * This function is automatically called by the Controller class and
		 * represents the main body of the action.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$db      = Registry::getDB();
			$parser  = Registry::getParser();
			$form    = Registry::getForm();
			$session = Registry::getSession();
			$errors  = array();
			
			// Initialise the objects.
			$coupon  = null;
			$prize   = null;
			$game    = null;
			$address = null;
			
			// Get the coupon from the session.
			$coupon = $session->getVar('coupon');
			
			// If the coupon was NULL then...
			if (empty($coupon)) {
				// Redirect the user to the login page.
				$url = $application->getURL();
				$url->setQuery('action', 'login');
				$application->setRedirect($url);
				return $application->done();
			}
			// Otherwise, if the coupon was NOT NULL...
			else {
				// Get the prize for this coupon (if one exists).
				if ($coupon->getPrizeId()) {
					$prize = Query_Prize::create($db)->filterById($coupon->getPrizeId())->findOne();
				}
				
				// Get the game for this coupon.
				$game = Query_Game::create($db)->filterByCouponId($coupon->getId())->findOne();
				
				// If the game is NULL then create and save a new game for this coupon.
				if (empty($game)) {
					$game = new Value_Game();
					$game->setCouponId($coupon->getId());
					$game->save($db);
				}
				
				// Get the address for this coupon.
				$address = Query_Address::create($db)->filterByCouponId($coupon->getId())->findOne();
				
				// If the address is NULL then create a new address for this coupon.
				if (empty($address)) {
					$address = new Value_Address();
					$address->setCouponId($coupon->getId());
				}
			}
			
			// If the coupon was NULL...
			if (empty($coupon)) {
				// Set an error to say the coupon could not be found.
				$errors['general'] = 'The coupon does not exist.';
			}
			// Otherwise, if the game was NULL...
			elseif (empty($game)) {
				// Set an error to say the game could not be found.
				$errors['general'] = 'The game does not exist.';
			}
			// Otherwise, if the address was NULL...
				elseif (empty($address)) {
				// Set an error to say the address could not be found.
				$errors['general'] = 'The address does not exist.';
			}
			// Otherwise, if the coupon has already been won...
				elseif ($coupon->getStatusId() == Value_Status::STATUS_WON) {
				// Set an error to say the coupon has already been won.
				$errors['general'] = 'The coupon has already been won.';
			}
			// Otherwise, if the coupon has a prize assigned to it...
				elseif ($coupon->getPrizeId()) {
				// Set an error to say this is a losing coupon.
				$errors['general'] = 'The coupon is not a loser.';
			}
			// Otherwise, everything is fine...
			else {
				// If the coupon is unclaimed then...
				if ($coupon->getStatusId() == Value_Status::STATUS_UNCLAIMED) {
					// Set the coupon as lost.
					$coupon->setStatusId(Value_Status::STATUS_LOST);
					// Save the coupon.
					$coupon->save($db);
				}
			}
			
			// Set the variables into the template parser.
			$parser->setVar('errors', $errors);
			$parser->setVar('coupon', $coupon);
			$parser->setVar('game', $game);
			$parser->setVar('prize', $prize);
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>