<?php
	
	/**
	 * Action_Website_Santa_Default
	 *
	 * This action class represents the default action of the website and is as
	 * such only called if no other action could be found.
	 *
	 * @since 2013-10-17
	 * @see Action
	 */
	
	class Action_Website_Santa_Default extends Action {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * This function is automatically called by the Controller class and
		 * represents the main body of the action.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$session = Registry::getSession();
			
			// Get the coupon from the session.
			$coupon = $session->getVar('coupon');
			
			// If no database exists...
			if (is_file(ROOT_DIR . '/data/database.db') == false) {
				// Redirect the user to the login page.
				$url = $application->getURL();
				$url->setQuery('action', 'install');
				$application->setRedirect($url);
				return $application->done();
			}
			// If no coupon exists...
			elseif (empty($coupon)) {
				// Redirect the user to the login page.
				$url = $application->getURL();
				$url->setQuery('action', 'login');
				$application->setRedirect($url);
				return $application->done();
			} else {
				// Redirect the user to the 'how to play' pgae.
				$url = $application->getURL();
				$url->setQuery('action', 'how');
				$application->setRedirect($url);
				return $application->done();
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>