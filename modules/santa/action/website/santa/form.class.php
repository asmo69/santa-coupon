<?php
	
	/**
	 * Action_Website_Santa_Game
	 *
	 * This action class represents the delivery form action of the website and is
	 * used to gather a players name and delivery details (assuming the game was a
	 * winner).
	 *
	 * i.e.
	 * index.php?action=form
	 *
	 * @since 2013-10-17
	 * @see Action
	 */
	
	class Action_Website_Santa_Form extends Action {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * This function is automatically called by the Controller class and
		 * represents the main body of the action.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$db      = Registry::getDB();
			$parser  = Registry::getParser();
			$form    = Registry::getForm();
			$session = Registry::getSession();
			$config  = Registry::getConfig();
			$errors  = array();
			
			// Initialise the objects.
			$coupon  = null;
			$prize   = null;
			$game    = null;
			$address = null;
			
			// Get the coupon from the session.
			$coupon = $session->getVar('coupon');
			
			// If the coupon was NULL then...
			if (empty($coupon)) {
				// Redirect the user to the login page.
				$url = $application->getURL();
				$url->setQuery('action', 'login');
				$application->setRedirect($url);
				return $application->done();
			}
			// Otherwise, if the coupon was NOT NULL...
			else {
				// Get the prize for this coupon (if one exists).
				if ($coupon->getPrizeId()) {
					$prize = Query_Prize::create($db)->filterById($coupon->getPrizeId())->findOne();
				}
				
				// Get the game for this coupon.
				$game = Query_Game::create($db)->filterByCouponId($coupon->getId())->findOne();
				
				// If the game is NULL then create and save a new game for this coupon.
				if (empty($game)) {
					$game = new Value_Game();
					$game->setCouponId($coupon->getId());
					$game->save($db);
				}
				
				// Get the address for this coupon.
				$address = Query_Address::create($db)->filterByCouponId($coupon->getId())->findOne();
				
				// If the address is NULL then create a new address for this coupon.
				if (empty($address)) {
					$address = new Value_Address();
					$address->setCouponId($coupon->getId());
				}
			}
			
			// If the coupon was NULL...
			if (empty($coupon)) {
				// Set an error to say the coupon could not be found.
				$errors['general'] = 'The coupon does not exist.';
			}
			// Otherwise, if the game was NULL...
			elseif (empty($game)) {
				// Set an error to say the game could not be found.
				$errors['general'] = 'The game does not exist.';
			}
			// Otherwise, if the prize was NULL...
				elseif (empty($prize)) {
				// Set an error to say the prize could not be found.
				$errors['general'] = 'The prize does not exist.';
			}
			// Otherwise, if the address was NULL...
				elseif (empty($address)) {
				// Set an error to say the address could not be found.
				$errors['general'] = 'The address does not exist.';
			}
			// Otherwise, if the coupon has already been won...
				elseif ($coupon->getStatusId() == Value_Status::STATUS_WON) {
				// Set an error to say the coupon has already been won.
				$errors['general'] = 'The coupon has already been won.';
			}
			// Otherwise, if the coupon has already been lost...
				elseif ($coupon->getStatusId() == Value_Status::STATUS_LOST) {
				// Set an error to say the coupon has already been won.
				$errors['general'] = 'The coupon has already been lost.';
			}
			// Otherwise, if the coupon has no prize assigned to it...
				elseif (!$coupon->getPrizeId()) {
				// Set an error to say this is not a winning coupon.
				$errors['general'] = 'The coupon is not a winner.';
			}
			// Otherwise, everything is fine...
			else {
				// Initialise the form variables.
				$first    = $form->getVar('first');
				$last     = $form->getVar('last');
				$address1 = $form->getVar('address1');
				$address2 = $form->getVar('address2');
				$address3 = $form->getVar('address3');
				$town     = $form->getVar('town');
				$county   = $form->getVar('county');
				$postcode = $form->getVar('postcode');
				$email    = $form->getVar('email');
				$complete = $form->getVar('complete');
				
				// If the form was submitted...
				if ($complete) {
					// Set the validation to be TRUE.
					$valid = true;
					
					// If no first name was entered...
					if (empty($first)) {
						// Set an error for the first name.
						$errors['first'] = 'You must enter your first name';
						// Set the validation to FALSE
						$valid           = false;
					}
					// If no last name was entered...
					if (empty($last)) {
						// Set an error for the last name.
						$errors['last'] = 'You must enter your last name';
						// Set the validation to FALSE
						$valid          = false;
					}
					// If no email address was entered...
					if (empty($email)) {
						// Set an error for the email address.
						$errors['email'] = 'You must enter your email';
						// Set the validation to FALSE
						$valid           = false;
					}
					// If no address was entered...
					if (empty($address1)) {
						// Set an error for the address.
						$errors['address1'] = 'You must enter your address';
						// Set the validation to FALSE
						$valid              = false;
					}
					// If no town was entered...
					if (empty($town)) {
						// Set an error for the town.
						$errors['town'] = 'You must enter your town';
						// Set the validation to FALSE
						$valid          = false;
					}
					// If no county was entered...
					if (empty($county)) {
						// Set an error for the county.
						$errors['county'] = 'You must enter your county';
						// Set the validation to FALSE
						$valid            = false;
					}
					// If no postcode was entered...
					if (empty($postcode)) {
						// Set an error for the postcode.
						$errors['postcode'] = 'You must enter your postcode';
						// Set the validation to FALSE
						$valid              = false;
					}
					
					// If the validation is TRUE...
					if ($valid) {
						// Copy the form variables into the address.
						$address->setCouponId($coupon->getId());
						$address->setFirst($first);
						$address->setLast($last);
						$address->setAddress1($address1);
						$address->setAddress2($address2);
						$address->setAddress3($address3);
						$address->setTown($town);
						$address->setCounty($county);
						$address->setPostcode($postcode);
						$address->setEmail($email);
						// Save the address.
						$address->save($db);
						
						// Set the variables into the template parser.
						$parser->setVar('coupon', $coupon);
						$parser->setVar('game', $game);
						$parser->setVar('prize', $prize);
						$parser->setVar('address', $address);
						$parser->setVar('config', $config);
						
						// Send a confirmation email to the user.
						$mail = new Net_Email();
						$mail->addRecipient($email, trim($first . ' ' . $last));
						$mail->setFrom($config->getVar('email_from_address'), $config->getVar('email_from_name'));
						$mail->setSubject($config->getVar('email_subject'));
						$mail->setBody($parser->fetch('email/text.tpl'));
						$mail->setHTML($parser->fetch('email/html.tpl'));
						$mail->send();
						
						// If the coupon is unclaimed then...
						if ($coupon->getStatusId() == Value_Status::STATUS_UNCLAIMED) {
							// Set the coupon as won.
							$coupon->setStatusId(Value_Status::STATUS_WON);
							// Save the coupon.
							$coupon->save($db);
						}
						
						// Redirect the user to the win page.
						$url = $application->getURL();
						$url->setQuery('action', 'win');
						$url->unsetQuery('first');
						$url->unsetQuery('last');
						$url->unsetQuery('address1');
						$url->unsetQuery('address2');
						$url->unsetQuery('address3');
						$url->unsetQuery('town');
						$url->unsetQuery('county');
						$url->unsetQuery('postcode');
						$url->unsetQuery('email');
						$url->unsetQuery('complete');
						$application->setRedirect($url);
						return $application->done();
					}
				}
			}
			
			// Set the variables into the template parser.
			$parser->setVar('errors', $errors);
			$parser->setVar('coupon', $coupon);
			$parser->setVar('game', $game);
			$parser->setVar('prize', $prize);
			$parser->setVar('address', $address);
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>