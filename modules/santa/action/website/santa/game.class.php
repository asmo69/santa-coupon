<?php
	
	/**
	 * Action_Website_Santa_Game
	 *
	 * This action class represents the game action of the website and is used
	 * to provide the logic for the actual scratch card. Just like a regular
	 * scratch card bought from a newsagents, the outcome of the card is
	 * predetermined long before the user ever logs in. The game is basically
	 * a fancy and fake way of making the user believe they are in control.
	 *
	 * i.e.
	 * index.php?action=game
	 *
	 * @since 2013-10-17
	 * @see Action
	 */
	
	class Action_Website_Santa_Game extends Action {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * This function is automatically called by the Controller class and
		 * represents the main body of the action.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$db      = Registry::getDB();
			$parser  = Registry::getParser();
			$form    = Registry::getForm();
			$session = Registry::getSession();
			$config  = Registry::getConfig();
			$errors  = array();
			
			// Initialise the objects.
			$coupon  = null;
			$prize   = null;
			$game    = null;
			$address = null;
			$data    = null;
			
			// Get the coupon from the session.
			$coupon = $session->getVar('coupon');
			
			// If the coupon was NULL then...
			if (empty($coupon)) {
				// Redirect the user to the login page.
				$url = $application->getURL();
				$url->setQuery('action', 'login');
				$application->setRedirect($url);
				return $application->done();
			}
			// Otherwise, if the coupon was NOT NULL...
			else {
				// Get the prize for this coupon (if one exists).
				if ($coupon->getPrizeId()) {
					$prize = Query_Prize::create($db)->filterById($coupon->getPrizeId())->findOne();
				}
				
				// Get the game for this coupon.
				$game = Query_Game::create($db)->filterByCouponId($coupon->getId())->findOne();
				
				// If the game is NULL then create and save a new game for this coupon.
				if (empty($game)) {
					$game = new Value_Game();
					$game->setCouponId($coupon->getId());
					$game->save($db);
				}
				
				// Get the address for this coupon.
				$address = Query_Address::create($db)->filterByCouponId($coupon->getId())->findOne();
				
				// If the address is NULL then create a new address for this coupon.
				if (empty($address)) {
					$address = new Value_Address();
					$address->setCouponId($coupon->getId());
				}
			}
			
			// If the coupon was NULL...
			if (empty($coupon)) {
				// Set an error to say the coupon could not be found.
				$errors['general'] = 'The coupon does not exist.';
			}
			// Otherwise, if the game was NULL...
			elseif (empty($game)) {
				// Set an error to say the game could not be found.
				$errors['general'] = 'The game does not exist.';
			}
			// Otherwise, if the address was NULL...
				elseif (empty($address)) {
				// Set an error to say the address could not be found.
				$errors['general'] = 'The address does not exist.';
			}
			// Otherwise, if the coupon has already been won...
				elseif ($coupon->getStatusId() == Value_Status::STATUS_WON) {
				// Set an error to say the coupon has already been won.
				$errors['general'] = 'The coupon has already been won.';
			}
			// Otherwise, if the coupon has already been lost...
				elseif ($coupon->getStatusId() == Value_Status::STATUS_LOST) {
				// Set an error to say the coupon has already been won.
				$errors['general'] = 'The coupon has already been lost.';
			}
			// Otherwise, everything is fine...
			else {
				// Initialise the game (setting the config data for the tiles
				// etc if nothing exists).
				$game->init($db, explode(',', $config->getVar('game_tiles')), $config->getVar('game_size'), $config->getVar('game_match'));
				// Open individual tiles of the game (this is used to support
				// browsers that do not have javascript enabled)
				$game->open($db, $form->getVar('open'));
				// Get the config data the game.
				$data = json_decode($game->getConfig(), true);
			}
			
			// Set the variables into the template parser.
			$parser->setVar('errors', $errors);
			$parser->setVar('coupon', $coupon);
			$parser->setVar('game', $game);
			$parser->setVar('prize', $prize);
			$parser->setVar('address', $address);
			$parser->setVar('data', $data);
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>