<?php
	
	/**
	 * Action_Website_Santa_Login
	 *
	 * This action class represents the login action of the website and is used
	 * to valid the coupon code and store it in the session before redirecting
	 * the user to the games 'how to play' page.
	 *
	 * i.e.
	 * index.php?action=login
	 *
	 * @since 2013-10-17
	 * @see Action
	 */
	
	class Action_Website_Santa_Login extends Action {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * This function is automatically called by the Controller class and
		 * represents the main body of the action.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$db      = Registry::getDB();
			$parser  = Registry::getParser();
			$form    = Registry::getForm();
			$session = Registry::getSession();
			$errors  = array();
			
			// Initialise form variables.
			$code     = $form->getVar('code');
			$complete = $form->getVar('complete');
			
			// Find an unused coupon code.
			if (empty($code)) {
				$temp = Query_Coupon::create($db)->filterByStatusID(Value_Status::STATUS_UNCLAIMED)->orderByRand()->findOne();
				if ($temp) {
					$code = $temp->getCode();
					$form->setVar('code', $temp->getCode());
				}
			}
			
			// If the form was submitted...
			if ($complete) {
				// Set the validation to be TRUE.
				$valid = true;
				
				// If no coupon code was entered then...
				if (empty($code)) {
					// Set an error for the coupon code.
					$errors['code'] = 'You must enter a code';
					// Set the validation to FALSE
					$valid          = false;
				}
				
				// If the validation is TRUE...
				if ($valid) {
					// Get the specified coupon from the database.
					$coupon = Query_Coupon::create($db)->filterByCode($code)->findOne();
					
					// If the coupon was NULL...
					if (empty($coupon)) {
						// Set an error to say the coupon could not be found.
						$errors['general'] = 'The coupon does not exist.';
					}
					// If the coupon has already been won/lost...
					elseif ($coupon->getStatusId() != Value_Status::STATUS_UNCLAIMED) {
						// Set an error to say the coupon has already been claimed.
						$errors['general'] = 'The coupon has already been claimed';
					} elseif ($coupon) {
						// Set the session coupon.
						$session->setVar('coupon', $coupon);
						
						// Redirect the user to the 'how to play' page.
						$url = $application->getURL();
						$url->setQuery('action', 'how');
						$url->unsetQuery('code');
						$url->unsetQuery('complete');
						
						$application->setRedirect($url);
						return $application->done();
					}
				}
			}
			
			// Set the variables into the template parser.
			$parser->setVar('errors', $errors);
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>