<?php
	
	/**
	 * Action_Website_Santa_Logout
	 *
	 * This action class represents the logout action of the website and is used
	 * to clear the users session and return them to the login page.
	 *
	 * i.e.
	 * index.php?action=logout
	 *
	 * @since 2013-10-17
	 * @see Action
	 */
	
	class Action_Website_Santa_Logout extends Action {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * This function is automatically called by the Controller class and
		 * represents the main body of the action.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$db      = Registry::getDB();
			$parser  = Registry::getParser();
			$form    = Registry::getForm();
			$session = Registry::getSession();
			$errors  = array();
			
			// Set the session coupon to be NULL.
			$session->setVar('coupon', null);
			
			// Redirect the user to the login page.
			$url = $application->getURL();
			$url->setQuery('action', 'login');
			$application->setRedirect($url);
			return $application->done();
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>