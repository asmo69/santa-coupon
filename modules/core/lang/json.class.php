<?php
	
	class Lang_JSON extends Lang {
		
		/* ------------------------------------------------------------------ */
		
		protected function flattern($data, $prefix = null, $flat = array()) {
			foreach ($data as $name => $value) {
				if (is_object($value)) {
					$flat = $this->flattern($value, $prefix ? $prefix . '_' . $name : $name, $flat);
				} else {
					$flat[$prefix ? $prefix . '_' . $name : $name] = json_decode(json_encode($value), true);
				}
			}
			return $flat;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function load($filename) {
			$temp = array();
			if (is_file($filename)) {
				try {
					$data = $this->flattern(json_decode(file_get_contents($filename)));
					foreach ($data as $name => $value) {
						$this->setVar($name, $value);
					}
				}
				catch (Exception $ex) {
					if ($log = Registry::getLog()) {
						$log->write($ex->getMessage(), Log::ERROR);
					}
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>