<?php
	
	class Parser_String extends Parser {
		
		/* ------------------------------------------------------------------ */
		
		protected $data = array();
		
		/* ------------------------------------------------------------------ */
		
		public function __construct() {
			if (is_dir(ROOT_DIR . '/temp/') == false) {
				@mkdir(ROOT_DIR . '/temp/', 0777);
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setVar($name, $value) {
			$this->data[$name] = $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getVar($name) {
			return array_key_exists($name, $this->data) ? $this->data[$name] : null;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function parse($filename) {
			$ret = '';
			
			if (strcasecmp(substr($filename, 0, 9), 'string://') == 0) {
				$ret = substr($filename, 9);
			} else {
				if (strcasecmp(substr($filename, 0, 7), 'file://') == 0) {
					$filename = substr($filename, 7);
				}
				
				if (is_file(ROOT_DIR . '/' . $this->getTemplates() . '/' . $filename)) {
					$ret = file_get_contents(ROOT_DIR . '/' . $this->getTemplates() . '/' . $filename);
				}
			}
			
			foreach ($this->data as $name => $value) {
				$ret = preg_replace('/\{\$' . $name . '\}/i', $value, $ret);
			}
			
			return $ret;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>