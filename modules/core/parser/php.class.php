<?php
	
	/**
	 * Parser
	 *
	 * The Parser class is the abstract base class from which all
	 * Parser's should inherit. Parser classes are used manipulate and
	 * data from PHP into HTML, XML or other user formats.
	 *
	 * @since 2013-01-02
	 * @abstract
	 */
	
	class Parser_PHP extends Parser {
		
		/* ------------------------------------------------------------------ */
		
		protected $data = array();
		
		/* ------------------------------------------------------------------ */
		
		public function __construct() {
			if (is_dir(ROOT_DIR . '/temp/') == false) {
				@mkdir(ROOT_DIR . '/temp/', 0777);
			}
			if (is_dir(ROOT_DIR . '/temp/php/templates/') == false) {
				@mkdir(ROOT_DIR . '/temp/php/templates/', 0777);
			}
			if (is_readable(ROOT_DIR . '/temp/php/templates/') == false) {
				throw new Exception_File_NotReadable('File is not readable - ' . ROOT_DIR . '/temp/php/templates/');
			}
			if (is_writable(ROOT_DIR . '/temp/php/templates/') == false) {
				throw new Exception_File_NotWritable('File is not writable - ' . ROOT_DIR . '/temp/php/templates/');
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setVar($name, $value) {
			$this->data[$name] = $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getVar($name) {
			return array_key_exists($name, $this->data) ? $this->data[$name] : null;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function parse($filename) {
			extract($this->data);
			
			$ret = '';
			
			ob_start();
			if (strcasecmp(substr($filename, 0, 9), 'string://') == 0) {
				if (is_file(ROOT_DIR . '/temp/php/templates/' . md5($filename) . '.tpl') == false) {
					@file_put_contents(ROOT_DIR . '/temp/php/templates/' . md5($filename) . '.tpl', substr($filename, 9));
				}
				
				if (is_file(ROOT_DIR . '/temp/php/templates/' . md5($filename) . '.tpl')) {
					include(ROOT_DIR . '/temp/php/templates/' . md5($filename) . '.tpl');
				}
			} else {
				if (strcasecmp(substr($filename, 0, 7), 'file://') == 0) {
					$filename = substr($filename, 7);
				}
				
				if (is_file(ROOT_DIR . '/' . $this->getTemplates() . '/' . $filename)) {
					include(ROOT_DIR . '/' . $this->getTemplates() . '/' . $filename);
				}
			}
			$ret = ob_get_contents();
			ob_end_clean();
			
			return $ret;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>