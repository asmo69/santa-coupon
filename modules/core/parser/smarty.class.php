<?php
	
	class Parser_Smarty extends Parser {
		
		/* ------------------------------------------------------------------ */
		
		public function __construct() {
			if (is_file(ROOT_DIR . '/external/smarty/smarty.class.php') == false) {
				throw new Exception_Library_NotExists('External Smarty Does Not Exist');
			}
			require_once ROOT_DIR . '/external/smarty/smarty.class.php';
			$this->engine               = new Smarty();
			$this->engine->compile_dir  = null;
			$this->engine->cache_dir    = null;
			$this->engine->template_dir = null;
			
			if (is_dir(ROOT_DIR . '/temp/') == false) {
				@mkdir(ROOT_DIR . '/temp/', 0777);
			}
			if (is_dir(ROOT_DIR . '/temp/smarty/') == false) {
				@mkdir(ROOT_DIR . '/temp/smarty/', 0777);
			}
			if (is_dir(ROOT_DIR . '/temp/smarty/cached/') == false) {
				@mkdir(ROOT_DIR . '/temp/smarty/cached/', 0777);
			}
			if (is_dir(ROOT_DIR . '/temp/smarty/compiled/') == false) {
				@mkdir(ROOT_DIR . '/temp/smarty/compiled/', 0777);
			}
			if (is_dir(ROOT_DIR . '/temp/smarty/templates/') == false) {
				@mkdir(ROOT_DIR . '/temp/smarty/templates/', 0777);
			}
			if (is_readable(ROOT_DIR . '/temp/smarty/cached/') == false) {
				throw new Exception_File_NotReadable('File is not readable - ' . ROOT_DIR . '/temp/smarty/cached/');
			}
			if (is_writable(ROOT_DIR . '/temp/smarty/cached/') == false) {
				throw new Exception_File_NotWritable('File is not writable - ' . ROOT_DIR . '/temp/smarty/cached/');
			}
			if (is_readable(ROOT_DIR . '/temp/smarty/compiled/') == false) {
				throw new Exception_File_NotReadable('File is not readable - ' . ROOT_DIR . '/temp/smarty/compiled/');
			}
			if (is_writable(ROOT_DIR . '/temp/smarty/compiled/') == false) {
				throw new Exception_File_NotWritable('File is not writable - ' . ROOT_DIR . '/temp/smarty/compiled/');
			}
			if (is_readable(ROOT_DIR . '/temp/smarty/templates/') == false) {
				throw new Exception_File_NotReadable('File is not readable - ' . ROOT_DIR . '/temp/smarty/templates/');
			}
			if (is_writable(ROOT_DIR . '/temp/smarty/templates/') == false) {
				throw new Exception_File_NotWritable('File is not writable - ' . ROOT_DIR . '/temp/smarty/templates/');
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setVar($name, $value) {
			$this->engine->assign($name, $value);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getVar($name) {
			return $this->engine->get_template_vars($name);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function parse($filename) {
			$ret = '';
			
			if (strcasecmp(substr($filename, 0, 9), 'string://') == 0) {
				if (is_file(ROOT_DIR . '/temp/smarty/templates/' . md5($filename) . '.tpl') == false) {
					@file_put_contents(ROOT_DIR . '/temp/smarty/templates/' . md5($filename) . '.tpl', substr($filename, 9));
				}
				
				$this->engine->compile_dir  = ROOT_DIR . '/temp/smarty/compiled/';
				$this->engine->cache_dir    = ROOT_DIR . '/temp/smarty/cached/';
				$this->engine->template_dir = ROOT_DIR . '/temp/smarty/templates/';
				
				if (is_file(ROOT_DIR . '/temp/smarty/templates/' . md5($filename) . '.tpl')) {
					$ret = $this->engine->fetch(md5($filename) . '.tpl');
				}
			} else {
				if (strcasecmp(substr($filename, 0, 7), 'file://') == 0) {
					$filename = substr($filename, 7);
				}
				
				$this->engine->compile_dir  = ROOT_DIR . '/temp/smarty/compiled/';
				$this->engine->cache_dir    = ROOT_DIR . '/temp/smarty/cached/';
				$this->engine->template_dir = ROOT_DIR . '/' . $this->getTemplates() . '/';
				
				if (is_file(ROOT_DIR . '/' . $this->getTemplates() . '/' . $filename)) {
					$ret = $this->engine->fetch($filename);
				}
			}
			
			return $ret;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>