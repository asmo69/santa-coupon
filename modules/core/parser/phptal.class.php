<?php
	
	class Parser_PHPTal extends Parser {
		
		/* ------------------------------------------------------------------ */
		
		protected $data = array();
		
		/* ------------------------------------------------------------------ */
		
		public function __construct() {
			if (is_file(ROOT_DIR . '/external/phptal/PHPTAL.php') == false) {
				throw new Exception_Library_NotExists('External PHPTal Does Not Exist');
			}
			require_once ROOT_DIR . '/external/phptal/PHPTAL.php';
			$this->engine = new PHPTAL();
			if (is_dir(ROOT_DIR . '/temp/') == false) {
				@mkdir(ROOT_DIR . '/temp/', 0777);
			}
			if (is_dir(ROOT_DIR . '/temp/phptal/') == false) {
				@mkdir(ROOT_DIR . '/temp/phptal/', 0777);
			}
			if (is_dir(ROOT_DIR . '/temp/phptal/compiled/') == false) {
				@mkdir(ROOT_DIR . '/temp/phptal/compiled/', 0777);
			}
			if (is_dir(ROOT_DIR . '/temp/phptal/templates/') == false) {
				@mkdir(ROOT_DIR . '/temp/phptal/templates/', 0777);
			}
			if (is_readable(ROOT_DIR . '/temp/phptal/compiled/') == false) {
				throw new Exception_File_NotReadable('File is not readable - ' . ROOT_DIR . '/temp/phptal/compiled/');
			}
			if (is_writable(ROOT_DIR . '/temp/phptal/compiled/') == false) {
				throw new Exception_File_NotWritable('File is not writable - ' . ROOT_DIR . '/temp/phptal/compiled/');
			}
			if (is_readable(ROOT_DIR . '/temp/phptal/templates/') == false) {
				throw new Exception_File_NotReadable('File is not readable - ' . ROOT_DIR . '/temp/phptal/templates/');
			}
			if (is_writable(ROOT_DIR . '/temp/phptal/templates/') == false) {
				throw new Exception_File_NotWritable('File is not writable - ' . ROOT_DIR . '/temp/phptal/templates/');
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setVar($name, $value) {
			$this->data[$name] = $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getVar($name) {
			return array_key_exists($name, $this->data) ? $this->data[$name] : null;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function parse($filename) {
			$ret = '';
			
			if (strcasecmp(substr($filename, 0, 9), 'string://') == 0) {
				if (is_file(ROOT_DIR . '/temp/smarty/templates/' . md5($filename) . '.tpl') == false) {
					@file_put_contents(ROOT_DIR . '/temp/smarty/templates/' . md5($filename) . '.tpl', substr($filename, 9));
				}
				
				$this->engine->setPhpCodeDestination(ROOT_DIR . '/temp/phptal/compiled/');
				$this->engine->setTemplateRepository(ROOT_DIR . '/temp/phptal/templates/');
				
				foreach ($this->data as $name => $value) {
					$this->engine->set($name, $value);
				}
				
				if (is_file(ROOT_DIR . '/temp/smarty/templates/' . md5($filename) . '.tpl')) {
					$this->engine->setTemplate(md5($filename) . '.tpl');
					$ret = $this->engine->execute();
				}
			} else {
				if (strcasecmp(substr($filename, 0, 7), 'file://') == 0) {
					$filename = substr($filename, 7);
				}
				
				$this->engine->setPhpCodeDestination(ROOT_DIR . '/temp/phptal/compiled/');
				$this->engine->setTemplateRepository(ROOT_DIR . '/' . $this->getTemplates() . '/');
				
				foreach ($this->data as $name => $value) {
					$this->engine->set($name, $value);
				}
				
				if (is_file(ROOT_DIR . '/' . $this->getTemplates() . '/' . $filename)) {
					$this->engine->setTemplate($filename);
					$ret = $this->engine->execute();
				}
			}
			
			return $ret;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>