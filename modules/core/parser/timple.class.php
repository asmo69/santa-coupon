<?php
	
	class Parser_Timple extends Parser {
		
		/* ------------------------------------------------------------------ */
		
		public function __construct() {
			if (is_file(ROOT_DIR . '/external/timple/timple.class.php') == false) {
				throw new Exception_Library_NotExists('External Timple Does Not Exist');
			}
			require_once ROOT_DIR . '/external/timple/timple.class.php';
			$this->engine = new Timple();
			if (is_dir(ROOT_DIR . '/temp/') == false) {
				@mkdir(ROOT_DIR . '/temp/', 0777);
			}
			if (is_dir(ROOT_DIR . '/temp/timple/') == false) {
				@mkdir(ROOT_DIR . '/temp/timple/', 0777);
			}
			if (is_dir(ROOT_DIR . '/temp/timple/compiled/') == false) {
				@mkdir(ROOT_DIR . '/temp/timple/compiled/', 0777);
			}
			if (is_dir(ROOT_DIR . '/temp/timple/templates/') == false) {
				@mkdir(ROOT_DIR . '/temp/timple/templates/', 0777);
			}
			if (is_readable(ROOT_DIR . '/temp/timple/compiled/') == false) {
				throw new Exception_File_NotReadable('File is not readable - ' . ROOT_DIR . '/temp/timple/compiled/');
			}
			if (is_writable(ROOT_DIR . '/temp/timple/compiled/') == false) {
				throw new Exception_File_NotWritable('File is not writable - ' . ROOT_DIR . '/temp/timple/compiled/');
			}
			if (is_readable(ROOT_DIR . '/temp/timple/templates/') == false) {
				throw new Exception_File_NotReadable('File is not readable - ' . ROOT_DIR . '/temp/timple/templates/');
			}
			if (is_writable(ROOT_DIR . '/temp/timple/templates/') == false) {
				throw new Exception_File_NotWritable('File is not writable - ' . ROOT_DIR . '/temp/timple/templates/');
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setVar($name, $value) {
			$this->engine->setVar($name, $value);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getVar($name) {
			return $this->engine->getVar($name);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function parse($filename) {
			$ret = '';
			
			if (strcasecmp(substr($filename, 0, 9), 'string://') == 0) {
				if (is_file(ROOT_DIR . '/temp/timple/templates/' . md5($filename) . '.tpl') == false) {
					@file_put_contents(ROOT_DIR . '/temp/timple/templates/' . md5($filename) . '.tpl', substr($filename, 9));
				}
				
				$this->engine->setCompiled(ROOT_DIR . '/temp/timple/compiled/');
				$this->engine->setTemplates(ROOT_DIR . '/temp/timple/templates/');
				
				if (is_file(ROOT_DIR . '/temp/timple/templates/' . md5($filename) . '.tpl')) {
					$ret = $this->engine->parse(md5($filename) . 'tpl');
				}
			} else {
				$this->engine->setCompiled(ROOT_DIR . '/temp/timple/compiled/');
				$this->engine->setTemplates(ROOT_DIR . '/' . $this->getTemplates() . '/');
				
				if (strcasecmp(substr($filename, 0, 7), 'file://') == 0) {
					$filename = substr($filename, 7);
				}
				
				if (is_file(ROOT_DIR . '/' . $this->getTemplates() . '/' . $filename)) {
					$ret = $this->engine->parse($filename);
				}
			}
			
			return $ret;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>