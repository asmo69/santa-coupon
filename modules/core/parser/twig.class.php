<?php
	
	class Parser_Twig extends Parser {
		
		/* ------------------------------------------------------------------ */
		
		protected $data = array();
		
		/* ------------------------------------------------------------------ */
		
		public function __construct() {
			if (is_file(ROOT_DIR . '/external/twig/lib/Twig/Autoloader.php') == false) {
				throw new Exception_Library_NotExists('External Twig Does Not Exist');
			}
			require_once ROOT_DIR . '/external/twig/lib/Twig/Autoloader.php';
			Twig_Autoloader::register(true);
			
			if (is_dir(ROOT_DIR . '/temp/') == false) {
				@mkdir(ROOT_DIR . '/temp/', 0777);
			}
			if (is_dir(ROOT_DIR . '/temp/twig/') == false) {
				@mkdir(ROOT_DIR . '/temp/twig/', 0777);
			}
			if (is_dir(ROOT_DIR . '/temp/twig/cache/') == false) {
				@mkdir(ROOT_DIR . '/temp/twig/cache/', 0777);
			}
			if (is_dir(ROOT_DIR . '/temp/twig/templates/') == false) {
				@mkdir(ROOT_DIR . '/temp/twig/templates/', 0777);
			}
			if (is_readable(ROOT_DIR . '/temp/twig/cache/') == false) {
				throw new Exception_File_NotReadable('File is not readable - ' . ROOT_DIR . '/temp/twig/cache/');
			}
			if (is_writable(ROOT_DIR . '/temp/twig/cache/') == false) {
				throw new Exception_File_NotWritable('File is not writable - ' . ROOT_DIR . '/temp/twig/cache/');
			}
			if (is_readable(ROOT_DIR . '/temp/twig/templates/') == false) {
				throw new Exception_File_NotReadable('File is not readable - ' . ROOT_DIR . '/temp/twig/templates/');
			}
			if (is_writable(ROOT_DIR . '/temp/twig/templates/') == false) {
				throw new Exception_File_NotWritable('File is not writable - ' . ROOT_DIR . '/temp/twig/templates/');
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setVar($name, $value) {
			$this->data[$name] = $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getVar($name) {
			return array_key_exists($name, $this->data) ? $this->data[$name] : null;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function parse($filename) {
			$ret = '';
			
			if (strcasecmp(substr($filename, 0, 9), 'string://') == 0) {
				if (is_file(ROOT_DIR . '/temp/twig/templates/' . md5($filename) . '.tpl') == false) {
					@file_put_contents(ROOT_DIR . '/temp/twig/templates/' . md5($filename) . '.tpl', substr($filename, 9));
				}
				
				$loader       = new Twig_Loader_Filesystem(ROOT_DIR . '/temp/twig/templates/');
				$this->engine = new Twig_Environment($loader, array(
					'cache' => ROOT_DIR . '/temp/twig/cache/'
				));
				
				if (is_file_(ROOT_DIR . '/temp/twig/templates/' . md5($filename) . '.tpl')) {
					$ret = $this->engine->render(md5($filename) . '.tpl', $this->data);
				}
			} else {
				if (strcasecmp(substr($filename, 0, 7), 'file://') == 0) {
					$filename = substr($filename, 7);
				}
				
				$loader       = new Twig_Loader_Filesystem(ROOT_DIR . '/' . $this->getTemplates() . '/');
				$this->engine = new Twig_Environment($loader, array(
					'cache' => ROOT_DIR . '/temp/twig/cache/'
				));
				
				if (is_file(ROOT_DIR . '/' . $this->getTemplates() . '/' . $filename)) {
					$ret = $this->engine->render($filename, $this->data);
				}
			}
			
			return $ret;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>