<?php
	
	/**
	 * Constants
	 *
	 * The Constants class contains common values. These are mostly used to define
	 * HTTP response values and mime types used by the Application class.
	 *
	 * @since 2013-01-02
	 */
	
	class Constants {
		
		/* ------------------------------------------------------------------ */
		
		const HTTP_OK = '200 OK';
		const HTTP_REDIRECT = '301 Moved Permanently';
		const HTTP_MOVED = '302 Found';
		const HTTP_NOT_MODIFIED = '304 Not Modified';
		const HTTP_NOT_AUTH = '401 Unauthorized';
		const HTTP_FORBIDDEN = '403 Forbidden';
		const HTTP_NOT_FOUND = '404 Not Found';
		const HTTP_ERROR = '500 Internal Server Error';
		
		/* ------------------------------------------------------------------ */
		
		const CHARSET_UTF8 = 'UTF-8';
		const CHARSET_ISO8859 = 'ISO-8859-1';
		
		/* ------------------------------------------------------------------ */
		
		const MIME_CSV = 'application/csv';
		const MIME_JSON = 'application/json';
		const MIME_HTML = 'text/html';
		const MIME_TEXT = 'text/plain';
		const MIME_XML = 'text/xml';
		const MIME_RSS = 'application/rss+xml';
		const MIME_JPEG = 'image/jpeg';
		const MIME_GIF = 'image/gif';
		const MIME_PNG = 'image/png';
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>