<?php
	
	/**
	 * Session
	 *
	 * The Form class provides a warpper around the default SESSION
	 * functionality of PHP, allowing them to be treated in a more
	 * consistent manner with objects like Cookie, Form, Cache, etc.
	 *
	 * @since 2013-01-02
	 */
	
	class Session {
		
		/* ------------------------------------------------------------------ */
		
		protected $open = false;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * create
		 *
		 * This static function is used as a factory method for creating
		 * different types of Session subclasses. The function will check
		 * that the specified class exists, is named correctly and is a
		 * valid subclass of Session before creating an instance. If a
		 * suitable class cannot be found then a default Session class
		 * will be created.
		 *
		 * e.g.
		 *	$session = Session::create();
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Session
		 */
		
		public static function create($type = '') {
			if ($type) {
				$class = 'Session_' . preg_replace('/[^a-z0-9_]/i', '', str_replace('/', '_', $type));
				if (class_exists($class) && is_subclass_of($class, 'Session')) {
					return new $class();
				}
			}
			return new Session();
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * open
		 *
		 * This function opens the users session and loads all their
		 * saved data.
		 *
		 * @access public
		 */
		
		public function open() {
			try {
				if ($this->open == false && headers_sent() == false) {
					@session_save_path(ROOT_DIR . '/temp/session/');
					@session_start();
					$this->open = true;
				}
			}
			catch (Exception $ex) {
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * close
		 *
		 * This function closes the users session and writes all their
		 * data.
		 *
		 * @access public
		 */
		
		public function close() {
			try {
				@session_write_close();
				$this->open = true;
			}
			catch (Exception $ex) {
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * clear
		 *
		 * This function clears the users session.
		 *
		 * @access public
		 */
		
		public function clear() {
			$_SESSION = array();
		}
		
		/* ------------------------------------------------------------------ */
		
		
		/**
		 * regenerate
		 *
		 * This function regenerates the users session and assigns a new ID.
		 *
		 * @access public
		 */
		
		public function regenerate() {
			$temp = $_SESSION;
			@session_regenerate_id(true);
			$_SESSION = $temp;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setVar
		 *
		 * This function is used to store a name and value key pair of
		 * data into the session.
		 *
		 * i.e.
		 *	$sessioin->setVar('name', 'Steve Brewster');
		 *
		 * @access public
		 * @param string $name
		 * @param mixed $value
		 */
		
		public function setVar($name, $value) {
			$this->open();
			$_SESSION[$name] = $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getVar
		 *
		 * This function is used to retrive a value from the session. The
		 * function also allows a default value to be specified.
		 *
		 * i.e.
		 *	$name = $form->getVar('name');
		 *
		 * @access public
		 * @param string $name
		 * @param string $default
		 * @return mixed
		 */
		
		public function getVar($name, $default = null) {
			$this->open();
			if (array_key_exists($name, $_SESSION)) {
				return $_SESSION[$name];
			} else {
				return $default;
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>
