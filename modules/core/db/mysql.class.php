<?php
	
	class DB_MySQL extends DB {
		
		/* ------------------------------------------------------------------ */
		
		protected $db;
		protected $server;
		protected $user;
		protected $pass;
		protected $name;
		protected $port;
		protected $charset;
		protected $mode;
		protected $prepared;
		
		/* ------------------------------------------------------------------ */
		
		public function __construct() {
			if (function_exists('mysql_connect') == false) {
				throw new Exception_Library_NotExists('MySQL Does Not Exist');
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function init($server = null, $user = null, $pass = null, $name = null, $port = 3306, $charset = null, $mode = null) {
			$this->db       = null;
			$this->server   = $server;
			$this->user     = $user;
			$this->pass     = $pass;
			$this->name     = $name;
			$this->port     = $port;
			$this->charset  = $charset;
			$this->mode     = $mode;
			$this->prepared = array();
		}
		
		/* ------------------------------------------------------------------ */
		
		public function connect() {
			if (is_resource($this->db) == false) {
				$this->db = mysql_connect($this->server . ':' . $this->port, $this->user, $this->pass);
				mysql_select_db($this->name, $this->db);
				if ($this->charset) {
					$this->query('SET NAMES "' . $this->charset . '"');
					$this->query('SET CHARACTER SET "' . $this->charset . '"');
				}
				if ($this->mode) {
					$this->query('SET sql_mode = "' . $this->mode . '"');
				} else {
					$this->query('SET sql_mode = ""');
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function close() {
			if (is_resource($this->db)) {
				mysql_close($this->db);
				$this->db = null;
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function query($sql) {
			$this->connect();
			$key = md5($sql);
			if (array_key_exists($key, $this->prepared) == false) {
				$this->prepared[$key] = new DB_MySQLStatement($this->db, $sql);
			}
			$stmt = clone $this->prepared[$key];
			$stmt->setParams(array_slice(func_get_args(), 1));
			$stmt->execute();
			
			return $stmt;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getInsertID() {
			if (is_resource($this->db)) {
				return mysql_insert_id($this->db);
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getAffectedRows() {
			if (is_resource($this->db)) {
				return mysql_affected_rows($this->db);
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
?>