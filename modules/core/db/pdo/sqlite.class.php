<?php
	
	class DB_PDO_SQLite extends DB_PDO {
		
		/* ------------------------------------------------------------------ */
		
		protected $driver = 'sqlite';
		protected $path;
		protected $charset;
		
		/* ------------------------------------------------------------------ */
		
		public function __construct() {
			$drivers = array_map('strtolower', PDO::getAvailableDrivers());
			if (class_exists('PDO') == false || in_array(strtolower($this->driver), $drivers) == false) {
				throw new Exception_Library_NotExists('PDO SQLite Does Not Exist');
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function init($path = null, $charset = null) {
			$this->db      = null;
			$this->path    = $path;
			$this->charset = $charset;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function connect() {
			if (is_object($this->db) == false) {
				if (is_file($this->path) && is_writable($this->path) == false) {
					throw new Exception_File_NotWritable('File is not writable - ' . $this->path);
				} elseif (is_dir(dirname($this->path)) && is_writable(dirname($this->path)) == false) {
					throw new Exception_File_NotWritable('File is not writable - ' . dirname($this->path));
				}
				$dsn = $this->driver . ':';
				if ($this->path) {
					$dsn .= $this->path;
				}
				
				$this->db = new PDO($dsn);
				
				$this->db->query('PRAGMA foreign_keys = ON');
				
				if ($this->charset) {
					$this->db->query('PRAGMA encoding = "' . $this->charset . '"');
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function close() {
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>