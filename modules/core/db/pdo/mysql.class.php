<?php
	
	class DB_PDO_MYSQL extends DB_PDO {
		
		/* ------------------------------------------------------------------ */
		
		protected $driver = 'mysql';
		protected $server;
		protected $user;
		protected $pass;
		protected $name;
		protected $port;
		protected $charset;
		protected $mode;
		
		/* ------------------------------------------------------------------ */
		
		public function __construct() {
			$drivers = array_map('strtolower', PDO::getAvailableDrivers());
			if (class_exists('PDO') == false || in_array(strtolower($this->driver), $drivers) == false) {
				throw new Exception_Library_NotExists('PDO MySQL Does Not Exist');
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function init($server = null, $user = null, $pass = null, $name = null, $port = 3306, $charset = null, $mode = null) {
			$this->db      = null;
			$this->server  = $server;
			$this->user    = $user;
			$this->pass    = $pass;
			$this->name    = $name;
			$this->port    = $port;
			$this->charset = $charset;
			$this->mode    = $mode;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function connect() {
			if (is_object($this->db) == false) {
				$dsn = $this->driver . ':';
				if ($this->server) {
					$dsn .= 'host=' . $this->server . ';';
				}
				if ($this->port) {
					$dsn .= 'port=' . $this->port . ';';
				}
				if ($this->name) {
					$dsn .= 'dbname=' . $this->name . ';';
				}
				
				$this->db = new PDO($dsn, $this->user, $this->pass);
				
				if ($this->charset) {
					$this->db->query('SET NAMES "' . $this->charset . '"');
					$this->db->query('SET CHARACTER SET "' . $this->charset . '"');
				}
				
				if ($this->mode) {
					$this->db->query('SET sql_mode = "' . $this->mode . '"');
				} else {
					$this->db->query('SET sql_mode = ""');
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function close() {
		}
		
		/* ------------------------------------------------------------------ */
	}
	
?>