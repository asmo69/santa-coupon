<?php
	
	abstract class DB_Statement {
		
		/* ------------------------------------------------------------------ */
		
		abstract public function execute();
		
		/* ------------------------------------------------------------------ */
		
		abstract public function fetch($type = null);
		
		/* ------------------------------------------------------------------ */
		
		abstract public function setParams($value);
		
		/* ------------------------------------------------------------------ */
		
		abstract public function addParam($value);
		
		/* ------------------------------------------------------------------ */
		
		abstract public function getNumRows();
		
		/* ------------------------------------------------------------------ */
		
		abstract public function getInsertID();
		
		/* ------------------------------------------------------------------ */
		
		abstract public function getAffectedRows();
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>