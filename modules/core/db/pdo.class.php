<?php
	
	abstract class DB_PDO extends DB {
		
		/* ------------------------------------------------------------------ */
		
		protected $db;
		protected $stmt;
		protected $prepared = array();
		
		/* ------------------------------------------------------------------ */
		
		public function __construct() {
			if (class_exists('PDO') == false) {
				throw new Exception_Library_NotExists('PDO Does Not Exist');
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function query($sql) {
			$this->connect();
			$key = md5($sql);
			if (array_key_exists($key, $this->prepared) == false) {
				$this->prepared[$key] = new DB_PDOStatement($this->db, $sql);
			}
			$this->stmt = clone $this->prepared[$key];
			$this->stmt->setParams(array_slice(func_get_args(), 1));
			$this->stmt->execute();
			
			return $this->stmt;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getInsertID() {
			if (is_object($this->db)) {
				return $this->db->lastInsertId();
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getAffectedRows() {
			if (is_object($this->stmt)) {
				return $this->stmt->getAffectedRows();
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
?>