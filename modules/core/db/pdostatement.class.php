<?php
	
	class DB_PDOStatement extends DB_Statement {
		
		/* ------------------------------------------------------------------ */
		
		protected $sql = null;
		protected $db = null;
		protected $result = null;
		protected $params = array();
		
		/* ------------------------------------------------------------------ */
		
		public function __construct($db, $sql) {
			$this->db  = $db;
			$this->sql = $sql;
			
			$this->result = $this->db->prepare($this->sql);
			
			if (is_object($this->result) && intval($this->result->errorCode())) {
				$info = $this->result->errorInfo();
				throw new Exception_DB_Prepare($info[2], $info[0]);
			} elseif (is_object($this->db) && intval($this->db->errorCode())) {
				$info = $this->db->errorInfo();
				throw new Exception_DB_Prepare($info[2], $info[0]);
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function __destruct() {
		}
		
		/* ------------------------------------------------------------------ */
		
		public function execute() {
			if (is_object($this->result)) {
				$this->result->execute($this->params);
				
				if (intval($this->result->errorCode())) {
					$info = $this->result->errorInfo();
					throw new Exception_DB_Execute($info[2], $info[0]);
				} elseif (intval($this->db->errorCode())) {
					$info = $this->db->errorInfo();
					throw new Exception_DB_Execute($info[2], $info[0]);
				}
			}
			
			return $this->result;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function fetch($type = null) {
			if (is_object($this->result)) {
				if (is_string($type)) {
					return $this->result->fetchObject($type);
				} elseif ($type === DB::FETCH_ARRAY) {
					return $this->result->fetch(PDO::FETCH_NUM);
				} else {
					return $this->result->fetch(PDO::FETCH_ASSOC);
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setParams($value) {
			if (is_array($value)) {
				if (count($value) == 1 && is_array($value[0])) {
					$this->params = $value[0];
				} else {
					$this->params = $value;
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function addParam($value) {
			$this->params[] = $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getNumRows() {
			if (is_object($this->result)) {
				return $this->result->rowCount();
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getInsertID() {
			if (is_object($this->db)) {
				return $this->db->lastInsertId();
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getAffectedRows() {
			if (is_object($this->result)) {
				return $this->result->rowCount();
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		
	}
	
?>