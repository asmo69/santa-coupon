<?php

	class DB_MySQLStatement extends DB_Statement {

		/* ------------------------------------------------------------------ */

		protected $sql = null;
		protected $db = null;
		protected $result = null;
		protected $params = array();

		/* ------------------------------------------------------------------ */

		public function __construct($db, $sql) {
			$this->db  = $db;
			$this->sql = $sql;

			$sql = sprintf("PREPARE stmt_%s FROM '%s'", md5($this->sql), mysql_real_escape_string($this->sql, $this->db));
			//printf("%s;\n", $sql);
			mysql_query($sql, $this->db);

			if (mysql_errno($this->db)) {
				throw new Exception_DB_Prepare(mysql_error($this->db), mysql_errno($this->db));
			}
		}

		/* ------------------------------------------------------------------ */

		public function __destruct() {
			if (is_resource($this->result)) {
				mysql_free_result($this->result);
			}
		}

		/* ------------------------------------------------------------------ */

		public function execute() {
			if (count($this->params)) {
				for ($x = 0; $x < count($this->params); $x++) {
					if (is_null($this->params[$x])) {
						$sql = sprintf("SET @prm_%s = NULL", $x);
					}
					elseif (is_bool($this->params[$x])) {
						$sql = sprintf("SET @prm_%s = %s", $x, $this->params[$x] ? 1 : 0);
					}
					elseif (is_numeric($this->params[$x])) {
						$sql = sprintf("SET @prm_%s = %s", $x, $this->params[$x]);
					}
					elseif (is_string($this->params[$x])) {
						$sql = sprintf("SET @prm_%s = '%s'", $x, mysql_real_escape_string($this->params[$x], $this->db));
					}
					//printf("%s;\n", $sql);
					mysql_query($sql, $this->db);
				}

				$sql = sprintf("EXECUTE stmt_%s USING @prm_%s", md5($this->sql), implode(', @prm_', range(0, count($this->params) - 1)));
			}
			else {
				$sql = sprintf("EXECUTE stmt_%s", md5($this->sql));
			}
			//printf("%s;\n", $sql);
			$this->result = mysql_query($sql, $this->db);

			if (mysql_errno($this->db)) {
				throw new Exception_DB_Execute(mysql_error($this->db), mysql_errno($this->db));
			}

			return $this->result;
		}

		/* ------------------------------------------------------------------ */

		public function fetch($type = null) {
			if (is_resource($this->result)) {
				if (is_string($type)) {
					return mysql_fetch_object($this->result, $type);
				}
				else {
					return mysql_fetch_assoc($this->result);
				}
				elseif ( $type === DB::FETCH_ARRAY ) {
					return mysql_fetch_row($this->result);
				}
				else {
					return mysql_fetch_assoc($this->result);
				}
			}
		}

		/* ------------------------------------------------------------------ */

		public function setParams($value) {
			if (is_array($value)) {
				if (count($value) == 1 && is_array($value[0])) {
					$this->params = $value[0];
				}
				else {
					$this->params = $value;
				}
			}
		}

		/* ------------------------------------------------------------------ */

		public function addParam($value) {
			$this->params[] = $value;
		}

		/* ------------------------------------------------------------------ */

		public function getNumRows() {
			return mysql_num_rows($this->result);
		}

		/* ------------------------------------------------------------------ */

		public function getInsertID() {
			return mysql_insert_id($this->db);
		}

		/* ------------------------------------------------------------------ */

		public function getAffectedRows() {
			return mysql_affected_rows($this->db);
		}

		/* ------------------------------------------------------------------ */

	}

?>