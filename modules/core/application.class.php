<?php
	
	/**
	 * Application
	 *
	 * The Application class forms the root of the entire application. The
	 * Application class itself is mostly concerned with the input and
	 * out of data. To perform specific functionalities an Application
	 * class must be passed a Controller so that it knows what kind of
	 * application it is and what sort of Action classes to execute.
	 *
	 * @since 2013-01-02
	 * @abstract
	 * @see Controller, Plugin, Action
	 */
	
	class Application {
		
		/* ------------------------------------------------------------------ */
		
		protected static $instance = null;
		
		/* ------------------------------------------------------------------ */
		
		protected $controller = null;
		protected $plugins = null;
		
		/* ------------------------------------------------------------------ */
		
		protected $url = null;
		protected $redirect = null;
		protected $output = null;
		
		protected $mime = null;
		protected $charset = null;
		protected $status = null;
		protected $authenticate = null;
		protected $done = false;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getInstance
		 *
		 * This function turns the Application class into a singleton, meaning
		 * that there can only ever be one instance of the class.
		 *
		 * @access public
		 * @return Application
		 */
		
		public static function getInstance() {
			if (self::$instance === null) {
				self::$instance = new static();
			}
			return self::$instance;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * __construct
		 *
		 * This function is the constructor for the Application class
		 * and sets up all the default settings. Declaring this function as
		 * protected re-enforces the singleton pattern and prevents anyone
		 *from creating another instance.
		 *
		 * @access protected
		 */
		
		protected function __construct() {
			// Start the output buffer.
			ob_start();
			
			// Initalise the plugin arrays.
			$this->plugins = array(
				'init' => array(),
				'output' => array()
			);
			
			// Initialise the output settings.
			$this->setStatus(Constants::HTTP_OK);
			$this->setMime(Constants::MIME_HTML);
			$this->setCharset(Constants::CHARSET_UTF8);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function __destruct() {
			// Flush the output buffer.
			ob_end_flush();
		}
		
		/* ------------------------------------------------------------------ */
		
		public function done($done = null) {
			if ($done === null) {
				return $this->done;
			} else {
				$this->done = $done ? true : false;
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * start
		 *
		 * This function is the main body of the Application. Its main
		 * function is to load the configuration files, execute the input
		 * Plugin classes, execute the Controller class and execute the
		 * output Plugin classes.
		 *
		 * i.e.
		 *	$application = new Application();
		 *	$application->setController(Controller::create('Website'));
		 *	$application->start();
		 *
		 * @access public
		 */
		
		public function start() {
			// Load the default config
			Registry::getConfig()->load(ROOT_DIR . '/configs/application.conf');
			
			// Initialise the controller
			if ($this->getController()) {
				$this->getController()->init($this);
			}
			
			// Open the session
			Registry::getSession()->open();
			
			// Set the page URL
			if (array_key_exists('REQUEST_URI', $_SERVER)) {
				$this->setURL(new Net_URL($_SERVER['REQUEST_URI']));
			}
			
			// Run the Init Plugins.
			if (empty($this->done)) {
				foreach ($this->plugins['init'] as $plugin) {
					if (empty($this->done)) {
						$plugin->execute($this);
					}
				}
			}
			
			// Run the Controller
			if (empty($this->done)) {
				if ($this->getController()) {
					$this->getController()->execute($this);
				}
			}
			
			// Set the HTTP headers.
			if (headers_sent() == false) {
				if ($this->status) {
					header('HTTP/1.1 ' . $this->status, true);
				} else {
					header('HTTP/1.1 200 OK', true);
				}
				if ($this->mime && $this->charset) {
					header('Content-Type: ' . $this->mime . '; charset=' . $this->charset, true);
				} elseif ($this->mime) {
					header('Content-Type: ' . $this->mime, true);
				} else {
					header('Content-Type: text/html', true);
				}
				if ($this->redirect) {
					header('Location: ' . $this->redirect->getString(), true);
				}
				if ($this->authenticate) {
					header('WWW-Authenticate: Basic realm="' . addslashes($this->authenticate) . '"', true);
				}
			}
			
			// Run the Output Plugins
			if (empty($this->done)) {
				foreach ($this->plugins['output'] as $plugin) {
					if (empty($this->done)) {
						$plugin->execute($this);
					}
				}
			}
			
			// Close the session
			Registry::getSession()->close();
			
			// Display the output
			print($this->output);
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setController
		 *
		 * This function allows the programmer to set the controller type
		 * of the Application. Controller classes are used to provide a
		 * number of different functionality types such XML API's, SOAP,
		 * HTML Websites, etc. The Controller will then determine the
		 * appropriate Action class to execute and how to process or
		 * display the resulting data.
		 *
		 * i.e.
		 *	$application = new Application();
		 *	$application->setController(Controller::create('Website'));
		 *	$application->start();
		 *
		 * @access public
		 * @param Controller $controller
		 */
		
		
		public function setController(Controller $controller) {
			$this->controller = $controller;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getController
		 *
		 * Ths function returns the Controller which is currently
		 * assigned to the Application.
		 *
		 * @access public
		 * @return Controller
		 */
		
		public function getController() {
			return $this->controller;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getOutput
		 *
		 * This function returns the output data which should have been
		 * set during the execution of the Controller and Action classes.
		 * This could contain HTML, XML, CSV or even binary data.
		 *
		 * @access public
		 * @return string
		 */
		
		public function getOutput() {
			return $this->output;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setOutput
		 *
		 * This function sets the output data. This would usually be set
		 * as a result of the execution of the Controller and Action
		 * classes. This could contain HTML, XML, CSV or even binary data.
		 *
		 * i.e.
		 *	$application->setOutput('<html><body>Hello World</body></html>');
		 *
		 * @access public
		 * @param string $output
		 */
		
		public function setOutput($output) {
			$this->output = $output;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setMime
		 *
		 * This function sets the mime type of output data. This allows
		 * for the output data to contain virutally any kind of content,
		 * not just HTML but XML, CSV or even binary data.
		 *
		 * i.e.
		 *	$application->setMime(Constants::MIME_XML);
		 *
		 * @access public
		 * @param string $mime
		 */
		
		public function setMime($mime) {
			$this->mime = $mime;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getMime
		 *
		 * This function returns the current mime type of the output data.
		 *
		 * @access public
		 * @return string
		 */
		
		public function getMime() {
			return $this->mime;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setCharset
		 *
		 * This function sets the character set used in the output data.
		 * This allows for the data to be displayed as UTF-8, UTF-16,
		 * ISO-8859-1, or any other existing character set.
		 *
		 * i.e.
		 *	$application->setCharset(Constants::CHARSET_UTF8);
		 *
		 * @access public
		 * @param string $charset
		 */
		
		public function setCharset($charset) {
			$this->charset = $charset;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getCharset
		 *
		 * This function returns the current character set of the output
		 * data.
		 *
		 * @access public
		 * @return string
		 */
		
		public function getCharset() {
			return $this->charset;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setStatus
		 *
		 * This function sets the HTTP status of the output data. This
		 * allows the output data to contain content which describe HTTP
		 * errors like 301, 404, 500, etc.
		 *
		 * i.e.
		 *	$application->setStatus(Constants::HTTP_OK);
		 *
		 * @access public
		 * @param string $status
		 */
		
		public function setStatus($status) {
			$this->status = $status;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getStatus
		 *
		 * This function returns the current HTTP status of the output
		 * data.
		 *
		 * @access public
		 * @return string
		 */
		
		public function getStatus() {
			return $this->status;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setAuthenticate
		 *
		 * This function turns off and on the HTTP authentication by
		 * specifying what the login message should be.
		 *
		 * i.e.
		 *	$application->setAuthenticate('Please Login');
		 *
		 * @access public
		 * @param string $authenticate
		 */
		
		public function setAuthenticate($authenticate) {
			$this->authenticate = $authenticate;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getAuthenticate
		 *
		 * This function returns the current HTTP authentication message.
		 *
		 * @access public
		 * @return string
		 */
		
		public function getAuthenticate() {
			return $this->authenticate;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setRedirect
		 *
		 * This function sets a URL redirect.
		 *
		 * i.e.
		 *	$application->setRedirect(new Net_URL('/index.html'));
		 *
		 * @access public
		 * @param Net_URL $redirect
		 */
		
		public function setRedirect(Net_URL $redirect) {
			$this->redirect = $redirect;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getRedirect
		 *
		 * This function returns the URL redirect.
		 *
		 * i.e.
		 *	$redirect = $application->getRedirect();
		 *
		 * @access public
		 * @return Net_URL
		 */
		
		public function getRedirect() {
			return $this->redirect;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setURL
		 *
		 * This function sets the URL of the application.
		 *
		 * i.e.
		 *	$application->setURL(new Net_URL('http://myserver.com/path/to/script'));
		 *
		 * @access public
		 * @param Net_URL $url
		 */
		
		public function setURL(Net_URL $url) {
			$this->url = $url;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getURL
		 *
		 * This function returns the URL of the application.
		 *
		 * @access public
		 * @return Net_URL
		 */
		
		public function getURL() {
			return $this->url;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * addInitPlugin
		 *
		 * This function adds an input plugin to the application. Input
		 * Plugin classes can be used to perform a number of common
		 * startup/initialisation functions. They are commonly used to
		 * reformat and/or manipulate the URL before the Controller and
		 * Action class are executed. One example would be to make sure
		 * that a URL starts with 'www'.
		 *
		 * i.e.
		 *	$application->addInitPlugin(Plugin::create('www'));
		 *
		 * @param Plugin $plugin
		 */
		
		function addInitPlugin(Plugin $plugin) {
			$this->plugins['init'][] = $plugin;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * addOutputPlugin
		 *
		 * This function adds an input plugin to the application. Output
		 * Plugin classes can be used to perform a number of common
		 * shutdown/finalisation functions. They are commonly used to
		 * reformat and/or manipulate the output data that the Controller
		 * and Action class created. One example of this would be to
		 * tidy up the HTML source with proper line breaks and
		 * indentation.
		 *
		 * i.e.
		 *	$application->addOutputPlugin(Plugin::create('tidy'));
		 *
		 * @param Plugin $plugin
		 */
		
		function addOutputPlugin(Plugin $plugin) {
			$this->plugins['output'][] = $plugin;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>