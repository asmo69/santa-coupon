<?php
	
	class AutoLoader {
		
		/* ------------------------------------------------------------------ */
		
		protected static $instance = null;
		protected $modules = array('core');
		
		/* ------------------------------------------------------------------ */
		
		public static function getInstance() {
			if (self::$instance === null) {
				self::$instance = new static();
			}
			return self::$instance;
		}
		
		/* ------------------------------------------------------------------ */
		
		protected function __construct() {
			if (defined('ROOT_DIR') == false) {
				define('ROOT_DIR', rtrim(dirname(dirname(dirname(__FILE__))), '/') . '/');
			}
			spl_autoload_register(array(
				$this,
				'load'
			));
		}
		
		/* ------------------------------------------------------------------ */
		
		public function addModule($module) {
			$this->modules[] = strtolower($module);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getModules() {
			return $this->modules;
		}
		
		/* ------------------------------------------------------------------ */
		
		function load($classname) {
			$found   = false;
			$x       = 0;
			$modules = array_reverse($this->modules);
			
			while ($found == false && $x < count($modules)) {
				if (is_file(ROOT_DIR . '/modules/' . $modules[$x] . '/' . strtolower(str_replace('_', '/', $classname)) . '.class.php')) {
					require_once ROOT_DIR . '/modules/' . $modules[$x] . '/' . strtolower(str_replace('_', '/', $classname)) . '.class.php';
					$found = true;
				}
				$x++;
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>