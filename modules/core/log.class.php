<?php
	
	/**
	 * Log
	 *
	 * The Log class is the abstract base class from which all
	 * Logs's should inherit. Log classes are used to log messages from
	 * the application. These messages maybe important debug information,
	 * warnings, errors, or just items of interest in the code.
	 *
	 * @since 2013-01-02
	 * @abstract
	 */
	
	abstract class Log {
		
		/* ------------------------------------------------------------------ */
		
		const DEBUG = 1;
		const NOTICE = 2;
		const WARNING = 3;
		const ERROR = 4;
		
		/* ------------------------------------------------------------------ */
		
		public $level;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * create
		 *
		 * This static function is used as a factory method for creating
		 * different types of Log subclasses. The function will check
		 * that the specified class exists, is named correctly and is a
		 * valid subclass of Log before creating an instance.
		 *
		 * e.g.
		 *	$log = Log::create('file');
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Log
		 */
		
		public static function create($type = '') {
			if ($type) {
				$class = 'Log_' . preg_replace('/[^a-z0-9_]/i', '', str_replace('/', '_', $type));
				if (class_exists($class) && is_subclass_of($class, 'Log')) {
					return new $class();
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * init
		 *
		 * This function sets the default logging level of the
		 * application, allowing certain log messages to be turned on or
		 * off as the situation demmands. For instance, having debug
		 * messages shown on screen could be useful during development
		 * but disasterous in production.
		 *
		 * @access public
		 * @param string $level
		 */
		
		public function init($level) {
			$this->level = $level;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * write
		 *
		 * This function writes a message to the log.
		 *
		 * @access public
		 * @abstract
		 * @param string $value
		 * @param string $level
		 */
		
		abstract public function write($value, $level = null);
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>