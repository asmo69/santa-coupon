<?php
	
	/**
	 * Form
	 *
	 * The Form class provides a warpper around the default GET, POST and
	 * FILES functionality of PHP, allowing them to be treated in a more
	 * consistent manner with objects like Cookie, Session, Cache, etc.
	 *
	 * @since 2013-03-15
	 */
	
	class Form_Secure extends Form {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * __construct
		 *
		 * This function copies all of the values from the default GET,
		 * POST and FILES variables of PHP into itself. The code also
		 * makes sure to strip any slashes and HTML on the values, making
		 * data sanitisation less of a hassle.
		 *
		 * @access public
		 */
		
		public function __construct() {
			$this->data = array();
			foreach (array_map_recursive('strip_tags', array_map_recursive('stripslashes', $_POST)) as $name => $item) {
				$this->setVar($name, $item);
			}
			foreach (array_map_recursive('strip_tags', array_map_recursive('stripslashes', $_GET)) as $name => $item) {
				$this->setVar($name, $item);
			}
			foreach ($_FILES as $name => $item) {
				$this->setVar($name, $item);
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>