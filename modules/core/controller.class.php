<?php
	
	/**
	 * Controller
	 *
	 * The Controller class is the abstract base class from which all
	 * Controllers should inherit. A Controller class is used by the main
	 * Application class to determine how the application should
	 * function. Is it an XML API, a HTML website, etc. This then
	 * determines which Action class should be executed.
	 *
	 * @since 2013-01-02
	 * @abstract
	 * @see \Asmo\Application, \Asmo\Action
	 */
	
	abstract class Controller {
		
		/* ------------------------------------------------------------------ */
		
		protected $action = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * create
		 *
		 * This static function is used as a factory method for creating
		 * different types of Controller subclasses. The function will
		 * check that the specified class exists, is named correctly and
		 * is a valid subclass of Controller before creating an instance.
		 *
		 * e.g.
		 *	$controller = Controller::create('website');
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Controller
		 */
		
		public static function create($type) {
			if ($type) {
				$class = 'Controller_' . preg_replace('/[^a-z0-9_]/i', '', str_replace('/', '_', $type));
				if (class_exists($class) && is_subclass_of($class, 'Controller')) {
					return new $class();
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * init
		 *
		 * This abstract function should be defined by all subclasses
		 * which inherit from Controller. This function provides a means
		 * for Controller classes to initalise themselves before being
		 * executed. This function is commonly used to set the default
		 * Action class and add the input and output plugins.
		 *
		 * @access public
		 * @abstract
		 * @param Application $application
		 */
		
		abstract public function init(Application $application);
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * This abstract function should be defined by all subclasses
		 * which inherit from Controller. This function provides provides
		 * the main body functionality of the Controller. For example, a
		 * 'website' Controller could determing the Action class from the
		 * query string, execute the class and then run a template parser
		 * before copying the HTML into the Application out data.
		 *
		 * @access public
		 * @abstract
		 * @param Application $application
		 */
		
		abstract public function execute(Application $application);
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setAction
		 *
		 * This function sets the name of the Action class that the
		 * Controller will execute.
		 *
		 * i.e.
		 *	$controller->setAction(Action::create($_GET['action']));
		 *
		 * @access public
		 * @param string $action
		 */
		
		public function setAction($action) {
			$this->action = $action;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getAction
		 *
		 * This function returns the name of the Action class that the
		 * Controller will execute.
		 *
		 * @access public
		 * @return string
		 */
		
		public function getAction() {
			return $this->action;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>