<?php
	
	class Config_XML extends Config {
		
		/* ------------------------------------------------------------------ */
		
		protected function flattern($xml, $prefix = null) {
			$results = array();
			foreach ($xml as $name => $value) {
				if ($prefix) {
					$temp = $this->flattern($value, $prefix . '_' . $name);
				} else {
					$temp = $this->flattern($value, $name);
				}
				if ($temp) {
					foreach ($temp as $name => $value) {
						$results[$name] = trim($value);
					}
				} elseif ($prefix) {
					$results[$prefix . '_' . $name] = trim($value);
				} else {
					$results[$name] = trim($value);
				}
			}
			
			return $results;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function load($filename) {
			$temp = array();
			if (is_file($filename)) {
				try {
					$data = $this->flattern(new SimpleXMLElement(file_get_contents($filename)));
					foreach ($data as $name => $value) {
						$this->setVar($name, $value);
					}
				}
				catch (Exception $ex) {
					if ($log = Registry::getLog()) {
						$log->write($ex->getMessage(), Log::ERROR);
					}
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>