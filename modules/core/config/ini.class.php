<?php
	
	class Config_INI extends Config {
		
		/* ------------------------------------------------------------------ */
		
		public function load($filename) {
			$temp = array();
			if (is_file($filename)) {
				$file    = explode("\n", str_replace("\r", "\n", str_replace("\r\n", "\n", file_get_contents($filename))));
				$section = null;
				foreach ($file as $line) {
					$line = trim($line);
					if ($line && substr($line, 0, 1) != '#') {
						if (substr($line, 0, 1) == '[' && substr($line, -1) == ']') {
							$section = substr($line, 1, strlen($line) - 2);
						} else {
							list($name, $value) = explode('=', $line, 2);
							$name  = trim($name);
							$value = trim($value);
							
							if (substr($value, 0, 1) == '{' && substr($value, -1) == '}') {
								$value = json_decode($value, true);
							} elseif (substr($value, 0, 1) == '[' && substr($value, -1) == ']') {
								$value = json_decode($value, true);
							}
							
							if (substr($name, -2) == '[]') {
								$name = substr($name, 0, -2);
								$data = $this->getVar($section . '_' . $name);
								if (empty($data)) {
									$data = array(
										$value
									);
								} else {
									$data[] = $value;
								}
							} else {
								$data = $value;
							}
							
							$this->setVar($section . '_' . $name, $data);
						}
					}
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>