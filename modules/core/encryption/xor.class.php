<?php
	
	class Encryption_XOR extends Encryption {
		
		/* ------------------------------------------------------------------ */
		
		public function encode($value, $key) {
			return str_replace('=', '_', base64_encode($this->crypt($value, $key)));
		}
		
		/* ------------------------------------------------------------------ */
		
		public function decode($value, $key) {
			return $this->crypt(base64_decode(str_replace('_', '=', $value)), $key);
		}
		
		/* ------------------------------------------------------------------ */
		
		protected function crypt($value, $key) {
			$ret = '';
			$x   = 0;
			$y   = 0;
			
			for ($x = 0; $x < strlen($value); $x++) {
				$ret .= chr(ord(substr($value, $x, 1)) ^ ord(substr($key, $y, 1)));
				$y++;
				if ($y > strlen($key)) {
					$y = 0;
				}
			}
			
			return $ret;
		}
		
		/* ------------------------------------------------------------------ */
	}
	
?>