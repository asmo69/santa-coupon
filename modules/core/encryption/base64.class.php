<?php
	
	class Encryption_Base64 extends Encryption {
		
		/* ------------------------------------------------------------------ */
		
		public function encode($value, $key = null) {
			return str_replace('=', '_', base64_encode($value));
		}
		
		/* ------------------------------------------------------------------ */
		
		public function decode($value, $key = null) {
			return base64_decode(str_replace('_', '=', $value));
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>