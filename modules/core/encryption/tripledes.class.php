<?php
	
	class Encryption_TripleDes extends Encryption {
		
		/* ------------------------------------------------------------------ */
		
		public function encode($value, $key) {
			if (function_exists('mcrypt_encrypt') == false) {
				throw new Exception_Library_NotExists('MCrypt Does Not Exist');
			}
			$size  = mcrypt_get_iv_size(MCRYPT_TRIPLEDES, MCRYPT_MODE_ECB);
			$iv    = mcrypt_create_iv($size, MCRYPT_RAND);
			$value = mcrypt_encrypt(MCRYPT_TRIPLEDES, str_pad(substr($key, 0, 24), 24, '.'), $value, MCRYPT_MODE_ECB, $iv);
			$value = str_replace('=', '_', base64_encode($value));
			
			return trim($value);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function decode($value, $key) {
			if (function_exists('mcrypt_decrypt') == false) {
				throw new Exception_Library_NotExists('MCrypt Does Not Exist');
			}
			$value = base64_decode(str_replace('_', '=', $value));
			$size  = mcrypt_get_iv_size(MCRYPT_TRIPLEDES, MCRYPT_MODE_ECB);
			$iv    = mcrypt_create_iv($size, MCRYPT_RAND);
			$value = mcrypt_decrypt(MCRYPT_TRIPLEDES, str_pad(substr($key, 0, 24), 24, '.'), $value, MCRYPT_MODE_ECB, $iv);
			
			return trim($value);
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>