<?php
	
	/**
	 * DB
	 *
	 * The DB class is the abstract base class from which all
	 * DB's should inherit. DB subclasses may be implement MySQL, MySQLi,
	 * Postgress, SQL Server, or any other kind of database storage you
	 * wish.
	 *
	 * @since 2013-01-02
	 * @abstract
	 */
	
	abstract class DB {
		
		/* ------------------------------------------------------------------ */
		
		const READ_ONLY = 'ro';
		const READ_WRITE = 'rw';
		
		/* ------------------------------------------------------------------ */
		
		const FETCH_ASSOC = 1;
		const FETCH_ARRAY = 2;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * create
		 *
		 * This static function is used as a factory method for creating
		 * different types of DB subclasses. The function will check
		 * that the specified class exists, is named correctly and is a
		 * valid subclass of Config before creating an instance.
		 *
		 * e.g.
		 *	$db = DB::create('mysql');
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return DB
		 */
		
		public static function create($type) {
			if ($type) {
				$class = 'DB_' . preg_replace('/[^a-z0-9_]/i', '', str_replace('/', '_', $type));
				if (class_exists($class) && is_subclass_of($class, 'DB')) {
					return new $class();
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * init
		 *
		 * This function is to initialise the settings for the database
		 * connection.
		 * N.B. A connection to the database will no be made a call to
		 * connect() has been made. This should usually be handled from
		 * within the query() function, ensuring that the database
		 * connection is not created until it is needed.
		 *
		 * i.e.
		 *	$db = DB::create('mysql');
		 *	$db->init('localhost', 'my-username', 'my-password', 'my-database', '3306');
		 *
		 * @access public
		 * @abstract
		 */
		
		abstract public function init();
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * connect
		 *
		 * This function is to make the connection to the database. This
		 * should usually be handled from within the query() function,
		 * ensuring that the database connection is not created until it
		 * is needed.
		 *
		 * i.e.
		 *	$db = DB::create('mysql');
		 *	$db->init('localhost', 'my-username', 'my-password', 'my-database', '3306');
		 *	$db->connect();
		 *
		 * @access public
		 * @abstract
		 */
		
		abstract public function connect();
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * close
		 *
		 * This function closes the connection to the database and should
		 * be used to free up any resources associated with that
		 * connection.
		 *
		 * i.e.
		 *	$db->close();
		 *
		 * @access public
		 * @abstract
		 */
		
		abstract public function close();
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * query
		 *
		 * This function is used to actually query the database. The
		 * data returned should either be empty (null) or an instance of
		 * a DB_Statement class.
		 *
		 * i.e.
		 *	$result = $db->query('SHOW TABLES');
		 *
		 * @access public
		 * @abstract
		 * @param string $sql
		 * @return DB_Statement
		 */
		
		abstract public function query($sql);
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getInsertID
		 *
		 * This function returns the last auto increment ID created by
		 * the database during an INSERT command.
		 *
		 * i.e.
		 *	$id = $db->getInsertID();
		 *
		 * @access public
		 * @abstract
		 * @return int
		 */
		
		abstract public function getInsertID();
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getAffectedRows
		 *
		 * This function returns the number of database rows affected by
		 * the last UPDATE, DELETE, REPLACE command.
		 *
		 * i.e.
		 *	$num = $db->getAffectedRows();
		 *
		 * @access public
		 * @abstract
		 * @return int
		 */
		
		abstract public function getAffectedRows();
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>