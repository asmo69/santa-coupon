<?php
	
	/**
	 * Controller_CLI
	 *
	 * Controller_CLI classes are executed by the Application classes.
	 * Each type controller represents a different kind of application.
	 * This one is for creating command line or console applications.
	 *
	 * i.e.
	 *
	 * /usr/bin/php ./console.php --action=MyAction
	 *
	 * The above URL will result in the Controller trying to make an
	 * execute() call the Action_CLI_Console_MyAction class.
	 *
	 * @since 2013-01-02
	 */
	
	abstract class Controller_CLI extends Controller {
		
		/* ------------------------------------------------------------------ */
		
		protected $cached = false;
		protected $template = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * init
		 *
		 * This function provides a means for Controller class to
		 * initalise itself before being  executed. This function is
		 * commonly used to set the default Action class and add the
		 * input and output plugins.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function init(Application $application) {
			// Set the error and exception handlers.
			error_reporting(E_ALL);
			set_error_handler(array(
				$this,
				'errorHandler'
			), E_ALL);
			set_exception_handler(array(
				$this,
				'exceptionHandler'
			));
			
			// Get the controller name.
			$name = str_replace('Controller_', '', get_called_class());
			
			// Set the default MIME type, charset and status.
			$application->setStatus(Constants::HTTP_OK);
			$application->setMime(Constants::MIME_TEXT);
			$application->setCharset(Constants::CHARSET_UTF8);
			
			// Load the controller config
			Registry::getConfig()->load(ROOT_DIR . '/configs/controller/' . str_replace('_', '/', strtolower($name)) . '.conf');
			
			// Set the memory limit
			if (Registry::getConfig()->getVar('controller_limit_memory')) {
				ini_set('memory_limit', Registry::getConfig()->getVar('controller_limit_memory'));
			}
			
			// Set the time limit
			if (Registry::getConfig()->getVar('controller_limit_time')) {
				set_time_limit(Registry::getConfig()->getVar('controller_limit_time'));
			}
			
			// Set the default action
			if (Registry::getConfig()->getVar('controller_default_action')) {
				$this->setAction(Registry::getConfig()->getVar('controller_default_action'));
			}
			
			// Set the default templates directory
			if (Registry::getConfig()->getVar('controller_default_templates')) {
				Registry::getParser()->setTemplates(Registry::getConfig()->getVar('controller_default_templates'));
			}
			
			// Set the init plugins based on the value in the Config
			foreach (explode(',', Registry::getConfig()->getVar('controller_plugins_init')) as $name) {
				if ($plugin = Plugin::create(trim($name))) {
					$application->addInitPlugin($plugin);
				}
			}
			
			// Set the output plugins based on the value in the Config
			foreach (explode(',', Registry::getConfig()->getVar('controller_plugins_output')) as $name) {
				if ($plugin = Plugin::create(trim($name))) {
					$application->addOutputPlugin($plugin);
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * This function provides provides the main body functionality
		 * of the Controller_API. The function takes the input data (GET,
		 * POST, SESSION, etc) and decides which Action class to create
		 * and execute().
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			// Get the parser.
			$parser = Registry::getParser();
			
			// Get the controller name.
			$name = str_replace('Controller_', '', get_called_class());
			
			// Initiailise the output.
			$out = '';
			
			// Set the template.
			$this->setTemplate(strtolower(str_replace('_', '/', preg_replace('/[^a-z0-9_\/]/i', '', $this->getAction()))));
			
			// Get the cache key.
			$key = 'out.' . preg_replace('/[^a-z0-9]/i', '_', $this->getAction()) . '.' . md5(serialize($application->getURL()));
			
			// Try to retrieve the cache.
			if (preg_match('/^(yes|on|true|1)$/i', Registry::getConfig()->getVar('controller_cached')) && Registry::getCache() && $this->getCached()) {
				if ($out = Registry::getCache()->getVar($key)) {
					// Store that the page is cached.
					$this->setCached(true);
				} else {
					// Store that the page is not cached.
					$this->setCached(false);
				}
			}
			
			// If the cache is empty then build the output.
			if (empty($out)) {
				// Call the execute method of the action class.
				$action = Action::create($name . '_' . $this->getAction());
				if (empty($action)) {
					$action = Action::create($name . '_404');
				}
				if ($action) {
					$action->execute($application);
				}
				
				// Get the output content after running the action.
				$out = $application->getOutput();
				
				// If the output is null then attempt to parse a
				// template.
				if ($application->done() == false && empty($out)) {
					// Set the common elements
					$parser->setVar('application', $application);
					$parser->setVar('controller', $this);
					$parser->setVar('config', Registry::getConfig());
					$parser->setVar('lang', Registry::getLang());
					$parser->setVar('form', Registry::getForm());
					$parser->setVar('session', Registry::getSession());
					$parser->setVar('cookie', Registry::getCookie());
					$parser->setVar('action', $this->getAction());
					$parser->setVar('template', $this->getTemplate());
					
					// Check to see if a template exists in the
					// templates directory and if so parse it.
					if (is_file(ROOT_DIR . '/' . Registry::getParser()->getTemplates() . '/' . str_replace('_', '/', $this->getTemplate()) . '.tpl')) {
						$out = Registry::getParser()->fetch(str_replace('_', '/', $this->getTemplate()) . '.tpl');
					}
					// Check to see if a 404 file exists in the
					// templates directory and if so parse it.
					elseif (is_file(ROOT_DIR . '/' . Registry::getParser()->getTemplates() . '/404.tpl')) {
						$application->setStatus(Constants::HTTP_NOT_FOUND);
						$out = Registry::getParser()->fetch('404.tpl');
					}
					// If no class or 404 HTML exists then use a generic 404 error message.
					else {
						$application->setStatus(Constants::HTTP_NOT_FOUND);
						$out = '';
						
					}
				}
				
				// Run the Cache Plugins
				$plugins = Registry::getConfig()->getVar('controller_plugins_cache');
				$plugins = is_string($plugins) ? explode(',', $plugins) : $plugins;
				if (empty($plugins) == false) {
					foreach ($plugins as $name) {
						if ($plugin = Plugin::create(trim($name))) {
							$plugin->execute($application);
						}
					}
				}
				
				// Cache the output.
				if (preg_match('/^(yes|on|true|1)$/i', Registry::getConfig()->getVar('controller_cached')) && Registry::getCache() && $this->getCached()) {
					Registry::getCache()->setVar($key, $out);
				}
			} else {
				// Call the execute method of the action class.
				$action = Action::create($name . '_' . $this->getAction());
				if (empty($action)) {
					$action = Action::create($name . '_404');
				}
				if ($action) {
					$action->execute($application);
				}
			}
			
			// Set the output
			$application->setOutput($out);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setTemplate($template) {
			$this->template = $template;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getTemplate() {
			return $this->template;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setCached($value) {
			$this->cached = $value ? true : false;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getCached() {
			return $this->cached;
		}
		
		/* ------------------------------------------------------------------ */
		
		function errorHandler($number, $string, $file, $line, $context) {
			$error_is_enabled = (bool) ($number & ini_get('error_reporting'));
			
			if (in_array($number, array(
				E_ERROR,
				E_USER_ERROR,
				E_RECOVERABLE_ERROR,
				E_NOTICE,
				E_WARNING
			)) && $error_is_enabled) {
				throw new ErrorException($string, 0, $number, $file, $line);
			} elseif ($error_is_enabled) {
				error_log($string, 0);
				return false;
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		function exceptionHandler($ex) {
			$trace = $ex->getTrace();
			
			if ($log = Registry::getLog()) {
				$error   = array();
				$error[] = get_class($ex);
				$error[] = $ex->getMessage();
				$error[] = $ex->getFile();
				$error[] = $ex->getLine();
				
				if ($trace) {
					$error[] = array_key_exists('class', $trace[0]) ? $trace[0]['class'] : '';
					$error[] = array_key_exists('function', $trace[0]) ? $trace[0]['function'] : '';
				}
				
				$log->write(implode(' : ', $error), Log::ERROR);
			}
			
			$out = '';
			$out .= sprintf("%s - %s\n", get_class($ex), $ex->getMessage());
			$out .= sprintf("%s - line %s\n", $ex->getFile(), $ex->getLine());
			$out .= print_r($trace, true);
			
			print($out);
		}
		
		/* ------------------------------------------------------------------ */
		
		
	}
	
?>