<?php
	
	/**
	 * Config
	 *
	 * The Config class is the abstract base class from which all
	 * Config's should inherit. Config classes are used to load
	 * configuration information about the Application, Controller,
	 * Action and other classes. They are commonly used to specify
	 * settings for things like the database, the session, the cache,
	 * etc.
	 *
	 * @since 2013-01-02
	 * @abstract
	 */
	
	abstract class Config implements Iterator {
		
		/* ------------------------------------------------------------------ */
		
		protected $data = array();
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * create
		 *
		 * This static function is used as a factory method for creating
		 * different types of Config subclasses. The function will check
		 * that the specified class exists, is named correctly and is a
		 * valid subclass of Config before creating an instance.
		 *
		 * e.g.
		 *	$config = Config::create('file');
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Config
		 */
		
		public static function create($type = '') {
			if ($type) {
				$class = 'Config_' . preg_replace('/[^a-z0-9_]/i', '', str_replace('/', '_', $type));
				if (class_exists($class) && is_subclass_of($class, 'Config')) {
					return new $class();
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setVar
		 *
		 * This function is used to store a name and value key pair of
		 * data into the config. Data written in this way is not meant
		 * be saved back into the configs but is meant to allow the
		 * Controller and Action classes to override or add values they
		 * may need for this exectuion. An example of this might be
		 * looking for a 'mode' parameter on the query string which can
		 * then change the configuration of your application.
		 *
		 * i.e.
		 *	$config->setVar('mode', $_GET['mode']);
		 *
		 * @access public
		 * @param string $name
		 * @param mixed $value
		 */
		
		public function setVar($name, $value) {
			$this->data[strtoupper($name)] = $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getVar
		 *
		 * This function is used to retrive a value from the config.
		 *
		 * i.e.
		 *	$mode = $config->getVar('mode');
		 *
		 * @access public
		 * @param string $name
		 * @return mixed
		 */
		
		public function getVar($name) {
			if (array_key_exists(strtoupper($name), $this->data)) {
				return $this->data[strtoupper($name)];
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function rewind() {
			ksort($this->data);
			return reset($this->data);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function current() {
			return current($this->data);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function key() {
			return key($this->data);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function next() {
			return next($this->data);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function valid() {
			return key($this->data) !== null;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>