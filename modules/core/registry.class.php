<?php
	
	/**
	 * Registry
	 *
	 * The Registry class contains functions to create and cache many of
	 * the standard objects used by the application. This removes the
	 * coupling of many of this objects from the Application, Controller,
	 * Action or any other class. This in turn promotes greater ability
	 * to scale and extend the system. For example, if the cache type
	 * changes from being file based to memcache or database based then
	 * you only need to make changes here.
	 * N.B. The current class is highly dependant on the values loaded in
	 * the Config object, but these could just as easily be hard coded.
	 *
	 * @since 2013-01-02
	 */
	
	class Registry {
		
		/* ------------------------------------------------------------------ */
		
		protected static $loader = null;
		protected static $application = null;
		protected static $cache = array();
		protected static $config = array();
		protected static $lang = array();
		protected static $form = array();
		protected static $session = array();
		protected static $cookie = array();
		protected static $parser = array();
		protected static $log = array();
		protected static $lock = array();
		protected static $db = array();
		protected static $xml = null;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getDB
		 *
		 * This function uses the values loaded in the Config object to
		 * determine what kind of database the application should be
		 * using. The code allows for databases in read-only and
		 * read-write mode to be created (thus better supporting systems
		 * in which a master and slave database setup are used) and also
		 * provides a level of caching to ensure that additional calls
		 * with the same parameters are returned the same database.
		 *
		 * i.e.
		 *	$db = Registry::getDB('ro');
		 *	$result = $db->query('SHOW TABLES');
		 *
		 * N.B. The current functionality uses the values loaded in the
		 * Config object to determine the type of DB to create and what
		 * the initialisation paramaters are.
		 *
		 * @access public
		 * @static
		 * @param string $mode
		 * @return DB
		 */
		
		public static function getDB($mode = null) {
			if (empty($mode)) {
				$mode = 'rw';
			}
			if ($mode) {
				if (array_key_exists($mode, self::$db) === false) {
					if (self::$db[$mode] = DB::create(self::getConfig()->getVar('database_' . $mode . '_type'))) {
						self::$db[$mode]->init(self::getConfig()->getVar('database_' . $mode . '_server'), self::getConfig()->getVar('database_' . $mode . '_username'), self::getConfig()->getVar('database_' . $mode . '_password'), self::getConfig()->getVar('database_' . $mode . '_database'), self::getConfig()->getVar('database_' . $mode . '_port'), self::getConfig()->getVar('database_' . $mode . '_charset'), self::getConfig()->getVar('database_' . $mode . '_modes'));
					}
				}
				return self::$db[$mode];
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getAutoLoader
		 *
		 * This function creates and caches the auto loader class
		 * responsible for loading all other classes of the framework.
		 *
		 * i.e.
		 *	$loader = Registry::getAutoLoader();
		 *
		 * @access public
		 * @static
		 * @return AutoLoader
		 */
		
		public static function getAutoLoader() {
			if (self::$loader === null) {
				self::$loader = AutoLoader::getInstance();
			}
			return self::$loader;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getApplication
		 *
		 * This function creates and caches the main Application class
		 * used by the application. This allows for the main structure of
		 * the application to be de-coupled from the Application context.
		 *
		 * i.e.
		 *	$application = Registry::getApplication();
		 *
		 * @access public
		 * @static
		 * @return Application
		 */
		
		public static function getApplication() {
			if (self::$application === null) {
				self::$application = Application::getInstance();
			}
			return self::$application;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getConfig
		 *
		 * This function creates and caches the main Config class
		 * used by the application. This allows for the main structure of
		 * the application to be de-coupled from the Config context.
		 *
		 * i.e.
		 *	$config = Registry::getConfig();
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Config
		 */
		
		public static function getConfig($type = null) {
			if ($type === null) {
				$type = 'xml';
			}
			
			if (array_key_exists($type, self::$config) === false) {
				self::$config[$type] = Config::create($type);
			}
			
			return self::$config[$type];
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getLang
		 *
		 * This function creates and caches the main Lang class
		 * used by the application. This allows for the main structure of
		 * the application to be de-coupled from the Lang context.
		 *
		 * i.e.
		 *	$lang = Registry::getLang();
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Config
		 */
		
		public static function getLang($type = null) {
			if ($type === null) {
				$type = self::getConfig()->getVar('lang_type');
			}
			
			if (array_key_exists($type, self::$lang) === false) {
				self::$lang[$type] = Lang::create($type);
			}
			
			return self::$lang[$type];
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getForm
		 *
		 * This function creates and caches the main Form class
		 * used by the application. This allows for the main structure of
		 * the application to be de-coupled from the Form context.
		 *
		 * i.e.
		 *	$form = Registry::getForm('secure');
		 *
		 * N.B. The current functionality uses the values loaded in the
		 * Config object to determine the type of Form to create.
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Form
		 */
		
		public static function getForm($type = null) {
			if ($type === null) {
				$type = self::getConfig()->getVar('form_type');
			}
			
			if (array_key_exists($type, self::$form) === false) {
				self::$form[$type] = Form::create($type);
			}
			
			return self::$form[$type];
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getSession
		 *
		 * This function creates and caches the main Session class
		 * used by the application. This allows for the main structure of
		 * the application to be de-coupled from the Session context.
		 *
		 * i.e.
		 *	$session = Registry::getSession();
		 *
		 * N.B. The current functionality uses the values loaded in the
		 * Config object to determine the type of Session to create.
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Session
		 */
		
		public static function getSession($type = null) {
			if ($type === null) {
				$type = self::getConfig()->getVar('session_type');
			}
			
			if (array_key_exists($type, self::$session) === false) {
				self::$session[$type] = Session::create($type);
			}
			
			return self::$session[$type];
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getCookie
		 *
		 * This function creates and caches the main Cookie class
		 * used by the application. This allows for the main structure of
		 * the application to be de-coupled from the Cookie context.
		 *
		 * i.e.
		 *	$cookie = Registry::getCookie('secure');
		 *
		 * N.B. The current functionality uses the values loaded in the
		 * Config object to determine the type of Cookie to create.
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Cookie
		 */
		
		public static function getCookie($type = null) {
			if ($type === null) {
				$type = self::getConfig()->getVar('cookie_type');
			}
			
			if (array_key_exists($type, self::$cookie) === false) {
				self::$cookie[$type] = Cookie::create($type);
			}
			
			return self::$cookie[$type];
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getParser
		 *
		 * This function creates and caches the main Parser class
		 * used by the application. This allows for the main structure of
		 * the application to be de-coupled from the Parser context.
		 *
		 * i.e.
		 *	$parser = Registry::getParser('smarty');
		 *
		 * N.B. The current functionality uses the values loaded in the
		 * Config object to determine the type of Parser to create.
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Parser
		 */
		
		public static function getParser($type = null) {
			if ($type === null) {
				$type = self::getConfig()->getVar('parser_type');
			}
			
			if (array_key_exists($type, self::$parser) === false) {
				self::$parser[$type] = Parser::create($type);
			}
			
			return self::$parser[$type];
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getCache
		 *
		 * This function creates and caches the main Cache class
		 * used by the application. This allows for the main structure of
		 * the application to be de-coupled from the Cache context.
		 *
		 * i.e.
		 *	$cache = Registry::getCache('memcache');
		 *
		 * N.B. The current functionality uses the values loaded in the
		 * Config object to determine the type of Cache to create.
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Cache
		 */
		
		public static function getCache($type = null) {
			if ($type === null) {
				$type = self::getConfig()->getVar('cache_type');
			}
			
			if (array_key_exists($type, self::$cache) === false) {
				self::$cache[$type] = Cache::create($type);
			}
			
			return self::$cache[$type];
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getLog
		 
		 *
		 * This function creates and caches the main Log class
		 * used by the application. This allows for the main structure of
		 * the application to be de-coupled from the Log context.
		 *
		 
		 * i.e.
		 *	$log = Registry::getLog('file');
		 *
		 * N.B. The current functionality uses the values loaded in the
		 * Config object to determine the type of Log to create and what
		 * initialisation parameters to pass to it.
		 
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Log
		 */
		
		public static function getLog($type = null) {
			if ($type === null) {
				$type = self::getConfig()->getVar('log_type');
			}
			
			if (array_key_exists($type, self::$log) === false) {
				if (self::$log[$type] = Log::create($type)) {
					self::$log[$type]->init(self::getConfig()->getVar('log_level'));
				}
			}
			
			return self::$log[$type];
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getLock
		 
		 *
		 * This function creates and caches the main Lock class
		 * used by the application. This allows for the main structure of
		 * the application to be de-coupled from the Log context.
		 *
		 
		 * i.e.
		 *	$lock = Registry::getLock('file');
		 *
		 * N.B. The current functionality uses the values loaded in the
		 * Config object to determine the type of Log to create and what
		 * initialisation parameters to pass to it.
		 
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Lock
		 */
		
		public static function getLock($type = null) {
			if ($type === null) {
				$type = self::getConfig()->getVar('lock_type');
			}
			
			if (array_key_exists($type, self::$lock) === false) {
				self::$lock[$type] = Lock::create($type);
			}
			
			return self::$lock[$type];
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getXML
		 *
		 * This function creates and caches the XML Post data class used
		 * by the application.
		 *
		 * i.e.
		 *	$xml = Registry::getXML();
		 *
		 * @access public
		 * @static
		 * @return XML_CData
		 */
		
		public static function getXML() {
			if (self::$xml === null) {
				self::$xml = null;
				if (empty(self::$xml) && array_key_exists('xml', $_REQUEST)) {
					self::$xml = stripslashes($_REQUEST['xml']);
				}
				if (empty(self::$xml) && array_key_exists('XML', $_REQUEST)) {
					self::$xml = stripslashes($_REQUEST['XML']);
				}
				if (empty(self::$xml)) {
					self::$xml = rawurldecode(file_get_contents('php://input'));
				}
				if (empty(self::$xml)) {
					self::$xml = '<unknown/>';
				}
				self::$xml = preg_replace('/<\?(.*?)\?>/', '', preg_replace('/<([^\s>]*)/ie', '"<" . strtoupper("\\1")', self::$xml));
				try {
					self::$xml = new XML_CData(self::$xml);
				}
				catch (Excetion $ex) {
				}
			}
			return self::$xml;
		}
		
		/* ------------------------------------------------------------------ */
		
		public static function getUID() {
			$uid = self::getSession()->getVar('uid');
			if (empty($uid)) {
				$uid = time() . '-' . rand(0, 99999) . '-' . (array_key_exists('REMOTE_PORT', $_SERVER) ? $_SERVER['REMOTE_PORT'] : 0);
				self::getSession()->setVar('uid', $uid);
			}
			return $uid;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>