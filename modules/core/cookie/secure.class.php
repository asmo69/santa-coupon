<?php
	
	/**
	 * Cookie_Secure
	 *
	 * The Cookie_Secure class extends the default functionality created
	 * by the Cookie class but adds a level of security and encryption to
	 * the data.
	 *
	 * @since 2013-03-15
	 */
	
	class Cookie_Secure extends Cookie {
		
		/**
		 * setVar
		 *
		 * This function is used to store a name and value key pair of
		 * data into the cookie. This function also allows the expiry
		 * date, path and domain of the cookie to be set.
		 *
		 * i.e.
		 *	$cookie->setVar('mode', $_GET['mode']);
		 *
		 * @access public
		 * @param string $name
		 * @param mixed $value
		 */
		
		public function setVar($name, $value, $expire = 0, $path = '', $domain = '', $secure = false, $http = false) {
			$encryption = Registry::getConfig()->getVar('cookie_encryption');
			$key        = Registry::getConfig()->getVar('cookie_key');
			if ($encryption && $key) {
				if ($encrypt = Encryption::create($encryption)) {
					$value = $encrypt->encode($value, $key);
					
					$_COOKIE[$name] = $value;
					if (headers_sent() == false) {
						setcookie($name, $value, $expire, $path, $domain, $secure, $http);
					}
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getVar
		 *
		 * This function is used to retrive a value from the cookie.
		 *
		 * i.e.
		 *	$mode = $cookie->getVar('mode');
		 *
		 * @access public
		 * @param string $name
		 * @return mixed
		 */
		
		public function getVar($name) {
			$encryption = Registry::getConfig()->getVar('cookie_encryption');
			$key        = Registry::getConfig()->getVar('cookie_key');
			if ($encryption && $key) {
				if ($encrypt = Encryption::create($encryption)) {
					if (array_key_exists($name, $_COOKIE)) {
						return $encrypt->decode($_COOKIE[$name], $key);
					}
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>