<?php
	
	class Session_Memcache extends Session {
		
		/* ------------------------------------------------------------------ */
		
		protected $memcache;
		
		/* ------------------------------------------------------------------ */
		
		public function __construct() {
			if (class_exists('Memcache') == false) {
				throw new Exception_Library_NotExists('Memcache Does Not Exist');
			}
			session_set_save_handler(array(
				$this,
				"handler_open"
			), array(
				$this,
				"handler_close"
			), array(
				$this,
				"handler_read"
			), array(
				$this,
				"handler_write"
			), array(
				$this,
				"handler_destroy"
			), array(
				$this,
				"handler_gc"
			));
		}
		
		/* ------------------------------------------------------------------ */
		
		public function handler_open() {
			$this->memcache = new Memcache();
			foreach (explode(',', Registry::getConfig()->getVar('session_server')) as $temp) {
				list($name, $port) = explode(':', trim($temp) . ':11211');
				$this->memcache->addServer($name, $port);
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function handler_close() {
			if ($this->memcache) {
				$this->memcache->close();
				$this->memcache = null;
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function handler_read($id) {
			return $this->memcache->get('session.' . $id);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function handler_write($id, $data) {
			if ($data) {
				$this->memcache->set('session.' . $id, $data, preg_match('/(true|yes|on|1)/i', Registry::getConfig()->getVar('session_compress')) ? 1 : 0, Registry::getConfig()->getVar('session_lifetime'));
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function handler_destroy($id) {
			$this->memcache->delete('session.' . $id);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function handler_gc() {
		}
		
		/* ------------------------------------------------------------------ */
		
		
	}
	
?>