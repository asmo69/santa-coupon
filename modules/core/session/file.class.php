<?php
	
	class Session_File extends Session {
		
		/* ------------------------------------------------------------------ */
		
		public function __construct() {
			if (is_dir(ROOT_DIR . '/temp/') == false) {
				@mkdir(ROOT_DIR . '/temp/', 0777);
			}
			if (is_dir(ROOT_DIR . '/temp/session/') == false) {
				@mkdir(ROOT_DIR . '/temp/session/', 0777);
			}
			if (is_readable(ROOT_DIR . '/temp/session/') == false) {
				throw new Exception_File_NotReadable('File is not readable - ' . ROOT_DIR . '/temp/session/');
			}
			if (is_writable(ROOT_DIR . '/temp/session/') == false) {
				throw new Exception_File_NotWritable('File is not writable - ' . ROOT_DIR . '/temp/session/');
			}
			
			session_set_save_handler(array(
				$this,
				"handler_open"
			), array(
				$this,
				"handler_close"
			), array(
				$this,
				"handler_read"
			), array(
				$this,
				"handler_write"
			), array(
				$this,
				"handler_destroy"
			), array(
				$this,
				"handler_gc"
			));
		}
		
		/* ------------------------------------------------------------------ */
		
		public function __destruct() {
		}
		
		/* ------------------------------------------------------------------ */
		
		public function handler_open() {
		}
		
		/* ------------------------------------------------------------------ */
		
		public function handler_close() {
		}
		
		/* ------------------------------------------------------------------ */
		
		public function handler_read($id) {
			$compress = preg_match('/^(yes|ok|true|on|1)$/i', Registry::getConfig()->getVar('session_compress')) ? 1 : 0;
			if ($compress) {
				return unserialize(gzuncompress(file_get_contents(ROOT_DIR . '/temp/session/' . $id)));
			} else {
				return unserialize(file_get_contents(ROOT_DIR . '/temp/session/' . $id));
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function handler_write($id, $data) {
			if ($data) {
				$compress = preg_match('/^(yes|ok|true|on|1)$/i', Registry::getConfig()->getVar('session_compress')) ? 1 : 0;
				if ($compress) {
					$ret = file_put_contents(ROOT_DIR . '/temp/session/' . $id, gzcompress(serialize($data), 9));
				} else {
					$ret = file_put_contents(ROOT_DIR . '/temp/session/' . $id, serialize($data));
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function handler_destroy($id) {
			if (is_file(ROOT_DIR . '/temp/session/' . $id)) {
				@unlink(ROOT_DIR . '/temp/session/' . $id);
			}
			clearstatcache();
			
			return true;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function handler_gc() {
			$lifetime = intval(Registry::getConfig()->getVar('session_lifetime'));
			foreach (glob(ROOT_DIR . '/temp/session/*') as $file) {
				if (filemtime($file) + $lifetime < time()) {
					@unlink($file);
				}
			}
			clearstatcache();
			
			return true;
		}
		
		/* ------------------------------------------------------------------ */
		
		
	}
	
?>