<?php
	
	/**
	 * Cache
	 *
	 * The Cache class is the abstract base class from which all
	 * Cache's should inherit. A Cache class is used by the DAO,
	 * Controller and Action classes to store and retrieve data from a
	 * cached location. This data could be individual items of data like
	 * string and numbers, larger record sets of data, or even contents
	 * of the entire webpage. When used properly, Cache classes can
	 * significantly reduce the processing required to build the outout
	 * data.
	 *
	 * @since 2013-01-02
	 * @abstract
	 */
	
	abstract class Cache {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * create
		 *
		 * This static function is used as a factory method for creating
		 * different types of Cache subclasses. The function will check
		 * that the specified class exists, is named correctly and is a
		 * valid subclass of Cache before creating an instance.
		 *
		 * e.g.
		 *	$cache = Cache::create('memcache');
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Cache
		 */
		
		public static function create($type) {
			if ($type) {
				$class = 'Cache_' . preg_replace('/[^a-z0-9_]/i', '', str_replace('/', '_', $type));
				if (class_exists($class) && is_subclass_of($class, 'Cache')) {
					return new $class();
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setVar
		 *
		 * This function is used to store a name and value key pair of
		 * data into the cache.
		 *
		 * i.e.
		 *	$cache->setVar('fruit', array('apple', 'orange', 'pear'));
		 *
		 * @access public
		 * @abstract
		 * @param string $name
		 * @param mixed $value
		 */
		
		abstract public function setVar($name, $value);
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getVar
		 *
		 * This function is used to retreive a named key pair of data
		 * from the cache.
		 *
		 * i.e.
		 *	$fruit = $cache->getVar('fruit');
		 *
		 * @access public
		 * @abstract
		 * @param string $name
		 * @return mixed
		 */
		
		abstract public function getVar($name);
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * flush
		 *
		 * This function is used to flush/delete data from the cache.
		 *
		 * i.e.
		 *	$cache->flush('fruits');
		 *
		 * @access public
		 * @abstract
		 * @param string $pattern
		 */
		
		abstract public function flush($pattern);
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>