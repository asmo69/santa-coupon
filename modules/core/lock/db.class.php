<?php
	
	/**
	 * Lock_DB
	 *
	 * The Lock_DB class implements named locks using the database.
	 *
	 * @since 2013-01-02
	 * @abstract
	 */
	
	
	class Lock_DB extends Lock {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * open
		 *
		 * This function attempts to game exclusive access to the named
		 * lock resource. The function will timeout after a specified
		 * number of seconds and return a value of 'false'.
		 *
		 * @access public
		 * @param string $name
		 * @param int $timeout
		 * @return boolean
		 */
		
		public function open($name, $timeout = 1) {
			$db   = Registry::getDB(DB::READ_WRITE);
			$sql  = sprintf("SELECT GET_LOCK('LOCK_%s', '%s')", preg_replace('/[^a-z0-9]/i', '_', $name), intval($timeout));
			$temp = array_values($db->query($sql)->fetch());
			return $temp[0] ? true : false;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * close
		 *
		 * This function attempts to game release access to the named
		 * lock resource.
		 *
		 * @access public
		 * @param string $name
		 * @return boolean
		 */
		
		public function close($name) {
			$db  = Registry::getDB(DB::READ_WRITE);
			$sql = sprintf("SELECT RELEASE_LOCK('LOCK_%s')", preg_replace('/[^a-z0-9]/i', '_', $name));
			$db->query($sql);
			return true;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>