<?php
	
	/**
	 * Lang
	 *
	 * The Lang class is the abstract base class from which all
	 * Lang's should inherit.
	 *
	 * @since 2013-05-28
	 * @abstract
	 */
	
	abstract class Lang implements Iterator {
		
		/* ------------------------------------------------------------------ */
		
		protected $data = array();
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * create
		 *
		 * This static function is used as a factory method for creating
		 * different types of Lang subclasses. The function will check
		 * that the specified class exists, is named correctly and is a
		 * valid subclass of Lang before creating an instance.
		 *
		 * e.g.
		 *	$lang = Lang::create('file');
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Lang
		 */
		
		public static function create($type = '') {
			if ($type) {
				$class = 'Lang_' . preg_replace('/[^a-z0-9_]/i', '', str_replace('/', '_', $type));
				if (class_exists($class) && is_subclass_of($class, 'Lang')) {
					return new $class();
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setVar
		 *
		 * This function is used to store a name and value key pair of
		 * data into the language.
		 *
		 * i.e.
		 *	$lang->setVar('mode', $_GET['mode']);
		 *
		 * @access public
		 * @param mixed $value
		 */
		
		public function setVar() {
			$args = func_get_args();
			if (count($args) >= 2) {
				$name                          = implode('_', array_slice($args, 0, -1));
				$value                         = $args[count($args) - 1];
				$this->data[strtoupper($name)] = $value;
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getVar
		 *
		 * This function is used to retrive a value from the language.
		 *
		 * i.e.
		 *	$mode = $lang->getVar('mode');
		 *
		 * @access public
		 * @return mixed
		 */
		
		public function getVar() {
			$args = func_get_args();
			$name = implode('_', $args);
			if (array_key_exists(strtoupper($name), $this->data)) {
				return $this->data[strtoupper($name)];
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function rewind() {
			ksort($this->data);
			return reset($this->data);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function current() {
			return current($this->data);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function key() {
			return key($this->data);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function next() {
			return next($this->data);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function valid() {
			return key($this->data) !== null;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>