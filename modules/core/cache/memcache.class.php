<?php
	
	class Cache_Memcache extends Cache {
		
		/* ------------------------------------------------------------------ */
		
		protected $memcache = array();
		
		/* ------------------------------------------------------------------ */
		
		public function __construct() {
			if (class_exists('Memcache') == false) {
				throw new Exception_Library_NotExists('Memcache Does Not Exist');
			}
			foreach (explode(',', Registry::getConfig()->getVar('cache_server')) as $temp) {
				list($name, $port) = explode(':', trim($temp) . ':11211');
				$temp = new Memcache();
				$temp->addServer($name, $port);
				$this->memcache[] = $temp;
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setVar($name, $value) {
			$ret      = false;
			$name     = strtolower(preg_replace('/[^0-z0-9\.]/i', '_', $name));
			$compress = preg_match('/^(yes|ok|true|on|1)$/i', Registry::getConfig()->getVar('cache_compress')) ? 2 : 0;
			$lifetime = Registry::getConfig()->getVar('cache_lifetime') ? intval(Registry::getConfig()->getVar('cache_lifetime')) : 60 * 60;
			$ret      = $this->memcache->set($name, $value, $compress, $lifetime);
			
			$keys = $this->memcache->get('__CACHE_KEYS__');
			if (empty($keys)) {
				$keys = array();
			}
			if (in_array($name, $keys) === false) {
				$keys[] = $name;
			}
			$this->memcache->set('__CACHE_KEYS__', $keys);
			
			return $ret;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getVar($name) {
			$ret  = false;
			$name = strtolower(preg_replace('/[^0-z0-9\.]/i', '_', $name));
			$ret  = $this->memcache->get($name);
			
			return $ret;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function flush($pattern = '*') {
			$keys = $this->memcache->get('__CACHE_KEYS__');
			if (empty($keys)) {
				$keys = array();
			}
			$pattern = str_replace('*', '(.*?)', str_replace('.', '\.', $pattern));
			$temp    = array();
			foreach ($keys as $key) {
				if (preg_match('/^' . $pattern . '/i', $key)) {
					$this->memcache->delete($key);
				} else {
					$temp[] = $key;
				}
			}
			$this->memcache->set('__CACHE_KEYS__', $temp);
		}
		
		/* ------------------------------------------------------------------ */
		
		
	}
	
?>