<?php
	
	class Cache_File extends Cache {
		
		/* ------------------------------------------------------------------ */
		
		public function __construct() {
			if (is_dir(ROOT_DIR . '/temp/') == false) {
				@mkdir(ROOT_DIR . '/temp/', 0777);
			}
			if (is_dir(ROOT_DIR . '/temp/cache/') == false) {
				@mkdir(ROOT_DIR . '/temp/cache/', 0777);
			}
			if (is_readable(ROOT_DIR . '/temp/cache/') == false) {
				throw new Exception_File_NotReadable('File is not readable - ' . ROOT_DIR . '/temp/cache/');
			}
			if (is_writable(ROOT_DIR . '/temp/cache/') == false) {
				throw new Exception_File_NotWritable('File is not writable - ' . ROOT_DIR . '/temp/cache/');
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setVar($name, $value) {
			$ret      = false;
			$filename = strtolower(preg_replace('/[^0-z0-9\.]/i', '_', $name));
			$compress = preg_match('/^(yes|ok|true|on|1)$/i', Registry::getConfig()->getVar('cache_compress')) ? 1 : 0;
			$lifetime = Registry::getConfig()->getVar('cache_lifetime') ? intval(Registry::getConfig()->getVar('cache_lifetime')) : 60 * 60;
			if (is_writable(ROOT_DIR . '/temp/cache/')) {
				if ($compress) {
					$ret = file_put_contents(ROOT_DIR . '/temp/cache/' . $filename . '.cache', gzcompress(serialize($value), 9));
				} else {
					$ret = file_put_contents(ROOT_DIR . '/temp/cache/' . $filename . '.cache', serialize($value));
				}
			}
			
			return $ret;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getVar($name) {
			
			$ret      = false;
			$filename = strtolower(preg_replace('/[^0-z0-9\.]/i', '_', $name));
			$compress = preg_match('/^(yes|ok|true|on|1)$/i', Registry::getConfig()->getVar('cache_compress')) ? 1 : 0;
			$lifetime = Registry::getConfig()->getVar('cache_lifetime') ? intval(Registry::getConfig()->getVar('cache_lifetime')) : 60 * 60;
			if (is_readable(ROOT_DIR . '/temp/cache/' . $filename . '.cache') && (filemtime(ROOT_DIR . '/temp/cache/' . $filename . '.cache') >= time() - $lifetime)) {
				if ($compress) {
					$ret = unserialize(gzuncompress(file_get_contents(ROOT_DIR . '/temp/cache/' . $filename . '.cache')));
				} else {
					$ret = unserialize(file_get_contents(ROOT_DIR . '/temp/cache/' . $filename . '.cache'));
				}
			}
			
			return $ret;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function flush($pattern = '*') {
			@array_map('unlink', glob(ROOT_DIR . '/temp/cache/' . str_replace('/', '', $pattern)));
			clearstatcache();
		}
		
		/* ------------------------------------------------------------------ */
		
		
	}
	
?>