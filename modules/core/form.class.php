<?php
	
	/**
	 * Form
	 *
	 * The Form class provides a warpper around the default GET, POST and
	 * FILES functionality of PHP, allowing them to be treated in a more
	 * consistent manner with objects like Cookie, Session, Cache, etc.
	 *
	 * @since 2013-01-02
	 */
	
	class Form implements Iterator {
		
		/* ------------------------------------------------------------------ */
		
		protected $data = array();
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * create
		 *
		 * This static function is used as a factory method for creating
		 * different types of Form subclasses. The function will check
		 * that the specified class exists, is named correctly and is a
		 * valid subclass of Form before creating an instance. If a
		 * suitable class cannot be found then a default Form class
		 * will be created.
		 *
		 * e.g.
		 *	$form = Form::create();
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Form
		 */
		
		public static function create($type = '') {
			if ($type) {
				$class = 'Form_' . preg_replace('/[^a-z0-9_]/i', '', str_replace('/', '_', $type));
				if (class_exists($class) && is_subclass_of($class, 'Form')) {
					return new $class();
				}
			}
			return new Form();
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * __construct
		 *
		 * This function copies all of the values from the default GET,
		 * POST and FILES variables of PHP into itself. The code also
		 * makes sure to strip any slashes on the values, making data
		 * sanitisation less of a hassle.
		 *
		 * @access public
		 */
		
		public function __construct() {
			foreach (array_map_recursive('stripslashes', $_POST) as $name => $item) {
				$this->setVar($name, $item);
			}
			foreach (array_map_recursive('stripslashes', $_GET) as $name => $item) {
				$this->setVar($name, $item);
			}
			foreach ($_FILES as $name => $item) {
				$this->setVar($name, $item);
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setVar
		 *
		 * This function is used to store a name and value key pair of
		 * data into the form.
		 * N.B. Under normal circumstances, you should really be writting
		 * data to a Form but the ability is included for the sake of
		 * completeness.
		 *
		 * i.e.
		 *	$form->setVar('name', 'Steve Brewster');
		 *
		 * @access public
		 * @param string $name
		 * @param mixed $value
		 */
		
		public function setVar($name, $value) {
			$this->data[$name] = is_string($value) ? addslashes($value) : $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getVar
		 *
		 * This function is used to retrive a value from the form. The
		 * function also allows a default value to be specified.
		 *
		 * i.e.
		 *	$name = $form->getVar('name', 'Joe Bloggs');
		 *
		 * @access public
		 * @param string $name
		 * @param string $default
		 * @return mixed
		 */
		
		public function getVar($name, $default = null) {
			if (array_key_exists($name, $this->data)) {
				return is_string($this->data[$name]) ? stripslashes($this->data[$name]) : $this->data[$name];
			} else {
				return $default;
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getMethod
		 *
		 * This function returns the method of the request (GET, POST, PUT, DELETE)
		 *
		 * i.e.
		 *	$method = $form->getMethod();
		 *
		 * @access public
		 * @return string
		 */
		
		public function getMethod() {
			return array_key_exists('REQUEST_METHOD', $_SERVER) ? strtoupper($_SERVER['REQUEST_METHOD']) : 'GET';
		}
		
		/* ------------------------------------------------------------------ */
		
		public function rewind() {
			ksort($this->data);
			return reset($this->data);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function current() {
			return current($this->data);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function key() {
			return key($this->data);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function next() {
			return next($this->data);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function valid() {
			return key($this->data) !== null;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>