<?php
	
	/**
	 * Plugin_HTML_Tidy
	 *
	 * This Plugin class provides functionality for tidying the HTML
	 * output of the application, making it nicely indented and easier
	 * to read.
	 *
	 * i.e.
	 * 	<html>
	 * 	<head>
	 * 	<title>Test Page</title>
	 *	</head>
	 * 	<body>
	 * 	<div>Hello World</div>
	 *	</body>
	 * 	</html>
	 *
	 * The above code when cleaned would result in...
	 *
	 * 	<html>
	 * 		<head>
	 * 			<title>Test Page</title>
	 *		</head>
	 * 		<body>
	 * 			<div>Hello World</div>
	 *		</body>
	 * 	</html>
	 *
	 * N.B. This class can be especially useful when developing using
	 * template parsers like Smarty, Twig and PHPTal since the HTML code
	 * they write can often be badly indented and aligned.
	 *
	 * @since 2013-08-05
	 */
	
	class Plugin_HTML_Tidy extends Plugin {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * The execute() function is automatically called by the
		 * Controller class.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$source = $application->getOutput();
			
			$map = array();
			if (preg_match_all('/<script(.*?)>(.*?)<\/script>/is', $source, $matches, PREG_SET_ORDER)) {
				foreach ($matches as $match) {
					$source = str_replace($match[0], '<!-- ' . md5($match[0]) . ' -->', $source);
					$map[]  = $match;
				}
			}
			if (preg_match_all('/<style(.*?)>(.*?)<\/style>/is', $source, $matches, PREG_SET_ORDER)) {
				foreach ($matches as $match) {
					$source = str_replace($match[0], '<!-- ' . md5($match[0]) . ' -->', $source);
					$map[]  = $match;
				}
			}
			if (preg_match_all('/<pre(.*?)>(.*?)<\/pre>/is', $source, $matches, PREG_SET_ORDER)) {
				foreach ($matches as $match) {
					$source = str_replace($match[0], '<!-- ' . md5($match[0]) . ' -->', $source);
					$map[]  = $match;
				}
			}
			if (preg_match_all('/<textarea(.*?)>(.*?)<\/textarea>/is', $source, $matches, PREG_SET_ORDER)) {
				foreach ($matches as $match) {
					$source = str_replace($match[0], '<!-- ' . md5($match[0]) . ' -->', $source);
					$map[]  = $match;
				}
			}
			
			$source = str_replace(chr(9), '', $source);
			while (strpos($source, '  ') !== false) {
				$source = str_replace('  ', ' ', $source);
			}
			$source = str_replace(' />', '/>', $source);
			$source = str_replace(' >', '>', $source);
			$source = str_replace('><', ">\n<", $source);
			$source = str_replace('>', ">\n", $source);
			$source = str_replace('<', "\n<", $source);
			
			$temp   = explode("\n", $source);
			$source = array();
			
			$source = '';
			$indent = 0;
			foreach ($temp as $line) {
				$line = trim($line);
				
				if (strlen($line)) {
					if (substr($line, 0, 2) == '<?' || substr($line, 0, 2) == '<!') {
						$source .= sprintf("%s%s\n", str_repeat("   ", $indent), $line);
					} elseif (preg_match('%^<(.*?)/>$%', $line)) {
						$source .= sprintf("%s%s\n", str_repeat("   ", $indent), $line);
					} elseif (preg_match('%^</(.*?)>$%', $line)) {
						$indent = $indent > 0 ? $indent - 1 : 0;
						$source .= sprintf("%s%s\n", str_repeat("   ", $indent), $line);
					} elseif (preg_match('%^<(.*?)>$%', $line)) {
						$source .= sprintf("%s%s\n", str_repeat("   ", $indent), $line);
						$indent++;
					} else {
						$source .= sprintf("%s%s\n", str_repeat("   ", $indent), $line);
					}
				}
				
			}
			
			foreach ($map as $item) {
				$source = str_replace('<!-- ' . md5($item[0]) . ' -->', $item[0], $source);
			}
			
			$application->setOutput($source);
		}
		
		/* ------------------------------------------------------------------ */
		
		
	}
	
?>