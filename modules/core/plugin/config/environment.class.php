<?php
	
	/**
	 * Plugin_Config_Environment
	 *
	 * This Plugin class provides functionality for accessing variables
	 * defined in the shell environment and copying them into the Config
	 * class. This allows for a more private level of site by site
	 * config if you dont want to specify everything in the usual config
	 * files.
	 *
	 * i.e.
	 * 	SET CONFIG_DATABASE_RW_SERVER "192.168.0.10"
	 *
	 * The above would override the config values 'database_rw_server'
	 * value.
	 *
	 * @since 2013-08-05
	 */
	
	class Plugin_Config_Environment extends Plugin {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * The execute() function is automatically called by the
		 * Controller class.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			// Copy config values from the environment variables.
			foreach ($_ENV as $name => $value) {
				if (strcasecmp(substr($name, 0, 7), 'CONFIG_') == 0) {
					Registry::getConfig()->setVar(substr($name, 7), $_ENV[$name]);
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>