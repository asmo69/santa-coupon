<?php
	
	/**
	 * Plugin_Template_Dynamic
	 *
	 * This Plugin class provides template parser independant
	 * functionality for allowing dynamic areas of HTML within an
	 * otherwise cached page by specifiying a <dynamic> tag.
	 *
	 * For instance the TV product page of an online shop maye be cached
	 * on the server so that each time a customer looks at the page they
	 * do not need to keep loading data from the database. However, the
	 * shopping cart area of the page needs to remain dynamic so that it
	 * can be updated with the users current basket and total price. The
	 *  can be achieved by declaring the area as 'dynamic'.
	 *
	 * i.e.
	 * 	<html>
	 * 		<head>
	 * 			<title>Test Page</title>
	 *		</head>
	 * 		<body>
	 * 			<div id="products">
	 * 				Sony 32" Flatscreen TV - £300
	 * 			</div>
	 * 			<div id="basket">
	 * 				<dynamic>
	 * 					...
	 * 					Logic to display bask here
	 * 					...
	 * 				</dynamic>
	 * 			</div>
	 *		</body>
	 * 	</html>
	 *
	 * @since 2013-08-05
	 */
	
	class Plugin_Template_Dynamic extends Plugin {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * The execute() function is automatically called by the
		 * Controller class.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$source = $application->getOutput();
			
			// If the HTML contains dynamic tags then parse them.
			if (preg_match_all('/<dynamic(.*?)>(.*)<\/dynamic(.*?)>/is', $source, $matches, PREG_SET_ORDER)) {
				foreach ($matches as $match) {
					$source = str_replace_once($match[0], Registry::getParser()->fetch('string://' . $match[2]), $source);
				}
			}
			
			$application->setOutput($source);
		}
		
		/* ------------------------------------------------------------------ */
		
		
	}
	
?>