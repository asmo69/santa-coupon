<?php
	
	/**
	 * Plugin_Template_Include
	 *
	 * This Plugin class provides template parser independant
	 * functionality for allowing templates to be parsed and included
	 * within an otherwise cached page by specifiying a <include> tag.
	 *
	 * For instance the TV product page of an online shop maye be cached
	 * on the server so that each time a customer looks at the page they
	 * do not need to keep loading data from the database. However, the
	 * shopping cart area of the page needs to remain dynamic so that it
	 * can be updated with the users current basket and total price. The
	 *  can be achieved by declaring the area as 'dynamic'.
	 *
	 * i.e.
	 * 	<html>
	 * 		<head>
	 * 			<title>Test Page</title>
	 *		</head>
	 * 		<body>
	 * 			<div id="products">
	 * 				Sony 32" Flatscreen TV - £300
	 * 			</div>
	 * 			<div id="basket">
	 * 				<include file="includes/basket.tpl" />
	 * 			</div>
	 *		</body>
	 * 	</html>
	 *
	 * @since 2013-08-05
	 */
	
	class Plugin_Template_Include extends Plugin {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * The execute() function is automatically called by the
		 * Controller class.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$source = $application->getOutput();
			
			// If the HTML contains include tags then parse them.
			if (preg_match_all('/<include(\s+)file="(.*?)"(.*?)>/i', $source, $matches, PREG_SET_ORDER)) {
				foreach ($matches as $match) {
					if (is_file(ROOT_DIR . '/' . Registry::getParser()->getTemplates() . '/' . $match[3])) {
						$source = str_replace_once($match[0], Registry::getParser()->fetch($match[3]), $source);
					} else {
						$source = str_replace_once($match[0], '<!-- Unknown: ' . $match[3] . ' -->', $source);
					}
				}
			}
			
			$application->setOutput($source);
		}
		
		/* ------------------------------------------------------------------ */
		
		
	}
	
?>