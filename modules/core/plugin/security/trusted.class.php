<?php
	
	/**
	 * Plugin_Security_Trusted
	 *
	 * This Plugin class provides a basic functionality for implementing
	 * IP based access restrictions.
	 *
	 * @since 2013-08-08
	 */
	
	class Plugin_Security_Trusted extends Plugin {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * check
		 *
		 * This function checks the IP address against another IP address or range of addresses. The values of $against can be in any common IP format.
		 *
		 * i.e.
		 * 	all
		 * 	*
		 * 	*.*.*.*
		 * 	192.168.*.*
		 * 	192.168.0.0-192.168.255.255
		 * 	192.168.0.0/16
		 * 	192.168.0.0/255.255.0.0
		 *
		 * @access protected
		 * @param string $address
		 * @param string $against
		 * @return boolean
		 */
		
		protected function check($address, $against) {
			if (strcasecmp($against, 'all') == 0 || $against == '*') {
				$against = '*.*.*.*';
			}
			if (strpos($against, '*') !== false) {
				$against = str_replace('*', '0', $against) . '-' . str_replace('*', '255', $against);
			}
			if (strpos($against, '-') !== false) {
				$temp        = explode('-', $against);
				$address_dec = (float) sprintf("%u", ip2long($address));
				$low_dec     = (float) sprintf("%u", ip2long($temp[0]));
				$high_dec    = (float) sprintf("%u", ip2long($temp[1]));
				return ($address_dec >= $low_dec) && ($address_dec <= $high_dec);
			}
			
			if (strpos($against, '/') !== false) {
				if (preg_match('#^(\d+\.\d+\.\d+\.\d+)/(\d+)$#', $against, $match)) {
					$range = $match[1];
					$mask  = $match[2];
					
					$address_dec = ip2long($address);
					$range_dec   = ip2long($range);
					$mask_dec    = bindec(str_pad('', $mask, '1') . str_pad('', 32 - $mask, '0'));
					
					return ($address_dec & $mask_dec) == ($range_dec & $mask_dec);
				} elseif (preg_match('#^(\d+\.\d+\.\d+\.\d+)/(\d+\.\d+\.\d+\.\d+)$#', $against, $match)) {
					$range = $match[1];
					$mask  = $match[2];
					
					$address_dec = ip2long($address);
					$range_dec   = ip2long($range);
					$mask_dec    = ip2long($mask);
					
					return ($address_dec & $mask_dec) == ($range_dec & $mask_dec);
				}
			}
			
			return $address == $against;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * The execute() function is automatically called by the
		 * Controller class.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$config = Registry::getConfig();
			
			$address = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0');
			$allow   = array_map('trim', explode(',', $config->getVar('security_trusted_allow')));
			
			$allowed = false;
			foreach ($allow as $temp) {
				if ($temp && $this->check($address, $temp)) {
					$allowed = true;
				}
			}
			
			if ($allowed == false) {
				$application->setStatus(Constants::HTTP_FORBIDDEN);
				$application->done(true);
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>