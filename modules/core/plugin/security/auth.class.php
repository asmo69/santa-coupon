<?php
	
	/**
	 * Plugin_Security_Auth
	 *
	 * This Plugin class provides a basic functionality for implementing
	 * HTTP username/password authentication.
	 *
	 * @since 2013-08-08
	 */
	
	class Plugin_Security_Auth extends Plugin {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * The execute() function is automatically called by the
		 * Controller class.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$config = Registry::getConfig();
			
			$address = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0');
			$passwd  = array_map('trim', explode(',', $config->getVar('security_auth_passwd')));
			$message = trim($config->getVar('security_auth_message'));
			
			$username = isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : '';
			$password = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : '';
			
			$allowed = false;
			if ($username && $password) {
				foreach ($passwd as $temp) {
					$temp = explode(':', $temp, 2);
					if (count($temp) == 2) {
						if (strcasecmp($temp[0], $username) == 0 && strcasecmp($temp[1], $password) == 0) {
							$allowed = true;
						} elseif (strcasecmp($temp[0], $username) == 0 && $temp[1] == md5($password)) {
							$allowed = true;
						}
					}
				}
			}
			
			if ($allowed == false) {
				$application->setStatus(Constants::HTTP_NOT_AUTH);
				$application->setAuthenticate($message ? $message : 'Please Login');
				$application->done(true);
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>