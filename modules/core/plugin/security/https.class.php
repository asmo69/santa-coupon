<?php
	
	/**
	 * Plugin_Security_HTTPS
	 *
	 * This Plugin class provides a functionality for redirecting a user
	 * to a HTTPS connection.
	 *
	 * @since 2013-08-08
	 */
	
	class Plugin_Security_Auth extends Plugin {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * The execute() function is automatically called by the
		 * Controller class.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$url = clone $application->getURL();
			if (strcasecmp($url->getScheme(), 'http') == 0) {
				$url->setScheme('https');
				$application->redirect($url);
				$application->setStatus(Constants::HTTP_MOVED);
				$application->done(true);
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>