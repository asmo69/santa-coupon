<?php
	
	/**
	 * Plugin_Action_Query
	 *
	 * This Plugin class provides a basic functionality for setting the
	 * application action class based on the 'action' field of the query
	 * string or post data.
	 *
	 * i.e.
	 * 	http://site.com/?action=store/products
	 *
	 * The above would result the action being set to Store_Product.
	 *
	 * @since 2013-08-05
	 */
	
	class Plugin_Action_Query extends Plugin {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * The execute() function is automatically called by the
		 * Controller class.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$form = Registry::getForm();
			if ($form->getVar('action')) {
				$application->getController()->setAction($form->getVar('action'));
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		
	}
	
?>