<?php
	
	/**
	 * Plugin_Action_Path
	 *
	 * This Plugin class provides a basic functionality for setting the
	 * application action class based on the path on the URL.
	 *
	 * i.e.
	 * 	http://site.com/store/products/
	 *
	 * The above would result the action being set to Store_Product.
	 *
	 * @since 2013-08-05
	 */
	
	class Plugin_Action_Path extends Plugin {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * The execute() function is automatically called by the
		 * Controller class.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$path = $application->getURL()->getPath();
			
			$application->getController()->setAction(trim($path, '/'));
		}
		
		/* ------------------------------------------------------------------ */
		
		
	}
	
?>