<?php
	
	/**
	 * Plugin_Website_Javascript
	 *
	 * This Plugin class ateempts to add javascript files to the
	 * application. As well as adding common files like default.js,
	 * jquery.js, etc it will also attempt to determine the users
	 * browser and add browser specific javascript files.
	 *
	 * @since 2013-08-05
	 */
	
	class Plugin_Website_Javascript extends Plugin {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * The execute() function is automatically called by the
		 * Controller class.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$x = 0;
			
			if ($application->getController() && is_a($application->getController(), 'Controller_Website')) {
				// Add the jquery javascript (if it exists).
				$application->getController()->addJavascript('jquery.js', $x++);
				
				// Add the jquery.tools javascript (if it exists).
				$application->getController()->addJavascript('jquery.tools.js', $x++);
				
				// Add the jquery.ui javascript (if it exists).
				$application->getController()->addJavascript('jquery.ui.js', $x++);
				
				// Add the default javascript (if it exists).
				$application->getController()->addJavascript('default.js', $x++);
				
				if ($browser = Browser::info()) {
					// Add the platform javascript (if it exists).
					$application->getController()->addJavascript(strtolower($browser['platform']) . '.js', $x++);
					
					// Add the browser javascript (if it exists).
					$application->getController()->addJavascript(strtolower($browser['browser']) . '.js', $x++);
					
					// Add the browser version javascript (if it exists).
					$application->getController()->addJavascript(strtolower($browser['browser']) . '-' . implode('.', array_slice(explode('.', $browser['version']), 0, 2)) . '.js', $x++);
				}
				
				// Add the javascript for this action file (if it exists).
				$application->getController()->addJavascript($application->getController()->getTemplate() . '.js', $x++);
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>