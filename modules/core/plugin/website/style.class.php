<?php
	
	/**
	 * Plugin_Website_Style
	 *
	 * This Plugin class ateempts to add stylesheet files to the
	 * application. As well as adding common files like default.css,
	 * etc it will also attempt to determine the users browser and add
	 * browser specific stylesheets.
	 *
	 * i.e.
	 * 	styles/all/windows.css
	 * 	styles/all/ie.css
	 * 	styles/all/ie-10.css
	 * 	styles/all/ie-10.5.css
	 *
	 * @since 2013-08-05
	 */
	
	class Plugin_Website_Style extends Plugin {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * The execute() function is automatically called by the
		 * Controller class.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			$x = 0;
			
			if ($application->getController() && is_a($application->getController(), 'Controller_Website')) {
				// Check to see if styles exist of any of the different media types.
				$medias = array(
					'screen',
					'tty',
					'tv',
					'projection',
					'handheld',
					'print',
					'braille',
					'aural',
					'all'
				);
				foreach ($medias as $media) {
					// Add the default style (if it exists).
					$application->getController()->addCSS('default.css', $media, $x++);
					
					if ($browser = Browser::info()) {
						// Add the platform style (if it exists).
						$application->getController()->addCSS(strtolower($browser['platform']) . '.css', $media, $x++);
						
						// Add the browser style (if it exists).
						$application->getController()->addCSS(strtolower($browser['browser']) . '.css', $media, $x++);
						
						// Add the browser major version style (if it exists).
						$application->getController()->addCSS(strtolower($browser['browser']) . '-' . implode('.', array_slice(explode('.', $browser['version']), 0, 1)) . '.css', $media, $x++);
						
						// Add the browser minor version style (if it exists).
						$application->getController()->addCSS(strtolower($browser['browser']) . '-' . implode('.', array_slice(explode('.', $browser['version']), 0, 2)) . '.css', $media, $x++);
					}
					
					// Add the style for this action file (if it exists).
					$application->getController()->addJavascript($application->getController()->getTemplate() . '.js', $media, $x++);
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>