<?php
	
	/**
	 * Plugin_Cli
	 *
	 * This Plugin class provides a basic functionality for accessing
	 * values provided on the command line. These values are copied into
	 * the GET, POST array.
	 *
	 * i.e.
	 * 	index.php --name=steve --age=35 --email=steve@email.com
	 *
	 *	results in...
	 *
	 * 	$_GET['name'] = 'steve';
	 * 	$_GET['age'] = '35';
	 * 	$_GET['email'] = 'steve@email.com';
	 * 	$_POST['name'] = 'steve';
	 * 	$_POST['age'] = '35';
	 * 	$_POST['email'] = 'steve@email.com';
	 *
	 * @since 2013-08-08
	 */
	
	class Plugin_Cli extends Plugin {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * The execute() function is automatically called by the
		 * Controller class.
		 *
		 * @access public
		 * @param Application $application
		 */
		
		public function execute(Application $application) {
			global $argv;
			
			if (php_sapi_name() == 'cli') {
				for ($x = 1; $x < count($argv); $x++) {
					if (substr($argv[$x], 0, 2) == '--') {
						if (strpos($argv[$x], '=') !== false) {
							list($name, $value) = explode('=', substr($argv[$x], 2), 2);
						} else {
							$name  = substr($argv[$x], 2);
							$value = 1;
						}
						$_GET[$name]     = $value;
						$_POST[$name]    = $value;
						$_REQUEST[$name] = $value;
					}
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		
	}
	
?>