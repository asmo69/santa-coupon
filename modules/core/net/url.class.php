<?php
	
	class Net_URL {
		
		/* ------------------------------------------------------------------ */
		
		protected $data = null;
		
		/* ------------------------------------------------------------------ */
		
		public function __construct($url = null) {
			if ($url) {
				$this->setURL($url);
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getString() {
			return self::implode($this->data);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setURL($url) {
			$this->data = self::explode($url);
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getScheme() {
			return $this->data['scheme'];
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setScheme($scheme) {
			$this->data['scheme'] = $scheme;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getHost() {
			return $this->data['host'];
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setHost($host) {
			$this->data['host'] = $host;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getPort() {
			return $this->data['port'];
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setPort($port) {
			$this->data['port'] = $port;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getUser() {
			return $this->data['user'];
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setUser($user) {
			$this->data['user'] = $user;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getPass() {
			return $this->data['pass'];
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setPass($pass) {
			$this->data['pass'] = $pass;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getPath() {
			return $this->data['path'];
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setPath($path) {
			$this->data['path'] = $path;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getFragment() {
			return $this->data['fragment'];
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setFragment($fragment) {
			$this->data['fragment'] = $fragment;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function getQuery($name = null) {
			if ($name === null) {
				return $this->data['query'];
			} else {
				return array_key_exists($name, $this->data['query']) ? $this->data['query'][$name] : null;
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		public function setQuery($name, $value) {
			$this->data['query'][$name] = $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function unsetQuery($name) {
			unset($this->data['query'][$name]);
		}
		
		/* ------------------------------------------------------------------ */
		
		public static function explode($url) {
			$retval = @parse_url($url);
			
			if (array_key_exists('scheme', $retval) == false) {
				$retval['scheme'] = array_key_exists('HTTPS', $_SERVER) ? 'https' : 'http';
			}
			if (array_key_exists('host', $retval) == false) {
				$retval['host'] = array_key_exists('HTTP_HOST', $_SERVER) ? $_SERVER['HTTP_HOST'] : 'localhost.localdomain';
			}
			if (array_key_exists('port', $retval) == false) {
				$retval['port'] = '80';
			}
			if (array_key_exists('user', $retval) == false) {
				$retval['user'] = '';
			}
			if (array_key_exists('pass', $retval) == false) {
				$retval['pass'] = '';
			}
			if (array_key_exists('path', $retval) == false) {
				$retval['path'] = '/';
			}
			if (array_key_exists('query', $retval) == false) {
				$retval['query'] = '';
			}
			if (array_key_exists('fragment', $retval) == false) {
				$retval['fragment'] = '';
			}
			
			if (array_key_exists('query', $retval)) {
				parse_str($retval['query'], $retval['query']);
			}
			
			return $retval;
		}
		
		/* ------------------------------------------------------------------ */
		
		public static function implode($parsed) {
			if (!is_array($parsed)) {
				return false;
			}
			
			$retval = '';
			
			if (array_key_exists('scheme', $parsed)) {
				$retval .= $parsed['scheme'] . '://';
			}
			if (array_key_exists('user', $parsed) && $parsed['user']) {
				$retval .= $parsed['user'];
				if (array_key_exists('pass', $parsed) && $parsed['pass']) {
					$retval .= ':' . $parsed['pass'];
				}
				$retval .= '@';
			}
			if (array_key_exists('host', $parsed)) {
				$retval .= $parsed['host'];
			}
			if (array_key_exists('port', $parsed) && $parsed['port'] != 80) {
				$retval .= ':' . $parsed['port'];
			}
			if (array_key_exists('path', $parsed)) {
				$retval .= $parsed['path'];
			}
			if (array_key_exists('query', $parsed) && $parsed['query']) {
				$retval .= '?' . http_build_query($parsed['query']);
			}
			if (array_key_exists('fragment', $parsed) && $parsed['fragment'] != '') {
				$retval .= '#' . $parsed['fragment'];
			}
			
			return $retval;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function fetch($post = null, $options = null) {
			$data = $this->data;
			unset($data['user'], $data['pass']);
			$url = self::implode($data);
			
			if ($ch = curl_init($url)) {
				if ($options) {
					curl_setopt_array($ch, $options);
				}
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
				curl_setopt($ch, CURLINFO_HEADER_OUT, true);
				if ($this->data['user'] !== null && $this->data['pass'] !== null) {
					curl_setopt($ch, CURLOPT_USERPWD, $this->data['user'] . ':' . $this->data['pass']);
				}
				if ($post) {
					curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
				}
				$response = curl_exec($ch);
				$info     = curl_getinfo($ch);
				curl_close($ch);
				
				$info['response'] = $response;
				return (object) $info;
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>