<?php
	
	
	class Net_Email {
		private $to;
		private $CC;
		private $BCC;
		private $headers;
		private $subject;
		private $body;
		private $html;
		private $attachments;
		private $username;
		private $secure;
		
		public function __construct($to = "", $subject = "", $body = "", $headers = "") {
			$this->to      = array();
			$this->CC      = array();
			$this->BCC     = array();
			$this->headers = array();
			$this->setBody($body);
			$this->setSubject($subject);
			$this->attachments = array();
			$this->html        = "";
			
			preg_match_all("/(^|[,;]\s*)(.+?)( <(.+?)>\s*|\s*[,;]|\s*$)/", $to, $matches);
			for ($i = 0; $i < count($matches[2]); $i++) {
				if ($matches[4][$i] == "")
					$this->to[] = $matches[2][$i];
				else
					$this->to[] = $matches[2][$i] . " <" . $matches[4][$i] . ">";
			}
			
			for ($i = 0; $i < count($this->to); $i++)
				$this->to[$i] = trim($this->to[$i]);
			
			if ($headers != "") {
				if (!is_array($headers)) {
					$headers = explode("\r\n", $headers);
				}
				
				foreach ($headers as $header) {
					if (!preg_match("/^(.*?): (.*?)(\r?\n)?$/", $header, $matches))
						continue;
					$this->headers[$matches[1]] = $matches[2];
				}
			}
		}
		
		static public function validateAddress($emailAddress) {
			return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $emailAddress);
		}
		
		public function setSecure($id) {
			$this->secure = $id;
		}
		
		public function addRecipient($address, $display = "") {
			if ($display == "")
				$this->to[] = $address;
			else
				$this->to[] = "\"" . $display . "\" <" . $address . ">";
		}
		
		public function addCC($address, $display = "") {
			if ($display == "")
				$this->CC[] = $address;
			else
				$this->CC[] = "\"" . $display . "\" <" . $address . ">";
		}
		
		public function addBCC($address, $display = "") {
			if ($display == "")
				$this->BCC[] = $address;
			else
				$this->BCC[] = "\"" . $display . "\" <" . $address . ">";
		}
		
		public function setFrom($address, $display = "") {
			if ($display == "")
				$this->headers["From"] = $address;
			else
				$this->headers["From"] = "\"" . $display . "\" <" . $address . ">";
		}
		
		public function setSubject($subject) {
			$this->subject = $subject;
		}
		
		public function setBody($body) {
			$body = preg_replace("/\r?\n/", "\r\n", $body);
			$body = explode("\r\n", $body);
			for ($i = 0; $i < count($body); $i++)
				$body[$i] = wordwrap($body[$i], 74, "\r\n", true);
			
			$this->body = implode("\r\n", $body);
		}
		
		public function setHTML($html) {
			$this->html = $html;
		}
		
		public function addAttachment($name, $contents, $mime = false) {
			if (!$mime)
				$mime = $this->getMime($name);
			$encodedContents     = chunk_split(base64_encode($contents));
			$this->attachments[] = array(
				"name" => $name,
				"contents" => $encodedContents,
				"raw_contents" => $contents,
				"mime" => $mime
			);
		}
		
		public function addAttachmentFile($name, $mime = false) {
			if (is_file($name)) {
				if ($fd = fopen($name, "rb")) {
					$contents = fread($fd, filesize($name));
					fclose($fd);
					
					$this->addAttachment(basename($name), $contents, $mime);
				} else {
					throw new Exception("Unable to read from file '" . $name . "'");
				}
			} else {
				throw new Exception("Unable to find file '" . $name . "'");
			}
		}
		
		public function send() {
			$plainText = false;
			
			if (!$this->html && !count($this->attachments))
				$plainText = true;
			
			$mimeBoundary    = "MIMEBOUNDARY" . md5(time());
			$altMimeBoundary = "MIMEBOUNDARY" . md5(time() + 1000);
			
			if (!$plainText) {
				$this->headers["MIME-Version"] = "1.0";
				$this->headers["Content-Type"] = "multipart/mixed; boundary=\"" . $mimeBoundary . "\"";
			}
			
			//Build the headers
			$h = "";
			foreach ($this->headers as $key => $value) {
				$h .= $key . ": " . $value . "\r\n";
			}
			
			if (count($this->CC))
				$h .= "Cc: " . implode(",", $this->CC) . "\r\n";
			if (count($this->BCC))
				$h .= "Bcc: " . implode(",", $this->BCC) . "\r\n";
			
			if ($plainText) {
				$m = $this->body;
			} else {
				$m = "";
				$m .= "This is a multi-part message in MIME format.\r\n";
				$m .= "\r\n";
				$m .= "--" . $mimeBoundary . "\r\n";
				if ($this->html) {
					$m .= "Content-Type: multipart/alternative; boundary=\"" . $altMimeBoundary . "\";\r\n";
					$m .= "Content-Transfer-Encoding: 7bit\r\n";
					$m .= "\r\n";
					
					$m .= "--" . $altMimeBoundary . "\r\n";
					
					$m .= "Content-Type: text/plain; charset=\"iso-8859-1\"\r\n";
					$m .= "Content-Transfer-Encoding: 7bit\r\n";
					$m .= "\r\n";
					$m .= $this->body;
					$m .= "\r\n";
					
					$m .= "--" . $altMimeBoundary . "\r\n";
					
					$m .= "Content-Type: text/html; charset=\"iso-8859-1\"\r\n";
					$m .= "Content-Transfer-Encoding: 7bit\r\n";
					$m .= "\r\n";
					$m .= wordwrap($this->html, 985, "\r\n", true);
					$m .= "\r\n";
					
					$m .= "--" . $altMimeBoundary . "--\r\n";
				} else {
					$m .= "Content-Type: text/plain; charset=\"iso-8859-1\"\r\n";
					$m .= "Content-Transfer-Encoding: 7bit\r\n";
					$m .= "\r\n";
					$m .= $this->body;
				}
				
				$m .= "\r\n";
				
				//if(count($this->attachments))
				//$m .= "--".$mimeBoundary."\r\n";
				
				for ($i = 0; $i < count($this->attachments); $i++) {
					$filename = $this->attachments[$i]["name"];
					$m .= "--" . $mimeBoundary . "\r\n";
					$m .= "Content-Type: " . $this->attachments[$i]["mime"] . "; name=\"" . $filename . "\"\r\n";
					$m .= "Content-Transfer-Encoding: base64\r\n";
					$m .= "Content-Disposition: attachment;\r\n";
					$m .= " filename=\"" . $filename . "\"\r\n";
					$m .= "\r\n";
					$m .= $this->attachments[$i]["contents"];
					$m .= "\r\n";
				}
				$m .= "--" . $mimeBoundary . "--";
			}
			if ($this->secure) {
				$username = "apache";
				$pgp      = "/usr/local/bin/gpg";
				$command  = 'echo "' . $m . '" | ' . $pgp . ' -a --always-trust --batch --no-secmem-warning -e -u "' . str_replace('"', "", $this->headers["From"]) . '" -r "' . $this->to[0] . '"';
				//printr($command);
				$oldhome  = getEnv("HOME");
				putenv("HOME=/home/$username");
				$result = exec($command, $encrypted, $errorcode);
				putenv("HOME=$oldhome");
				
				$message = implode("\n", $encrypted);
				if (ereg("-----BEGIN PGP MESSAGE-----.*-----END PGP MESSAGE-----", $message))
					$m = $message;
			}
			$mail_name = trim(implode(",", $this->to));
			
			// log activity to trace faults with emailing spreadsheets
			// $mail_text=mail($mail_name, $this->subject, $m, $h);
			// $filename="./mailulog.txt";
			// if (!$handle = fopen($filename, 'a')) {print "Cannot open file ($filename)<br/>";}else
			// {
			// $text=date("F j, Y, g:i:s a")." - ".$_SERVER[SERVER_ADDR]."(has $i attachments) - $mail_text\n";
			//if (fwrite($handle, $text) === FALSE) {print"Cannot write to file ($filename)<br/>";}
			//}
			//fclose($handle);
			
			return mail($mail_name, $this->subject, $m, $h);
		}
		
		public function getMime($file) {
			$file = pathinfo($file);
			$ext  = strtolower($file["extension"]);
			
			$files = array(
				"jpg" => "image/jpeg",
				"jpeg" => "image/jpeg",
				"gif" => "image/gif",
				"png" => "image/png",
				
				"css" => "text/css",
				"htm" => "text/html",
				"html" => "text/html",
				"txt" => "text/plain",
				"csv" => "text/csv",
				"xml" => "text/xml",
				
				"doc" => "application/msword",
				"xls" => "application/vnd.ms-excel",
				"ppt" => "application/vnd.ms-powerpoint"
			);
			
			if (array_key_exists($ext, $files))
				return $files[$ext];
			
			return "application/octet-stream";
		}
		
		
		
		public function parse($str) {
			//Read in a raw email and extract the data
			//Reset the object
			$this->Email();
			
			//Return the parts in case we need to do something special with them
			return $this->parseParts($str);
		}
		
		public function parseParts($str) {
			$part = array();
			
			preg_match("/^(.*?)\r?\n\r?\n(.*)$/s", $str, $matches);
			$part["head"] = $matches[1];
			$part["head"] = preg_replace("/\r?\n\s+/", " ", $part["head"]);
			
			$part["headers"] = array();
			$part["body"]    = trim($matches[2]);
			
			$mime = false;
			if (preg_match("/boundary=(\"?)(.*?)(\\1)\s/i", $part["head"] . " ", $matches))
				$mime = $matches[2];
			
			$tmp = preg_split("/\r?\n/", $part["head"]);
			array_map("trim", $tmp);
			foreach ($tmp as $line) {
				if (!$line)
					continue;
				$line                                        = explode(":", $line, 2);
				$part["headers"][strtolower(trim($line[0]))] = trim($line[1]);
			}
			
			//Set the email parameters
			if ($part["headers"]["subject"] && !$this->subject)
				$this->subject = $part["headers"]["subject"];
			if ($part["headers"]["bcc"] && !$this->bcc)
				$this->bcc = explode(",", $part["headers"]["bcc"]);
			if ($part["headers"]["cc"] && !$this->cc)
				$this->cc = explode(",", $part["headers"]["cc"]);
			if ($part["headers"]["to"] && !$this->to) {
				$this->to = explode(",", $part["headers"]["to"]);
			}
			if (!$this->headers)
				$this->headers = $part["headers"];
			
			//Decode the body if it's encoded
			$part["body_decoded"] = $part["body"];
			if (preg_match("/quoted-printable/i", $part["headers"]["content-transfer-encoding"]))
				$part["body_decoded"] = imap_qprint($part["body_decoded"]);
			if (preg_match("/base64/i", $part["headers"]["content-transfer-encoding"]))
				$part["body_decoded"] = base64_decode(preg_replace("/\s/", "", $part["body_decoded"]));
			
			$part["parts"] = array();
			
			if ($mime) {
				//Split the body based on the mime boundary
				$tmp = explode("--" . $mime, $part["body"]);
				array_map("trim", $tmp);
				if (trim($tmp[count($tmp) - 1]) == "--")
					array_pop($tmp);
				
				foreach ($tmp as $newStr) {
					if (!strlen($newStr))
						continue;
					$part["parts"][] = $this->parseParts($newStr);
				}
			}
			
			if (preg_match("/attachment/i", $part["headers"]["content-disposition"])) {
				//Get the filename
				$filename = "unknown";
				
				if (preg_match("/filename=(\"?)(.*)(\\1)/i", $part["headers"]["content-disposition"], $matches))
					$filename = $matches[2];
				elseif (preg_match("/name=(\"?)(.*)(\\1)/i", $part["headers"]["content-type"], $matches))
					$filename = $matches[2];
				
				
				//Get the content type
				$type = false;
				if (preg_match("/[^\s]+\/[^\s;]+/", $part["headers"]["content-type"], $matches))
					$type = $matches[0];
				
				$this->addAttachment($filename, $part["body_decoded"], $type);
			} elseif (preg_match("/text\/plain/i", $part["headers"]["content-type"]) && !$this->body)
				$this->body = $part["body_decoded"];
			elseif (preg_match("/text\/html/i", $part["headers"]["content-type"]) && !$this->html)
				$this->html = $part["body_decoded"];
			
			return $part;
		}
	}