<?php
	
	/**
	 * Log_Email
	 *
	 * This class is used to log information to a messages log. This can
	 * be used to monitor various messages and message levels written by
	 * the application without them affecting the user experience.
	 *
	 * @since 2013-01-02
	 */
	
	class Log_Email extends Log {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * write
		 *
		 * This function is used to write a message to the log file if
		 * the importance level of the message is greater than or equal
		 * to the specific threshold.
		 *
		 * i.e.
		 *	$log = Log::create('email');
		 *	$log->init(2);
		 *	$log->write('This message will be written to the file since 3 > 2', 3);
		 *	$log->write('This message will NOT be written to the file since 1 < 2', 1);
		 *
		 * @param string $value
		 * @param string $level
		 */
		
		public function write($value, $level = null) {
			if ($level === null || $level >= $this->level) {
				if (is_string($value) == false) {
					$value = print_r($value, true);
				}
				
				if (floatval(phpversion()) >= 5.4) {
					$trace = debug_backtrace(false, 2);
				} else {
					$trace = debug_backtrace(false);
				}
				
				$data   = array();
				$data[] = date('Y-m-d H:i:s');
				switch ($level) {
					case 1:
						$data[] = 'DEBUG';
						break;
					case 2:
						$data[] = 'NOTICE';
						break;
					case 3:
						$data[] = 'WARNING';
						break;
					default:
						$data[] = 'ERROR';
						break;
				}
				$data[] = php_sapi_name();
				if (php_sapi_name() != 'cli') {
					$data[] = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '';
					$data[] = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
					$data[] = isset($_SERVER['REMOTE_PORT']) ? $_SERVER['REMOTE_PORT'] : '';
					$data[] = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
					$data[] = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
				}
				$data[] = $trace[0]['file'];
				$data[] = $trace[0]['line'];
				
				$email = new Net_Email();
				$email->addRecipient(Registry::getConfig()->getVar('log_email_address'), Registry::getConfig()->getVar('log_email_name'));
				$email->setSubjet("[" . implode('] [', $data) . "]");
				$email->setBody(trim($value));
				$email->send();
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>