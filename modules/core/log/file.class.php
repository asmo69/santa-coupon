<?php
	
	/**
	 * Log_File
	 *
	 * This class is used to log information to a messages log. This can
	 * be used to monitor various messages and message levels written by
	 * the application without them affecting the user experience.
	 *
	 * @since 2013-01-02
	 */
	
	class Log_File extends Log {
		
		/* ------------------------------------------------------------------ */
		
		public function __construct() {
			if (is_dir(ROOT_DIR . '/temp/') == false) {
				@mkdir(ROOT_DIR . '/temp/', 0777);
			}
			if (is_dir(ROOT_DIR . '/temp/logs/') == false) {
				@mkdir(ROOT_DIR . '/temp/logs/', 0777);
			}
			if (is_readable(ROOT_DIR . '/temp/logs/') == false) {
				throw new Exception_File_NotReadable('File is not readable - ' . ROOT_DIR . '/temp/logs/');
			}
			if (is_writable(ROOT_DIR . '/temp/logs/') == false) {
				throw new Exception_File_NotWritable('File is not writable - ' . ROOT_DIR . '/temp/logs/');
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * write
		 *
		 * This function is used to write a message to the log file if
		 * the importance level of the message is greater than or equal
		 * to the specific threshold.
		 *
		 * i.e.
		 *	$log = Log::create('file');
		 *	$log->init(2);
		 *	$log->write('This message will be written to the file since 3 > 2', 3);
		 *	$log->write('This message will NOT be written to the file since 1 < 2', 1);
		 *
		 * @param string $value
		 * @param string $level
		 */
		
		public function write($value, $level = null) {
			if ($level === null || $level >= $this->level) {
				if (is_string($value) == false) {
					$value = print_r($value, true);
				}
				
				if (floatval(phpversion()) >= 5.4) {
					$trace = debug_backtrace(false, 2);
				} else {
					$trace = debug_backtrace(false);
				}
				
				$data   = array();
				$data[] = date('Y-m-d H:i:s');
				switch ($level) {
					case 1:
						$data[] = 'DEBUG';
						break;
					case 2:
						$data[] = 'NOTICE';
						break;
					case 3:
						$data[] = 'WARNING';
						break;
					default:
						$data[] = 'ERROR';
						break;
				}
				$data[] = php_sapi_name();
				if (php_sapi_name() != 'cli') {
					$data[] = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '';
					$data[] = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
					$data[] = isset($_SERVER['REMOTE_PORT']) ? $_SERVER['REMOTE_PORT'] : '';
					$data[] = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
					$data[] = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
				}
				$data[] = $trace[0]['file'];
				$data[] = $trace[0]['line'];
				
				$filename = ROOT_DIR . '/temp/logs/messages.log';
				file_put_contents($filename, "[" . implode('] [', $data) . "]\n" . trim($value) . "\n\n", FILE_APPEND);
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>