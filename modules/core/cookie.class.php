<?php
	
	/**
	 * Cookie
	 *
	 * The Cookie class provides a warpper around the default cookie
	 * functionality of PHP, allowing them to be treated in a more
	 * consistent manner with objects like Form, Session, Cache, etc.
	 *
	 * @since 2013-01-02
	 */
	
	class Cookie {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * create
		 *
		 * This static function is used as a factory method for creating
		 * different types of Cookie subclasses. The function will check
		 * that the specified class exists, is named correctly and is a
		 * valid subclass of Cookie before creating an instance. If a
		 * suitable class cannot be found then a default Cookie class
		 * will be created.
		 *
		 * e.g.
		 *	$cookie = Cookie::create();
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Cookie
		 */
		
		public static function create($type = '') {
			if ($type) {
				$class = 'Cookie_' . preg_replace('/[^a-z0-9_]/i', '', str_replace('/', '_', $type));
				if (class_exists($class) && is_subclass_of($class, 'Cookie')) {
					return new $class();
				}
			}
			return new Cookie();
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setVar
		 *
		 * This function is used to store a name and value key pair of
		 * data into the cookie. This function also allows the expiry
		 * date, path and domain of the cookie to be set.
		 *
		 * i.e.
		 *	$cookie->setVar('mode', $_GET['mode']);
		 *
		 * @access public
		 * @param string $name
		 * @param mixed $value
		 */
		
		public function setVar($name, $value, $expire = 0, $path = '', $domain = '') {
			$_COOKIE[$name] = $value;
			if (headers_sent() == false) {
				setcookie($name, $value, $expire, $path, $domain);
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getVar
		 *
		 * This function is used to retrive a value from the cookie.
		 *
		 * i.e.
		 *	$mode = $cookie->getVar('mode');
		 *
		 * @access public
		 * @param string $name
		 * @return mixed
		 */
		
		public function getVar($name) {
			if (array_key_exists($name, $_COOKIE)) {
				return $_COOKIE[name];
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * size
		 *
		 * This function returns the number of elemenents which are
		 * contained within the cookies.
		 *
		 * @access public
		 * @return int
		 */
		
		public function size() {
			return count($_COOKIE);
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>