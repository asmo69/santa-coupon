<?php
	
	/**
	 * Action
	 *
	 * Action classes are executed by the Controller classes. The Action
	 * class executed can be determined by the query string, directory
	 * path, or other criteria. The actual criteria depends on the
	 * individual Controller class.
	 *
	 * @since 2013-01-02
	 * @abstract
	 */
	
	abstract class Action {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * create
		 *
		 * This static function is used as a factory method for creating
		 * different types of Action subclasses. The function will check that
		 * the specified class exists, is named correctly and is a valid
		 * subclass of Action before creating an instance.
		 *
		 * e.g.
		 *	$action = Action::create($_GET['action']);
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Action
		 */
		
		public static function create($type) {
			if ($type) {
				$class = 'Action_' . preg_replace('/[^a-z0-9_]/i', '', str_replace('/', '_', $type));
				if (class_exists($class) && is_subclass_of($class, 'Action')) {
					return new $class();
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * This abstract function defines the prototype for the execute()
		 * function. Once a Controller class has created an instance of an
		 * Action class it will the execute() function, passing a reference to
		 * the gloabl Application.
		 *
		 * e.g.
		 *	$action = Action::create($_GET['action']);
		 *	$action->execute($this);
		 *
		 * @access public
		 * @abstract
		 * @param Application $application
		 */
		
		abstract public function execute(Application $application);
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>
