<?php
	
	/**
	 * Parser
	 *
	 * The Parser class is the abstract base class from which all
	 * Parser's should inherit. Parser classes are used manipulate and
	 * data from PHP into HTML, XML or other user formats.
	 *
	 * @since 2013-01-02
	 * @abstract
	 */
	
	abstract class Parser {
		
		/* ------------------------------------------------------------------ */
		
		protected $engine;
		protected $templates;
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * create
		 *
		 * This static function is used as a factory method for creating
		 * different types of Parser subclasses. The function will check
		 * that the specified class exists, is named correctly and is a
		 * valid subclass of Parser before creating an instance.
		 *
		 * e.g.
		 *	$parser = Parser::create('smarty');
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Parser
		 */
		
		public static function create($type) {
			$class = 'Parser_' . preg_replace('/[^a-z0-9_]/i', '', str_replace('/', '_', $type));
			if (class_exists($class) && is_subclass_of($class, 'Parser')) {
				return new $class();
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setTemplates
		 *
		 * This function sets the location of the templates folder.
		 *
		 * i.e.
		 *	$parser->setTemplates('./templates');
		 *
		 * @access public
		 * @param string $templates
		 */
		
		public function setTemplates($templates) {
			$this->templates = $templates;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getTemplates
		 *
		 * This function returns the location of the templates folder.
		 *
		 * i.e.
		 *	$templates = $parser->getTemplates();
		 *
		 * @access public
		 * @return string
		 */
		
		public function getTemplates() {
			return $this->templates;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setVar
		 *
		 * This function is used to store a name and value key pair of
		 * data into the parser so that it can then be used within the
		 * templates.
		 *
		 * i.e.
		 *	$parser->setVar('name', 'Steve Brewster');
		 *
		 * @access public
		 * @abstract
		 * @param string $name
		 * @param mixed $value
		 */
		
		abstract public function setVar($name, $value);
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getVar
		 *
		 * This function is used to retrive a value from the Parser
		 *
		 * i.e.
		 *	$name = $parser->getVar('name');
		 *
		 * @access public
		 * @abstract
		 * @param string $name
		 * @return mixed
		 */
		
		abstract public function getVar($name);
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * parse
		 *
		 * This abstract function provides the prototype that all
		 * subclasses should to implement their parsing mechanics. This
		 * could be by using Smarty, PHPTal, PHP or just plain old
		 * string replacement.
		 *
		 * i.e.
		 *	$parser->parse('template.tpl');
		 *
		 * @access public
		 * @abstract
		 * @param string $template
		 * @return string
		 */
		
		abstract public function parse($template);
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * fetch
		 *
		 * This function is merely a wrapper around the parse() function
		 * which returns the string that the parser creates. This data
		 * could then be placed into a file or a cache or some sort.
		 *
		 *
		 * i.e.
		 *	$html = $parser->fetch('template.tpl');
		 *
		 * @access public
		 * @param string $template
		 * @return string
		 */
		
		public final function fetch($template) {
			return $this->parse($template);
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * display
		 *
		 * This function is merely a wrapper around the parse() function
		 * which prints to the screen the string that the parser creates.
		 *
		 *
		 * i.e.
		 *	$parser->display('template.tpl');
		 *
		 * @access public
		 * @param string $template
		 */
		
		public final function display($template) {
			echo ($this->fetch($template));
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>