<?php
	
	/**
	 * XML_CData
	 *
	 * This class extends the php SimpleXMLElement class and adds extra
	 * functionality which allows the class to deal with more complex data
	 * types and CDATA elements.
	 *
	 * @author Steve Brewster <sbrewster@travelsphere.co.uk>
	 * @copyright Travelsphere 2010
	 * @since 02/03/2010
	 * @version 1.0
	 */
	
	class XML_CData extends SimpleXMLElement {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * addChild
		 *
		 * This function overrides the default addChild() function and adds the
		 * ability for elements to contain CDATA elements.
		 *
		 * @param string $name The name of the element you want to add.
		 * @param string $value The value of the element.
		 * @param string $namespace The name space of the element.
		 * @return XML_CData
		 */
		
		public function addChild($name, $value = null, $namespace = null) {
			if ($value != null && is_string($value)) {
				$temp  = explode('&', $value);
				$value = '';
				for ($x = 0; $x < count($temp); $x++) {
					if ($x == 0) {
						$value .= $temp[$x];
					} elseif (preg_match('/(.{1,5});/i', $temp[$x])) {
						$value .= '&' . $temp[$x] . '&';
					} else {
						$value .= '&amp;' . $temp[$x];
					}
				}
				$value = rtrim($value, '&');
			}
			
			if ($value == '' || htmlentities($value) == $value) {
				@$ret = parent::addChild($name, $value, $namespace);
			} else {
				$value = 'CDATA[[' . $value . ']]';
				$ret   = parent::addChild($name, $value, $namespace);
			}
			
			return $ret;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * strip_invalid_xml_chars
		 *
		 * This function makes sure that the values used in the XML file are all
		 * valid UTF-8. Any characters which are not valid are removed.
		 *
		 * @param string $in The XML string to check for non UTF-8 characters.
		 * @return string
		 */
		
		protected function strip_invalid_xml_chars($in) {
			$out    = "";
			$length = strlen($in);
			for ($i = 0; $i < $length; $i++) {
				$current = ord($in{$i});
				if (($current == 0x9) || ($current == 0xA) || ($current == 0xD) || (($current >= 0x20) && ($current <= 0xD7FF)) || (($current >= 0xE000) && ($current <= 0xFFFD)) || (($current >= 0x10000) && ($current <= 0x10FFFF))) {
					$out .= chr($current);
				} else {
					$out .= " ";
				}
			}
			return $out;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * asXML
		 *
		 * This function converts the internal DOM structure of the XML into an
		 * XML string.
		 *
		 * @return string
		 */
		
		public function asXML($tring = null) {
			$ret = parent::asXML();
			$ret = $this->strip_invalid_xml_chars($ret);
			$ret = str_replace('CDATA[[', '<![CDATA[', $ret);
			$ret = str_replace(']]', ']]>', $ret);
			$ret = str_replace('&gt;', '>', $ret);
			$ret = str_replace('&lt;', '<', $ret);
			
			return $ret;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * getAttribute
		 *
		 * This function provides a quick and easy way to get the parameters of
		 * an XML element.
		 *
		 * @param string $name The name of the parameter to return.
		 * @return string
		 */
		
		public function getAttribute($name) {
			$attrib = $this->attributes();
			return (string) $attrib[$name];
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>