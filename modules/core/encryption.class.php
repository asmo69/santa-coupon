<?php
	
	abstract class Encryption {
		
		/* ------------------------------------------------------------------ */
		
		public static function create($type) {
			if ($type) {
				$class = 'Encryption_' . preg_replace('/[^a-z0-9_]/i', '', str_replace('/', '_', $type));
				if (class_exists($class) && is_subclass_of($class, 'Encryption')) {
					return new $class();
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		abstract public function encode($value, $key);
		
		/* ------------------------------------------------------------------ */
		
		abstract public function decode($value, $key);
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>