<?php
	
	class Doc_PHP {
		
		/* ------------------------------------------------------------------ */
		
		private function dir_contents($dir = '.') {
			$files = array();
			
			$dir = rtrim($dir, '/') . '/';
			
			if ($fd = opendir($dir)) {
				while ($file = readdir($fd)) {
					if ($file != '.' && $file != '..') {
						
						if (is_file($dir . $file) && $file != 'console.php') {
							$files[] = $dir . $file;
						} elseif (is_dir($dir . $file) && $file != '.console' && $file != 'external' && $file != 'temp') {
							$files = array_merge($files, $this->dir_contents($dir . $file));
						}
					}
				}
				closedir($fd);
			}
			
			sort($files);
			
			return $files;
		}
		
		/* ------------------------------------------------------------------ */
		
		private function parse_php($file) {
			
			$data = array(
				'path' => '',
				'filename' => '',
				'note' => '',
				'abstract' => '',
				'name' => '',
				'type' => '',
				'extends' => '',
				'implements' => '',
				'functions' => array(),
				'vars' => array(),
				'consts' => array(),
				'creates' => array(),
				'throws' => array(),
				'source' => ''
			);
			
			$data['path']     = dirname($file);
			$data['filename'] = basename($file);
			
			$contents       = file_get_contents($file);
			$data['source'] = $contents;
			$temp           = str_replace(array(
				"\n",
				"\r",
				"\t"
			), array(
				' ',
				' ',
				' '
			), $contents);
			while (strpos($temp, '  ') !== false) {
				$temp = str_replace('  ', ' ', $temp);
			}
			$temp2 = preg_replace('/\/\*\*(.*?)\*\//i', '', $temp);
			
			
			if (preg_match('/\/\*\*(.*?)\*\//is', $contents, $matches)) {
				$data['note'] = str_replace('*', '<br/>', htmlentities(trim(trim($matches[1]), '*')));
			}
			
			if (preg_match('/(abstract\s+){0,1}(class|interface)\s+([a-z0-9_]+)\s+(extends\s+([a-z0-9_]+)\s+){0,1}(implements\s+(.*?)\s+){0,1}{/is', $temp2, $matches)) {
				$data['abstract'] = trim($matches[1]);
				$data['type']     = trim($matches[2]);
				$data['name']     = trim($matches[3]);
				if (count($matches) > 5) {
					$data['extends'] = trim($matches[5]);
				}
				if (count($matches) > 7) {
					$data['implements'] = explode(',', str_replace(array(
						" ",
						"\t"
					), array(
						'',
						''
					), trim($matches[7])));
				}
			}
			
			if (preg_match_all('/(\/\*\*(.*?)\*\/\s*){0,1}(public|private|protected|var)\s+\$([a-z0-9_]*)(\s+=\s+(.*?)){0,1};/i', $temp, $matches, PREG_SET_ORDER)) {
				foreach ($matches as $match) {
					if (strcasecmp($match[4], '_explicitType') !== 0) {
						while (($index = strpos($match[1], '/**')) !== false) {
							$match[1] = substr($match[1], $index + 3);
						}
						if (strpos($match[1], '/*') !== false) {
							$match[1] = '';
						}
						
						$data['vars'][] = array(
							'note' => str_replace('*', '<br/>', htmlentities(trim($match[1], "\r\n*/ "))),
							'modifier' => str_replace('var', 'public', trim($match[3])),
							'name' => trim($match[4]),
							'value' => count($match) > 6 ? trim($match[6]) : ''
						);
					}
				}
			}
			
			
			if (preg_match_all('/(\/\*\*(.*?)\*\/\s*){0,1}const\s+([a-z0-9_]*)(\s+=\s+(.*?)){0,1};/i', $temp, $matches, PREG_SET_ORDER)) {
				foreach ($matches as $match) {
					while (($index = strpos($match[1], '/**')) !== false) {
						$match[1] = substr($match[1], $index + 3);
					}
					if (strpos($match[1], '/*') !== false) {
						$match[1] = '';
					}
					
					$data['consts'][] = array(
						'note' => str_replace('*', '<br/>', htmlentities(trim($match[1], "\r\n*/ "))),
						'name' => trim($match[3]),
						'value' => count($match) > 5 ? trim($match[5]) : ''
					);
				}
			}
			
			if (preg_match_all('/(\/\*\*(.*?)\*\/\s*){0,1}(abstract){0,1}\s*(public|private|protected){0,1}\s+(static\s+){0,1}(final\s+){0,1}function\s+([a-z0-9_]+)\s*\((.*?)\)\s*(;|{)/is', $temp, $matches, PREG_SET_ORDER)) {
				foreach ($matches as $match) {
					while (($index = strpos($match[1], '/**')) !== false) {
						$match[1] = substr($match[1], $index + 3);
					}
					if (strpos($match[1], '/*') !== false) {
						$match[1] = '';
					}
					
					$func = array(
						'note' => str_replace('*', '<br/>', htmlentities(trim($match[1], "\r\n*/ "))),
						'abstract' => trim($match[3]),
						'modifier' => trim($match[4]),
						'static' => trim($match[5]),
						'final' => trim($match[6]),
						'name' => trim($match[7]),
						'params' => trim($match[8])
					);
					
					$data['functions'][$func['modifier'] . $func['static'] . $func['final'] . $func['name']] = $func;
				}
				
				ksort($data['functions']);
				$data['functions'] = array_values($data['functions']);
			}
			
			if (preg_match_all('/\bthrow\s+new\s+([a-z0-9_]*)\(/i', $temp2, $matches, PREG_SET_ORDER)) {
				foreach ($matches as $match) {
					$data['throws'][] = $match[1];
				}
			}
			
			if (preg_match_all('/\bnew\s+([a-z0-9_]*)\(/i', $temp2, $matches, PREG_SET_ORDER)) {
				foreach ($matches as $match) {
					if (in_array($match[1], $data['throws']) == false) {
						$data['creates'][] = $match[1];
					}
				}
			}
			if (preg_match_all('/\b([a-z0-9_]*)::/i', $temp2, $matches, PREG_SET_ORDER)) {
				foreach ($matches as $match) {
					if (in_array($match[1], $data['throws']) == false) {
						if ($match[1] == 'parent') {
							$match[1] = $data['extends'];
						}
						
						if ($match[1] != 'self') {
							$data['creates'][] = $match[1];
						}
					}
				}
			}
			
			$data['uri'] = '_' . ($data['name'] ? $data['name'] : $data['filename']) . '.' . md5($file) . '.html';
			
			//print_r($data['creates']);
			
			return $data;
		}
		
		/* ------------------------------------------------------------------ */
		
		public function start($paths = null, $exts = null) {
			$total = 0;
			$data  = array(
				'files' => array(),
				'classes' => array(),
				'extends' => array(),
				'implements' => array(),
				'path' => array(),
				'created' => array(),
				'thrown' => array()
			);
			
			@mkdir(ROOT_DIR . '/docs');
			
			if (empty($paths)) {
				$paths = './';
			}
			if (is_string($paths)) {
				$paths = array(
					$paths
				);
			}
			
			if (empty($exts)) {
				$exts = 'php';
			}
			if (is_string($exts)) {
				$exts = array(
					$exts
				);
			}
			
			foreach ($paths as $path) {
				foreach ($this->dir_contents($path) as $file) {
					if (preg_match('/\.(' . implode('|', $exts) . ')$/i', $file)) {
						$retval = $this->parse_php($file);
						
						$data['files'][] = $retval;
						if ($retval['name']) {
							$data['classes'][strtolower($retval['name'])] = $retval;
						}
						if ($retval['path']) {
							$data['path'][$retval['path']][] = $retval;
						}
						if ($retval['extends']) {
							$data['extends'][strtolower($retval['extends'])][] = $retval;
						}
						
						if ($retval['implements']) {
							foreach ($retval['implements'] as $temp) {
								$data['implements'][strtolower($temp)][] = $retval;
							}
						}
						
						foreach ($retval['creates'] as $temp) {
							$data['created'][strtolower($temp)][] = $retval;
						}
						
						foreach ($retval['throws'] as $temp) {
							$data['thrown'][strtolower($temp)][] = $retval;
						}
						
					}
				}
			}
			
			// Build the index file
			ksort($data['path']);
			$temp = array_values($data['path']);
			$html = '';
			$html .= sprintf("<title>%s</title>", basename(ROOT_DIR));
			$html .= sprintf("<frameset cols=\"350,*\" frameborder=\"0\">\n");
			$html .= sprintf("<frame src=\"menu.html\" name=\"menu\" />\n");
			$html .= sprintf("<frame src=\"%s\" name=\"main\" />\n", $temp[0][0]['uri']);
			$html .= sprintf("</frameset>\n");
			$html .= sprintf("<noframes>oops</noframes>\n");
			file_put_contents('./docs/index.html', $html);
			
			// Build the menu file
			$html = '';
			$html .= sprintf("<html>\n");
			$html .= sprintf("<head>\n");
			$html .= sprintf("<title>%s - Index</title>\n", basename(ROOT_DIR));
			$html .= sprintf("<style type=\"text/css\">\n");
			$html .= sprintf("	body { background-color: #AFC7DE }\n");
			$html .= sprintf("	h1 { margin: 0px 0px 3px 0px; padding: 0px; font-family: Verdana; font-size: 18px; color: #000000; font-weight: bold;  }\n");
			$html .= sprintf("	a { color: #0000FF; text-decoration: none; }\n");
			$html .= sprintf("	#links { margin: 0px 0px 10px 0px; padding: 5px; font-family: Verdana; font-size: 12px; color: #000000; background-color: #E8EBEE; border: 1px solid #4c6985 }\n");
			$html .= sprintf("	#links div { margin: 0px 0px 3px 0px; }\n");
			$html .= sprintf("</style>\n");
			$html .= sprintf("</body>\n");
			$html .= sprintf("<html>\n");
			$html .= sprintf("<h1>%s</h1>\n", basename(ROOT_DIR));
			$last = '';
			$html .= sprintf("<div id=\"links\">\n");
			ksort($data['path']);
			$base = ROOT_DIR;
			foreach ($data['path'] as $path) {
				foreach ($path as $item) {
					if ($last != $item['path']) {
						$html .= sprintf("<br/>\n<div><b>%s</b></div>\n", str_replace('/', ' / ', str_replace('./', '', str_replace('../', '', str_replace($base, '', $item['path'])))));
					}
					$last = $item['path'];
					$html .= sprintf("<div><a href=\"%s\" target=\"main\">%s</a></div>\n", $item['uri'], $item['name'] ? $item['name'] : $item['filename']);
				}
			}
			$html .= sprintf("<br/>\n</div>\n");
			$html .= sprintf("</body>\n");
			$html .= sprintf("</html>\n");
			file_put_contents('./docs/menu.html', $html);
			
			// Build a file for each class
			foreach ($data['files'] as $item) {
				$links = array(
					'name' => '',
					'extends' => ''
				);
				if (@$data['classes'][strtolower($item['name'])]) {
					@$links['name'] = $data['classes'][strtolower($item['name'])]['uri'];
				}
				if (@$data['classes'][strtolower($item['extends'])]) {
					@$links['extends'] = $data['classes'][strtolower($item['extends'])]['uri'];
				}
				if (is_array($item['implements'])) {
					foreach ($item['implements'] as $temp) {
						@$links['implements'][] = $data['classes'][strtolower($temp)]['uri'];
					}
				}
				
				$out = '';
				
				$html = '';
				$html .= sprintf("<html>\n");
				$html .= sprintf("<head>\n");
				$html .= sprintf("<title>%s</title>\n", $item['name'] ? $item['name'] : $item['filename']);
				$html .= sprintf("<style type=\"text/css\">\n");
				$html .= sprintf("	body { background-color: #AFC7DE }\n");
				$html .= sprintf("	a { color: #0000FF; text-decoration: none; }\n");
				$html .= sprintf("	h1 { margin: 0px 0px 3px 0px; padding: 0px; font-family: Verdana; font-size: 18px; color: #000000; font-weight: bold;  }\n");
				$html .= sprintf("	h2 { margin: 0px 0px 3px 0px; padding: 0px; font-family: Verdana; font-size: 12px; color: #000000; font-weight: bold;  }\n");
				$html .= sprintf("	#definition { margin: 0px 0px 10px 0px; padding: 5px; font-family: Verdana; font-size: 12px; color: #000000; background-color: #E8EBEE; border: 1px solid #4c6985 }\n");
				$html .= sprintf("	#definition div { margin: 0px 0px 3px 0px; }\n");
				$html .= sprintf("	#reference { margin: 0px 0px 10px 0px; padding: 5px; font-family: Verdana; font-size: 12px; color: #000000; background-color: #E8EBEE; border: 1px solid #4c6985 }\n");
				$html .= sprintf("	#note { margin: 0px 0px 10px 0px; padding: 5px; font-family: Verdana; font-size: 12px; color: #000000; background-color: #E8EBEE; border: 1px solid #4c6985 }\n");
				$html .= sprintf("	#functions { margin: 0px 0px 10px 0px; padding: 5px; font-family: Verdana; font-size: 12px; color: #000000; background-color: #E8EBEE; border: 1px solid #4c6985 }\n");
				$html .= sprintf("	#source { margin: 0px 0px 10px 0px; padding: 5px; font-family: Verdana; font-size: 12px; color: #000000; background-color: #E8EBEE; border: 1px solid #4c6985 }\n");
				$html .= sprintf("	.function { margin: 0px 0px 10px 0px; padding: 5px; font-family: Verdana; font-size: 12px; color: #000000; background-color: #E8EBEE; border: 1px solid #4c6985 }\n");
				$html .= sprintf("	#consts div { margin: 0px 0px 3px 0px; }\n");
				$html .= sprintf("	#consts { margin: 0px 0px 10px 0px; padding: 5px; font-family: Verdana; font-size: 12px; color: #000000; background-color: #E8EBEE; border: 1px solid #4c6985 }\n");
				$html .= sprintf("	#vars div { margin: 0px 0px 3px 0px; }\n");
				$html .= sprintf("	#vars { margin: 0px 0px 10px 0px; padding: 5px; font-family: Verdana; font-size: 12px; color: #000000; background-color: #E8EBEE; border: 1px solid #4c6985 }\n");
				$html .= sprintf("	#functions div { margin: 0px 0px 3px 0px; }\n");
				$html .= sprintf("	#source div { margin: 0px 0px 3px 0px; }\n");
				$html .= sprintf("</style>\n");
				$html .= sprintf("</head>\n");
				$html .= sprintf("<body>\n");
				$html .= sprintf("<h1 id=\"header\">%s</h1>\n", $item['name'] ? $item['name'] : $item['filename']);
				$html .= sprintf("<h2>Definition</h2>\n");
				$html .= sprintf("<div id=\"definition\">\n");
				$html .= sprintf("<div><b>File:</b> <a href=\"%s\">%s/%s</a></div>\n", $links['name'], str_replace('./', '', str_replace('../', '', $item['path'])), $item['filename']);
				if ($item['name']) {
					$html .= sprintf("<div><b>%s:</b> <a href=\"%s\">%s</a></div>\n", ucfirst($item['type']), $links['name'], htmlentities($item['name']));
				}
				if ($item['abstract']) {
					$html .= sprintf("<div><b>Abstract:</b> Yes</div>\n");
				}
				if ($item['extends']) {
					if (array_key_exists(strtolower($item['extends']), $data['classes'])) {
						$html .= sprintf("<div><b>Extends:</b> <a href=\"%s\">%s</a></div>\n", $links['extends'], htmlentities($item['extends']));
					} else {
						$html .= sprintf("<div><b>Extends:</b> <a href=\"http://uk.php.net/search.php?pattern=%s&amp;show=quickref\">%s</a></div>\n", rawurlencode($item['extends']), htmlentities($item['extends']));
					}
				}
				if ($item['implements']) {
					$temp = array();
					for ($x = 0; $x < count($item['implements']); $x++) {
						if (array_key_exists(strtolower($item['implements'][$x]), $data['classes'])) {
							$temp[] = sprintf("<a href=\"%s\">%s</a>", $links['implements'][$x], htmlentities($item['implements'][$x]));
						} else {
							$temp[] = sprintf("<a href=\"http://uk.php.net/search.php?pattern=%s&amp;show=quickref\">%s</a>", rawurlencode($item['implements'][$x]), htmlentities($item['implements'][$x]));
						}
					}
					$html .= sprintf("<div><b>Implements:</b> %s</div>\n", join(', ', $temp));
				}
				$html .= sprintf("</div>");
				
				if ($item['note']) {
					$html .= sprintf("<h2>Description</h2>");
					$html .= sprintf("<div id=\"note\" style=\"color: #3b814e\">%s</div>\n", str_replace("\t", "&nbsp;&nbsp;&nbsp;", $item['note']));
				} else {
					$out .= sprintf("	No header notes.\n");
				}
				
				if (array_key_exists(strtolower($item['name']), $data['implements']) || array_key_exists(strtolower($item['name']), $data['extends']) || array_key_exists(strtolower($item['name']), $data['created']) || count($item['creates']) || count($item['throws'])) {
					$html .= sprintf("<h2>References</h2>");
					$html .= sprintf("<div id=\"reference\">");
					$temp = array();
					if (array_key_exists(strtolower($item['name']), $data['extends'])) {
						foreach ($data['extends'][strtolower($item['name'])] as $ex) {
							$temp[$ex['name'] ? $ex['name'] : $ex['filename']] = sprintf("<a href=\"%s\">%s</a>", $ex['uri'], htmlentities($ex['name'] ? $ex['name'] : $ex['filename']));
						}
						if (count($temp)) {
							ksort($temp);
							$html .= sprintf("<b>Extended By:</b> %s<br/>", join(', ', array_unique($temp)));
						}
					}
					$temp = array();
					if (array_key_exists(strtolower($item['name']), $data['implements'])) {
						foreach ($data['implements'][strtolower($item['name'])] as $ex) {
							$temp[$ex['name'] ? $ex['name'] : $ex['filename']] = sprintf("<a href=\"%s\">%s</a>", $ex['uri'], htmlentities($ex['name'] ? $ex['name'] : $ex['filename']));
						}
						if (count($temp)) {
							ksort($temp);
							$html .= sprintf("<b>Implemented By:</b> %s<br/>", join(', ', array_unique($temp)));
						}
					}
					$temp = array();
					if (array_key_exists(strtolower($item['name']), $data['created'])) {
						foreach ($data['created'][strtolower($item['name'])] as $ex) {
							$temp[$ex['name'] ? $ex['name'] : $ex['filename']] = sprintf("<a href=\"%s\">%s</a>", $ex['uri'], htmlentities($ex['name'] ? $ex['name'] : $ex['filename']));
						}
						if (count($temp)) {
							ksort($temp);
							$html .= sprintf("<b>Referenced From:</b> %s<br/>", join(', ', array_unique($temp)));
						}
					}
					$temp = array();
					if (count($item['creates'])) {
						foreach (array_unique($item['creates']) as $name) {
							if (array_key_exists(strtolower($name), $data['classes'])) {
								$temp[$name] = sprintf("<a href=\"%s\">%s</a>", $data['classes'][strtolower($name)]['uri'], htmlentities($name));
							} else {
								$temp[$name] = sprintf("<a href=\"http://uk.php.net/search.php?pattern=%s&amp;show=quickref\">%s</a>", rawurlencode($name), htmlentities($name));
							}
						}
						if (count($temp)) {
							ksort($temp);
							$html .= sprintf("<b>References:</b> %s<br/>", join(', ', array_unique($temp)));
						}
					}
					if (array_key_exists(strtolower($item['name']), $data['thrown'])) {
						foreach ($data['thrown'][strtolower($item['name'])] as $ex) {
							$temp[$ex['name'] ? $ex['name'] : $ex['filename']] = sprintf("<a href=\"%s\">%s</a>", $ex['uri'], htmlentities($ex['name'] ? $ex['name'] : $ex['filename']));
						}
						if (count($temp)) {
							ksort($temp);
							$html .= sprintf("<b>Thrown By:</b> %s<br/>", join(', ', array_unique($temp)));
						}
					}
					$temp = array();
					if (count($item['throws'])) {
						foreach (array_unique($item['throws']) as $name) {
							if (array_key_exists(strtolower($name), $data['classes'])) {
								$temp[$name] = sprintf("<a href=\"%s\">%s</a>", $data['classes'][strtolower($name)]['uri'], htmlentities($name));
							} else {
								$temp[$name] = sprintf("<a href=\"http://uk.php.net/search.php?pattern=%s&amp;show=quickref\">%s</a>", rawurlencode($name), htmlentities($name));
							}
						}
						if (count($temp)) {
							ksort($temp);
							$html .= sprintf("<b>Throws:</b> %s<br/>", join(', ', array_unique($temp)));
						}
					}
					$html .= sprintf("</div>");
				}
				
				if (is_array($item['consts']) && count($item['consts'])) {
					$html .= sprintf("<h2>Constants</h2>");
					$html .= sprintf("<div id=\"consts\">");
					foreach ($item['consts'] as $func) {
						$html .= sprintf("<div><a href=\"#const_%s\">%s</a> = <span style=\"color: #a85d4d\">%s</span></div>", $func['name'], $func['name'], htmlentities($func['value']));
					}
					$html .= sprintf("<br/><div><a href=\"#header\">&laquo;Top</a></div>");
					$html .= sprintf("</div>");
				}
				
				if (is_array($item['vars']) && count($item['vars'])) {
					$html .= sprintf("<h2>Properties</h2>");
					$html .= sprintf("<div id=\"vars\">");
					foreach ($item['vars'] as $func) {
						if ($func['value']) {
							$html .= sprintf("<div><span style=\"color: #4d84a8\">%s</span> <a href=\"#var_%s\">\$%s</a> = <span style=\"color: #a85d4d\">%s</span></div>", $func['modifier'], $func['name'], $func['name'], htmlentities($func['value']));
						} else {
							$html .= sprintf("<div><span style=\"color: #4d84a8\">%s</span> <a href=\"#var_%s\">\$%s</a></div>", $func['modifier'], $func['name'], $func['name']);
						}
					}
					$html .= sprintf("<br/><div><a href=\"#header\">&laquo;Top</a></div>");
					$html .= sprintf("</div>");
				}
				
				if (is_array($item['functions']) && count($item['functions'])) {
					$html .= sprintf("<h2>Methods</h2>");
					$html .= sprintf("<div id=\"functions\">");
					foreach ($item['functions'] as $func) {
						$html .= sprintf("<div><span style=\"color: #4d84a8\">%s %s %s %s function</span> <a href=\"#func_%s\">%s</a> ( <span style=\"color: #a85d4d\">%s</span> )</div>", $func['abstract'], $func['modifier'], $func['static'], $func['final'], $func['name'], $func['name'], htmlentities($func['params']));
					}
					$html .= sprintf("<br/><div><a href=\"#header\">&laquo;Top</a></div>");
					$html .= sprintf("</div>");
				}
				
				if (is_array($item['consts']) && count($item['consts'])) {
					foreach ($item['consts'] as $const) {
						if ($const['note']) {
							$html .= sprintf("<h2>%s</h2>", $const['name']);
							$html .= sprintf("<div class=\"function\" id=\"const_%s\">", $const['name']);
							$html .= sprintf("<div style=\"color: #3b814e\">%s</div><br/>", $const['note']);
							$html .= sprintf("<div><a href=\"#const_%s\">%s</a> = <span style=\"color: #a85d4d\">%s</span></div>", $const['name'], $const['name'], htmlentities($const['value']));
							$html .= sprintf("<br/><div><a href=\"#header\">&laquo;Top</a></div>");
							$html .= sprintf("</div>");
						} elseif ($item['name']) {
							//$out .= sprintf("	No const notes on %s::%s.\n", $item['name'], $const['name']);
						} else {
							//$out .= sprintf("	No const notes on %s.\n", $const['name']);
						}
					}
				}
				
				if (is_array($item['vars']) && count($item['vars'])) {
					foreach ($item['vars'] as $var) {
						if ($var['note']) {
							$html .= sprintf("<h2>\$%s</h2>", $var['name']);
							$html .= sprintf("<div class=\"function\" id=\"var_%s\">", $var['name']);
							$html .= sprintf("<div style=\"color: #3b814e\">%s</div><br/>", $var['note']);
							if ($var['value']) {
								$html .= sprintf("<div><span style=\"color: #4d84a8\">%s</span> <a href=\"#var_%s\">\$%s</a> = <span style=\"color: #a85d4d\">%s</span></div>", $var['modifier'], $var['name'], $var['name'], htmlentities($var['value']));
							} else {
								$html .= sprintf("<div><span style=\"color: #4d84a8\">%s</span> <a href=\"#var_%s\">\$%s</a></div>", $var['modifier'], $var['name'], $var['name']);
							}
							$html .= sprintf("<br/><div><a href=\"#header\">&laquo;Top</a></div>");
							$html .= sprintf("</div>");
						} elseif ($item['name']) {
							//$out .= sprintf("	No property notes on %s->%s.\n", $item['name'], $var['name']);
						} else {
							//$out .= sprintf("	No property notes on %s.\n", $var['name']);
						}
					}
				}
				
				if (is_array($item['functions']) && count($item['functions'])) {
					foreach ($item['functions'] as $func) {
						if ($func['note']) {
							$html .= sprintf("<h2>%s()</h2>", $func['name']);
							$html .= sprintf("<div class=\"function\" id=\"func_%s\">", $func['name']);
							$html .= sprintf("<div style=\"color: #3b814e\">%s</div><br/>", str_replace("\t", "&nbsp;&nbsp;&nbsp;", $func['note']));
							$html .= sprintf("<div><span style=\"color: #4d84a8\">%s %s %s %s function</span> <a href=\"#func_%s\">%s</a> ( <span style=\"color: #a85d4d\">%s</span> )</div>", $func['abstract'], $func['modifier'], $func['static'], $func['final'], $func['name'], $func['name'], htmlentities($func['params']));
							$html .= sprintf("<br/><div><a href=\"#header\">&laquo;Top</a></div>");
							$html .= sprintf("</div>");
						} elseif ($item['name']) {
							$out .= sprintf("	No function notes on %s->%s().\n", $item['name'], $func['name']);
						} else {
							$out .= sprintf("	No function notes on %s().\n", $func['name']);
						}
					}
				}
				
				
				if ($item['source']) {
					$source = $item['source'];
					$temp   = '__PHPDOC_CLASS__';
					foreach ($data['classes'] as $class) {
						if ($class['name']) {
							$source = preg_replace('/\b' . $class['name'] . '\(/i', $temp . $class['name'] . '(', $source);
							$source = preg_replace('/\b' . $class['name'] . '::/i', $temp . $class['name'] . '::', $source);
							if (preg_match_all('/class\s+(.*?)(\s+extends\s+(.*?)){0,1}(\s+implements\s+(.*?)){0,1}\s*{/', $source, $matches, PREG_SET_ORDER)) {
								foreach ($matches as $match) {
									$source = str_replace($match[0], preg_replace('/\b' . $class['name'] . '\b/i', $temp . $class['name'], $match[0]), $source);
								}
							}
						}
					}
					$source = highlight_string($source, true);
					foreach ($data['classes'] as $class) {
						$source = preg_replace('/' . $temp . $class['name'] . '\b/i', '<a href="' . $class['uri'] . '" style="font-weight: bold; text-decoration: underline;">' . $class['name'] . '</a>', $source);
					}
					
					$html .= sprintf("<h2>source</h2>");
					$html .= sprintf("<div id=\"source\">");
					$html .= sprintf("%s", $source);
					$html .= sprintf("<br/><div><a href=\"#header\">&laquo;Top</a></div>");
					$html .= sprintf("</div>");
				}
				
				$html .= sprintf("</body></html>");
				
				file_put_contents(ROOT_DIR . '/docs/' . $item['uri'], $html);
				
				printf("Created: %s/docs/%s\n", ROOT_DIR, $item['uri']);
				
				if ($out) {
					$total++;
					file_put_contents(ROOT_DIR . '/docs/log.txt', sprintf("Created: %s/%s\n%s\n", $item['path'], $item['filename'], $out), FILE_APPEND);
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>