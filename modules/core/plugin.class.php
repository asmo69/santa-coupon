<?php
	
	/**
	 * Plugin
	 *
	 * The Plugin class is the abstract base class from which all
	 * Plugin's should inherit. Plugin classes are usualkly used to
	 * perform pre and post processing actions on the Controller's. For
	 * example, one pre processing Plugin might check to see the if the
	 * page URL start with 'www' and do a redirect if necessary. Another
	 * post processing plugin might take the HTMl created by the
	 * Controller and Tidy it up so that its easier to read.
	 *
	 * @since 2013-01-02
	 * @abstract
	 */
	
	abstract class Plugin {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * create
		 *
		 * This static function is used as a factory method for creating
		 * different types of Plugin subclasses. The function will check
		 * that the specified class exists, is named correctly and is a
		 * valid subclass of Plugin before creating an instance.
		 *
		 * e.g.
		 *	$plugin = Plugin::create('www');
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Plugin
		 */
		
		public static function create($type) {
			if ($type) {
				$class = 'Plugin_' . preg_replace('/[^a-z0-9_]/i', '', str_replace('/', '_', $type));
				if (class_exists($class) && is_subclass_of($class, 'Plugin')) {
					return new $class();
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * execute
		 *
		 * This abstract function defines the prototype for the execute()
		 * function. Once a Controller class has created an instance of a
		 * Plugin class it will the execute() function, passing a
		 * reference to the gloabl Application.
		 *
		 * e.g.
		 *	$plugin = Plugin::create('www');
		 *	$plugin->execute($this);
		 *
		 * @access public
		 * @abstract
		 * @param Application $application
		 */
		
		abstract public function execute(Application $application);
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>