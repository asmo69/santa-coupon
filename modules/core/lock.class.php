<?php
	
	/**
	 * Lock
	 *
	 * The Lock class provides the functionality for creating named lock
	 * resources. These can be used to make sure that sections of code
	 * are locked and only one running PHP script can access that code
	 * at a time.
	 *
	 * @since 2013-01-02
	 * @abstract
	 */
	
	abstract class Lock {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * create
		 *
		 * This static function is used as a factory method for creating
		 * different types of Lock subclasses. The function will check that
		 * the specified class exists, is named correctly and is a valid
		 * subclass of Lock before creating an instance.
		 *
		 * e.g.
		 *	$lock = Action::create('db');
		 *
		 * @access public
		 * @static
		 * @param string $type
		 * @return Lock
		 */
		
		public static function create($type) {
			if ($type) {
				$class = 'Lock_' . preg_replace('/[^a-z0-9_]/i', '', str_replace('/', '_', $type));
				if (class_exists($class) && is_subclass_of($class, 'Lock')) {
					return new $class();
				}
			}
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * open
		 *
		 * This function attempts to game exclusive access to the named
		 * lock resource. The function will timeout after a specified
		 * number of seconds and return a value of 'false'.
		 *
		 * @access public
		 * @abstract
		 * @param string $name
		 * @param int $timeout
		 * @return boolean
		 */
		
		abstract public function open($name, $timeout);
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * close
		 *
		 * This function attempts to game release access to the named
		 * lock resource.
		 *
		 * @access public
		 * @abstract
		 * @param string $name
		 * @return boolean
		 */
		
		abstract public function close($name);
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>