#!/usr/bin/php
<?php
	
	/**
	 * console.php
	 *
	 * This file holds the code through which all requests for console pages
	 * must pass. The file uses Application and Controller_Console classes to
	 * determine which Action class should be executed to handle the page
	 * request.
	 *
	 * @see      Application, Controller, Controller_Console
	 * @since    2013-01-02
	 */
	
	/* ---------------------------------------------------------------------- */
	
	// Load the Bootstrap
	require_once dirname(__FILE__) . '/bootstrap.php';
	
	// Add extra modules.
	$loader = Registry::getAutoLoader();
	$loader->addModule('console');
	$loader->addModule('query');
	
	// Create the application context, give it a controller and start
	// the application.
	$application = Registry::getApplication();
	$application->setController(Controller::create('CLI/Console'));
	$application->start();
	
	/* ---------------------------------------------------------------------- */
	
?>