<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>{$config->getVar('game_title')|escape:'html'}</title>
		<link rel="stylesheet" type="text/css" href="./assets/css/style.css" />
		<script type="text/javascript" src="./assets/js/common/jquery.min.js"></script>
		<script type="text/javascript" src="./assets/js/common/jquery.browser.js"></script>
		<script type="text/javascript" src="./assets/js/common/game.js"></script>
		<script type="text/javascript" src="./assets/js/common/snow.js"></script>
		<script type="text/javascript" src="./assets/js/common/smoke.js"></script>
		<script type="text/javascript" src="./assets/js/{$action}.js"></script>
	</head>
	<body>

		<div id="container">
			<div class="panel">
				<div class="inner">
					<div class="game" style="display: {if $action == 'game'}block{else}hidden{/if}">
						{if $data.complete}
							{assign var="action" value="form"}
						{/if}
						<a href="./index.php?action=game&open=all" class="reveal">&nbsp;</a>
						<div class="tiles">
							{if !$errors}
								{foreach from=$data.tiles item="tile" name="tiles"}
									<a href="./index.php?action=game&open={$smarty.foreach.tiles.index}" class="tile tile{$smarty.foreach.tiles.index} {if $data.open && in_array($smarty.foreach.tiles.index, $data.open)}open{/if}">
										<div class="{$tile}">&nbsp;</div>
									</a>
								{/foreach}
							{/if}
						</div>
						<div class="cache">
							{if !$errors}
								{foreach from=$data.tiles item="tile" name="tiles"}
									<div class="{$tile}">&nbsp;</div>
								{/foreach}
							{/if}
						</div>
					</div>

					{if $action == 'game'}
						{if $errors}
							<div class="modal" style="display: block"></div>
						{/if}
					{elseif $action == 'install' || $action == 'login' || $action == 'form' || $action == 'win' || $action == 'lose' || $action == 'how'}
						<div class="modal" style="display: block"></div>
					{else}
						<div class="modal" style="display: hidden"></div>
					{/if}

					{if $action == 'install'}
						<div class="popup install" style="display: block">
							<h1>Install</h1>
							<div>
								Database: {$config->getVar('database_rw_type')|escape:'html'}<br/>
								Path: {$config->getVar('database_rw_path')|escape:'html'}<br/>
								Status: {$status}<br/>
								Prize: {$prize}<br/>
								Coupon: {$coupon}<br/>
								Game: {$game}<br/>
								Address: {$address}<br/>
								<p><a href="./index.php?action=how" class="button">Play</a></p>
							</div>
						</div>
					{/if}

					{if $action == 'game' || $action == 'win' || $action == 'lose'}
						{if $errors}
							<div class="popup error" style="display: block">
								<h1>An Error Occurred</h1>
								<div>
									{foreach from=$errors item="error"}
										<p>{$error|escape:'html'}</p>
									{/foreach}
								</div>
							</div>
						{/if}
					{/if}

					<div class="popup login" style="display: {if $action == 'login'}block{else}hidden{/if}">
						<h1>Please Login</h1>
						<div>
							<p>Enter your coupon code below to login and play.</p>
							{if $errors && $errors.general}
								<p class="general">{$errors.general|escape:'html'}</p>
							{/if}
							<form method="post" action="./index.php">
								<input type="hidden" name="action" value="login" />
								<dl>
									<dt><label for="frm_code">Code:</label></dt>
									<dd><input type="text" id="frm_code" name="code" value="{$form->getVar('code')|escape:'html'}" class="input {if $errors.code}error{/if}" /></dd>
								</dl>
								<input type="submit" name="complete" value="Login" class="button" />
							</form>
						</div>
					</div>

					<div class="popup form {if $data.status == 'win'}result{/if}" style="display: {if $action == 'form'}block{else}hidden{/if}">
						<h1>Delivery Details</h1>
						<div>
							<p>You're a winner.</p>
							<p>Your prize: <span class="prize">{if $prize}{$prize->getName()|escape:'html'}{/if}</span>
							<p>Enter your delivery details.</p>
							{if $errors && $errors.general}
								<p class="general">{$errors.general|escape:'html'}</p>
							{/if}
							<form method="post" action="./index.php">
								<input type="hidden" name="action" value="form" />
								<dl>
									<dt><label for="frm_first">First Name:</label></dt>
									<dd><input type="text" id="frm_first" name="first" value="{$form->getVar('first')|escape:'html'}" class="input {if $errors.first}error{/if}" /></dd>
								</dl>
								<dl>
									<dt><label for="frm_last">Last Name:</label></dt>
									<dd><input type="text" id="frm_last" name="last" value="{$form->getVar('last')|escape:'html'}" class="input {if $errors.last}error{/if}" /></dd>
								</dl>
								<dl>
									<dt><label for="frm_email">Email:</label></dt>
									<dd><input type="text" id="frm_email" name="email" value="{$form->getVar('email')|escape:'html'}" class="input {if $errors.email}error{/if}" /></dd>
								</dl>
								<dl>
									<dt><label for="frm_address1">Address1:</label></dt>
									<dd><input type="text" id="frm_address1" name="address1" value="{$form->getVar('address1')|escape:'html'}" class="input {if $errors.address1}error{/if}" /></dd>
								</dl>
								<dl>
									<dt><label for="frm_address2">Address 2:</label></dt>
									<dd><input type="text" id="frm_address2" name="address2" value="{$form->getVar('address2')|escape:'html'}" class="input {if $errors.address2}error{/if}" /></dd>
								</dl>
<!--
								<dl>
									<dt><label for="frm_address3">Address3:</label></dt>
									<dd><input type="text" id="frm_address3" name="address3" value="{$form->getVar('address3')|escape:'html'}" class="input {if $errors.address3}error{/if}" /></dd>
								</dl>
-->								<dl>
									<dt><label for="frm_town">Town:</label></dt>
									<dd><input type="text" id="frm_town" name="town" value="{$form->getVar('town')|escape:'html'}" class="input {if $errors.town}error{/if}" /></dd>
								</dl>
								<dl>
									<dt><label for="frm_county">County:</label></dt>
									<dd><input type="text" id="frm_county" name="county" value="{$form->getVar('county')|escape:'html'}" class="input {if $errors.county}error{/if}" /></dd>
								</dl>
								<dl>
									<dt><label for="frm_postcode">Postcode:</label></dt>
									<dd><input type="text" id="frm_postcode" name="postcode" value="{$form->getVar('postcode')|escape:'html'}" class="input {if $errors.postcode}error{/if}" /></dd>
								</dl>
								<input type="submit" name="complete" value="Submit" class="button" />
							</form>
						</div>
					</div>

					<div class="popup win" style="display: {if $action == 'win'}block{else}hidden{/if}">
						<h1>Congratulations</h1>
						<div>
							<p>You're a winner.</p>
							<p>You reference number is <span class="reference">{if $coupon}#{$coupon->getId()|escape:'html'}{/if}</span></p>
							<p>Your <span class="prize">{if $prize}{$prize->getName()|escape:'html'}{/if}</span> should be delivered within the next 28 days to:</p>
							<p class="address">
								{if $address}
									{$address->getAddress1()|escape:'html'}<br/>
									{if $address->getAddress2()}
										{$address->getAddress2()|escape:'html'}<br/>
									{/if}
									{if $address->getAddress3()}
										{$address->getAddress3()|escape:'html'}<br/>
									{/if}
									{$address->getTown()|escape:'html'}<br/>
									{$address->getCounty()|escape:'html'}<br/>
									{$address->getPostcode()|escape:'html'}
								{/if}
							</p>
							<p><a href="./index.php?action=logout" class="button">Close</a></p>
						</div>
					</div>

					<div class="popup lose {if $data.status == 'lose'}result{/if}" style="display: {if $action == 'lose'}block{else}hidden{/if}">
						<h1>Sorry</h1>
						<div>
							<p>You did not win this time.</p>
							<p>Please try again.</p>
							<p><a href="./index.php?action=logout" class="button">Close</a></p>
						</div>
					</div>

					<div class="popup how" style="display: {if $action == 'how'}block{else}hidden{/if}">
						<h1>How To Play</h1>
						<div>
							<p>Click the windows on Santa's house to reveal Christmas treats. Reveal 3 Santa's to win a prize!</p>
							<p><a href="./index.php?action=game" class="button">Play</a></p>
						</div>
					</div>

				</div>
			</div>
		</div>

	</body>
</html>
