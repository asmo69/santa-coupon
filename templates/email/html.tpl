<html>
	<head>
		{if $coupon}<title>Ref #{$coupon->getId()|escape:'html'}</title>{/if}
	</head>
	<body>
		{if $address}
			<p>Dear {$address->getFirst()|escape:'html'} {$address->getLast()|escape:'html'},</p>
		{/if}
		<p>You're a winner. You reference number is <span class="reference">{if $coupon}#{$coupon->getId()|escape:'html'}{/if}</span></p>
		<p>Your <span class="prize">{if $prize}{$prize->getName()|escape:'html'}{/if}</span> should be delivered within the next 28 days to:</p>
		<p class="address">
			{if $address}
				{$address->getAddress1()|escape:'html'}<br/>
				{if $address->getAddress2()}
					{$address->getAddress2()|escape:'html'}<br/>
				{/if}
				{if $address->getAddress3()}
					{$address->getAddress3()|escape:'html'}<br/>
				{/if}
				{$address->getTown()|escape:'html'}<br/>
				{$address->getCounty()|escape:'html'}<br/>
				{$address->getPostcode()|escape:'html'}
			{/if}
		</p>
		<p>Regards,</p>
		<p>{$config->getVar('email_from_name')|escape:'html'}</p>
	</body>
</html>