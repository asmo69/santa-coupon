{if $address}Dear {$address->getFirst()} {$address->getLast()},{/if}


You're a winner. You reference number is {if $coupon}#{$coupon->getId()}{/if}


Your {if $prize}{$prize->getName()}{else}prize{/if} should be delivered within the next 28 days to:

{if $address}
{$address->getAddress1()}
{if $address->getAddress2()}{$address->getAddress2()}{/if}
{if $address->getAddress3()}{$address->getAddress3()}{/if}
{$address->getTown()}
{$address->getCounty()}
{$address->getPostcode()}
{/if}

Regards,
{$config->getVar('email_from_name')}