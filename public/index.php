<?php
	
	/**
	 * index.php
	 *
	 * This file holds the code through which all requests for front end
	 * web pages must pass. The file uses Application and
	 * Controller_Website_Front classes to determine which Action class
	 * should be executed to handle the page request.
	 *
	 * @since 2013-01-02
	 * @see Application, Controller, Controller_Website, Controller_Website_Santa
	 */
	
	/* ---------------------------------------------------------------------- */
	
	// Load the Bootstrap
	require_once dirname(__FILE__) . '/../bootstrap.php';
	
	// Create the application context, give it a controller and start
	// the application.
	$application = Application::getInstance();
	$application->setController(Controller::create('Website/Santa'));
	$application->start();
	
	/* ---------------------------------------------------------------------- */
	
?>