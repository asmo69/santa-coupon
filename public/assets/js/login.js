/* ----------------------------------------------------------------- */

$(document).ready(function() {
	$('#container .game').snow({
		number: 50,
		image: "./assets/img/flake.png"
	});

	$('#container .game').smoke({
		number: 15,
		width: 40,
		height: 35,
		x: 420,
		image: "./assets/img/smoke.png"
	});

	doInit();
})

/* ----------------------------------------------------------------- */
