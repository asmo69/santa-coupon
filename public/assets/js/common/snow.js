jQuery.fn.snow = function(args) {
	var container = $(this);
	var x = (args && args.x) ? parseInt(args.x) : 0;
	var y = (args && args.y) ? parseInt(args.y) : 0;
	var number = (args && args.number) ? parseInt(args.number) : 200;
	var duration = (args && args.duration) ? parseInt(args.duration) : 70;
	var width = (args && args.width) ? parseInt(args.width) : container.width();
	var height = (args && args.height) ? parseInt(args.height) : container.height();
	var image = (args && args.image) ? args.image : './flake.png';
	var flakes = new Array();
	var timer = null;
	
	function _init() {
		var i;

		if ( flakes.length == 0 ) {
			for ( i = 0; i < number; i++ ) {
				var temp = {};
				temp.x = Math.random() * width;
				temp.y = Math.random() * height;
				temp.size = Math.ceil(Math.random() * 5);
				temp.speed = (Math.random() * 5) + 0.5;
				temp.drift = Math.ceil(Math.random() * 30);
				temp.random = Math.ceil(Math.random() * 100);
				temp.opacity = Math.random() * 0.8;
				temp.flake = null;
				flakes.push(temp);
			}
		}
		
		for ( i = 0; i < number; i++ ) {
			if ( $('#snow-flake-' + i).length == 0 ) {
				var html = '';
				if ( jQuery.browser.msie && jQuery.browser.version < 9 ) {
					html = '<div id="snow-flake-' + i + '" style="position: absolute; left: ' + (x + flakes[i].x) + 'px; top: ' + (y + flakes[i].y) + 'px; width: ' + flakes[i].size + 'px; height: ' + flakes[i].size + 'px; opacity: ' + flakes[i].opacity + '; filter:alpha(opacity=' + (100 * flakes[i].opacity) + '); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=\'true\',sizingMethod=\'scale\',src=\'' + image + '\') progid:DXImageTransform.Microsoft.Alpha(' + (100 * flakes[i].opacity) + '); font-size: 0px">&nbsp;</div>';
				}
				else {
					html = '<div id="snow-flake-' + i + '" style="position: absolute; left: ' + (x + flakes[i].x) + 'px; top: ' + (y + flakes[i].y) + 'px; width: ' + flakes[i].size + 'px; height: ' + flakes[i].size + 'px; opacity: ' + flakes[i].opacity + '; filter:alpha(opacity=' + (100 * flakes[i].opacity) + ');"><img src="' + image + '" width="100%" height="100%" alt="" /></div>';
				}
				container.append(html);
			}
			flakes[i].flake = $('#snow-flake-' + i);	
		}
	}

	function _loop() {
		var i;
		
		if ( timer ) {
			clearTimeout(timer);
			timer = null;
		}

		for ( i = 0; i < number; i++ ) {
			if ( flakes[i].flake.length ) {
				flakes[i].y += flakes[i].speed;
				if ( flakes[i].y > (height + flakes[i].size) ) {
					flakes[i].y = -1 * flakes[i].size;
					flakes[i].x = Math.random() * width;
				}	
				
				flakes[i].flake.css('left', (x + (flakes[i].x + (Math.sin(flakes[i].y / 20 + flakes[i].random) * flakes[i].drift))) + 'px');
				flakes[i].flake.css('top', (y + flakes[i].y) + 'px');
			}
		}

		timer = setTimeout(function() {
		_loop();
		}, duration);
	}
	
	_init();
	_loop();
	
}
