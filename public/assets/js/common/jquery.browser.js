jQuery.browser = {};
jQuery.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit    /.test(navigator.userAgent.toLowerCase());
jQuery.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
jQuery.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
jQuery.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());

var ua = navigator.userAgent
var temp;
var match = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);


if ( match && (temp = ua.match(/version\/([\.\d]+)/i)) != null ) {
	match[2] = temp[1];
}
if ( match ) {
	jQuery.browser.version = parseInt(match[2]);
}
