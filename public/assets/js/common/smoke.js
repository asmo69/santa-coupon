jQuery.fn.smoke = function(args) {
	var container = $(this);
	var x = (args && args.x) ? parseInt(args.x) : 0;
	var y = (args && args.y) ? parseInt(args.y) : 0;
	var number = (args && args.number) ? parseInt(args.number) : 10;
	var duration = (args && args.duration) ? parseInt(args.duration) : 70;
	var width = (args && args.width) ? parseInt(args.width) : container.width();
	var height = (args && args.height) ? parseInt(args.height) : container.height();
	var image = (args && args.image) ? args.image : './smoke.png';
	var smoke = new Array();
	var timer = null;

	function _init() {
		var i;

		if ( smoke.length == 0 ) {
			for ( i = 0; i < number; i++ ) {
				var temp = {};
				temp.x = Math.random() * width;
				temp.y = Math.random() * height;
				temp.size = Math.ceil(Math.random() * 30);
				temp.grow = (Math.random() * 2);
				temp.speed = (Math.random() * 5) + 0.5;
				temp.drift = Math.ceil(Math.random() * 10);
				temp.random = Math.ceil(Math.random() * 100);
				temp.rotate = Math.ceil(Math.random() * 360);
				temp.opacity = (Math.random() * 0.5) + 0.5;
				temp.smoke = null;
				smoke.push(temp);
			}
		}
		
		for ( i = 0; i < number; i++ ) {
			if ( $('#smoke-' + i).length == 0 ) {
				var html = '';
				if ( jQuery.browser.msie && jQuery.browser.version < 9 ) {
					html = '<div id="smoke-' + i + '" style="position: absolute; left: ' + (x + smoke[i].x) + 'px; top: ' + (y + smoke[i].y) + 'px; width: ' + smoke[i].size + 'px; height: ' + smoke[i].size + 'px; opacity: ' + smoke[i].opacity + '; filter:alpha(opacity=' + (100 * smoke[i].opacity) + '); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=\'true\',sizingMethod=\'scale\',src=\'' + image + '\'),progid:DXImageTransform.Microsoft.Alpha(' + (100 * smoke[i].opacity) + '); font-size: 0px">&nbsp;</div>';
				}
				else {
					html = '<div id="smoke-' + i + '" style="position: absolute; left: ' + (x + smoke[i].x) + 'px; top: ' + (y + smoke[i].y) + 'px; width: ' + smoke[i].size + 'px; height: ' + smoke[i].size + 'px; opacity: ' + smoke[i].opacity + '; filter:alpha(opacity=' + (100 * smoke[i].opacity) + '); -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=' + (100 * smoke[i].opacity) + '); transform:rotate(' + smoke[i].rotate + 'deg); -ms-transform:rotate(' + smoke[i].rotate + 'deg); -webkit-transform:rotate(' + smoke[i].rotate + 'deg)"><img src="' + image + '" width="100%" height="100%" alt="" /></div>';
				}
				container.append(html);
				smoke[i].smoke = $('#smoke-' + i);	
			}
		}
	}
	

	function _loop() {
		var i;
		
		if ( timer ) {
			clearTimeout(timer);
			timer = null;
		}

		for ( i = 0; i < number; i++ ) {
			if ( smoke[i].smoke.length ) {
				smoke[i].y -= smoke[i].speed;
				if ( smoke[i].y < (-1 * smoke[i].smoke.height()) ) {
					smoke[i].y = height;
					smoke[i].x = Math.random() * width;
					smoke[i].smoke.css('width', smoke[i].size);
					smoke[i].smoke.css('height', smoke[i].size);
				}	
				if ( smoke[i].smoke.width() < 50 ) {
					smoke[i].smoke.css('width', smoke[i].smoke.width() + smoke[i].grow);
					smoke[i].smoke.css('height', smoke[i].smoke.width() + smoke[i].grow);
				}
				smoke[i].smoke.css('left', (x + (smoke[i].x + (Math.sin(smoke[i].y / 20 + smoke[i].random) * smoke[i].drift))) + 'px');
				smoke[i].smoke.css('top', (y + smoke[i].y) + 'px');
			}
		}

		timer = setTimeout(function() {
			_loop();
		}, duration);
	}

	_init();
	_loop();
	
}

