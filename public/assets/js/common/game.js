jQuery.fn.game = function(args) {
	var container = $(this);
	var size = (args && args.size) ? parseInt(args.size) : 9;
	var match = (args && args.match) ? parseInt(args.match) : 3;

	container.find('a.reveal').click(function() {
		blur();
		container.find('a.tile').each(function() {
			if ( $(this).hasClass('open') == false ) {
				$(this).addClass('open').children('div').fadeIn().fadeOut().fadeIn().fadeOut().fadeIn().fadeOut().fadeIn();
			}
		});
		_complete();
		return false;
	});
	container.find('a.tile').click(function() {
		blur();
		if ( $(this).hasClass('open') == false ) {
			$(this).addClass('open').children('div').fadeIn().fadeOut().fadeIn().fadeOut().fadeIn().fadeOut().fadeIn();
		}
		_complete();
		return false;
	});

	function _complete() {
		if ( container.find('a.open').length == 9 ) {
			setTimeout(function() {
				container.siblings('.modal').show();
				container.siblings('.result').fadeIn('slow');
			}, 3000);
		}
	}

};