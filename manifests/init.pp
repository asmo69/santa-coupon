Exec {
  path => ["/usr/bin", "/bin", "/usr/sbin", "/sbin", "/usr/local/bin", "/usr/local/sbin"]
}

# ----------------------------------------------------------------------
# Packages
# ----------------------------------------------------------------------

exec { "apt-get update":
    command => "/usr/bin/apt-get update -yfq",
    timeout => 0
}
->
package { "python-software-properties":
    ensure => "present"
}
->

exec { "apt-get upgrade":
    command => "/usr/bin/apt-get upgrade -yfq",
    timeout => 0
}
->
package { ["whois", "build-essential", "vim", "emacs", "nano", "git", "curl", "wget", "telnet", "postfix", "memcached", "apache2", "php5", "php5-cli", "php5-curl", "php5-gd", "php5-mysql", "php5-sqlite", "php5-mcrypt", "php5-xdebug", "php5-memcached", "php5-memcache", "php5-xcache", "php-pear", "libapache2-mod-php5", "mailutils", "libsasl2-2", "ca-certificates", "libsasl2-modules"]:
    ensure => latest
}
->

# ----------------------------------------------------------------------
# Install PHPUnit
# ----------------------------------------------------------------------

exec { "php-unit": 
    command => "wget http://phar.phpunit.de/phpunit.phar --output-document=/usr/local/bin/phpunit > /dev/null 2>&1",
    creates => "/usr/local/bin/phpunit",
    timeout => 0
}
->
file { "/usr/local/bin/phpunit":
    ensure => "file",
    mode => "0755"
}
->

# --------------------------------------------------------------------
# Configure XCache
# --------------------------------------------------------------------
exec { "sed -ri 's/xcache.optimizer\s+=\s+Off/xcache.optimizer = On/' /etc/php5/conf.d/xcache.ini":
    timeout => 0,
}
->

# ----------------------------------------------------------------------
# Configure PHP
# ----------------------------------------------------------------------

exec { "php-error-reporting":
    command => "sed -i 's/error_reporting = .*/error_reporting = E_ALL \\& ~E_NOTICE \\& ~E_STRICT \\& ~E_DEPRECATED/g' /etc/php5/apache2/php.ini",
    unless => "grep -i error_reporting /etc/php5/apache2/php.ini | grep E_ALL | grep ~E_NOTICE | grep ~E_STRICT | grep ~E_DEPRECATED"
}
->
exec { "php-display-errors":
    command => "sed -i 's/display_errors = Off/display_errors = On/g' /etc/php5/apache2/php.ini",
    unless => "grep -i display_errors /etc/php5/apache2/php.ini | grep -Pi \"On$\""
}
->
exec { "php-startup-errors":
    command => "sed -i 's/display_startup_errors = Off/display_startup_errors = On/g' /etc/php5/apache2/php.ini",
    unless => "grep -i display_startup_errors /etc/php5/apache2/php.ini | grep -Pi \"On$\""
}
->

# ----------------------------------------------------------------------
# Configure Project
# ----------------------------------------------------------------------

file { "/vagrant":
    ensure => "directory",
    force => true,
    mode => "0777"
}
->
file { "/vagrant_data":
    ensure => "directory",
    force => true,
    mode => "0777"
}
->
file { "/vagrant_data/temp":
    ensure => "directory",
    force => true,
    mode => "0777",
    recurse => true
}
->

file { "/vagrant/temp":
    ensure => "link",
    force => true,
    target => "/vagrant_data/temp"
}
->
file { "/vagrant_data/data":
    ensure => "directory",
    force => true,
    mode => "0777",
    recurse => true
}
->

file { "/vagrant/data":
    ensure => "link",
    force => true,
    target => "/vagrant_data/data"
}
->
exec { "cp /vagrant/install/* /vagrant/data":
}
->
exec { "chmod -R 777 /vagrant/data":
}
->

# ----------------------------------------------------------------------
# Configure apache
# ----------------------------------------------------------------------

exec { "a2enmod php5":
    unless => "apache2ctl -M | grep php5"
}
->
exec { "a2enmod rewrite":
    unless => "apache2ctl -M | grep rewrite"
}
->
exec { "a2enmod headers":
    unless => "apache2ctl -M | grep headers"
}
->
exec { "a2enmod expires":
    unless => "apache2ctl -M | grep expires"
}
->
exec { "a2enmod ssl":
    unless => "apache2ctl -M | grep ssl"
}
->
file { "/etc/apache2/ports.conf":
    ensure => "file",
    content =>"NameVirtualHost *:80

Listen 80
"
}
->
file { "/etc/apache2/sites-available/default":
    ensure => "file",
    content => "<VirtualHost *:80>
    DocumentRoot /vagrant/public

    <Directory /vagrant/public>
        Options All
        AllowOverride All
        Order allow,deny
        allow from all
    </Directory>
    EnableSendfile off
    ServerSignature Off

    ErrorLog \${APACHE_LOG_DIR}/default-error.log
    CustomLog \${APACHE_LOG_DIR}/default-access.log combined
</VirtualHost>
"
}
->

# ----------------------------------------------------------------------
# Configure Git
# ----------------------------------------------------------------------

file { "/home/vagrant/.git-prompt.sh":
    mode => "0755",
    content => "__git_ps1 ()
{
	PS1=\$(git branch 2>/dev/null | grep '*' | awk '{ print $2 }')
	if [ \"\$PS1\" != \"\" ];
	then
		echo \" (\$PS1)\"
	fi
}
"
}
->
file { "/home/vagrant/.bash_aliases":
    mode => "0755",
    content => "if [ -f ~/.git-prompt.sh ];
then
    source ~/.git-prompt.sh
    PS1=\"\\[\$BLUE\\]\\u\\[\$YELLOW\\] \\[\$YELLOW\\]\\w\\[\\033[m\\]\\[\$MAGENTA\\]\\$(__git_ps1)\\[\$WHITE\\]\\$ \"
fi
"
}
->

# ----------------------------------------------------------------------
# Restart Services
# ----------------------------------------------------------------------

exec { "service postfix restart":
}
->
exec { "service memcached restart":
}
->
exec { "service apache2 restart":
}
