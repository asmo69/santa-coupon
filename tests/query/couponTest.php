<?php
	
	require_once dirname(__FILE__) . '/../../bootstrap.php';
	
	/**
	 * CouponTest
	 *
	 * This unit test is designed to test the functionality of the Value_Coupon
	 * and Query_Coupon objects.
	 *
	 * @since 2013-11-06 14:38:45
	 * @see Value_Coupon, Query_Coupon
	 */
	
	class CouponTest extends PHPUnit_Framework_TestCase {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * lipsum
		 *
		 * This function just provides a neat and clean way to retreieve the
		 * lorum ipsum text.
		 *
		 * @access public
		 * @param integer|null $length
		 * @return string
		 */
		
		protected function lipsum($length = null) {
			$data = array_unique(explode(' ', ', , , , . . . . lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum'));
			usort($data, create_function('$a, $b', '{ return rand(-1, 1); }'));
			$ret = str_replace(' ,', '.', str_replace(' .', '.', implode(' ', $data))) . '.';
			return $length !== null ? substr($ret, 0, $length) : $ret;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setup
		 *
		 * This function provides the ability to setup and initialise variables
		 * before each test.
		 *
		 * @access public
		 */
		
		public function setup() {
			$config = Registry::getConfig();
			$config->load(ROOT_DIR . '/configs/application.conf');
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValue
		 *
		 * This function tests the creation and type of value class.
		 *
		 * @access public
		 * @return Value_Coupon
		 */
		
		public function testValue() {
			$value = new Value_Coupon();
			
			// Test the value is NOT NULL
			$this->assertNotNull($value);
			
			// Test the value is an instance of Value_Base_Coupon
			$this->assertInstanceOf('Value_Base_Coupon', $value);
			
			// Test the value is an instance of Value
			$this->assertInstanceOf('Value', $value);
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueId
		 *
		 * This function tests the type and value range of the 'id' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Coupon $value
		 * @return Value_Coupon
		 */
		
		public function testValueId(Value_Coupon $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setId(null)->getId());
			
			// Test for 0
			$this->assertEquals(0, $value->setId(0)->getId());
			
			// Test for random integer
			$this->assertEquals(22006, $value->setId(22006)->getId());
			
			// Test for random float
			$this->assertEquals(113, $value->setId(113.82)->getId());
			
			// Test for random string
			$this->assertEquals(0, $value->setId($this->lipsum())->getId());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueCode
		 *
		 * This function tests the type and value range of the 'code' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Coupon $value
		 * @return Value_Coupon
		 */
		
		public function testValueCode(Value_Coupon $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setCode(null)->getCode());
			
			// Test for random integer
			$this->assertEquals('30367', $value->setCode(30367)->getCode());
			
			// Test for random float
			$this->assertEquals('267.22', $value->setCode(267.22)->getCode());
			
			// Test for random string
			$lipsum = $this->lipsum();
			$this->assertEquals($lipsum, $value->setCode($lipsum)->getCode());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValuePrizeId
		 *
		 * This function tests the type and value range of the 'prize_id' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Coupon $value
		 * @return Value_Coupon
		 */
		
		public function testValuePrizeId(Value_Coupon $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setPrizeId(null)->getPrizeId());
			
			// Test for 0
			$this->assertEquals(0, $value->setPrizeId(0)->getPrizeId());
			
			// Test for random integer
			$this->assertEquals(24762, $value->setPrizeId(24762)->getPrizeId());
			
			// Test for random float
			$this->assertEquals(249, $value->setPrizeId(249.84)->getPrizeId());
			
			// Test for random string
			$this->assertEquals(0, $value->setPrizeId($this->lipsum())->getPrizeId());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueStatusId
		 *
		 * This function tests the type and value range of the 'status_id' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Coupon $value
		 * @return Value_Coupon
		 */
		
		public function testValueStatusId(Value_Coupon $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setStatusId(null)->getStatusId());
			
			// Test for 0
			$this->assertEquals(0, $value->setStatusId(0)->getStatusId());
			
			// Test for random integer
			$this->assertEquals(22727, $value->setStatusId(22727)->getStatusId());
			
			// Test for random float
			$this->assertEquals(284, $value->setStatusId(284.04)->getStatusId());
			
			// Test for random string
			$this->assertEquals(0, $value->setStatusId($this->lipsum())->getStatusId());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQueryInsert
		 *
		 * This function tests the query classes ability to insert a new coupon.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Coupon $value
		 * @return Value_Coupon
		 */
		
		public function testQueryInsert(Value_Coupon $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Fetch a status.
			$status_id = Query_Status::create(Registry::getDB(DB::READ_ONLY))->findOne();
			
			// Test the status is not Null
			$this->assertNotNull($status_id);
			
			// Test the status is an instance of Value_Base_Status
			$this->assertInstanceOf('Value_Base_Status', $status_id);
			
			// Test the status is an instance of Value
			$this->assertInstanceOf('Value', $status_id);
			
			// Fetch a prize.
			$prize_id = Query_Prize::create(Registry::getDB(DB::READ_ONLY))->findOne();
			
			// Test the prize is not Null
			$this->assertNotNull($prize_id);
			
			// Test the prize is an instance of Value_Base_Prize
			$this->assertInstanceOf('Value_Base_Prize', $prize_id);
			
			// Test the prize is an instance of Value
			$this->assertInstanceOf('Value', $prize_id);
			
			// Initialise the value object.
			$value->setStatusId($status_id->getId());
			$value->setPrizeId($prize_id->getId());
			$value->setCode($this->lipsum());
			
			// Save the value object.
			$value->save(Registry::getDB(DB::READ_WRITE));
			
			// Test the primary key the value object.
			$this->assertGreaterThan(0, $value->getId());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQuerySelect
		 *
		 * This function tests the query classes ability to select a coupon.
		 *
		 * @depends testQueryInsert
		 * @access public
		 * @param Value_Coupon $value
		 * @return Value_Coupon
		 */
		
		public function testQuerySelect(Value_Coupon $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Query the database for the value object.
			$query = new Query_Coupon(Registry::getDB(DB::READ_ONLY));
			$query->filterById($value->getId());
			$temp = $query->findOne();
			
			// Test the value is NOT NULL
			$this->assertNotNull($temp);
			
			// Test the value is an instance of Value_Base_Coupon
			$this->assertInstanceOf('Value_Base_Coupon', $temp);
			
			// Test the value is an instance of Value
			$this->assertInstanceOf('Value', $temp);
			
			// Test the values
			$this->assertEquals($value->getId(), $temp->getId());
			$this->assertEquals($value->getCode(), $temp->getCode());
			$this->assertEquals($value->getPrizeId(), $temp->getPrizeId());
			$this->assertEquals($value->getStatusId(), $temp->getStatusId());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQueryUpdate
		 *
		 * This function tests the query classes ability to update a coupon.
		 *
		 * @depends testQuerySelect
		 * @access public
		 * @param Value_Coupon $value
		 * @return Value_Coupon
		 */
		
		public function testQueryUpdate(Value_Coupon $value) {
			// Clone the value object.
			$value = clone $value;
			
			// // Update the value object.
			$value->setCode($this->lipsum());
			
			// Save the value object.
			$value->save(Registry::getDB(DB::READ_WRITE));
			
			// Query the database for the value object.
			$query = new Query_Coupon(Registry::getDB(DB::READ_ONLY));
			$query->filterById($value->getId());
			$temp = $query->findOne();
			
			// Test the value is NOT NULL
			$this->assertNotNull($temp);
			
			// Test the value is an instance of Value_Base_Coupon
			$this->assertInstanceOf('Value_Base_Coupon', $temp);
			
			// Test the value is an instance of Value
			$this->assertInstanceOf('Value', $temp);
			
			// Test the values
			$this->assertEquals($value->getId(), $temp->getId());
			$this->assertEquals($value->getCode(), $temp->getCode());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQueryDelete
		 *
		 * This function tests the query classes ability to delete a coupon.
		 *
		 * @depends testQueryUpdate
		 * @access public
		 * @param Value_Coupon $value
		 * @return Value_Coupon
		 */
		
		public function testQueryDelete(Value_Coupon $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Delete the value object from the database.
			$query = new Query_Coupon(Registry::getDB(DB::READ_WRITE));
			$query->filterById($value->getId());
			$temp = $query->delete();
			
			// Query the database for the value object.
			$query = new Query_Coupon(Registry::getDB(DB::READ_ONLY));
			$query->filterById($value->getId());
			$temp = $query->findOne();
			
			// Test the value is NULL
			$this->assertNull($temp);
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>