<?php
	
	require_once dirname(__FILE__) . '/../../bootstrap.php';
	
	/**
	 * StatusTest
	 *
	 * This unit test is designed to test the functionality of the Value_Status
	 * and Query_Status objects.
	 *
	 * @since 2013-11-06 14:38:45
	 * @see Value_Status, Query_Status
	 */
	
	class StatusTest extends PHPUnit_Framework_TestCase {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * lipsum
		 *
		 * This function just provides a neat and clean way to retreieve the
		 * lorum ipsum text.
		 *
		 * @access public
		 * @param integer|null $length
		 * @return string
		 */
		
		protected function lipsum($length = null) {
			$data = array_unique(explode(' ', ', , , , . . . . lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum'));
			usort($data, create_function('$a, $b', '{ return rand(-1, 1); }'));
			$ret = str_replace(' ,', '.', str_replace(' .', '.', implode(' ', $data))) . '.';
			return $length !== null ? substr($ret, 0, $length) : $ret;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setup
		 *
		 * This function provides the ability to setup and initialise variables
		 * before each test.
		 *
		 * @access public
		 */
		
		public function setup() {
			$config = Registry::getConfig();
			$config->load(ROOT_DIR . '/configs/application.conf');
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValue
		 *
		 * This function tests the creation and type of value class.
		 *
		 * @access public
		 * @return Value_Status
		 */
		
		public function testValue() {
			$value = new Value_Status();
			
			// Test the value is NOT NULL
			$this->assertNotNull($value);
			
			// Test the value is an instance of Value_Base_Status
			$this->assertInstanceOf('Value_Base_Status', $value);
			
			// Test the value is an instance of Value
			$this->assertInstanceOf('Value', $value);
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueId
		 *
		 * This function tests the type and value range of the 'id' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Status $value
		 * @return Value_Status
		 */
		
		public function testValueId(Value_Status $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setId(null)->getId());
			
			// Test for 0
			$this->assertEquals(0, $value->setId(0)->getId());
			
			// Test for random integer
			$this->assertEquals(22902, $value->setId(22902)->getId());
			
			// Test for random float
			$this->assertEquals(310, $value->setId(310.02)->getId());
			
			// Test for random string
			$this->assertEquals(0, $value->setId($this->lipsum())->getId());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueName
		 *
		 * This function tests the type and value range of the 'name' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Status $value
		 * @return Value_Status
		 */
		
		public function testValueName(Value_Status $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setName(null)->getName());
			
			// Test for random integer
			$this->assertEquals('23474', $value->setName(23474)->getName());
			
			// Test for random float
			$this->assertEquals('37.15', $value->setName(37.15)->getName());
			
			// Test for random string
			$lipsum = $this->lipsum();
			$this->assertEquals($lipsum, $value->setName($lipsum)->getName());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQueryInsert
		 *
		 * This function tests the query classes ability to insert a new status.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Status $value
		 * @return Value_Status
		 */
		
		public function testQueryInsert(Value_Status $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Initialise the value object.
			$value->setName($this->lipsum());
			
			// Save the value object.
			$value->save(Registry::getDB(DB::READ_WRITE));
			
			// Test the primary key the value object.
			$this->assertGreaterThan(0, $value->getId());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQuerySelect
		 *
		 * This function tests the query classes ability to select a status.
		 *
		 * @depends testQueryInsert
		 * @access public
		 * @param Value_Status $value
		 * @return Value_Status
		 */
		
		public function testQuerySelect(Value_Status $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Query the database for the value object.
			$query = new Query_Status(Registry::getDB(DB::READ_ONLY));
			$query->filterById($value->getId());
			$temp = $query->findOne();
			
			// Test the value is NOT NULL
			$this->assertNotNull($temp);
			
			// Test the value is an instance of Value_Base_Status
			$this->assertInstanceOf('Value_Base_Status', $temp);
			
			// Test the value is an instance of Value
			$this->assertInstanceOf('Value', $temp);
			
			// Test the values
			$this->assertEquals($value->getId(), $temp->getId());
			$this->assertEquals($value->getName(), $temp->getName());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQueryUpdate
		 *
		 * This function tests the query classes ability to update a status.
		 *
		 * @depends testQuerySelect
		 * @access public
		 * @param Value_Status $value
		 * @return Value_Status
		 */
		
		public function testQueryUpdate(Value_Status $value) {
			// Clone the value object.
			$value = clone $value;
			
			// // Update the value object.
			$value->setName($this->lipsum());
			
			// Save the value object.
			$value->save(Registry::getDB(DB::READ_WRITE));
			
			// Query the database for the value object.
			$query = new Query_Status(Registry::getDB(DB::READ_ONLY));
			$query->filterById($value->getId());
			$temp = $query->findOne();
			
			// Test the value is NOT NULL
			$this->assertNotNull($temp);
			
			// Test the value is an instance of Value_Base_Status
			$this->assertInstanceOf('Value_Base_Status', $temp);
			
			// Test the value is an instance of Value
			$this->assertInstanceOf('Value', $temp);
			
			// Test the values
			$this->assertEquals($value->getId(), $temp->getId());
			$this->assertEquals($value->getName(), $temp->getName());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQueryDelete
		 *
		 * This function tests the query classes ability to delete a status.
		 *
		 * @depends testQueryUpdate
		 * @access public
		 * @param Value_Status $value
		 * @return Value_Status
		 */
		
		public function testQueryDelete(Value_Status $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Delete the value object from the database.
			$query = new Query_Status(Registry::getDB(DB::READ_WRITE));
			$query->filterById($value->getId());
			$temp = $query->delete();
			
			// Query the database for the value object.
			$query = new Query_Status(Registry::getDB(DB::READ_ONLY));
			$query->filterById($value->getId());
			$temp = $query->findOne();
			
			// Test the value is NULL
			$this->assertNull($temp);
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>