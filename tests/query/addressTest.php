<?php
	
	require_once dirname(__FILE__) . '/../../bootstrap.php';
	
	/**
	 * AddressTest
	 *
	 * This unit test is designed to test the functionality of the Value_Address
	 * and Query_Address objects.
	 *
	 * @since 2013-11-06 14:38:45
	 * @see Value_Address, Query_Address
	 */
	
	class AddressTest extends PHPUnit_Framework_TestCase {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * lipsum
		 *
		 * This function just provides a neat and clean way to retreieve the
		 * lorum ipsum text.
		 *
		 * @access public
		 * @param integer|null $length
		 * @return string
		 */
		
		protected function lipsum($length = null) {
			$data = array_unique(explode(' ', ', , , , . . . . lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum'));
			usort($data, create_function('$a, $b', '{ return rand(-1, 1); }'));
			$ret = str_replace(' ,', '.', str_replace(' .', '.', implode(' ', $data))) . '.';
			return $length !== null ? substr($ret, 0, $length) : $ret;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setup
		 *
		 * This function provides the ability to setup and initialise variables
		 * before each test.
		 *
		 * @access public
		 */
		
		public function setup() {
			$config = Registry::getConfig();
			$config->load(ROOT_DIR . '/configs/application.conf');
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValue
		 *
		 * This function tests the creation and type of value class.
		 *
		 * @access public
		 * @return Value_Address
		 */
		
		public function testValue() {
			$value = new Value_Address();
			
			// Test the value is NOT NULL
			$this->assertNotNull($value);
			
			// Test the value is an instance of Value_Base_Address
			$this->assertInstanceOf('Value_Base_Address', $value);
			
			// Test the value is an instance of Value
			$this->assertInstanceOf('Value', $value);
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueId
		 *
		 * This function tests the type and value range of the 'id' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Address $value
		 * @return Value_Address
		 */
		
		public function testValueId(Value_Address $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setId(null)->getId());
			
			// Test for 0
			$this->assertEquals(0, $value->setId(0)->getId());
			
			// Test for random integer
			$this->assertEquals(27587, $value->setId(27587)->getId());
			
			// Test for random float
			$this->assertEquals(278, $value->setId(278.32)->getId());
			
			// Test for random string
			$this->assertEquals(0, $value->setId($this->lipsum())->getId());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueCouponId
		 *
		 * This function tests the type and value range of the 'coupon_id' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Address $value
		 * @return Value_Address
		 */
		
		public function testValueCouponId(Value_Address $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setCouponId(null)->getCouponId());
			
			// Test for 0
			$this->assertEquals(0, $value->setCouponId(0)->getCouponId());
			
			// Test for random integer
			$this->assertEquals(25904, $value->setCouponId(25904)->getCouponId());
			
			// Test for random float
			$this->assertEquals(184, $value->setCouponId(184.88)->getCouponId());
			
			// Test for random string
			$this->assertEquals(0, $value->setCouponId($this->lipsum())->getCouponId());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueFirst
		 *
		 * This function tests the type and value range of the 'first' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Address $value
		 * @return Value_Address
		 */
		
		public function testValueFirst(Value_Address $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setFirst(null)->getFirst());
			
			// Test for random integer
			$this->assertEquals('26834', $value->setFirst(26834)->getFirst());
			
			// Test for random float
			$this->assertEquals('173.78', $value->setFirst(173.78)->getFirst());
			
			// Test for random string
			$lipsum = $this->lipsum();
			$this->assertEquals($lipsum, $value->setFirst($lipsum)->getFirst());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueLast
		 *
		 * This function tests the type and value range of the 'last' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Address $value
		 * @return Value_Address
		 */
		
		public function testValueLast(Value_Address $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setLast(null)->getLast());
			
			// Test for random integer
			$this->assertEquals('22203', $value->setLast(22203)->getLast());
			
			// Test for random float
			$this->assertEquals('194.67', $value->setLast(194.67)->getLast());
			
			// Test for random string
			$lipsum = $this->lipsum();
			$this->assertEquals($lipsum, $value->setLast($lipsum)->getLast());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueAddress1
		 *
		 * This function tests the type and value range of the 'address1' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Address $value
		 * @return Value_Address
		 */
		
		public function testValueAddress1(Value_Address $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setAddress1(null)->getAddress1());
			
			// Test for random integer
			$this->assertEquals('21589', $value->setAddress1(21589)->getAddress1());
			
			// Test for random float
			$this->assertEquals('74.92', $value->setAddress1(74.92)->getAddress1());
			
			// Test for random string
			$lipsum = $this->lipsum();
			$this->assertEquals($lipsum, $value->setAddress1($lipsum)->getAddress1());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueAddress2
		 *
		 * This function tests the type and value range of the 'address2' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Address $value
		 * @return Value_Address
		 */
		
		public function testValueAddress2(Value_Address $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setAddress2(null)->getAddress2());
			
			// Test for random integer
			$this->assertEquals('30854', $value->setAddress2(30854)->getAddress2());
			
			// Test for random float
			$this->assertEquals('285.86', $value->setAddress2(285.86)->getAddress2());
			
			// Test for random string
			$lipsum = $this->lipsum();
			$this->assertEquals($lipsum, $value->setAddress2($lipsum)->getAddress2());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueAddress3
		 *
		 * This function tests the type and value range of the 'address3' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Address $value
		 * @return Value_Address
		 */
		
		public function testValueAddress3(Value_Address $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setAddress3(null)->getAddress3());
			
			// Test for random integer
			$this->assertEquals('9909', $value->setAddress3(9909)->getAddress3());
			
			// Test for random float
			$this->assertEquals('97.65', $value->setAddress3(97.65)->getAddress3());
			
			// Test for random string
			$lipsum = $this->lipsum();
			$this->assertEquals($lipsum, $value->setAddress3($lipsum)->getAddress3());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueTown
		 *
		 * This function tests the type and value range of the 'town' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Address $value
		 * @return Value_Address
		 */
		
		public function testValueTown(Value_Address $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setTown(null)->getTown());
			
			// Test for random integer
			$this->assertEquals('3006', $value->setTown(3006)->getTown());
			
			// Test for random float
			$this->assertEquals('273.63', $value->setTown(273.63)->getTown());
			
			// Test for random string
			$lipsum = $this->lipsum();
			$this->assertEquals($lipsum, $value->setTown($lipsum)->getTown());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueCounty
		 *
		 * This function tests the type and value range of the 'county' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Address $value
		 * @return Value_Address
		 */
		
		public function testValueCounty(Value_Address $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setCounty(null)->getCounty());
			
			// Test for random integer
			$this->assertEquals('17525', $value->setCounty(17525)->getCounty());
			
			// Test for random float
			$this->assertEquals('250.12', $value->setCounty(250.12)->getCounty());
			
			// Test for random string
			$lipsum = $this->lipsum();
			$this->assertEquals($lipsum, $value->setCounty($lipsum)->getCounty());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValuePostcode
		 *
		 * This function tests the type and value range of the 'postcode' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Address $value
		 * @return Value_Address
		 */
		
		public function testValuePostcode(Value_Address $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setPostcode(null)->getPostcode());
			
			// Test for random integer
			$this->assertEquals('6744', $value->setPostcode(6744)->getPostcode());
			
			// Test for random float
			$this->assertEquals('158.92', $value->setPostcode(158.92)->getPostcode());
			
			// Test for random string
			$lipsum = $this->lipsum();
			$this->assertEquals($lipsum, $value->setPostcode($lipsum)->getPostcode());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueEmail
		 *
		 * This function tests the type and value range of the 'email' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Address $value
		 * @return Value_Address
		 */
		
		public function testValueEmail(Value_Address $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setEmail(null)->getEmail());
			
			// Test for random integer
			$this->assertEquals('19733', $value->setEmail(19733)->getEmail());
			
			// Test for random float
			$this->assertEquals('315.07', $value->setEmail(315.07)->getEmail());
			
			// Test for random string
			$lipsum = $this->lipsum();
			$this->assertEquals($lipsum, $value->setEmail($lipsum)->getEmail());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQueryInsert
		 *
		 * This function tests the query classes ability to insert a new address.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Address $value
		 * @return Value_Address
		 */
		
		public function testQueryInsert(Value_Address $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Fetch a coupon.
			$coupon_id = Query_Coupon::create(Registry::getDB(DB::READ_ONLY))->findOne();
			
			// Test the coupon is not Null
			$this->assertNotNull($coupon_id);
			
			// Test the coupon is an instance of Value_Base_Coupon
			$this->assertInstanceOf('Value_Base_Coupon', $coupon_id);
			
			// Test the coupon is an instance of Value
			$this->assertInstanceOf('Value', $coupon_id);
			
			// Initialise the value object.
			$value->setCouponId($coupon_id->getId());
			$value->setFirst($this->lipsum());
			$value->setLast($this->lipsum());
			$value->setAddress1($this->lipsum());
			$value->setAddress2($this->lipsum());
			$value->setAddress3($this->lipsum());
			$value->setTown($this->lipsum());
			$value->setCounty($this->lipsum());
			$value->setPostcode($this->lipsum());
			$value->setEmail($this->lipsum());
			
			// Save the value object.
			$value->save(Registry::getDB(DB::READ_WRITE));
			
			// Test the primary key the value object.
			$this->assertGreaterThan(0, $value->getId());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQuerySelect
		 *
		 * This function tests the query classes ability to select a address.
		 *
		 * @depends testQueryInsert
		 * @access public
		 * @param Value_Address $value
		 * @return Value_Address
		 */
		
		public function testQuerySelect(Value_Address $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Query the database for the value object.
			$query = new Query_Address(Registry::getDB(DB::READ_ONLY));
			$query->filterById($value->getId());
			$temp = $query->findOne();
			
			// Test the value is NOT NULL
			$this->assertNotNull($temp);
			
			// Test the value is an instance of Value_Base_Address
			$this->assertInstanceOf('Value_Base_Address', $temp);
			
			// Test the value is an instance of Value
			$this->assertInstanceOf('Value', $temp);
			
			// Test the values
			$this->assertEquals($value->getId(), $temp->getId());
			$this->assertEquals($value->getCouponId(), $temp->getCouponId());
			$this->assertEquals($value->getFirst(), $temp->getFirst());
			$this->assertEquals($value->getLast(), $temp->getLast());
			$this->assertEquals($value->getAddress1(), $temp->getAddress1());
			$this->assertEquals($value->getAddress2(), $temp->getAddress2());
			$this->assertEquals($value->getAddress3(), $temp->getAddress3());
			$this->assertEquals($value->getTown(), $temp->getTown());
			$this->assertEquals($value->getCounty(), $temp->getCounty());
			$this->assertEquals($value->getPostcode(), $temp->getPostcode());
			$this->assertEquals($value->getEmail(), $temp->getEmail());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQueryUpdate
		 *
		 * This function tests the query classes ability to update a address.
		 *
		 * @depends testQuerySelect
		 * @access public
		 * @param Value_Address $value
		 * @return Value_Address
		 */
		
		public function testQueryUpdate(Value_Address $value) {
			// Clone the value object.
			$value = clone $value;
			
			// // Update the value object.
			$value->setFirst($this->lipsum());
			$value->setLast($this->lipsum());
			$value->setAddress1($this->lipsum());
			$value->setAddress2($this->lipsum());
			$value->setAddress3($this->lipsum());
			$value->setTown($this->lipsum());
			$value->setCounty($this->lipsum());
			$value->setPostcode($this->lipsum());
			$value->setEmail($this->lipsum());
			
			// Save the value object.
			$value->save(Registry::getDB(DB::READ_WRITE));
			
			// Query the database for the value object.
			$query = new Query_Address(Registry::getDB(DB::READ_ONLY));
			$query->filterById($value->getId());
			$temp = $query->findOne();
			
			// Test the value is NOT NULL
			$this->assertNotNull($temp);
			
			// Test the value is an instance of Value_Base_Address
			$this->assertInstanceOf('Value_Base_Address', $temp);
			
			// Test the value is an instance of Value
			$this->assertInstanceOf('Value', $temp);
			
			// Test the values
			$this->assertEquals($value->getId(), $temp->getId());
			$this->assertEquals($value->getFirst(), $temp->getFirst());
			$this->assertEquals($value->getLast(), $temp->getLast());
			$this->assertEquals($value->getAddress1(), $temp->getAddress1());
			$this->assertEquals($value->getAddress2(), $temp->getAddress2());
			$this->assertEquals($value->getAddress3(), $temp->getAddress3());
			$this->assertEquals($value->getTown(), $temp->getTown());
			$this->assertEquals($value->getCounty(), $temp->getCounty());
			$this->assertEquals($value->getPostcode(), $temp->getPostcode());
			$this->assertEquals($value->getEmail(), $temp->getEmail());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQueryDelete
		 *
		 * This function tests the query classes ability to delete a address.
		 *
		 * @depends testQueryUpdate
		 * @access public
		 * @param Value_Address $value
		 * @return Value_Address
		 */
		
		public function testQueryDelete(Value_Address $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Delete the value object from the database.
			$query = new Query_Address(Registry::getDB(DB::READ_WRITE));
			$query->filterById($value->getId());
			$temp = $query->delete();
			
			// Query the database for the value object.
			$query = new Query_Address(Registry::getDB(DB::READ_ONLY));
			$query->filterById($value->getId());
			$temp = $query->findOne();
			
			// Test the value is NULL
			$this->assertNull($temp);
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>