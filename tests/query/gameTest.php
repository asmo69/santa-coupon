<?php
	
	require_once dirname(__FILE__) . '/../../bootstrap.php';
	
	/**
	 * GameTest
	 *
	 * This unit test is designed to test the functionality of the Value_Game
	 * and Query_Game objects.
	 *
	 * @since 2013-11-06 14:38:45
	 * @see Value_Game, Query_Game
	 */
	
	class GameTest extends PHPUnit_Framework_TestCase {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * lipsum
		 *
		 * This function just provides a neat and clean way to retreieve the
		 * lorum ipsum text.
		 *
		 * @access public
		 * @param integer|null $length
		 * @return string
		 */
		
		protected function lipsum($length = null) {
			$data = array_unique(explode(' ', ', , , , . . . . lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum'));
			usort($data, create_function('$a, $b', '{ return rand(-1, 1); }'));
			$ret = str_replace(' ,', '.', str_replace(' .', '.', implode(' ', $data))) . '.';
			return $length !== null ? substr($ret, 0, $length) : $ret;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setup
		 *
		 * This function provides the ability to setup and initialise variables
		 * before each test.
		 *
		 * @access public
		 */
		
		public function setup() {
			$config = Registry::getConfig();
			$config->load(ROOT_DIR . '/configs/application.conf');
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValue
		 *
		 * This function tests the creation and type of value class.
		 *
		 * @access public
		 * @return Value_Game
		 */
		
		public function testValue() {
			$value = new Value_Game();
			
			// Test the value is NOT NULL
			$this->assertNotNull($value);
			
			// Test the value is an instance of Value_Base_Game
			$this->assertInstanceOf('Value_Base_Game', $value);
			
			// Test the value is an instance of Value
			$this->assertInstanceOf('Value', $value);
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueId
		 *
		 * This function tests the type and value range of the 'id' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Game $value
		 * @return Value_Game
		 */
		
		public function testValueId(Value_Game $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setId(null)->getId());
			
			// Test for 0
			$this->assertEquals(0, $value->setId(0)->getId());
			
			// Test for random integer
			$this->assertEquals(23701, $value->setId(23701)->getId());
			
			// Test for random float
			$this->assertEquals(154, $value->setId(154.53)->getId());
			
			// Test for random string
			$this->assertEquals(0, $value->setId($this->lipsum())->getId());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueCouponId
		 *
		 * This function tests the type and value range of the 'coupon_id' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Game $value
		 * @return Value_Game
		 */
		
		public function testValueCouponId(Value_Game $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setCouponId(null)->getCouponId());
			
			// Test for 0
			$this->assertEquals(0, $value->setCouponId(0)->getCouponId());
			
			// Test for random integer
			$this->assertEquals(8142, $value->setCouponId(8142)->getCouponId());
			
			// Test for random float
			$this->assertEquals(163, $value->setCouponId(163.33)->getCouponId());
			
			// Test for random string
			$this->assertEquals(0, $value->setCouponId($this->lipsum())->getCouponId());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueConfig
		 *
		 * This function tests the type and value range of the 'config' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Game $value
		 * @return Value_Game
		 */
		
		public function testValueConfig(Value_Game $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setConfig(null)->getConfig());
			
			// Test for random integer
			$this->assertEquals('26591', $value->setConfig(26591)->getConfig());
			
			// Test for random float
			$this->assertEquals('68.51', $value->setConfig(68.51)->getConfig());
			
			// Test for random string
			$lipsum = $this->lipsum();
			$this->assertEquals($lipsum, $value->setConfig($lipsum)->getConfig());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQueryInsert
		 *
		 * This function tests the query classes ability to insert a new game.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Game $value
		 * @return Value_Game
		 */
		
		public function testQueryInsert(Value_Game $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Fetch a coupon.
			$coupon_id = Query_Coupon::create(Registry::getDB(DB::READ_ONLY))->findOne();
			
			// Test the coupon is not Null
			$this->assertNotNull($coupon_id);
			
			// Test the coupon is an instance of Value_Base_Coupon
			$this->assertInstanceOf('Value_Base_Coupon', $coupon_id);
			
			// Test the coupon is an instance of Value
			$this->assertInstanceOf('Value', $coupon_id);
			
			// Initialise the value object.
			$value->setCouponId($coupon_id->getId());
			$value->setConfig($this->lipsum());
			
			// Save the value object.
			$value->save(Registry::getDB(DB::READ_WRITE));
			
			// Test the primary key the value object.
			$this->assertGreaterThan(0, $value->getId());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQuerySelect
		 *
		 * This function tests the query classes ability to select a game.
		 *
		 * @depends testQueryInsert
		 * @access public
		 * @param Value_Game $value
		 * @return Value_Game
		 */
		
		public function testQuerySelect(Value_Game $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Query the database for the value object.
			$query = new Query_Game(Registry::getDB(DB::READ_ONLY));
			$query->filterById($value->getId());
			$temp = $query->findOne();
			
			// Test the value is NOT NULL
			$this->assertNotNull($temp);
			
			// Test the value is an instance of Value_Base_Game
			$this->assertInstanceOf('Value_Base_Game', $temp);
			
			// Test the value is an instance of Value
			$this->assertInstanceOf('Value', $temp);
			
			// Test the values
			$this->assertEquals($value->getId(), $temp->getId());
			$this->assertEquals($value->getCouponId(), $temp->getCouponId());
			$this->assertEquals($value->getConfig(), $temp->getConfig());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQueryUpdate
		 *
		 * This function tests the query classes ability to update a game.
		 *
		 * @depends testQuerySelect
		 * @access public
		 * @param Value_Game $value
		 * @return Value_Game
		 */
		
		public function testQueryUpdate(Value_Game $value) {
			// Clone the value object.
			$value = clone $value;
			
			// // Update the value object.
			$value->setConfig($this->lipsum());
			
			// Save the value object.
			$value->save(Registry::getDB(DB::READ_WRITE));
			
			// Query the database for the value object.
			$query = new Query_Game(Registry::getDB(DB::READ_ONLY));
			$query->filterById($value->getId());
			$temp = $query->findOne();
			
			// Test the value is NOT NULL
			$this->assertNotNull($temp);
			
			// Test the value is an instance of Value_Base_Game
			$this->assertInstanceOf('Value_Base_Game', $temp);
			
			// Test the value is an instance of Value
			$this->assertInstanceOf('Value', $temp);
			
			// Test the values
			$this->assertEquals($value->getId(), $temp->getId());
			$this->assertEquals($value->getConfig(), $temp->getConfig());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQueryDelete
		 *
		 * This function tests the query classes ability to delete a game.
		 *
		 * @depends testQueryUpdate
		 * @access public
		 * @param Value_Game $value
		 * @return Value_Game
		 */
		
		public function testQueryDelete(Value_Game $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Delete the value object from the database.
			$query = new Query_Game(Registry::getDB(DB::READ_WRITE));
			$query->filterById($value->getId());
			$temp = $query->delete();
			
			// Query the database for the value object.
			$query = new Query_Game(Registry::getDB(DB::READ_ONLY));
			$query->filterById($value->getId());
			$temp = $query->findOne();
			
			// Test the value is NULL
			$this->assertNull($temp);
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>