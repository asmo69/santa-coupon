<?php
	
	require_once dirname(__FILE__) . '/../../bootstrap.php';
	
	/**
	 * PrizeTest
	 *
	 * This unit test is designed to test the functionality of the Value_Prize
	 * and Query_Prize objects.
	 *
	 * @since 2013-11-06 14:38:45
	 * @see Value_Prize, Query_Prize
	 */
	
	class PrizeTest extends PHPUnit_Framework_TestCase {
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * lipsum
		 *
		 * This function just provides a neat and clean way to retreieve the
		 * lorum ipsum text.
		 *
		 * @access public
		 * @param integer|null $length
		 * @return string
		 */
		
		protected function lipsum($length = null) {
			$data = array_unique(explode(' ', ', , , , . . . . lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum'));
			usort($data, create_function('$a, $b', '{ return rand(-1, 1); }'));
			$ret = str_replace(' ,', '.', str_replace(' .', '.', implode(' ', $data))) . '.';
			return $length !== null ? substr($ret, 0, $length) : $ret;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * setup
		 *
		 * This function provides the ability to setup and initialise variables
		 * before each test.
		 *
		 * @access public
		 */
		
		public function setup() {
			$config = Registry::getConfig();
			$config->load(ROOT_DIR . '/configs/application.conf');
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValue
		 *
		 * This function tests the creation and type of value class.
		 *
		 * @access public
		 * @return Value_Prize
		 */
		
		public function testValue() {
			$value = new Value_Prize();
			
			// Test the value is NOT NULL
			$this->assertNotNull($value);
			
			// Test the value is an instance of Value_Base_Prize
			$this->assertInstanceOf('Value_Base_Prize', $value);
			
			// Test the value is an instance of Value
			$this->assertInstanceOf('Value', $value);
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueId
		 *
		 * This function tests the type and value range of the 'id' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Prize $value
		 * @return Value_Prize
		 */
		
		public function testValueId(Value_Prize $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setId(null)->getId());
			
			// Test for 0
			$this->assertEquals(0, $value->setId(0)->getId());
			
			// Test for random integer
			$this->assertEquals(24634, $value->setId(24634)->getId());
			
			// Test for random float
			$this->assertEquals(42, $value->setId(42.11)->getId());
			
			// Test for random string
			$this->assertEquals(0, $value->setId($this->lipsum())->getId());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueCode
		 *
		 * This function tests the type and value range of the 'code' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Prize $value
		 * @return Value_Prize
		 */
		
		public function testValueCode(Value_Prize $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setCode(null)->getCode());
			
			// Test for random integer
			$this->assertEquals('17289', $value->setCode(17289)->getCode());
			
			// Test for random float
			$this->assertEquals('113.86', $value->setCode(113.86)->getCode());
			
			// Test for random string
			$lipsum = $this->lipsum();
			$this->assertEquals($lipsum, $value->setCode($lipsum)->getCode());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueName
		 *
		 * This function tests the type and value range of the 'name' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Prize $value
		 * @return Value_Prize
		 */
		
		public function testValueName(Value_Prize $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setName(null)->getName());
			
			// Test for random integer
			$this->assertEquals('6997', $value->setName(6997)->getName());
			
			// Test for random float
			$this->assertEquals('24.16', $value->setName(24.16)->getName());
			
			// Test for random string
			$lipsum = $this->lipsum();
			$this->assertEquals($lipsum, $value->setName($lipsum)->getName());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testValueStock
		 *
		 * This function tests the type and value range of the 'stock' field.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Prize $value
		 * @return Value_Prize
		 */
		
		public function testValueStock(Value_Prize $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Test for NULL allowed
			$this->assertEquals(null, $value->setStock(null)->getStock());
			
			// Test for 0
			$this->assertEquals(0, $value->setStock(0)->getStock());
			
			// Test for random integer
			$this->assertEquals(10912, $value->setStock(10912)->getStock());
			
			// Test for random float
			$this->assertEquals(64, $value->setStock(64.2)->getStock());
			
			// Test for random string
			$this->assertEquals(0, $value->setStock($this->lipsum())->getStock());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQueryInsert
		 *
		 * This function tests the query classes ability to insert a new prize.
		 *
		 * @depends testValue
		 * @access public
		 * @param Value_Prize $value
		 * @return Value_Prize
		 */
		
		public function testQueryInsert(Value_Prize $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Initialise the value object.
			$value->setCode($this->lipsum());
			$value->setName($this->lipsum());
			$value->setStock(545427);
			
			// Save the value object.
			$value->save(Registry::getDB(DB::READ_WRITE));
			
			// Test the primary key the value object.
			$this->assertGreaterThan(0, $value->getId());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQuerySelect
		 *
		 * This function tests the query classes ability to select a prize.
		 *
		 * @depends testQueryInsert
		 * @access public
		 * @param Value_Prize $value
		 * @return Value_Prize
		 */
		
		public function testQuerySelect(Value_Prize $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Query the database for the value object.
			$query = new Query_Prize(Registry::getDB(DB::READ_ONLY));
			$query->filterById($value->getId());
			$temp = $query->findOne();
			
			// Test the value is NOT NULL
			$this->assertNotNull($temp);
			
			// Test the value is an instance of Value_Base_Prize
			$this->assertInstanceOf('Value_Base_Prize', $temp);
			
			// Test the value is an instance of Value
			$this->assertInstanceOf('Value', $temp);
			
			// Test the values
			$this->assertEquals($value->getId(), $temp->getId());
			$this->assertEquals($value->getCode(), $temp->getCode());
			$this->assertEquals($value->getName(), $temp->getName());
			$this->assertEquals($value->getStock(), $temp->getStock());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQueryUpdate
		 *
		 * This function tests the query classes ability to update a prize.
		 *
		 * @depends testQuerySelect
		 * @access public
		 * @param Value_Prize $value
		 * @return Value_Prize
		 */
		
		public function testQueryUpdate(Value_Prize $value) {
			// Clone the value object.
			$value = clone $value;
			
			// // Update the value object.
			$value->setCode($this->lipsum());
			$value->setName($this->lipsum());
			$value->setStock(242481);
			
			// Save the value object.
			$value->save(Registry::getDB(DB::READ_WRITE));
			
			// Query the database for the value object.
			$query = new Query_Prize(Registry::getDB(DB::READ_ONLY));
			$query->filterById($value->getId());
			$temp = $query->findOne();
			
			// Test the value is NOT NULL
			$this->assertNotNull($temp);
			
			// Test the value is an instance of Value_Base_Prize
			$this->assertInstanceOf('Value_Base_Prize', $temp);
			
			// Test the value is an instance of Value
			$this->assertInstanceOf('Value', $temp);
			
			// Test the values
			$this->assertEquals($value->getId(), $temp->getId());
			$this->assertEquals($value->getCode(), $temp->getCode());
			$this->assertEquals($value->getName(), $temp->getName());
			$this->assertEquals($value->getStock(), $temp->getStock());
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
		/**
		 * testQueryDelete
		 *
		 * This function tests the query classes ability to delete a prize.
		 *
		 * @depends testQueryUpdate
		 * @access public
		 * @param Value_Prize $value
		 * @return Value_Prize
		 */
		
		public function testQueryDelete(Value_Prize $value) {
			// Clone the value object.
			$value = clone $value;
			
			// Delete the value object from the database.
			$query = new Query_Prize(Registry::getDB(DB::READ_WRITE));
			$query->filterById($value->getId());
			$temp = $query->delete();
			
			// Query the database for the value object.
			$query = new Query_Prize(Registry::getDB(DB::READ_ONLY));
			$query->filterById($value->getId());
			$temp = $query->findOne();
			
			// Test the value is NULL
			$this->assertNull($temp);
			
			return $value;
		}
		
		/* ------------------------------------------------------------------ */
		
	}
	
?>