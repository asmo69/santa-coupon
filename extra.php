<?php
	
	/**
	 * functions.php
	 *
	 * This file holds common and misc functions that are useful within
	 * the main code.
	 *
	 * @since 2013-01-02
	 */
	
	/* ---------------------------------------------------------------------- */
	
	defined('E_ERROR') || define('E_ERROR', 1);
	defined('E_WARNING') || define('E_WARNING', 2);
	defined('E_PARSE') || define('E_PARSE', 4);
	defined('E_NOTICE') || define('E_NOTICE', 8);
	defined('E_CORE_ERROR') || define('E_CORE_ERROR', 16);
	defined('E_CORE_WARNING') || define('E_CORE_WARNING', 32);
	defined('E_COMPILE_ERROR') || define('E_COMPILE_ERROR', 64);
	defined('E_COMPILE_WARNING') || define('E_COMPILE_WARNING', 128);
	defined('E_USER_ERROR') || define('E_USER_ERROR', 256);
	defined('E_USER_WARNING') || define('E_USER_WARNING', 512);
	defined('E_USER_NOTICE') || define('E_USER_NOTICE', 1024);
	defined('E_STRICT') || define('E_STRICT', 2048);
	defined('E_RECOVERABLE_ERROR') || define('E_RECOVERABLE_ERROR', 4096);
	defined('E_DEPRECATED') || define('E_DEPRECATED', 8192);
	defined('E_USER_DEPRECATED') || define('E_USER_DEPRECATED', 16384);
	defined('E_ALL') || define('E_ALL', 32767);
	
	/* ---------------------------------------------------------------------- */
	
	if (function_exists('array_map_recursive') == false) {
		/**
		 * array_map_recursive
		 *
		 * This function is the same idea as the standard php array_map()
		 * function except that it will recurse through sub arrays.
		 *
		 * e.g.
		 * $request = array_map_recursive('stripslashes', $_REQUEST);
		 *
		 * @param string $func The name of the function to call on each element
		 * in the array.
		 * @param array $arr The array to walk through.
		 * @return array
		 */
		
		function array_map_recursive($func, $arr) {
			$retval = array();
			foreach ($arr as $key => $value) {
				$retval[$key] = is_array($value) ? array_map_recursive($func, $value) : (is_array($func) ? call_user_func_array($func, $value) : $func($value));
			}
			
			return $retval;
		}
	}
	
	/* ---------------------------------------------------------------------- */
	
	if (!function_exists('printr')) {
		/**
		 * printr
		 *
		 * I am so used to using this function at work that I can't live
		 * without it anymore. The function works the same as the standard
		 * print_r function but created HTML rather than plain text.
		 *
		 * @param mixed $value
		 */
		function printr($value) {
			print('<pre style="background-color: #FFF; color: #000">');
			print(htmlentities(print_r($value, true)));
			print('</pre>');
		}
	}
	
	/* ---------------------------------------------------------------------- */
	
	if (function_exists('ucfirst') === false) {
		function ucfirst($value) {
			return strtoupper(substr($value, 0, 1)) . substr($value, 1);
		}
	}
	
	/* ---------------------------------------------------------------------- */
	
	if (!function_exists('str_replace_once')) {
		function str_replace_once($search, $replace, $subject) {
			$pos = strpos($subject, $search);
			if ($pos !== false) {
				$left  = substr($subject, 0, $pos);
				$right = substr($subject, $pos + strlen($search));
				return $left . $replace . $right;
			} else {
				return $subject;
			}
		}
	}
	
	/* ------------------------------------------------------------------- */
	
	function get_files($dir = '.', $pattern = '', $files = array()) {
		if ($fd = opendir($dir)) {
			while ($file = readdir($fd)) {
				if ($file != '.' && $file != '..') {
					if (is_file($dir . '/' . $file) && $pattern && preg_match($pattern, $dir . '/' . $file)) {
						$files[] = realpath($dir . '/' . $file);
					} elseif (is_file($dir . '/' . $file) && !$pattern) {
						$files[] = realpath($dir . '/' . $file);
					} elseif (is_dir($dir . '/' . $file)) {
						$files = get_files($dir . '/' . $file, $pattern, $files);
					}
				}
			}
			closedir($fd);
		}
		return $files;
	}
	
	/* ------------------------------------------------------------------- */
	
	function mkdir_recursive($path, $mode = 0777) {
		$dirs  = explode(DIRECTORY_SEPARATOR, $path);
		$count = count($dirs);
		$path  = '';
		for ($i = 0; $i < $count; ++$i) {
			$path .= DIRECTORY_SEPARATOR . $dirs[$i];
			if (!is_dir($path) && !mkdir($path, $mode)) {
				return false;
			}
		}
		return true;
	}
	
	/* ------------------------------------------------------------------- */
	
	function json_readable_encode($in, $indent = 0, $from_array = false) {
		$_myself = __FUNCTION__;
		$_escape = function($str) {
			return preg_replace("!([\b\t\n\r\f\"\\'])!", "\\\\\\1", $str);
		};
		
		$out = '';
		
		foreach ($in as $key => $value) {
			$out .= str_repeat("\t", $indent + 1);
			$out .= "\"" . $_escape((string) $key) . "\": ";
			
			if (is_object($value) || is_array($value)) {
				$out .= "\n";
				$out .= $_myself($value, $indent + 1);
			} elseif (is_bool($value)) {
				$out .= $value ? 'true' : 'false';
			} elseif (is_null($value)) {
				$out .= 'null';
			} elseif (is_string($value)) {
				$out .= "\"" . $_escape($value) . "\"";
			} else {
				$out .= $value;
			}
			
			$out .= ",\n";
		}
		
		if (!empty($out)) {
			$out = substr($out, 0, -2);
		}
		
		$out = str_repeat("\t", $indent) . "{\n" . $out;
		$out .= "\n" . str_repeat("\t", $indent) . "}";
		
		return $out;
	}
	
?>