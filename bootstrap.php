<?php
	
	/**
	 * bootstrap.php
	 *
	 * @since 2013-06-28
	 */
	
	// Include the extra.php (used to define extra functions etc).
	if (is_file(dirname(__FILE__) . '/extra.php')) {
		require_once dirname(__FILE__) . '/extra.php';
	}
	
	// Include the Autoloader.
	require_once dirname(__FILE__) . '/modules/core/autoloader.class.php';
	
	// Create a singleton of the Autoloader (by default only the 'core'
	// module is included)
	$loader = Autoloader::getInstance();
	$loader->addModule('santa');
	$loader->addModule('query');
	
?>