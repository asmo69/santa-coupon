<?php
	/* ---------------------------------------------------------------------- */

	function smarty_block_dynamic($param, $content, &$smarty) {
	    return $content;
	}

	/* ---------------------------------------------------------------------- */

	function smarty_strip_filter($source, &$smarty) {
		$source = str_replace(chr(9), '', $source);
		$source = str_replace(' />', '/>', $source);
		while ( strpos($source, '  ') !== false ) {
			$source = str_replace('  ', ' ', $source);
		}

		$source = str_replace(chr(13).chr(10), chr(10), $source);
		$source = str_replace(chr(13), chr(10), $source);
		while ( strpos($source, chr(10).chr(10)) !== false ) {
			$source = str_replace(chr(10).chr(10), chr(10), $source);
		}

		return $source;
	}

	/* ---------------------------------------------------------------------- */

	function smarty_text_get_template($tpl_name, &$tpl_source, &$smarty_obj) {
		$tpl_source = $tpl_name;
		return true;
	}

	/* ---------------------------------------------------------------------- */

	function smarty_text_get_timestamp($tpl_name, &$tpl_timestamp, &$smarty_obj) {
		return time();
	}

	/* ---------------------------------------------------------------------- */

	function smarty_text_get_secure($tpl_name, &$smarty_obj) {
	    return true;
	}

	/* ---------------------------------------------------------------------- */

	function smarty_text_get_trusted($tpl_name, &$smarty_obj) {
	}

	/* ---------------------------------------------------------------------- */

	function smarty_static_get_template($tpl_name, &$tpl_source, &$smarty_obj) {
		$sql = sprintf("SELECT content FROM static_pages WHERE name = '%s' AND active = 1 ORDER BY date DESC LIMIT 1", addslashes($tpl_name));
		$result = mysqlQuery($sql, 'e2web');
		$temp = mysql_fetch_assoc($result);
		$tpl_source = $temp['content'];
		return true;
	}

	/* ---------------------------------------------------------------------- */

	function smarty_static_get_timestamp($tpl_name, &$tpl_timestamp, &$smarty_obj) {
		$sql = sprintf("SELECT UNIX_TIMESTAMP(date) AS date FROM static_pages WHERE name = '%s' AND active = 1 ORDER BY date DESC LIMIT 1", addslashes($tpl_name));
		$result = mysqlQuery($sql, 'e2web');
		$temp = mysql_fetch_assoc($result);
		return $temp['date'];
	}

	/* ---------------------------------------------------------------------- */

	function smarty_static_get_secure($tpl_name, &$smarty_obj) {
	    return true;
	}

	/* ---------------------------------------------------------------------- */

	function smarty_static_get_trusted($tpl_name, &$smarty_obj) {
	}

	/* ---------------------------------------------------------------------- */

	function smarty_mysql_cache_handler($action, &$smarty, &$cache_content, $tpl_file = null, $cache_id = null, $compile_id = null, $exp_time = null) {
    	$db = new Mysql('main');
	    $use_gzip = false;

	    // create unique cache id
	    $CacheID = md5($tpl_file . $cache_id . $compile_id);

		$sql = sprintf("CREATE TABLE IF NOT EXISTS cachePages (cacheID CHAR(32) PRIMARY KEY, cacheContents MEDIUMTEXT NOT NULL)");
		$db->query($sql);

	    switch ( $action ) {
	        case 'read':
	            // read cache from database
	            $sql = sprintf("SELECT cacheContents FROM cachePages WHERE cacheID = '%s'", $CacheID);
	            $results = $db->query($sql);
	            if ( !$results ) {
	                $smarty->_trigger_error_msg('cache_handler: query failed.');
	            }
	            $row = $db->fetch($results);

	            if ( $use_gzip && function_exists('gzuncompress') ) {
	                $cache_content = gzuncompress($row['cacheContents']);
	            }
	            else {
	                $cache_content = $row['cacheContents'];
	            }
	            $return = $results;
	            break;
	        case 'write':
	            // save cache to database
	            if ( $use_gzip && function_exists("gzcompress") ) {
	                // compress the contents for storage efficiency
	                $contents = gzcompress($cache_content);
	            }
	            else {
	                $contents = $cache_content;
	            }
	            $sql = sprintf("REPLACE INTO cachePages VALUES ('%s', '%s')", $CacheID, addslashes($contents));
	            $results = $db->query($sql);
	            if ( !$results ) {
	                $smarty->_trigger_error_msg('cache_handler: query failed.');
	            }
	            $return = $results;
	            break;
	        case 'clear':
	            // clear cache info
	            if ( empty($cache_id) && empty($compile_id) && empty($tpl_file) ) {
	                // clear them all
	                $sql = sprintf("DELETE FROM cachePages");
	                $results = $db->query($sql);
	            }
	            else {
	                $sql = sprintf("DELETE FROM cachePages WHERE cacheID = '%s'", $cacheID);
	                $results = $db->query($sql);
	            }
	            if( !$results ) {
	                $smarty->_trigger_error_msg('cache_handler: query failed.');
	            }
	            $return = $results;
	            break;
	        default:
	            // error, unknown action
	            $smarty->_trigger_error_msg("cache_handler: unknown action \"$action\"");
	            $return = false;
	            break;
	    }
	    return $return;

	}

	/* ---------------------------------------------------------------------- */

	function smarty_memcache_cache_handler($action, &$smarty, &$cache_content, $tpl_file = null, $cache_id = null, $compile_id = null, $exp_time = null) {
		if ( array_key_exists('memcache', $GLOBALS) == false ) {
			$smarty->_trigger_error_msg("cache_handler: no memcache config specified");
		}
		if ( array_key_exists('servers', $GLOBALS['memcahe']) == false ) {
			$smarty->_trigger_error_msg("cache_handler: no memcache config servers specified");
		}
		foreach ( $GLOBALS['memcahe']['servers'] as $server ) {
			$mem->addServer($server[0], $server[1]);
		}

	    // create unique cache id
	    $CacheID = md5($tpl_file . $cache_id . $compile_id);

	    switch ( $action ) {
	        case 'read':
	            // read cache from memcache
                $cache_content = $mem->get($cacheID);
	            $return = true;
	            break;
	        case 'write':
	            // write cache to memcache
				$mem->set($cacheID, $cache_content, MEMCACHE_COMPRESSED, $smarty->cache_lifetime);
	            $return = true;
	            break;
	        case 'clear':
	            // clear cache info
	            if ( empty($cache_id) && empty($compile_id) && empty($tpl_file) ) {
					$mem->flush();
	            }
	            else {
					$mem->delete($cacheID);
				}
	            $return = true;
	            break;
	        default:
	            // error, unknown action
	            $smarty->_trigger_error_msg("cache_handler: unknown action \"$action\"");
	            $return = false;
	            break;
	    }
	    return $return;
	}

	/* ---------------------------------------------------------------------- */
?>