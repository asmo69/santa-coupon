<?php

	function smarty_compiler_debug($tag_attrs, &$compiler) {
		$params = $compiler->_parse_attrs( $tag_attrs );
		$var = $params['var'];

		if ( $var ) {
			return(sprintf('printr($this->get_template_vars(%s));', $var));
		}
		else {
			return(sprintf('printr($this->get_template_vars());'));
		}
	}

?>