<?php
function smarty_modifier_date($string, $format = 'U') {
    if ( !is_numeric($string) ) {
    	$string = strtotime($string);
    }

    return(date($format, $string));
}

?>