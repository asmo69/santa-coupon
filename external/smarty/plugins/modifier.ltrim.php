<?php

function smarty_modifier_ltrim($string, $chars = "")
{
    if ( $chars )
    	return ltrim($string, $chars);
    else
    	return ltrim($string);
}

?>