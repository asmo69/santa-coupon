<?php

	function smarty_compiler_static( $tag_attrs, &$compiler )
	{
	   $params = $compiler->_parse_attrs( $tag_attrs );

	   # class, method, "assign" optional

	   if( !isset( $params[ 'class' ] ) ) {
	      $compiler->_syntax_error( "static: missing 'class' parameter.", E_USER_ERROR );
	   }

	   if( !isset( $params[ 'method' ] ) ) {
	      $compiler->_syntax_error( "static: missing 'method' parameter.", E_USER_ERROR );
	   }

	   $class = str_replace( array( "'", '"' ), '', $params[ 'class' ] );
	   $method = str_replace( array( "'", '"' ), '', $params[ 'method' ] );

	   if( isset( $params[ 'assign' ] ) ) {
	      return( sprintf( '$this->assign( %s, %s::%s() );', $params[ 'assign' ], $class, $method ) );
	   }
	   else {
	      return( sprintf( '%s::%s();', $class, $method ) );
	   }
	}

?>