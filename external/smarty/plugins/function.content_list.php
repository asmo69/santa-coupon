<?php
    /**
     * Smarty (news_list) plugin
     *
     * PHP versions 4 and 5
     *
     * LICENSE: This source file is subject to version 3.0 of the PHP license
     * that is available through the world-wide-web at the following URI:
     * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
     * the PHP License and are unable to obtain it through the web, please
     * send a note to license@php.net so we can mail you a copy immediately.
     *
     * @package Smarty
     * @subpackage plugins
     * @author Stephen Brewster <steve@cite.co.uk>
     * @copyright 2005 Cite DMS
     * @license http://www.php.net/license/3_0.txt PHP License 3.0
     * @version 1.1
     * @since August 8th 2005
     */

    // {{{ smarty_function_news_list()

        /**
         * Get a list of news stories
         *
         * Interfaces with the newsDAO object to provide control over a news
         * list using smarty template tags.
         *
         * @param string config The configuration file to look for database
         * connection details in
         * @param string section The section heading in the config file to look
         * for the database connection details in
         * @param string preifx Prefix all database tables with this string
         * @param string create The variable name to create in smarty (Holds
         * the record set array)
         * @param integer id The unique row id of the story to search for
         * @param integer category The category number to search for stories
         * in
         * @param integer archived The archive number to search for stories in
         * @param datetime startdate The start date to search for stories after
         * @param datetime enddate The end date to search for stories before
         * @param integer topstory Search for the top story
         * @param string keywords The keywords to search for stories
         * @param integer pageno The page number to display (Allows for stories
         *  to be displayed across multiple pages)
         * @param integer pagesize The page size to display
         */

        function smarty_function_content_list($params, &$smarty) {
            // Get the database connection details from the parameters array.
            $config = isset($params['config']) ? $params['config'] : 'global.conf';
            $section = isset($params['section']) ? $params['section'] : 'Database';
            $prefix = isset($params['prefix']) ? $params['prefix'] : '';

            // Get the name of the array to create from the parameters array.
            $create = isset($params['create']) ? $params['create'] : 'news';

            // Get the story search details from the parameters array.
            $id = isset($params['id']) ? $params['id'] : '';
            $category = isset($params['category']) ? $params['category'] : '';
            $archived = isset($params['archived']) ? $params['archived'] : '';
            $startdate = isset($params['startdate']) ? $params['startdate'] : '';
            $enddate = isset($params['enddate']) ? $params['enddate'] : '';
            $topstory = isset($params['topstory']) ? $params['topstory'] : '';
            $keywords = isset($params['keywords']) ? $params['keywords'] : '';

            // Get the paging details from the parameters array.
            $pageno = isset($params['pageno']) ? $params['pageno'] : '0';
            $pagesize = isset($params['pagesize']) ? $params['pagesize'] : '';

            // If the $config variable is not a string then throw an error.
            if (validate($config, 'S') == false) {
                $smarty->trigger_error("'config' must be a string");
                exit();
            }

            // If the $section variable is not a string then throw an error.
            if (validate($section, 'S') == false) {
                $smarty->trigger_error("'section' must be a string");
                exit();
            }

            // If the $prefix variable is not a string then throw an error.
            if (validate($prefix, 'S') == false) {
                $smarty->trigger_error("'prefix' must be a string");
                exit();
            }

            // If the $id variable is not a string then throw an error.
            if (validate($create, 'S:1') == false) {
                $smarty->trigger_error("'create' must be a non empty string");
                exit();
            }

            // If the $id variable is not empty and is not a natural
            // number then throw an error.
            if (validate($id, 'NN') == true) {
                if (validate($id, 'N') == false) {
                    $smarty->trigger_error("'id' must be a natural number");
                    exit();
                }
            }

            // If the $category variable is not empty and is not a natural
            // number then throw an error.
            if (validate($category, 'NN') == true) {
                if (validate($category, 'N') == false) {
                    $smarty->trigger_error("'category' must be a natural number");
                    exit();
                }
            }

            // If the $archived variable is not empty and is not a natural
            // number then throw an error.
            if (validate($archived, 'NN') == true) {
                if (validate($archived, 'N') == false) {
                    $smarty->trigger_error("'archived' must be a natural number");
                    exit();
                }
            }

            // If the $startdate variable is not empty and is not either a
            // natural number or a date then throw an error.
            if (validate($startdate, 'NN') == true) {
                if ((validate($startdate, 'I') == false) && (validate($startdate, 'D') == false)) {
                    $smarty->trigger_error("'startdate' must be a string");
                    exit();
                }
            }

            // If the $enddate variable is not empty and is not either a
            // natural number or a date then throw an error.
            if (validate($enddate, 'NN') == true) {
                if ((validate($enddate, 'I') == false) && (validate($enddate, 'D') == false)) {
                    $smarty->trigger_error("'enddate' must be a string");
                    exit();
                }
            }

            // If the $topstory variable is not empty and is not a natural
            // number then throw an error.
            if (validate($topstory, 'NN') == true) {
                if (validate($topstory, 'N') == false) {
                    $smarty->trigger_error("'topstory' must be a natural number");
                    exit();
                }
            }

            // If the $keywords variable is not a string then throw an error.
            if (validate($keywords, 'S') == false) {
                $smarty->trigger_error("'keywords' must be a string");
                exit();
            }

            // If the $pageno variable is not empty and is not a natural
            // number then throw an error.
            if (validate($pageno, 'NN') == true) {
                if (validate($pageno, 'N') == false) {
                    $smarty->trigger_error("'pageno' must be a natural number");
                    exit();
                }
            }

            // If the $pagesize variable is not empty and is not a natural
            // number then throw an error.
            if (validate($pagesize, 'NN') == true) {
                if (validate($pagesize, 'N') == false) {
                    $smarty->trigger_error("'pagesize' must be a natural number");
                    exit();
                }
            }

            // If there is currently no database connection then...
            if (!isset($smarty->db)) {
                // Load the configuration file data specified by $config and
                // $section.
                $smarty->config_load($config, $section);
                $host = $smarty->get_config_vars('mysql_host');
                $username = $smarty->get_config_vars('mysql_username');
                $password = $smarty->get_config_vars('mysql_password');
                $database = $smarty->get_config_vars('mysql_database');

                // Create the database conenction.
                $smarty->db = new mysql($host, $username, $password, $database);
            }

            // If there is a database connection then...
            if ($smarty->db) {
                // Create a contentDAO object on the database.
                $contentDAO = new contentDAO($smarty->db, $prefix);
                // Create the database if they dont already exist.
                $contentDAO->createTables();
                // Get the record rows that match the search criteria.
                $rows = $contentDAO->getContentList($id, $category, $keywords, $pageno, $pagesize);
                // Assign the record rows to the smarty template parser.
                $smarty->assign($create, $rows);
            }
        }

    // }}}
?>