<?php
	function smarty_modifier_const($object, $constName) {
	  $class = new ReflectionClass($object);
	  return $class->getConstant($constName);
	}
?>