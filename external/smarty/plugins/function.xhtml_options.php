<?php
	if ( function_exists('xhtml_get_var') == false ) {
		function xhtml_get_var($item, $field) {
			$retval = '';

			if ( is_string($field) && $field ) {
				if ( is_object($item) ) {
					if ( in_array($field, get_object_vars($item)) ) {
						$retval = $item->$field;
					}
					else {
						$retval = $item->$field();
					}
				}
				elseif ( is_array($item) && array_key_exists($item) ) {
					$retval = $item[$field];
				}
			}

			return $retval;
		}
	}

	function smarty_function_xhtml_options($params, &$smarty) {
		$from				= array_key_exists('from', $params) ? $params['from'] : array();
		$group_field		= array_key_exists('group_field', $params) ? $params['group_field'] : 'group';
		$value_field		= array_key_exists('value_field', $params) ? $params['value_field'] : 'value';
		$label_field		= array_key_exists('label_field', $params) ? $params['label_field'] : 'label';
		$selected_field		= array_key_exists('selected_field', $params) ? $params['selected_field'] : 'selected';
		$selected_value		= array_key_exists('selected_value', $params) ? $params['selected_value'] : '';
		$escape				= array_key_exists('escape', $params) ? $params['escape'] : '';

		$retval = '';

		if ( is_array($from) ) {
			$last = null;
			foreach ( $from as $item ) {
				$group		= xhtml_get_var($item, $group_field);
				$value		= xhtml_get_var($item, $value_field);
				$label		= xhtml_get_var($item, $label_field);
				$selected	= '';

				if ( $selected_value == $value ) {
					$selected = sprintf(" selected=\"selected\"");
				}

				if ( $group_field && $group != $last ) {
					if ( $last != null ) {
						$retval .= sprintf("</optgroup>\n");
					}
					$retval .= sprintf("<optgroup label=\"%s\">\n", $group);
				}
				$last = $group;

				if ( $escape == 'html' ) {
					$retval .= sprintf("<option value=\"%s\"%s>%s</option>\n", htmlentities($value), $selected, htmlentities($label));
				}
				else {
					$retval .= sprintf("<option value=\"%s\"%s>%s</option>\n", ($value), $selected, ($label));
				}
			}
			if ( $last != null ) {
				$retval .= sprintf("</optgroup>\n");
			}
		}

		return $retval;
	}

?>