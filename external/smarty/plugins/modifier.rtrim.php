<?php

function smarty_modifier_rtrim($string, $chars = "")
{
    if ( $chars )
    	return rtrim($string, $chars);
    else
    	return rtrim($string);
}

?>