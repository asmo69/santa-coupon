<?php

function smarty_modifier_regex($string, $search)
{
    return preg_match($search, $string);
}

/* vim: set expandtab: */

?>