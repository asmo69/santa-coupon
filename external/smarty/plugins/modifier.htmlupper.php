<?php

/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty htmlupper modifier plugin
 *
 * Type:     modifier<br>
 * Name:     htmlupper<br>
 * Purpose:  convert HTML string to uppercase with proper handling of entities
 * @author   Michael Schwarz <mschwarz at technodane dot com>
 * @param string
 * @return string
 */
function smarty_modifier_htmlupper($string)
{
	$isdelim = false;
	$retstr = "";

	$parsearray = preg_split("/(&[A-Za-z0-9#]*;)/", $string, -1, PREG_SPLIT_DELIM_CAPTURE);

	foreach ($parsearray as $fragment) {
		if ($isdelim) {
			// The following big-ass "if" handles all the annoying accented
			// character HTML entities. Does not handle Icelandic entities. Tyler
			// can fix that.
			if (preg_match("/(&.)grave;/", $fragment, $matches) > 0) {
				$retstr .= strtoupper($matches[1]) . "grave;";
			} elseif (preg_match("/(&.)acute;/", $fragment, $matches) > 0) {
				$retstr .= strtoupper($matches[1]) . "acute;";
			} elseif (preg_match("/(&.)circ;/", $fragment, $matches) > 0) {
				$retstr .= strtoupper($matches[1]) . "circ;";
			} elseif (preg_match("/(&.)tilde;/", $fragment, $matches) > 0) {
				$retstr .= strtoupper($matches[1]) . "tilde;";
			} elseif (preg_match("/(&.)uml;/", $fragment, $matches) > 0) {
				$retstr .= strtoupper($matches[1]) . "uml;";
			} elseif (preg_match("/(&.)ring;/", $fragment, $matches) > 0) {
				$retstr .= strtoupper($matches[1]) . "ring;";
			} elseif (preg_match("/(&.)cedil;/", $fragment, $matches) > 0) {
				$retstr .= strtoupper($matches[1]) . "cedil;";
			} elseif (preg_match("/(&..)lig;/", $fragment, $matches) > 0) {
				$retstr .= strtoupper($matches[1]) . "lig;";
			} else {
				// If it isn't one of those uppercasable entities, leave it
				// alone!
				$retstr .= $fragment;
			}
		} else {
			$retstr .= strtoupper($fragment);
		}

		$isdelim = !$isdelim;
	}

	return $retstr;
}

?>