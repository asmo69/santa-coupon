<?php
    function smarty_block_notempty($params, $content, &$smarty, &$repeat) {
        // Get the $var value from the parameters.
        $var = isset($params['var']) ? $params['var'] : '';
        $create = isset($params['create']) ? $params['create'] : 'result';

        if ( $content == '' ) {
           $repeat = true;
        }
        else {
            $repeat = false;
            $result = empty($var) == false ? true : false;
            $smarty->assign($create, $result);
            if ( $result ) {
                return($content);
            }
            else {
                return;
            }
        }
    }
?>