<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty upper modifier plugin
 *
 * Type:     modifier<br>
 * Name:     pad<br>
 * Purpose:  Pad a string to a certain length with another string
 * @param string
 * @param length
 * @param pad
 * @param type
 * @return string
 */
function smarty_modifier_pad($string, $length, $pad = ' ', $type = 'left') {
    $ret = '';
    $type = strtoupper($type);
    switch ($type) {
        case 'LEFT': $ret = str_pad($string , $length, $pad, STR_PAD_LEFT); break;
        case 'RIGHT': $ret = str_pad($string , $length, $pad, STR_PAD_RIGHT); break;
        case 'BOTH': $ret = str_pad($string , $length, $pad, STR_PAD_BOTH); break;
        default: $ret = str_pad($string , $length, $pad, STR_PAD_RIGHT); break;
    }
    return($ret);;
}

?>