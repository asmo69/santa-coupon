<?php
    /**
     * Smarty (content_item) plugin
     *
     * PHP versions 4 and 5
     *
     * LICENSE: This source file is subject to version 3.0 of the PHP license
     * that is available through the world-wide-web at the following URI:
     * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
     * the PHP License and are unable to obtain it through the web, please
     * send a note to license@php.net so we can mail you a copy immediately.
     *
     * @package Smarty
     * @subpackage plugins
     * @author Stephen Brewster <steve@cite.co.uk>
     * @copyright 2005 Cite DMS
     * @license http://www.php.net/license/3_0.txt PHP License 3.0
     * @version 1.1
     * @since August 8th 2005
     */

    // {{{ smarty_function_content_item()

        /**
         * Get a content item
         *
         * Interfaces with the contentDAO object to provide control over a text
         * based content used for constructing welcome pages, about us sections
         * etc.
         *
         * @param array $params An associative array containing all of the
         * parameters passed to the tag in the smarty template.
         * @param object $smarty A reference to the smarty template parser.
         */

        function smarty_function_content_item($params, &$smarty) {
            // Get the database connection details from the parameters array.
            $config = isset($params['config']) ? $params['config'] : 'global.conf';
            $section = isset($params['section']) ? $params['section'] : 'Database';
            $prefix = isset($params['prefix']) ? $params['prefix'] : '';

            // Get the name of the array to create from the parameters array.
            $create = isset($params['create']) ? $params['create'] : 'news';

            // Get the story search details from the parameters array.
            $id = isset($params['id']) ? $params['id'] : '';
            $category = isset($params['category']) ? $params['category'] : '';
            $archived = isset($params['archived']) ? $params['archived'] : '';
            $keywords = isset($params['keywords']) ? $params['keywords'] : '';

            // If the $config variable is not a string then throw an error.
            if (validate($config, 'S') == false) {
                $smarty->trigger_error("'config' must be a string");
                exit();
            }

            // If the $section variable is not a string then throw an error.
            if (validate($section, 'S') == false) {
                $smarty->trigger_error("'section' must be a string");
                exit();
            }

            // If the $prefix variable is not a string then throw an error.
            if (validate($prefix, 'S') == false) {
                $smarty->trigger_error("'prefix' must be a string");
                exit();
            }

            // If the $id variable is not a string then throw an error.
            if (validate($create, 'S:1') == false) {
                $smarty->trigger_error("'create' must be a non empty string");
                exit();
            }

            // If the $id variable is not empty and is not a natural
            // number then throw an error.
            if (validate($id, 'NN') == true) {
                if (validate($id, 'N') == false) {
                    $smarty->trigger_error("'id' must be a natural number");
                    exit();
                }
            }

            // If the $category variable is not empty and is not a natural
            // number then throw an error.
            if (validate($category, 'NN') == true) {
                if (validate($category, 'N') == false) {
                    $smarty->trigger_error("'category' must be a natural number");
                    exit();
                }
            }

            // If the $keywords variable is not a string then throw an error.
            if (validate($keywords, 'S') == false) {
                $smarty->trigger_error("'keywords' must be a string");
                exit();
            }

            // If there is currently no database connection then...
            if (!isset($smarty->db)) {
                // Load the configuration file data specified by $config and
                // $section.
                $smarty->config_load($config, $section);
                $host = $smarty->get_config_vars('mysql_host');
                $username = $smarty->get_config_vars('mysql_username');
                $password = $smarty->get_config_vars('mysql_password');
                $database = $smarty->get_config_vars('mysql_database');

                // Create the database conenction.
                $smarty->db = new mysql($host, $username, $password, $database);
            }

            // If there is a database connection then...
            if ($smarty->db) {
                // Create a contentDAO object on the database.
                $contentDAO = new contentDAO($smarty->db, $prefix);
                // Create the database if they dont already exist.
                $contentDAO->createTables();
                // Get the record that match the search criteria.
                $row = $contentDAO->getContent($id, $category, $keywords);
                // Assign the record to the smarty template parser.
                $smarty->assign($create, $row);
            }
        }

    // }}}
?>