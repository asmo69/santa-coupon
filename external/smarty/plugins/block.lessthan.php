<?php
    function smarty_block_lessthan($params, $content, &$smarty, &$repeat) {
        // Get the $var, $compare and $id values from the parameters.
        $var = isset($params['var']) ? $params['var'] : '';
        $compare = isset($params['compare']) ? $params['compare'] : '';
        $create = isset($params['create']) ? $params['create'] : 'result';

        if ( $content == '' ) {
           $repeat = true;
        }
        else {
            $repeat = false;
            $result = $var < $compare ? true : false;
            $smarty->assign($create, $result);
            if ( $result ) {
                return($content);
            }
            else {
                return;
            }
        }
    }
?>