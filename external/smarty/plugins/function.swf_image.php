<?php

	function smarty_function_swf_image($params, &$smarty) {
		$src = array_key_exists('src', $params) ? $params['src'] : '';
		$width = array_key_exists('width', $params) ? $params['width'] : '';
		$height = array_key_exists('height', $params) ? $params['height'] : '';
		$alt = array_key_exists('alt', $params) ? $params['alt'] : '';
		$title = array_key_exists('title', $params) ? $params['title'] : '';
		$style = array_key_exists('style', $params) ? $params['style'] : '';
		$id = array_key_exists('id', $params) ? $params['id'] : '';

		$html = '';
		if ( preg_match('/\.swf$/i', $src) ) {
			$html .= sprintf("<div");
			if ( $title ) {
				$html .= sprintf(" title=\"%s\"", $title);
			}
			if ( $id ) {
				$html .= sprintf(" id=\"%s\"", $id);
			}
			if ( $style ) {
				$html .= sprintf(" style=\"%s\"", $style);
			}
			$html .= sprintf(">");
			$html .= sprintf("<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0\" width=\"%s\" height=\"%s\">\r\n", $width, $height);
            $html .= sprintf("<param name=\"movie\" value=\"%s\">\r\n", $src);
			$html .= sprintf("<!--[if !IE]> <-->\r\n");
			$html .= sprintf("<object data=\"%s\" width=\"%s\" height=\"%s\" type=\"application/x-shockwave-flash\">\r\n", $src, $width, $height);
			$html .= sprintf("<param name=\"pluginurl\" value=\"http://www.macromedia.com/go/getflashplayer\" />\r\n");
			$html .= sprintf("</object>\r\n");
			$html .= sprintf("<!--> <![endif]-->\r\n");
			$html .= sprintf("</object>\r\n");
			$html .= sprintf("</div>");
		}
		else {
			$html .= sprintf("<img src=\"%s\" width=\"%s\" height=\"%s\" alt=\"%s\"", $src, $width, $height, $alt);
			if ( $title ) {
				$html .= sprintf(" title=\"%s\"", $title);
			}
			if ( $id ) {
				$html .= sprintf(" id=\"%s\"", $id);
			}
			if ( $style ) {
				$html .= sprintf(" style=\"%s\"", $style);
			}
			$html .= sprintf(" />");
		}

	    return $html;
	}

?>