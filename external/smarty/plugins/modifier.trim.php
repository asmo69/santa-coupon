<?php

function smarty_modifier_trim($string, $chars = "")
{
    if ( $chars )
    	return trim($string, $chars);
    else
    	return trim($string);
}

?>