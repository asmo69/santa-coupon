<?php
    /**
     * Smarty (iterate) plugin
     *
     * PHP versions 4 and 5
     *
     * LICENSE: This source file is subject to version 3.0 of the PHP license
     * that is available through the world-wide-web at the following URI:
     * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
     * the PHP License and are unable to obtain it through the web, please
     * send a note to license@php.net so we can mail you a copy immediately.
     *
     * @package Smarty
     * @subpackage plugins
     * @author Stephen Brewster <steve@cite.co.uk>
     * @copyright 2005 Cite DMS
     * @license http://www.php.net/license/3_0.txt PHP License 3.0
     * @version 1.1
     * @since July 27th 2005
     */

    // {{{ smarty_block_iterate()

        /**
         * Iterates through an array or an object
         *
         * Objects passed into the $var parameter muct have first(), next()
         * and count() methods otherwise an error is thrown.
         *
         * @param array $params The associative array or object passed in on
         * the template tag.
         * @param string $content The text containt contained within the block
         * on the template.
         * @param object $smarty The smarty parser object.
         * @param boolean $repeat Should this block be run again?
         * @return string
         */

        function smarty_block_iterate(&$params, $content, &$smarty, &$repeat) {
            // Initialise the $var and $id values from the parameters.
            $var = '';
            $create = '';

            // Get the $var and $id values from the parameters.
            if (isset($params['var']))
                $var =& $params['var'];
            if (isset($params['create']))
                $create =& $params['create'];

            // If the $var variable is an array then...
            if (is_array($var) == false) {
                // If the $var variable is not an array and not an object then
                // throw an error and exit.
                if (is_object($var) == false) {
                    $smarty->trigger_error("'var' must be either an array or an object");
                    exit();
                }
            }

            // If the $var variable is an object then...
            if (is_object($var) == true) {
                // If the $var variable is an array but does not have a first()
                // method then throw an array and exit.
                if (method_exists($var, 'first') == false) {
                    $smarty->trigger_error("'var' must have a first() method");
                    exit();
                }
                // If the $var variable is an array but does not have a next()
                // method then throw an array and exit.
                if (method_exists($var, 'next') == false) {
                    $smarty->trigger_error("'var' must have a next() method");
                    exit();
                }
                // If the $var variable is an array but does not have a count()
                // method then throw an array and exit.
                if (method_exists($var, 'count') == false) {
                    $smarty->trigger_error("'var' must have a count() method");
                    exit();
                }
            }

            // If the $create variable is a string and is empty then throw an
            // error and exit.
            if ((is_string($create) == false) || (strlen($create) == 0)) {
                $smarty->trigger_error("'create' must be a non empty string");
                exit();
            }

            // If this is the first time through the block then...
            if ($content == '') {
                // Set the block index at position $create as zero.
                $params['index'] = 0;

                // If the $var variable is an array then get the first element.
                if (is_array($var)) {
                    // If the list of array keys in the $var variable is the
                    // same as the number range from 0 to array size minus one
                    // then this is not an associative array so set the
                    // 'assoc' flag as false.
                    if (array_keys($var) == range(0, count($var) - 1)) {
                        $params['assoc'] = false;
                    }
                    // Otherwise it is so we need to set the 'assoc' flag as
                    // true.
                    else {
                        $params['assoc'] = true;
                    }
                    // Reset the $var varible to the begining.
                    reset($var);
                    // Get the first record from the $var variable.
                    $record = each($var);
                    // If the $var variable is an associate array then assign
                    // the whole record to the smarty $create parameter.
                    if ($params['assoc']) {
                        $smarty->assign($create, $record);
                    }
                    // Otherwise only assign the 'value' field to the smarty
                    // $create parameter.
                    else {
                        $smarty->assign($create, $record['value']);
                    }
                }
                // If the $var variable is an object then get the first()
                // element.
                if (is_object($var)) {
                    $smarty->assign($create, $var->first());
                }
            }
            else {
                // Increment the block index at position $create.
                $params['index']++;

                // If the $var variable is an array then get the next element.
                if (is_array($var)) {
                    // Get the next record from the $var variable.
                    $record = each($var);
                    // If the $var variable is an associate array then assign
                    // the whole record to the smarty $create parameter.
                    if ($params['assoc']) {
                        $smarty->assign($create, $record);
                    }
                    // Otherwise only assign the 'value' field to the smarty
                    // $create parameter.
                    else {
                        $smarty->assign($create, $record['value']);
                    }
                }
                // If the $var variable is an object then get the next()
                // element.
                if (is_object($var)) {
                    $smarty->assign($create, $var->next());
                }
            }

            // Default the block repeat value to false.
            $repeat = false;
            // If the $var variable is an array and you have not yet reached the
            // end then set the block repeat value to true.
            if ((is_array($var)) && ($params['index'] < count($var))) {
                $repeat = true;
            }
            // If the $var variable is an object and you have not yet reached
            // the end then set the block repeat value to true.
            if ((is_object($var)) && ($params['index'] < $var->count())) {
                $repeat = true;
            }

            // Return the value of $contents.
            return($content);
        }

    // }}}
?>